<?php

use Illuminate\Http\Request;
use Joinery\Groups\Group;
use Joinery\Tasks\Task;
use Joinery\Projects\Project;
use App\User;
use App\Block;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::middleware('auth:api')->post('/userByToken', function(Request $request){
	$user = User::where('api_token', $request->api_token)->first();
	return $user;
});
Route::post('/breaks', function(Request $request){
	$user = User::where('api_token', $request->api_token)->first();

	$current_group = Group::find($user->current_group);
	$projects = $current_group->projects->where('active',1);
	$viewer = $user;
	$breaks = [];
	$excludes = [];
	//print_r($projects);
	foreach($projects as $project){
		foreach( $project->breaks('','', $viewer, $user) as $projectBreak){
			if(!in_array($projectBreak->id, $excludes)){
				$breaks[] = $projectBreak;
				$excludes[] = $projectBreak->id;
			}
		}
	}
	//print_r($breaks);
	foreach( $user->breaks() as $userBreak){
		if(!in_array($userBreak->id, $excludes)){
			$breaks[] = $userBreak;
			$excludes[] = $userBreak->id;
		}
	}
	foreach($current_group->teams as $team){
		$teamBreaks = $team->breaks('','',$user);
//		print_r($teamBreaks);
		foreach($teamBreaks as $teamBreak){
//		print_r($teamBreak);
			if(!in_array($teamBreak->id, $excludes)){
				$breaks[] = $teamBreak;
				$excludes[] = $teamBreak->id;
			}

		}
		//print_r($breaks);
	}
	return response()->json($breaks);
});
Route::middleware('auth:api')->post('/breaks/delete', function(Request $request){
	//$user = User::where('api_token', $request->api_token)->first();
	//if(!$user){
	//	return false;
	//}

	        \Log::error( print_r($request->all(), true) );

	$block = Block::find($request->id);
	$block->delete();
	return response()->json($block);
});
Route::middleware('auth:api')->post('/breaks/update', function(Request $request){
	$user = User::where('api_token', $request->api_token)->first();
	if(!$user){
		return false;
	}
	$request_schedule = $request->schedule;
	$block = Block::find($request->id);
	$block->title = $request_schedule['title']; 
	$startDate = date_create_from_format('D M d Y H:i:s e+', $request_schedule['start']);	
	$block->startDate = Carbon::parse($startDate)->startOfDay(); 
	$endDate = date_create_from_format('D M d Y H:i:s e+', $request_schedule['end']);
	$block->endDate = Carbon::parse($endDate)->startOfDay(); 
	$block->save();
	return response()->json($block);
});

Route::middleware('auth:api')->post('/breaks/new', function(Request $request){
	$user = User::where('api_token', $request->api_token)->first();
	if(!$user){
		return false;
	}
	$block = new Block;
	$block->ownerType = $request->breakType;
	if('user' == $request->breakType){
		$block->owner_id = $request->user;
	}
	else{
		$block->owner_id = $request->group;
	}
	if(!$request->title){
		$block->title = 'Break';
	}
	else{
		$block->title = $request->title; 
	}
	if ($request->has('group')) {
		$block->group_id = $request->group;
	}
	if ($request->has('user')) {
		$block->user_id = $request->user;
	}
	$block->active = 1;
	$block->public = $request->public;
	$block->project_id = $request->project;
	$block->created_by = $user->id;

	$startDate = date_create_from_format('D M d Y H:i:s e+', $request->start);
	$block->startDate = Carbon::parse($startDate)->startOfDay(); 
	$endDate = date_create_from_format('D M d Y H:i:s e+', $request->end);
	$block->endDate = Carbon::parse($endDate)->startOfDay(); 
	$block->save();
	return response()->json($block);
});



/*
Route::middleware('auth:api')->get('/taskTree/{project_id}', function( $project_id){
	$project = Project::find($project_id);
	return response()->json($project->easyTree);
});
*/
Route::middleware('auth:api')->get('/taskTree/{project_id}', function( $project_id){
	$project = Project::find($project_id);
	return response()->json($project->easyTree);
});
Route::get('/taskTree/{project_id}', function( $project_id){
	$project = Project::where('id',$project_id)->first();
	if(!$project){
		$response = [
			'error' => 1,
			'errorType' => 'timeline',
			'errorMessage' => 'That project does not exist.'
		];
		return response()->json($response);
	}
	else{
		if(count($project->tasks)< 1){
			$response = [
				'error' => 1,
				'errorType' => 'timeline',
				'errorMessage' => 'That project does not have any tasks to create a timeline.'
			];
			return response()->json($response);
		}
		else{
			return response()->json($project->easyTree);
		}
	}
});
Route::get('/team-complete/{group_name}/{project_id?}', function( $group_name,$project_id = null){
	$projectPartners = [];
	if($project_id){
		$project = Project::find($project_id);
		foreach($project->partners as $partner){
			$projectPartners[] = $partner->id;
		}
	}
	$groups = Group::where('name','like', '%' . $group_name . '%')->get();
	$output = [];
	$invited = [];
	if(is_object($groups)){
		foreach($groups as $group){
			$admin_email = "";
			$admin = $group->admin;
			if($admin && $admin->email){
				$admin_email = " | " . $admin->email;
			}
			$obj = new stdClass();
			$prefix = "";
			if(in_array($group->id,$projectPartners)){
				$prefix = "ALREADY INVITED - ";
				$obj->value = 'invited';
			}
			else{
				$obj->value = $group->id;
			}
			$obj->label = $prefix . $group->name . $admin_email;
			if( $prefix ){
				$invited[] = $obj;
			}
			else{
				$output[] = $obj;
			}
		}
		$output = array_merge($output, $invited);
	}
	return response()->json($output);
});
Route::get('/team-admin-complete/{email}', function( $email){
	$users = User::where('email', 'like', '%'.$email.'%')->get();
	$output = [];

	if(is_object($users)){
		foreach($users as $user){
			$groups = $user->groups()->where('role','admin')->get();
			foreach($groups as $group){
				$obj = new stdClass();
				$obj->label = $user->email . " | " . $group->name;
				$obj->value = $group->id;
				$output[] = $obj;
			}
		}
	}
	return response()->json($output);
});

?>