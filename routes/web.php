<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//use Group;
use Joinery\Projects\Project;
use Joinery\Tasks\Task;
use Joinery\Groups\Group;
Route::get('/subscription/index', 'SubscriptionsController@index')->name('subscription.index');

Route::get('/', function () {
    return view('welcome');
});
	Route::get('/current-group', function () {
	    return view('current-group');
	});
Route::group(['middleware' => ['web','auth']], function () {
	Route::get('updateCurrentGroup/{group_id}', function ( $group_id) {
		$user = \Auth::user();
			session_start();
		$current_group = Group::find($group_id);
		$user->current_group = $group_id;
		$user->save();
		if( $current_group){

			if(array_key_exists('requestedUrl', $_SESSION)){
				return Redirect::to( $_SESSION['requestedUrl'] );
			}
			else{

//				return redirect()->back();

				$permission_alert = "You are now logged in with " . $current_group->name;
				return redirect('/home')->with('status', $permission_alert);            
			}
		}
		else{
			$permission_alert = "We were not able to log you in with that group. Please contact Traverse.";
			return redirect('/current-group')->with('status', $permission_alert);
		}
	});
});



Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['web','auth']], function () {
	
	Route::get('/invites', function () {
	    return view('invites');
	});
	Route::get('/permissions', function () {
	    return view('permission');
	});
	Route::get('/breaks', function () {
	    return view('breaks');
	});
});
