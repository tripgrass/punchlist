<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blocks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->timestamp('startDate')->nullable();
            $table->timestamp('endDate')->nullable();
            $table->text('ownerType')->nullable();
            $table->integer('owner_id');
            $table->integer('group_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('created_by');
            $table->integer('project_id')->nullable();
            $table->text('description')->nullable();
            $table->text('title')->nullable();
            $table->text('category')->nullable();
            $table->boolean('active')->nullable();
            $table->boolean('public')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blocks');
    }
}
