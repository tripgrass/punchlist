import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class Example extends Component {
    constructor(props) {
        super(props);
        this.state = {
            prevUrl : "",
            nextUrl : "",
            groups: []
        };
        axios
            .post('/api/groups')
            .then(response => {
                console.log('from handle submit', response);
                this.setState({
                    prevUrl: response.data.prev_page_url,
                    nextUrl: response.data.next_page_url,
                     groups: [...response.data.data]
                })
            });
        this.handlePrev = this.handlePrev.bind(this);            
        this.handleNext = this.handleNext.bind(this);            
    }
    handlePrev(){
     axios
            .post(this.state.prevUrl)
            .then(response => {
                this.setState({
                    prevUrl: response.data.prev_page_url,
                    nextUrl: response.data.next_page_url,
                     groups: [...response.data.data]
                })
            });

    }
    handleNext(){
         axios
            .post(this.state.nextUrl)
            .then(response => {
                this.setState({
                    prevUrl: response.data.prev_page_url,
                    nextUrl: response.data.next_page_url,
                     groups: [...response.data.data]
                })
            });
    }
    render() {
        return (
            <div>
                <div className="table-responsive users-table">
                    <table className="table table-striped table-sm data-table">
                        <caption id="user_count"></caption>
                        <thead className="thead">
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th className="hidden-sm hidden-xs hidden-md">Created</th>
                                <th className="hidden-sm hidden-xs hidden-md">Updated</th>
                                <th className="no-search no-sort"></th>
                                <th className="no-search no-sort"></th>
                            </tr>
                        </thead>
                        <tbody id="users_table">
                            { this.state.groups.map(group => (
                                 <tr key={group.id}>
                                    <td>{group.id}</td>
                                    <td>{group.name}</td>
                                    <td className="hidden-sm hidden-xs hidden-md">{group.created_at}</td>
                                    <td className="hidden-sm hidden-xs hidden-md">{group.updated_at}</td>
                                    <td>
                                    <form method="POST" accept-charset="UTF-8" className="" data-toggle="tooltip" title="Delete">
                                    <input name="_token" type="hidden" value=""></input>
                                                                        <input name="_method" type="hidden" value="DELETE"></input>
                                                                        <input name="group_id" type="hidden" value={group.id}></input>
                                                                        <button className="btn btn-danger btn-sm" type="button"  data-toggle="modal" data-target="#confirmDelete" data-title="Delete Group" data-message="Are you sure you want to delete this group?">Delete</button>
                                                                    </form>
                                    </td>
                                    <td>
                                        <a className="btn btn-sm btn-info btn-block" href={"/groups/" + group.id + "/edit"} data-toggle="tooltip">
                                            Edit Group
                                        </a>
                                    </td>
                                </tr>))
                            } 
                        </tbody>
                    </table>
                </div>
                <div className="paginated-links">
                    <span onClick={this.handlePrev}>Prev</span><span onClick={this.handleNext}>Next</span>
                </div>
            </div>
        );
    }
}
if (document.getElementById('card-body')) {
    alert(2);
    ReactDOM.render(<Example />, document.getElementById('card-body'));
}
