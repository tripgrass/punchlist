
@extends('layouts.app')

@section('template_title')
    @lang('laravelusers::laravelusers.showing-all-users')
@endsection
@section('class', 'listview-users')
@section('template_linked_css')
    @if(config('laravelusers.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('laravelusers.datatablesCssCDN') }}">
    @endif
    @if(config('laravelusers.fontAwesomeEnabled'))
        <link rel="stylesheet" type="text/css" href="{{ config('laravelusers.fontAwesomeCdn') }}">
    @endif
    @include('laravelusers::partials.styles')
    @include('laravelusers::partials.bs-visibility-css')
@endsection
<?php
    $current_user = \Auth::user();
    $current_group = Joinery\Groups\Group::find($current_user->current_group);

?>
@section('content')
    <div class="container">
        @if(config('laravelusers.enablePackageBootstapAlerts'))
            <div class="row">
                <div class="col-sm-12">
                    @include('laravelusers::partials.form-status')
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                Manage the  Team
                            </span>

                            <div class="btn-group pull-right btn-group-xs">
                                    <a href="{{ route('users.create') }}" class="btn btn-default btn-sm pull-right" data-toggle="tooltip" data-placement="left" title="@lang('laravelusers::laravelusers.tooltips.create-new')">
                                        @lang('+ Add New User')
                                    </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">

                        <div class="table-responsive users-table">
                            <table class="table table-striped table-sm data-table">
                                <caption id="user_count">
                                    {{ trans_choice('laravelusers::laravelusers.users-table.caption', 1, ['userscount' => $users->count()]) }}
                                </caption>
                                <thead class="thead">
                                    <tr>
                                        <th>@lang('laravelusers::laravelusers.users-table.name')</th>
                                        <th class="hidden-xs">@lang('laravelusers::laravelusers.users-table.email')</th>
                                        <th class="">Role</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody  id="users_table">
                                    @foreach($users as $user) 
                                        <?php
                                            $group_user = $current_group->users->where('id',$user->id)->first();
                                            $group_role = $group_user->pivot->role;

                                        ?>
                                        <tr class="tr-href" data-href="{{ URL::to('users/' . $user->id . '/edit') }}">

                                            <td>{{$user->name}}</td>
                                            <td class="hidden-xs">{{$user->email}}</td>
                                            <td class="hidden-xs"><?php echo ucfirst($group_role); ?></td>
                                            <td><a href="{{ URL::to('users/' . $user->id . '/edit') }}">Edit</a></td>

                                        </tr>
                                    @endforeach
                                </tbody>
                                @if(config('laravelusers.enableSearchUsers'))
                                    <tbody id="search_results"></tbody>
                                @endif
                            </table>


                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    @include('laravelusers::modals.modal-delete')

@endsection

@section('template_scripts')
    @if ((count($users) > config('laravelusers.datatablesJsStartCount')) && config('laravelusers.enabledDatatablesJs'))
        @include('laravelusers::scripts.datatables')
    @endif
    @include('laravelusers::scripts.delete-modal-script')
    @include('laravelusers::scripts.save-modal-script')
    @if(config('laravelusers.tooltipsEnabled'))
        @include('laravelusers::scripts.tooltips')
    @endif
    @if(config('laravelusers.enableSearchUsers'))
        @include('laravelusers::scripts.search-users')
    @endif

@endsection
