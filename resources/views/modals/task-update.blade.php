<div class="sidebar fade modal-danger" id="updateTask" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true" tabindex="-1">
                <div class="thread-info">
      
    </div>
    <div id="new-task-style">
        
    </div>
    <div class="dialog-wrapper">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-content-top-header">
                    <h2 class="modal-title" style="padding-left:0">
                        <div id="form-task-title-reading">  </div>
                    </h2>
                    
                </div>
                <div class="modal-header">
                    <div class="container" id="sidebar-add-task" style="display:none"></div>
                    <div class="container" id="sidebar-update-task">
                                                        <button class="edit-button" id="task-edit-toggle" title="Edit Task" data-id="">
                                                            
                                                        </button>    
                        <div class="task-header">
                            <div class="row project-settings task-settings" id="save-task-wrapper">
                            </div>
                            <div class="row">
                                <h3 class="new-task-header">Add a New Task
                                <div class="modal-cancel" style="text-align:right">
                                    <button type="button" class="cancel-add-new" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        <span class="sr-only">close</span>
                                    </button>
                                </div>

                                </h3>

                                <h2 class="modal-title" style="padding-left:0">
                                    
                                    <textarea rows="1" class="expanding-textarea no-border project-input task-input" name="form-task-title" id="form-task-title" style="width:100%" disabled="" placeholder="Task Name"></textarea>
                                </h2>                                
                                <div class="modal-close" style="text-align:right">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        <span class="sr-only">close</span>
                                    </button>
                                </div>
                            </div>
                            <div class="task-error-messages general"></div>
                            <div class="row task-status-wrapper">
                                <div class="col-lg-5" id="task-status">Status: Open</div>
                            </div>
                        </div>
                        <div id="task-schedule">
                            <div class="field-wrapper project-settings task-settings">
                                <div class="label-wrapper  date-button">
                                    <label class="">Task Length in Work Days: </label>
                                    <input id="task-update-length" type="text" size="4" />
                                </div>
                            </div>
                            <div class="row task-settings">
                                <div class="col-lg-12" style="">
                                    <div class="task-error-messages dates"></div>
                                </div>
                                <div class="col-lg-12" style="">
                                    <div class="" id="task-length-label">Estimated Length:</div>
                                    <div class="" id="task-length-value">5 days</div>
                                </div>
                                <div class="col-lg-6 date-button" id="start-date-button">
                                <div class="col-lg-12 input-error" id="task-start-date-error-messages">
                                </div>
                                    <div id="start-label" class="label">
                                    </div>
                                    <div id="start-value"  class="value">
                                    </div>
                                    <input id="task-update-start-date" type="text" class="hidden-input" />
                                    <i class="input-clear fas fa-times-circle hidden-input" id="clear-start-date" title="Clear Date"></i>

                                </div>
                                <div class="col-lg-6 date-button" id="end-date-button">
                                <div class="col-lg-12 input-error" id="task-end-date-error-messages">
                                </div>

                                    <div id="end-label"  class="label">
                                    </div>
                                    <div id="end-value" class="value">
                                    </div>
                                   <input id="task-close-date" class="hidden-input" type="text" />
                                    <i class="input-clear fas fa-times-circle hidden-input" id="clear-close-date" title="Clear Date"></i>

                                </div>


                            </div>    
                            <div class="row" id="task-description">                           
                                <div class="col-lg-12" >
                                    <div id="task-description-label" class="label">Task Description
                                    </div>

                                    <textarea rows="1" class="expanding-textarea no-border project-input task-input" name="task-description-content" id="task-description-content" style="width:100%" ></textarea>                                    

                                    <div id="task-description-content-view"></div>                                                                        
                                </div>
                            </div>

                            <div class="row" id="main-sidebar-row">
                                <div class="col-lg-12 task-owner-label-wrapper-wrapper">
                                    <div id="task-owner-unassigned">
                                        <div id="task-owner-label-wrapper">
                                            This task is not assigned.
                                        </div>
                                    </div>
                                    <div id="task-owner-assigned">
                                        <div id="task-owner-label-wrapper">
                                            Assigned to:
                                            <div id="task-owner-label" style='display:block'></div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            
                            <div class="" id="project-partners">
                            </div>
                        </div>


                        <div id="task-properties">
                            test
                                                        
                        </div>
                        <div id="length-properties"></div>
                    </div>
                    
                </div>
                <div class="modal-body" id="task-form">
                </div>
            </div>
        </div>
    </div>
</div>
