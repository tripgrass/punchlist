@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <p>You belong to multiple Teams. Select the Team you'd like to work in right now.</p> 
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <?php $user = \Auth::user();
                        if($user->groups): ?>
                        <ul class="current-group-select">
                        <?php foreach($user->groups as $group): ?>
                            <li><a href="/updateCurrentGroup/<?php echo $group->id; ?>"><?php echo $group->name; ?></a></li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    
</div>
@endsection
