<?php
    $user = \Auth::user();
    $guest = 0;
    if( !$user ){
        $guest = 1;
    }
    $projects = [];
    $group = "";
    if($user){
        $projects = Joinery\Projects\Project::whereHas('groups', function($q) use ($user){
            $q->where('group_id', '=',$user->current_group);
        })->get();

        $group = Joinery\Groups\Group::find($user->current_group);
        if($group){
            $group_id = $group->id;
            $group_name = $group->name;
        }
        else{
            $group_id = "";
            $group_name = "";            
        }
    }
    $body_class = "";
    if('welcome' == $view_name){
        $body_class = "landing";
    }
    $userSettings = "";
    if( $user ){
        $userSettings = App\UserSettings::where('user_id',$user->id)->get();
    }
?>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="api_token" content="{{ (Auth::user()) ? Auth::user()->api_token : '' }}">

    <title>{{ config('app.name', 'Traverse') }}</title>


    <!-- Fonts -->
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <script type="text/javascript"> (function() { var css = document.createElement('link'); css.href = 'https://use.fontawesome.com/releases/v5.1.0/css/all.css'; css.rel = 'stylesheet'; css.type = 'text/css'; document.getElementsByTagName('head')[0].appendChild(css); })(); </script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
            {{-- Styles --}}
        @if(config('projects.enableBootstrapCssCdn'))
            <link rel="stylesheet" type="text/css" href="{{ config('projects.bootstrapCssCdn') }}">
        @endif
        @if(config('projects.enableAppCss'))
            <link rel="stylesheet" type="text/css" href="{{ asset(config('projects.appCssPublicFile')) }}">
        @endif
        @yield('template_linked_css')

</head>
        <?php if($userSettings): ?>
            <?php 
                $configs = [];
                foreach($userSettings as $userSetting ){
                    $configs[] =  $userSetting['setting'] . " : '" . $userSetting['configuration'] . "'";
                }

                $configs_str = implode(',', $configs);
            ?>
            <script>
                var userSettings = {
                    <?php echo $configs_str; ?>
                };
                console.log( 'USERSEEEEEEEEE' , userSettings );
            </script>
        <?php endif; ?>
<body class="<?php echo $body_class . " " . $view_name; ?>">
<?php 
    $canEdit = 0;
    if( $user ){
        $canEdit = 1;
        //check if user has auth to edit
    }
    $bodyClasses = [];
    if($canEdit && isset($_GET['project-edit'])){ 
        $bodyClasses['edit'] = "project-edit";
    }
    if($canEdit && isset($_GET['task-edit'])){ 
        $bodyClasses['edit'] = "task-edit";
    }
    if($canEdit && isset($_GET['task-edit-new'])){ 
        $bodyClasses['edit'] = "task-edit-new";
    }
    if(array_key_exists('tour', $_GET)){ 
        $bodyClasses[] = "project-tour tour-intro";
        unset($bodyClasses['edit']);
    }
?>
    <div id="app" class="<?php echo implode(" ",$bodyClasses); ?> @yield('class')" >
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="/img/Wordmark-Negative.svg">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        @if ($user)
                            <li class="nav-item dropdown">
                                <a id="projectsDropdown" class="nav-link" href="/groups" >
                                    Partners 
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a id="projectsDropdown" class="nav-link" href="/breaks" >
                                    Breaks 
                                </a>
                            </li>
                        @endif                        
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="traverse-button" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="traverse-button negative" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown"  style="margin-right:20px;" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <br>
                                    <?php if( $group && $group->isGroup() ) : ?>
                                                <a class="dropdown-item" href="/users">Your Team</a>
                                    <?php endif; ?>
                                    <a class="dropdown-item" href="/users/{{ $user->id }}/edit">Edit Your Profile</a>
                                    <a class="dropdown-item" href="/groups/{{ $user->current_group }}/edit">Manage Account</a>


                                    <hr>
                                    <?php if(count($user->groups)> 1) : ?>
                                        <b class="dropdown-item no-link" style="margin-top:15px; font-weight:bold">
                                            Switch to Another Team:
                                        </b>
                                        <?php if($user->groups): ?>
                                            <?php foreach($user->groups as $group): ?>
                                                <?php if( $group->id != $user->current_group ) : ?>
                                                    <a class="dropdown-item sub-dropdown-item" href="/updateCurrentGroup/<?php echo $group->id; ?>"><?php echo $group->name; ?></a>
                                                    <hr>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <a style="font-weight:bold" class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                        @if (count($projects) > 1)

                            <li class="nav-item dropdown  timeline">
                                <a id="projectsDropdown" class="nav-link button-wrapper " href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre style="font-weight: bold;">
                                    <button class="dropdown-toggle traverse-button negative">Project Timelines</button> 
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="projectsDropdown">
                                    @foreach($projects as $project)
                                    <a class="dropdown-item" href="/projects/{{$project->id}}/timeline">
                                        {{ $project->name }}
                                    </a>
                                    @endforeach
                                </div>
                            </li>
                        @endif
                        @if ($user)
                            <li class="nav-item">
                                <a href="/projects/new/timeline"><button class="add-button">Project</button></a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        <div class="promotion">
            @if (!$user)
                <h2>Use Traverse for Your Next Project</h2>
            @endif
            <?php

            // check if user's group is secondary - if so, does it have an account or just viewing? 
            ?>
        </div>        

        <main class="main">
            <div class="tutorial-banner-wrapper"><div class="tutorial-banner"><img src="/img/booking.svg"></div></div>
            @yield('content')
        </main>
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

</body>
</html>
