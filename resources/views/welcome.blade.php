@extends(config('projects.ProjectsBladeExtended'))


@section('template_linked_css')
    @if(config('projects.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('projects.datatablesCssCDN') }}">
    @endif
    @if(config('projects.fontAwesomeEnabled'))
        <link rel="stylesheets" type="text/css" href="{{ config('projects.fontAwesomeCdn') }}">
    @endif
    @include('projects::partials.bs-visibility-css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

@endsection

<?php 
        $h1 = "Put your projects on autopilot.";
        $p = "Create and manage self-adjusting timelines for your contracts.";
    if(is_array($_SERVER) && array_key_exists('HTTP_REFERER', $_SERVER) ){
        //echo $_SERVER['HTTP_REFERER'];
        $h1 = "Put your projects on autopilot.";
        $p = "Create and manage self-adjusting timelines for your contracts.";
    }
?>
@section('content')
        <div id="hero">
            <h1><?php echo $h1; ?></h1>
            <p><?php echo $p; ?></p>
            <button>Sign Up</button>
        </div>
@endsection