<?php
/*
use Jasny\SSO\NotAttachedException;
require_once __DIR__ . '/../../../vendor/autoload.php';

$broker = new Jasny\SSO\Broker(getenv('SSO_SERVER_URL'), getenv('SSO_BROKER_ID'), getenv('SSO_BROKER_SECRET'));
$broker->attach(true);
try {
    if (!empty($_GET['logout'])) {
        $broker->logout();
    } elseif ($broker->getUserInfo() || ($_SERVER['REQUEST_METHOD'] == 'POST' && $broker->login($_POST['username'], $_POST['password']))) {
        echo"ppppppppppp";
        print_r($broker->getUserInfo());
      //  header("Location: index.php", true, 303);
       // exit;
    }
echo "lllllllllll";
    if ($_SERVER['REQUEST_METHOD'] == 'POST') $errmsg = "Login failed";
} catch (NotAttachedException $e) {
    header('Location: ' . $_SERVER['REQUEST_URI']);
    exit;
} catch (Jasny\SSO\Exception $e) {
    $errmsg = $e->getMessage();
}
*/

?>
@extends('layouts.app')

@section('content')

<div class="vertical-center">
        <div class="page-title-wrapper" style="border-bottom:solid 1px grey">
            <div class="container" >
                <div class="row" style="">
                    <div class="col-lg-12" style="">
                        <h1>Login</h1>
                    </div>
                </div>
            </div>

        </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">

            <div class="custom-control custom-switch"> 
                <input type="checkbox" 
                       class="custom-control-input" 
                       id="customSwitch1" name="remember" {{ old('remember') ? 'checked' : '' }}/> 
                <label class="custom-control-label"
                       for="customSwitch1"> {{ __('Remember Me') }}</label> 
            </div> 
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="traverse-button">
                                        {{ __('Login') }}
                                    </button>

                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                    <a class="btn btn-link" href="{{ route('register') }}">No account? Register for one.
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
