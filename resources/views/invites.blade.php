@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h3>Project Invites</h3>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <?php
                    $user = Auth::user();
                    foreach($user->invites as $invite){
                        echo "Invite for " .$invite['project_name'].". <a class='btn' href='/projects/".$invite['project_id']."/invite/".$invite['inviteCode']."'>Accept</a>";
                    } 
                    ?>
                </div>
            </div>
        </div>
    </div>
    
</div>
@endsection
