@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('status') }}

                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <?php if( 'timelines' == session('list') ) : ?> 
                    <div class="card-header">Your Timelines</div>

                    <div class="card-body">
                        <?php 
                            $user = \Auth::user();
                            $projects = [];
                            if($user){
                                $projects = Joinery\Projects\Project::whereHas('groups', function($q) use ($user){
                                    $q->where('group_id', '=',$user->current_group);
                                })->get();
                            }
                        ?>
                        @foreach($projects as $project)
                                    <a class="dropdown-item" href="/projects/{{$project->id}}/timeline">
                                        {{ $project->name }}
                                    </a>
                                    @endforeach
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    
</div>
@endsection
