<?php 
    use App\User;
    use Joinery\Groups\Group;
    use Joinery\Projects\Project;

    $edit = 0;
    $title = "Create New Partner";
    $btn_text = "Create New Partner";
    $form_name = "";
    $email = "";
    $form_id = "NULL";
    $create_new = 1;
    $types = "Groups";
    $type = "Group";
    $current_user = Auth::user();
    $is_partner = 1;
    if( isset($group) && is_object($group)){
        $edit = 1;
        $create_new = 0;
        $title = "Edit Partner";
        $btn_text = "Update Partner";
        $types = "Partners";
        $type = "Partner";
        $form_name = $group->name;
        $form_id = $group->id;
        //print_r($group->users);
        foreach( $group->users as $group_user ){
            if( $group_user->id == $current_user->id ){
                $is_partner = 0;
            }
        }
        // get projects
    }
    else{
        $group = new Group;
    }
    $form_projects = [];
    if (Auth::check()) {
        //get current_group->projects
        $current_group = Group::first( 'id',$current_user->current_group );

        $curr_projects = $current_group->projects;
        $projects_array = [];
        foreach( $curr_projects as $curr_project){
            $projects_array[] = $curr_project->id;
        }
        if( isset( $group->projects )  ){
            foreach( $group->projects as $group_project ){
                $group_projects_ids[] = $group_project->id;
                if( in_array( $group_project->id, $projects_array )){
                    $form_projects[] = $group_project;
                }
            }
        }
        if(  $group->id != $current_group->id ){
            $types = "Partners";
            $type = "Partner";
        }
        $projects_label = "Manage Your Partner's Project Access:";
         if( isset( $_GET['addToProject'] ) ){
            $form_projects = [];
            $form_projects[] = Project::find( $_GET['addToProject'] );
            $curr_projects = $form_projects;
            $projects_label = "Add to Project:";
         }
        /*
        if( ( isset($group) && $group->id != $current_group->id && is_array($group->projects ) ) ){
            foreach( $group->projects as $group_project ){
echo "ppppppppp";
                if( in_array( $group_project->id, $projects_array )){
                    $form_projects[] = $group_project;
                }
            }
        }
        */
    }
    $edited_by_owner = 0;
    if( ( isset($group) && $group->id == $current_group->id ) ){
        $edited_by_owner = 1;
    }
    $form_group = [];
    $form_group['name'] = "";
    $form_group['email'] = "";
    $form_group['alias_name'] = "";
    if( isset($group) ){
        $form_group['name'] = $group->name; 
        $form_group['email'] = $group->email; 
        $form_group['alias_name'] = $group->aliasName; 
    }
?>
    <div class="container">
        @if(config('groups.enablePackageBootstapAlerts'))
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <?php 
//                    @include('groups::partials.form-status')
                ?>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">
                            {{ $title }}
                            @if( $group->approved )
                                <span>The Partner has accepted the invite and some fields may only be edited by them</span>
                            @endif
                            <div class="pull-right">
                                <a href="/groups" class="model-back" data-toggle="tooltip" data-placement="left" title="@lang('Back to Groups')">
                                   Back to {{$types}}
                                </a>
                                <a href="/groups/create" class="model-add-new" data-toggle="tooltip" data-placement="left" title="@lang('groups::groups.tooltips.create-new')">
                                        Add New {{$type}}
                                    </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        {!! Form::open(array('url' => '/groups/addPartner', 'method' => 'POST', 'role' => 'form', 'class' => 'needs-validation')) !!}
                            {!! Form::hidden('id', $form_id, array('id' => 'id')) !!}
                            <?php if($create_new): ?>
                                {!! Form::hidden('groupType', 'new', array('id' => 'groupType')) !!}
                            <?php endif; ?>
                            @if($is_partner && $create_new)
                                {!! Form::hidden('creating_group_id', $current_group->id, array('id' => 'creating_group_id')) !!}
                            @elseif( !$create_new )
                                {!! Form::hidden('group_id', $group->id, array('id' => 'group_id')) !!}                            
                                {!! Form::hidden('group_type', 'edit') !!}                            
                            @endif

                            @if( $edited_by_owner )
                                {!! Form::hidden('edited_by_owner', '1') !!}                            
                            @endif
                            {!! csrf_field() !!}
                            <div class="form-group has-feedback row {{ $errors->has('name') ? ' has-error ' : '' }}">
                                @if( $edit  || $create_new )
                                    @if( $is_partner )
                                        {!! Form::label('name', 'Your Name for ' . $type, array('class' => 'col-md-3 control-label')); !!}
                                    @else
                                        {!! Form::label('name', $type . ' Name', array('class' => 'col-md-3 control-label')); !!}
                                    @endif
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            @if( $is_partner )
                                                {!! Form::text('alias_name', $form_group['alias_name'], array('id' => 'alias_name', 'class' => 'form-control', 'placeholder' => 'Name (alias)')) !!}
                                            @else
                                                {!! Form::text('name', $form_group['name'], array('id' => 'name', 'class' => 'form-control', 'placeholder' => 'Name')) !!}
                                            @endif
                                        </div>

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                @else
                                        {!! Form::label('name', 'Noneditable Real Name (editable by owner only): ' . $form_name, array('class' => 'col-md-3 control-label')); !!}
                                @endif                                    
                            </div>
                            @if( $edit  || $create_new )
                                @if( $is_partner && !$create_new && $form_group['name'] && $form_group['name'] != $form_group['alias_name'])
                                    <div class="form-group has-feedback row">
                                        {!! Form::label('name', 'Real Name', array('class' => 'col-md-3 control-label')); !!}
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <div  class="label-no-input real-group-name"><?php echo $form_group['name']; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endif

                            <div class="form-group has-feedback row {{ $errors->has('email') ? ' has-error ' : '' }}">
                                @if(config('groups.fontAwesomeEnabled'))
                                    {!! Form::label('email', 'Admin Email', array('class' => 'col-md-3 control-label')); !!}
                                @endif
                                <div class="col-md-9">
                                    <div class="input-group">
                                        @if( $create_new )
                                            {!! Form::text('email', '', array('id' => 'email', 'class' => 'form-control', 'placeholder' => 'admin@yourclient.com')) !!}
                                        @elseif( !$group->approved )
                                            {!! Form::text('email', $group->admin->email, array('id' => 'email', 'class' => 'form-control', 'placeholder' => 'admin@yourclient.com')) !!}                                        
                                        @else
                                            <label class="label-no-input">{{ $group->admin->name . " | " . $group->admin->email }}</label>
                                        @endif
                                    </div>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            @if( !$create_new && !$is_partner)
                                <div class="form-group has-feedback row {{ $errors->has('email') ? ' has-error ' : '' }}">
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            {!! Form::label('users', 'Group Members', array('class' => 'col-md-3 control-label')); !!}
                                        </div>
                                    </div>
                                    <?php 
                                        $selected_users = [];
                                        if(isset($group) && isset($group->users)){
                                            foreach( $group->users as $user){
                                                $selected_users[$user->id] = []; 
                                            }
                                        }
                                        foreach($users as $user): 
                                            $selected = 0;
                                            if( in_array($user->id, $selected_users) ){
                                                $selected = 1;
                                            }
                                            ?>
                                            <div class="col-md-9">
                                                <div class="checkbox-list">

                                                    {{Form::checkbox('users',$user->id, $selected, array('id' => $user->email, 'class' => 'form-control'))}}
                                                    {!! Form::label($user->email, $user->name, array('class' => ' control-label')); !!}
                                                </div>
                                            </div>
                                    <?php endforeach; ?>
                                </div>
                            @endif
                                <div class="form-group has-feedback row">
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            {!! Form::label('projects[]', $projects_label, array('class' => ' control-label')); !!}
                                        </div>
                                    </div>
                                    <?php 
                                        $selected_projects = [];
                                        if( is_array($form_projects) && count($form_projects)>0 ){
                                            foreach( $form_projects as $form_project ){
                                                $selected_projects[] = $form_project->id; 
                                            }
                                        }
                                        foreach($curr_projects as $project): 
                                            $selected_project = 0;
                                            if( in_array($project->id, $selected_projects) ){
                                                $selected_project = 1;
                                            }
                                            ?>
                                            <div class="col-md-9">
                                                <div class="checkbox-list">

                                                    {{Form::checkbox('projects[]',$project->id, $selected_project, array('id' => $project->id, 'class' => 'form-control'))}}
                                                    {!! Form::label($project->name, $project->name, array('class' => ' control-label')); !!}
                                                </div>
                                            </div>
                                    <?php endforeach; ?>
                                </div>
                        {!! Form::button( $btn_text, array('class' => ' margin-bottom-1 mb-1 float-right','type' => 'submit' )) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
