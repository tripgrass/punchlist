export function toggleEditMode( target ){
	var allowEdit = 1;
	if( $('#app').hasClass('project-tour') ){
		allowEdit = 0;
	}
	if( $('#app').hasClass('task-edit') || $('#app').hasClass('task-edit-new') || $('#app').hasClass('project-edit') ){
		var edit = 1;
	}    
	else{
		var edit = 0;
	}
	if(allowEdit){
		if( !edit ){
			$('#app').removeClass('project-edit');
			if( 'task' == target ){
				$('.task-input').prop("disabled", true).removeClass('edit-input');
			}
			else if( 'project' == target ){
				$('.project-input').prop("disabled", true).removeClass('edit-input');

			}
			if( !$('#project-description').val() ){
//				$('#project-description-wrapper').hide();
			}
		}
		else{

			$('#task-update-start-date').show();
			if( 'task' == target ){
				$('.task-input').prop("disabled", false).addClass('edit-input');
				$('#app').addClass('task-edit');
			}
			else if( 'project' == target ){
				$('.project-input').prop("disabled", false).addClass('edit-input');
			}
			else if( 'callback' == target ){
				$('#app').addClass('task-edit-new');
			}
			else{
				$('#app').addClass('project-edit');
			}
			//$('#project-description-wrapper').show();
		}
	}
}
	export function closeSidebar(){
		//toggleSidebar(null,true,true);
//$('body').removeClass('task-open');
//		$('#updateTask').css('width',0);
//		$('#project-container').css('width','100%');
	}
	export function validateTask(){
        var task_id = $('#task-update-id').val(),
            task_close_date = $('#task-close-date').val(),
            task_start_date = $('#task-update-start-date').val(),
            task_users = $('#users').val(),
            task_length = $('#task-length').val(),
            mssg = "",
            valid = 1;
        if(task_close_date && !task_start_date){
            mssg = "This task has a close date with no open date.";
        }
        if(valid){
            return true;
        }
        else{
            $('#task-error-message .message').html(mssg);
            return false;
        }		
	}
	export function saveTask(){
var task_id = $('#task-update-id').val(),
            task_close_date = $('#task-close-date').val(),
            task_start_date = $('#task-update-start-date').val(),
            task_users = $('#users').val(),
            task_length = $('#task-update-length').val(),
            task_name = $('#form-task-title').val(),
            task_description = $('#task-description-content').val(),
            task_owner = $("select[name='user-assign']").find(":selected").val(),
            task_parent_radio = $('input[name="task-parent"]:checked'),
            task_parent_id = task_parent_radio.val(),
            is_new_thread = task_parent_radio.siblings('.new-thread-wrapper').find('.make-new-thread').is(':checked'),
            task_group = $("select[name='group-assign']").find(":selected").val(),
            canEditTask = 0;
        // validate -- should be as function

        if( canUserEditTask() ){
            if(validateTask()){
                if( $('#saveTask').hasClass('active') ){

                    ;(async () => { const response = await axios.post('/api/taskUpdate', {
                      id: task_id,
                      length: task_length,
                      closedDate: task_close_date,
                      startDate: task_start_date,
                      users: task_users,
                      name: task_name,
                      description: task_description,
                      ownerUser: task_owner,
                      ownerGroup: task_group,
                      taskParentId: task_parent_id,
                      isNewThread: is_new_thread  
                        })
                    if( task_start_date){
                        $('#task-close-date').datepicker("option","minDate",task_start_date).attr('disabled',false);
                        $('#clear-close-date').removeClass('disabled');                        
                    }
                    else{
                        $('#task-close-date').attr('disabled',true);
                        $('#clear-close-date').removeClass('disabled');                        
                    }
                    $('#updateListener').html('in this ratio');
                    })()
                }

            }
        }
        else{
            alert('Cannot edit task');
        }		
	}
	export function attachDragRange(e){
        // check if is within the calendar; disable taskDrag element if outside
		var calendarWrapper = document.getElementById('calendar-wrapper').getBoundingClientRect();
		if ( e.pageY >= calendarWrapper.top && e.pageY <= calendarWrapper.bottom) {
			//is contained
            $('body').addClass('cursor-contained');
            document.addEventListener('mousemove', dragRange);
		}
		else{
            $('body').removeClass('cursor-contained');
            document.removeEventListener('mousemove', dragRange);            
		}
	}
	export function dragRange(e){
        console.log( 'my_dragging.offset0.left : ' + my_dragging.offset0.left );
        console.log( 'e.pageX : ' + e.pageX   );
        console.log(  'my_dragging.pageX0 : ' + my_dragging.pageX0 );
	        var left = my_dragging.offset0.left + (e.pageX - my_dragging.pageX0);
	        var top = my_dragging.offset0.top + (e.pageY - my_dragging.pageY0) - 97;
	        $(my_dragging.elem).offset({top: top});
			var vertical = checkVerticalRangeContainment( e );	        
	        if( vertical ){
				$('#newTaskRange').addClass('inactive');			
				$('#newTaskRangeBubble').addClass('inactive');			
	        }
	        else{
				$('#newTaskRange').removeClass('inactive');				        	
				$('#newTaskRangeBubble').removeClass('inactive');				        	
	        }	        	        
	}	
	export function setBubble(range, bubble) {
		const val = range.val();
		const min = range.prop('min') ? range.prop('min') : 0;
		const max = range.prop('max') ? range.prop('max') : 100;
		var newVal = Number(((val - min) * 100) / (max - min));
		bubble.innerHTML = val;
		console.log('bubble',bubble);
		// Sorta magic numbers based on size of the native UI thumb
		bubble.css('right', newVal + "%");
	}
	export function checkVerticalRangeContainment(e, isContained){
		var $range = $('#newTaskRange');
		var projectLength = window.appVars.projectLength;

		// set rangeVal based on  mouse position
		var mouseY = e.pageY;
		var mouseX = e.pageX;
		var rangeOffset = $('.slidecontainer').offset();
		var offset = mouseX - rangeOffset.left;
		var step = $('#newTaskRange').attr('step');
		var max = $('#newTaskRange').attr('max');
		var rangeWidth = $('#newTaskRange').outerWidth();
		var dayWidth = rangeWidth / ( max / step );

		var rangeVal = max - ( Math.round( offset / dayWidth ) * 10 );
		$('#newTaskRange').val(rangeVal)
/*
console.log('rangeVal: ' + rangeVal );
console.log('mouseX: ' + mouseX );
console.log('mouseY: ' + mouseY );
console.log('rangeOffset: ' , rangeOffset );
*/
		var rangeVal = projectLength - ($range.val() / 10);
		var thisDate = window.appVars.openDays[rangeVal];	
			$('.task').removeClass('newTaskOnLeft');
			$('.task').removeClass('newTaskOnRight');
	
           if( 'undefined' != typeof thisDate ){
           var vertical = false;
	        var startAndEnd = thisDate.end.concat(thisDate.start);   
	        Object.keys( startAndEnd ).forEach( function( k ) {
				var taskDiv = $('.task[data-id="' + startAndEnd[k] + '"]');
				var offset = taskDiv.offset();

				var l = offset.left;
				var t = offset.top;
				var h = taskDiv.height();
				var w = taskDiv.width();
				var maxx = l + w;
				var maxy = t + h;

				var slider = document.getElementById('newTaskRange').getBoundingClientRect();
				var y = slider.top;
				if( !vertical ){
					vertical = (y <= maxy && y >= t);
				}
				if( vertical ){
				}
	        });
			if( vertical ){
		        Object.keys( thisDate.start ).forEach( function( k ) {
					var taskDiv = $('.task[data-id="' + thisDate.start[k] + '"]');
					taskDiv.addClass('newTaskOnLeft');
				});			
		        Object.keys( thisDate.end ).forEach( function( k ) {
					var taskDiv = $('.task[data-id="' + thisDate.end[k] + '"]');
					taskDiv.addClass('newTaskOnRight');
				});							
			}
			//console.log('thisDate: ' , rangeVal);
	        return vertical;
	      }
	      else{
	      	return false;
	      }
	}
	export function newTaskRangeInput(e){
		alert('newTaskrangeInuput commented out');
/*
		var $range = $('#newTaskRange');
		var projectLength = window.appVars.projectLength;
		var rangeVal = projectLength - ($range.val() / 10);
		var thisDate = window.appVars.openDays[rangeVal];
		if( 'undefined' == typeof thisDate  ){
			$('#newTaskRange').removeClass('inactive');			
// check start and end tasks
			//alert('in opendays' + window.appVars.openDays[rangeVal]);

		}
		else{
	            console.log('check start and end. event:: '  );
	        // this checks lateral; need to check during vertical
	           var rangeClass = 'inactive';
	            // go through end then start if any ar epositive set to active; default inactive
	           alert(1);
	           var vertical = checkRangeContainment();
	        if( vertical ){
				$('#newTaskRange').addClass('inactive');			
				$('#newTaskRangeBubble').addClass('inactive');			
	        }
	        else{
				$('#newTaskRange').removeClass('inactive');				        	
				$('#newTaskRangeBubble').removeClass('inactive');				        	
	        }
	        
		}
		*/
	}
  	export function canSaveTask(){
		if(
            $('#form-task-title').val() &&
            $('#task-update-length').val()
        ){
            var canSave = 1;
        }
        else{
            var canSave = 0;
        }
        if( $('#updateTask').hasClass('new-task') ){
        	canSave = 1;
        }
        if( canSave ){
        	$('#saveTask').attr('disabled', false);
        	$('button.new-task').attr('disabled', false);
        }
        else{
        	$('#saveTask').attr('disabled', true);
        	$('button.new-task').attr('disabled', true);
        }
  	}

	export function openEditTask(e, source){
		var $ps = $('.project-settings'),
			$toggle = $('#settings-toggle'),
			isTaskOnly = 0;
		if(source == 'callback'){
			isTaskOnly = 1;
		}
		else{
			var $target = $(e.target);
			if('task-edit-toggle' == $target.attr('id') ){
				$('.assign-parent-task-wrapper').removeAttr("open");
				var task_id = $target.data('id'),
				tasks = window.appVars.tasks,
				task = tasks[task_id];
				updateTaskCard(task);
				isTaskOnly = 1;
			}
		}
		var editMode = 'task';
		if( source == 'callback'){
			editMode = 'callback';
		}
		if( $ps.hasClass('open') ){
			$ps.hide(50);
			$('.project-input').attr("disabled", true);
			$ps.removeClass('open');
			$toggle.removeClass('active');
			$toggle.removeClass('projectOpen').find('.text');
			$toggle.find('.arrow').removeClass('up').addClass('down');
			toggleEditMode(editMode);
			saveProject();
			$('.project-filters').show();
			$('#project-start-date').show();
			$('#project-end-date').show();
		}
		else{
			if(isTaskOnly){
				$('.task-settings').show();		
			}
			else{
				$('.project-filters').hide();
				$('#project-start-date').hide();
				$('#project-end-date').hide();
				$('.project-input').attr("disabled", false);
				$ps.show(700);
			}
			openSettings();
			toggleEditMode(editMode);
		}
	}
   	export function manageTaskMove( task ){
   		if( !task ){
   			var task_id = $('.activeSidebarTask').data('id'),
   				task = window.appVars.tasks[task_id];
   		}
   		var closingMove = 1;
   		if( closingMove ){
	   		$('#threads-wrapper .children').each(function(k,v){
	   			var originalHeight = $(v).data('original-height');
	   			$(v).height( originalHeight );
	   		})
	   		$('.task.moving-inactive').removeClass('moving-inactive');
	   	}
	   	else{
	   	}

	}  
   	export function manageMoveTaskToggle( ){
        if( 'undefined' != typeof $('.assign-parent-task-wrapper').attr('open') ){
            $('#app').removeClass('moving-task');
            $('#task-schedule .task-settings').show();
            $('#task-description').show();
        }
        else{
            $('#app').addClass('moving-task');
            $('#task-description').hide();
            $('#task-schedule .task-settings').not('.task-buttons').hide();

        }

   	}
   	export function manageGroupAssign( task, clickTarget = null ){
		var selectedGroupId = 0;
		if( clickTarget ){
            var selectedGroupId = parseInt( $(clickTarget.currentTarget).val() ),
                selectedGroupType = $(clickTarget.currentTarget).find('option[value="' + selectedGroupId + '"]').data('type');
	        	$('#user-assign').val(0);
		}
		else{

	   		if( 'undefined' != typeof task.groups && task.groups.length > 0 ){
				selectedGroupId = parseInt( task.groups[0].id );
			}
			var selectedGroupType = $('#group-assign').find('option[value="' + selectedGroupId + '"]').data('type');
        	 $('select#group-assign').val(selectedGroupId);
		}
        if( !selectedGroupId ){
            $("#task-owner-label-wrapper, #updateTask .dialog-wrapper, .activeSidebarTask").removeClass(function (index, className) {
                return (className.match (/(^|\s)group-\S+/g) || []).join(' ');
            });
            $("#task-owner-label-wrapper, #updateTask .dialog-wrapper, .activeSidebarTask").removeClass(function (index, className) {
                return (className.match (/(^|\s)user-\S+/g) || []).join(' ');
            });
        }
        else{

            $("#task-owner-label-wrapper, #updateTask .dialog-wrapper, .activeSidebarTask").addClass('group-' +  selectedGroupId);
        }
		if( 'group' == selectedGroupType){
            $('select[name="user-assign"], #user-assign-label').animate({opacity: 1}, '0.5s', function(){
                $('select[name="user-assign"], #user-assign-label').css('visibility', 'visible');
            });
            $('#user-assign option').not('.null-select').hide();
            $('#user-assign option[data-group-id="' + selectedGroupId + '"]').show();
        }else{
            $('select[name="user-assign"], #user-assign-label' ).animate({opacity: 0}, '0.5s', function(){
                $('select[name="user-assign"], #user-assign-label').css('visibility', 'hidden');
            });
        }                               		
	}  
   	export function manageUserAssign( task, clickTarget = null ){
   		        var numOfUsers = 0;
	   		    var selectedGroupId = 0;
   		        if( clickTarget ){
	            	selectedGroupId = parseInt( $('select#group-assign').val() ),
	            		numOfUsers = $('#user-assign option[data-group-id="' + selectedGroupId + '"]').length;
   		        }
   		        else{
	   		        if( task.groups.length ){
	   		        	selectedGroupId = task.groups[0].id;
	            		numOfUsers = $('#user-assign option[data-group-id="' + selectedGroupId + '"]').length;
	   		        }
	   		    }
            if( selectedGroupId && numOfUsers > 0 ){
                $('select[name="user-assign"]').animate({opacity: 1}, '0.5s', function(){
                    $('select[name="user-assign"]').css('visibility', 'visible');
                });
            }
            else{
                $('select[name="user-assign"]').animate({opacity: 0}, '0.5s', function(){
                    $('select[name="user-assign"]').css('visibility', 'hidden');
                });
            }
	} 
   	export function manageOwnerAssign( task, clickTarget = null ){
   		manageGroupAssign(task, clickTarget );
   				var activeTaskElement = $('.activeSidebarTask');
                $("#task-owner-label-wrapper, #updateTask .dialog-wrapper").removeClass(function (index, className) {
                    return (className.match (/(^|\s)user-\S+/g) || []).join(' ');
                });
                $("#task-owner-label-wrapper, #updateTask .dialog-wrapper").removeClass(function (index, className) {
                    return (className.match (/(^|\s)group-\S+/g) || []).join(' ');
                });
                if( task.users.length ){
                    $('select#user-assign').val(task.users[0].id);
                }
                else{
                    $('select#users-assign').val(0);                    
                }
                var ownerhtml = "";
                var is_assigned = false;
                var user = null;
                if(task.users && task.users.length > 0){
                    user = task.users[0];
                    ownerhtml += "<span class='owner-user'>" + user.name + "</span>";
                    $("input[data-type='user'][value='" + user.id + "']").prop("checked", true);
                    $("#task-owner-label-wrapper, #updateTask .dialog-wrapper").addClass('user-' + user.id);
                	is_assigned = true;
                }
                if(task.groups && task.groups.length > 0){
                    var group = task.groups[0];
                    if(user){
                    	// only show user if current user belongs to this
				        if( window.appVars.currentUser && window.appVars.currentUser.canEditTask( task ) ){
	            	        ownerhtml = group.name + " (" + ownerhtml + ")";
	            	    }
	                }
	                else{
	                    ownerhtml = group.name;
	                }
                	is_assigned = true;
					$("#updateTask .dialog-wrapper").addClass('group-' + group.id);
				}
				if( is_assigned ){     
    	            $("#task-owner-label").html( ownerhtml);

    
                    //$("#updateTask .dialog-wrapper").attr('class','dialog-wrapper');
                    activeTaskElement.addClass('active-user');
                    $('#task-owner-unassigned').hide();
                    $('#task-owner-assigned').show();   
                }
                else{
                    $('#task-assign-partner-0').prop("checked", true);
                    $('#task-owner-unassigned').show();
                    $('#task-owner-assigned').hide();
//                        $('.task-owner-label-wrapper-wrapper').html('');
                    $("#task-owner-label").html('Unassigned');
                }
                if(task.groups && task.groups.length > 0){
                }

	   		manageUserAssign(task, clickTarget);
	}  
  	export function toggleTaskEdit(){
		if( $('#updateTask').hasClass('new-task') ){
			$('#updateTask').removeClass('editing').removeClass('new-task');

		}
		else{
//			alert('is not new task');
			if( $('#updateTask').hasClass('editing') ){
				$('#updateTask').removeClass('editing');
			}
			else{
				$('#updateTask').addClass('editing');			
			}
		}  		
		$('#threads-wrapper .children').each(function(k,v){
			var origHeight = $(v).data('original-height');
			$(v).height(origHeight );
		});
        $('.make-new-thread').prop('checked', false);
        $('input[name="task-parent"]').prop('checked', false);

  	}
  	export function toggleTaskGroupOwner(e){
  		if(!e){
  			$('ul.user-filter-group').each(function(k,v){
  				var groupID = $(v).data('group-id');
	            $('body').addClass('group-' + groupID);
	            $(v).addClass( 'active' );
	            $('body').addClass('active-group-filter');	            
  			})
  		}
  		else{
	        var active = 0,
	            $this = $(e.target).closest('.user-filter-group'),
	            groupId = $this.data('group-id'); 
	        if( $this.hasClass('active') ){
	            active = 1;
	        }
	        $('body').removeClass (function (index, className) {
	            return (className.match (/group-\S+/g) || []).join(' ');
	        });
	        $('body').removeClass (function (index, className) {
	            return (className.match (/user-\S+/g) || []).join(' ');
	        });
	        $('.user-filter-group').removeClass('active');
	        $('.filter-user').removeClass('active');
	        if( active ){
	            $('body').removeClass('group-' + groupId);
	            $this.removeClass( 'active' );
	        }
	        else{
	            $('body').addClass('group-' + groupId);
	            $this.addClass( 'active' );
	        }
	        if( $('.filter-group.active').length ){
	            $('body').addClass('active-group-filter');
	        }
	        else{
	            $('body').removeClass('active-group-filter');        
	        }
	    }
  	}
  	export function updateTaskCard( task ){
  		$('#task-edit-toggle').data('id', task.id );

  		$('.task-list input[name="task-parent"]').siblings('label').each(function(k,v){
  			$(v).html( $(v).data('original-name') );
  		});
  		$('.task-list .radio-wrapper').show();
//  		$('.task-list input[name="task-parent"][value="' + task.id + '"]').closest('.radio-wrapper').hide();
  		var parentTask = window.appVars.tasks[task.parent_id];
  		if( parentTask ){
	  		$('.task-list input[name="task-parent"][value="' + task.parent_id + '"]').siblings('label').html( parentTask.name + ': Current Location');
	  	}
        var cardState = getCardState(task); // card state can be 'view','edit','edit-new';
        $('.task').removeClass('new-parent');

        setCardState(cardState);
  	}
	export function getCardState( task ){
		var cardState = 'view',
			editable = false;
        if( window.appVars.currentUser && window.appVars.currentUser.canEditTask( task ) ){
        	editable = true;
        }        

		if(task && 'new_task' == task.type && editable){
			return 'edit-new';
		}
		if ( editable && ( $('#app').hasClass('project-edit') || $('#updateTask').hasClass('editing') ) ){
			return 'edit';
		}
		return cardState;
	}
	export function setCardState( state ){
		console.log('STATE::::',state);
		if('edit' == state){
			$('#app').addClass('task-edit').removeClass('task-view').removeClass('task-edit-new');
			//$('#updateTask').addClass('editing');
			$('#form-task-title').prop('disabled', false);
            window.autoExpand($('#task-description-content')[0])

//            $('#updateTask').removeClass('new-task');
		}
		else if('edit-new' == state){
			$('#app').addClass('task-edit-new').removeClass('task-view').removeClass('task-edit');
            $('#updateTask').addClass('new-task');
			//$('#updateTask').addClass('editing');
			$('#form-task-title').prop('disabled', false);
            $('.task-error-messages').html('new task message');
            //openEditTask(null, 'callback');
		}
		else{
			$('#app').addClass('task-view').removeClass('task-edit-new').removeClass('task-edit');
			$('#updateTask').removeClass('editing');
			$('#updateTask').removeClass('new-task');
			$('.sidebar .project-settings').removeClass('open').hide();

		}
        window.autoExpand($('#form-task-title')[0])
		manageTaskMove();
	}
	export function makeNewThread(e){
		$('#threads-wrapper .children').each(function(k,v){
			var origHeight = $(v).data('original-height');
			$(v).height(origHeight );
		})
		$('.make-new-thread').not("[value='" + $(e.target).val() + "']").prop('checked', false);
		if( $(e.target).is(':checked') ){
			var $newParent = $('.task.new-parent'),
				$childThread = $newParent.siblings('.children'),
				numOfChildren = 1; // will need to get this
			var childHeight = $childThread.height(),
				childParentHeight = $childThread.parent().height(),
				newChildHeight = childHeight / ( numOfChildren + 1),
				childHeightPercent = childHeight / childParentHeight * 100,
				newChildHeightPercent = newChildHeight / childParentHeight * 100,
                activeTaskElementID = $('.activeSidebarTask').data('id'),
				tasks = window.appVars.tasks,
                activeTask = tasks[activeTaskElementID];

			$childThread.data('original-height',childHeightPercent + "%");	
			$childThread.height( newChildHeightPercent + "%"); 
            var newLocationText = `New Location for ` + activeTask.name + `...`;
            if( null == activeTask.name ){
                newLocationText = `New Task Location`;
            }

			$('#new-task-style').html(`
                <style>
                    .new-parent:before{ 
                        height:` + newChildHeightPercent + `%;
                        width:30px !important;
                        left:-45px !important;
                    }
	                .new-parent:after{
                        content: '` + newLocationText + `';

	                    height:100%;
	                }

                </style>`
            );			
		}
		else{
			$('#new-task-style').html(`
                <style>
                    .new-parent:before{ 
                        height:100%;
                    }
                </style>`
            );						
		}
	}

	export function openSettings(){
	var $ps = $('.project-settings'),
	  $toggle = $('#settings-toggle');    
	$ps.addClass('open');
	$toggle.addClass('projectOpen').addClass('active').find('.text').html('Exit & Save Edit');
	$toggle.find('.arrow').removeClass('down').addClass('up');
  }
  	export function getActiveProject( project ){
  		if( !project ){
			var project = window.appVars.project;
		}
		return project;
  	}
  	export function getActiveTask( task ){
  		if( !task ){
			var task_id = $('.activeSidebarTask').data('id');
			task = window.appVars.tasks[task_id];
		}
		return task;
  	}
  	export function getProjectOwner( project ){
		project = getActiveProject(project);
  		if( !project ){
	  		return window.appVars.currentUser;
	  	}
	  	else{
	  		return user;
	  	}
  	}
  	export function getCurrentUser( user ){
  		if( !user ){
	  		return window.appVars.currentUser;
	  	}
	  	else{
	  		return user;
	  	}
  	}
  	export function isUserGroupAdmin( group, user ){
  		var userIsGroupAdmin = 0;
  		// refactor for multi admins
  		if( group.admin.id == user.id ){
  			userIsGroupAdmin = 1;
  		}
  		return userIsGroupAdmin;
  	}
  	export function canUserEditTask( task, user ){
  		var userCanEditTask = 0;
  		task = getActiveTask(task);
  		var project = window.appVars.project;
  		user = getCurrentUser( user );

  		if( isUserTaskOwner( task ,user ) ){
			userCanEditTask = 1;
  		}
  		if( !userCanEditTask && task.groups.length > 0 ){
  			$.each( task.groups,function(k,group){  				
		  		if( isUserGroupAdmin( group, user ) ){
			  		userCanEditTask = 1;
		  		}
  			})
	  	}
	  	if( !userCanEditTask ){
		  	if( isUserProjectOwner( project, user) ){
		  		userCanEditTask = 1;
		  	}
		}
  		return userCanEditTask;
  	}
  	export function isTaskAssigned(task){
  		if( task.groups.length > 0 ){
  			return true;
  		}
  		if( task.users.length > 0 ){
  			return true;
  		}
  		return false;
  	}
  	export function isUserTaskOwner( task, user ){
  		task = getActiveTask(task);
  		user = getCurrentUser( user );
  		var userIsTaskOwner = 0;
  		if(isTaskAssigned(task)){
	  		$.each(task.users, function(k, taskUser ){
	  			if( taskUser.id == user.id ){
					userIsTaskOwner = 1;
				}
	  		})
	  		return userIsTaskOwner;
  		}
  	}
  	export function isUserProjectOwner( project, user){
  		project = getActiveProject(project);
  		var projectOwner = project.owner; // group object
  		user = getCurrentUser( user );
  		var userIsProjectOwner = 0;
  		if( projectOwner.admin.id == user.id ){
	  		userIsProjectOwner = 1;
  		}
		return userIsProjectOwner;
  	}
  	export function closeEndDate(start_msg = "" , end_msg = "" ){
  		if( !end_msg ){
  			end_msg = "";
	        $('#end-label').removeClass( "active" );  		
  		}  		
  		else{
	        $('#end-label').addClass( "active" );
  		}
  		if( !start_msg ){
  			start_msg = "x";
	        $('#start-label').removeClass( "active" );  		
  		}  		
  		else{
	        $('#start-label').addClass( "active" );
  		}
        $('#task-close-date').prop( "disabled", true );
        $('#end-label').addClass( "disabled" );
        $('#clear-close-date').addClass('disabled'); 
        $('#task-start-date-error-messages').html( start_msg ).addClass('active');
        $('#task-end-date-error-messages').html( end_msg ).addClass('active');
  	}
  	export function openEndDate(start_msg = "" , end_msg = ""){
  		var start_label_class = "";
  		if( !start_msg ){
  			start_msg = "x";
	        $('#start-label').removeClass( "active" );  		
  		}  		
  		else{
	        $('#start-label').addClass( "active" );
  		}
  		if( !end_msg ){
  			end_msg = "";
	        $('#end-label').removeClass( "active" );  		
  		}  		
  		else{
	        $('#end-label').addClass( "active" );
  		}
        $('#task-close-date').prop( "disabled", false );
        $('#end-label').removeClass( "disabled" );  		
        $('#clear-close-date').removeClass('disabled');
        $('#task-start-date-error-messages').html( start_msg ).addClass('active');
        $('#task-end-date-error-messages').html( end_msg ).addClass('active');
  	}
  	export function closeStartDate( start_msg = "" , end_msg = "" ){
  		if( !start_msg ){
  			start_msg = "Close out the previous task to set the start date.";
	        $('#start-label').removeClass( "active" );  		
  		}  		
  		else{
	        $('#start-label').addClass( "active" );
  		}
  		if( !end_msg ){
  			end_msg = "";
	        $('#end-label').removeClass( "active" );  		
  		}  		
  		else{
	        $('#end-label').addClass( "active" );
  		}
        $('#task-update-start-date').prop( "disabled", true );
        $('#start-label').addClass( "disabled" );
        $('#clear-start-date').addClass('disabled'); 
        $('#task-start-date-error-messages').html( start_msg ).addClass('active');
  	}
  	export function openStartDate( start_msg = "x" , end_msg = ""  ){
  		if( !start_msg ){
  			start_msg = "";
	        $('#start-label').removeClass( "active" );  		
  		}  		
  		else{
	        $('#start-label').addClass( "active" );
  		}
  		if( !end_msg ){
  			end_msg = "";
	        $('#end-label').removeClass( "active" );  		
  		}  		
  		else{
	        $('#end-label').addClass( "active" );
  		}
        $('#task-update-start-date').prop( "disabled", false );
        $('#start-label').removeClass( "disabled" );  		
        $('#clear-start-date').removeClass('disabled');
        $('#task-start-date-error-messages').html( start_msg);  		
        $('#task-end-date-error-messages').html( end_msg ).addClass('active');
  	}
   export function saveProject(){
		var project = {},
			api_token = $("meta[name='api_token']").attr("content");

		var name = $('#project-name').val(),
			id = $('#project-container').data('project_id'),
			description = $('#project-description').val(),
			isPublic = ( $('#project-public').is(":checked") ? 1 : 0);
		$.ajax({
			url: "/api/projects/updateProject" ,
			dataType: "json",
			type : 'POST',
			data: {
				api_token: api_token,
				name: name,
				id : id,
				isPublic : isPublic,
				description: description
			},
			success: function( data ) {
				console.log( 'project udpate::', data );
			},
			error : function(request,error){
				console.log("error Request: "+JSON.stringify(request));
			}

		});
		console.log('PROJECT:::',project);
	}
