import React, { Component } from 'react';
import ReactDOM from 'react-dom';

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};
if (document.getElementById('project-container')) {
    var project_id = document.getElementById('project-container').dataset.project_id,
        tasks = {},
        taskTree = {},
        project_thread_lengths = {},
        earliestStart = {};              
    axios
        .get('/api/taskTree/' + project_id)
        .then(response => {
            tasks = response.data.tasks;
            taskTree = response.data.branch;
            project_thread_lengths = response.data.project_thread_lengths;
            earliestStart = response.data.earliestStart;
            ReactDOM.render(<App />, document.getElementById('project-container'));

    });
}
export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            project_id : document.getElementById('project-container').dataset.project_id,
            tasks: tasks,
            taskTree: taskTree,
            project_thread_lengths: {},
            greatest_thread_length: 0,
            length_ratio : 1,
            lastChildren : [],
            first_task : {},
            threadStarts : {},
            estStartDate : earliestStart.date,
            latestEnd : earliestStart.date, // this is calculated below
            timezone : earliestStart.timezone,
            today : new Date('dec 6 2018')
        };
        this.fn = this.fn.bind(this)
        this.calendarRef = React.createRef();
        this.zoomIn = this.zoomIn.bind(this);
        this.zoomOut = this.zoomOut.bind(this);
        this.handleEvent = this.handleEvent.bind(this);
        this.handleUpdateEvent = this.handleUpdateEvent.bind(this);
        this.handleTaskChange = this.handleTaskChange.bind(this);
        this.ancestors = [];
        this.threads = {};
        this.buildLastChildren();
        this.buildTaskTree();
    }
    buildTaskTree(){
        console.log('#11 when startdate is nov 15 doesnt work');
        var thread_lengths = this.state.project_thread_lengths;
        $.each(this.last_children, function(k,id){
            this.state.lastChildren.push(id);
            var task = this.state.tasks[id],
                descendants = [];

            this.buildDates(task, descendants, id);
        }.bind(this));

        $.each(this.state.threadStarts,function(id,v){
            this.setDates(this.state.tasks[id]);
        }.bind(this));

    
        // this sets the threadlengths:
        // START BUFFER SHOULD BE DEFINED HERE, SO.....ACCOUNT FOR IT?
        $.each(this.last_children, function(k,id){
            var task = this.state.tasks[id],
                descendants = [],
                thread_length = 0;
            this.threads[task.id].descendants = [];
            this.buildThread(task, descendants, id, thread_length);
        }.bind(this));
// 10/23 this loop seems excessive: review
        $.each(this.state.threadStarts, function(f,g){
            var id = f,
                thread_length = 0;
            $.each(thread_lengths[id],function(key, thisTask){
                if( (thisTask.realLength ) > thread_length){
                    thread_length = thisTask.realLength;
                }
            })
            var $task =  this.state.tasks[id],
                test_thread_length = thread_length;
            if(!$task.parent_id){
                test_thread_length = thread_length;
            }
            if( (test_thread_length  ) > (this.state.greatest_thread_length )){

                this.state.greatest_thread_length = (test_thread_length  ) * this.state.length_ratio;
            }
        }.bind(this));
    }
    setEndDates(task_id, testDate = null, compare = null){
    //  var thisEndDate = (this.state.tasks[task.id].endDate ? new Date(this.state.tasks[task.id].endDate) : null ),
        if(task_id == 12){
            if (this.state.tasks[task_id].endDate) {
                if (this.state.tasks[task_id].endDate.getMonth) {
//                  return this.state.tasks[task_id].endDate;
                }
                else{
                }
            }
            else{
                if (this.state.tasks[task_id].estEndDate) {
//                  return this.state.tasks[task_id].estEndDate
                }
                else{
                }
            }
        }
    }
    buildDates(task, descendants, i){

        var thisTask = this.state.tasks[task.id],
            today = this.state.today,
            latest_descendant_end_date = new Date(this.state.estStartDate);

        thisTask.userLength = thisTask.length;
        //set initial estEndDate:
        if(thisTask.startDate ){
            var startDate = new Date(thisTask.startDate),
            //check if startDate is today or earlier - set to null if not
            startCompare = dates.compare(today, startDate); //negative if today is earlier than startDate
            if(startCompare < 0){               
                // cannot have a startDate after "today"
                thisTask.startDate = null;
            }
            else{
                thisTask.estEndDate = new Date( startDate.addDays(thisTask.length -1)); //-1 inlcusive reckoning
            }
        }
        if(thisTask.estStartDate){
            var estStartDate = new Date( thisTask.estStartDate);
            thisTask.estEndDate = new Date( estStartDate.addDays(thisTask.length -1)); //-1 inlcusive reckoning
        }
        //set initial estEndDate:
        if(thisTask.closedDate){
            //check if closedDate is later than today - set to null if it is
            var closedCompare = dates.compare(thisTask.closedDate,today); //positive if today is earlier than closedDate
            if(closedCompare > 0){
                // cannot have a startDate after "today"
                var closedDate = thisTask.closedDate = thisTask.endDate = null;
            }
            else{
                thisTask.endDate = thisTask.estEndDate = thisTask.closedDate;
            }
        }
    //  var thisEndDate = (this.state.tasks[task.id].endDate ? new Date(this.state.tasks[task.id].endDate) : null ),
    //      thisEstEndDate = new Date(thisEndDate ? thisEndDate : new Date(this.state.tasks[task.id].estEndDate));

        $.each(descendants,function(k,v){
            console.log('NEED TO GET RID OF ESTENDDATE');
            var descEndDate = this.state.tasks[v].endDate,
                desc_task_end_date = new Date(descEndDate);


            var compare = dates.compare(desc_task_end_date,latest_descendant_end_date);
            if( compare > 0){
                latest_descendant_end_date = desc_task_end_date;
            }
            if(thisTask.startDate){
                thisTask.estStartDate = new Date(thisTask.startDate);
            }
            else{
                if(thisTask.estStartDate){
                    var earliestCompare = dates.compare(  latest_descendant_end_date, thisTask.estStartDate);
                    if(earliestCompare > 0){
                        thisTask.estStartDate = new Date(latest_descendant_end_date.addDays(1));
                        var endMinusStart = dateDiffInDays( thisTask.estStartDate, this.state.tasks[task.id].estEndDate);
                        if(endMinusStart <= 0){
                             this.state.tasks[task.id].estEndDate = thisTask.estStartDate.addDays(this.state.tasks[task.id].length - 1);
                        }
                    }
                }
                else{        
                    thisTask.estStartDate = new Date(latest_descendant_end_date.addDays(1));
                }
            }
            if(!thisTask.closedDate){
//              testEndDate = new Date( thisTask.estStartDate.addDays(thisTask.length -1 ));
                if( dateDiffInDays( new Date(thisTask.estEndDate), latest_descendant_end_date)> 0){
                    //var newDate = new Date(latest_descendant_end_date.addDays(1));
                    //alert("length is jacked" + thisTask.length);
                    var testStartDate = new Date( thisTask.estStartDate.addDays(thisTask.length -1 )),
                        testMinusStart = dateDiffInDays( thisTask.estStartDate, testStartDate ),
                        testMinusEnd = dateDiffInDays( thisTask.estEndDate, testStartDate );

                    if( (testMinusStart > 0 ) && ( testMinusEnd < 0)){
                        thisTask.estEndDate = new Date( thisTask.estStartDate.addDays(thisTask.length -1 ));
                    }
                }

            }
        }.bind(this))

        if(this.isThreadStart(task)){
            if( 'undefined' == typeof this.state.threadStarts[task.id]){
                this.state.threadStarts[task.id] = {'id' : task.id};
            }
        }
        if(!thisTask.closedDate){
            thisTask.endDate = thisTask.estEndDate;
            var compare = dates.compare(thisTask.endDate, this.state.today);

            if( compare <= 0){
                thisTask.endDate = thisTask.estEndDate= this.state.today;

                if( this.state.tasks[task.id].startDate ){
                    var setStartDate = new Date(this.state.tasks[task.id].startDate);
                }
                else if(this.state.tasks[task.id].estStartDate){
                    var setStartDate = new Date(this.state.tasks[task.id].estStartDate);
                }
                else{
                    var setStartDate = new Date(this.state.tasks[task.id].estStartDate);
                }
                this.state.tasks[task.id].length = 1 + dateDiffInDays( setStartDate, this.state.tasks[task.id].estEndDate);
                this.state.tasks[task.id].pending = true;
            }
        }
        else{
            thisTask.endDate = thisTask.closedDate;
            thisTask.length = 1 + dateDiffInDays( new Date(this.state.tasks[task.id].startDate), new Date(this.state.tasks[task.id].closedDate));
        }
        if(!thisTask.endDate && thisTask.estStartDate){

            thisTask.estEndDate = new Date( thisTask.estStartDate.addDays(thisTask.length -1 ));
            //thisTask.endDate = thisTask.estEndDate = new Date( thisTask.estStartDate.addDays(thisTask.length -1 ));
            if(!thisTask.endDate && thisTask.estEndDate){
                thisTask.endDate = new Date(thisTask.estEndDate);
            }
        }
        if(task.parent_id){
            descendants.push(task.id);
            return this.buildDates(this.state.tasks[task.parent_id], descendants, i);
        }
        return true;
    }
    setDates(task){
        if(this.state.tasks[task.id].length<1){
            this.state.tasks[task.id].length = 1;
        }
        if(task.parent_id){
            var parent = this.state.tasks[task.parent_id],
                this_end_at_parent = new Date(parent.estStartDate),
                length = this.state.tasks[task.id].length,
                startDate = 0;
            if(this.state.tasks[task.id].startDate){
                startDate = new Date(this.state.tasks[task.id].startDate);
                var startCompare = dates.compare(this.state.today, startDate);
                if(startCompare < 0){
                    startDate = this.state.tasks[task.id].startDate = null;
                }
            }
            if(parent.startDate){
                this_end_at_parent = new Date(parent.startDate);
            }
            this.state.tasks[task.id].parent_start = this_end_at_parent;
            var parent_start_minus = this.state.tasks[task.id].parent_start_minus_length = this_end_at_parent.addDays(-length);
            if(startDate){
                if( dates.compare(parent_start_minus, startDate) > 0 ){
                    // check (estEnddate - length) if thats earlier than parent_start_minus,  
                    if(!this.state.tasks[task.id].startDate){
                        this.state.tasks[task.id].estStartDate = parent_start_minus;
                    }
                    else{                       
                        this.state.tasks[task.id].estStartDate = this.state.tasks[task.id].startDate;   
                    }
                }
            }
        }
        var startDiff = 0, closedDiff =0;
        this.state.tasks[task.id].startBuffer = 0;
        if(this.state.tasks[task.id].estStartDate && this.state.tasks[task.id].startDate){
            startDiff = dateDiffInDays( new Date(this.state.tasks[task.id].estStartDate), new Date(this.state.tasks[task.id].startDate));
            if(startDiff> 0 ){
                this.state.tasks[task.id].startBuffer = parseInt(startDiff); 
            }
            else{
                startDiff = 0;
            }
        }   
        this.state.tasks[task.id].realLength = this.state.tasks[task.id].length;
        this.state.tasks[task.id].endBuffer = 0;
        if(this.state.tasks[task.id].closedDate){
            var estClosedDate = new Date(this.state.tasks[task.id].closedDate); 
        }
        else{
            var estClosedDate = new Date(this.state.tasks[task.id].estEndDate);         
        }
        if(startDate && estClosedDate){
            if(task.parent_id){
                var parent = this.state.tasks[task.parent_id];

                var closedDate = new Date(this.state.tasks[task.id].closedDate); 
                if(parent.startDate){
                    var parentStartDate = new Date(parent.startDate); 
                }
                else{
                    var parentStartDate = new Date(parent.estStartDate);                
                }
                var closedDiff = dateDiffInDays( parentStartDate, closedDate);
            }
        }       
        //combine this with immeditaely above????
        if(startDate && task.parent_id ){
            var parent = this.state.tasks[task.parent_id];
            if(parent.startDate){
                var parentStartDate = new Date(parent.startDate); 
            }
            else{
                var parentStartDate = new Date(parent.estStartDate);                
            }
            var closedDiff = dateDiffInDays( estClosedDate, parentStartDate) -1; // -1 for inclusive reckoning
            if(closedDiff>0){
                this.state.tasks[task.id].endBuffer = closedDiff ;
            }
        }
//      this.state.tasks[task.id].realLength = this.state.tasks[task.id].startBuffer + this.state.tasks[task.id].length;
        if("undefined" != typeof task.children ){
            $.each(task.children, function(k,v){
                var child = this.state.tasks[v.id];
                this.setDates(child);
            }.bind(this));
        }

    }
    buildThread(task, descendants, i, thread_length){
        //calendr dates here
        /*
        Compare thread length to ...? 
***NEED TO CALCULATE THE PROJECT END DATE - ONE THREAD COULD HAVE A LATER/DELAYED START**** 
        lastchidlren should have an endDate - either real or planned
        that date will be the "task open" date for the next. actual start date may be later. (lull can be planned)
        lull will be part of thread length
        have four values: task planned length, lull, buffer days, total task length

        then, need to add team down days (check task owner and check blocked days) -
        (say...team X has 11/15 & 11/16 off during this calendar calc, so add two buffers days to task length; buffer days push out calendar and new days need to be checked)

        */
        var taskLength = parseInt(task.length),
            taskRealLength = parseInt(task.realLength),
            taskStartBuffer = parseInt(task.startBuffer),
            taskEndBuffer = parseInt(task.endBuffer);
            //alert(taskStartBuffer);
        if( 'undefined' != typeof taskStartBuffer && taskStartBuffer > 0){
            if(task.id == 12){
                //alert(taskStartBuffer);
            }
            taskRealLength += taskStartBuffer;
        }
        if( 'undefined' != typeof taskEndBuffer && taskEndBuffer > 0){
            taskRealLength += taskEndBuffer;
        }
        thread_length += taskRealLength;//total task length = plaanned plus lull
        if('undefined' == typeof this.state.tasks[task.id].descendant_thread_length){
            this.state.tasks[task.id].descendant_thread_length = [];
        }
        if('undefined' == typeof this.state.tasks[task.id].descendant_thread_length[i]){
            this.state.tasks[task.id].descendant_thread_length[i] = 0;
        }
        if(this.isThreadStart(task)){
            $.each(descendants,function(k,v){
                this.state.tasks[task.id].descendant_thread_length[i] += parseInt(this.state.tasks[v].length);
                this.threads[task.id].descendants.push(descendants[k]);
            }.bind(this))
            this.state.tasks[task.id].descendant_thread_length[i] += task.length;
            if('undefined' == typeof this.state.project_thread_lengths[task.id]){
                this.state.project_thread_lengths[task.id] = [];
            }
            var subTask = {
                'id':i,
                'length':thread_length,
                'realLength':thread_length
            };
            this.state.project_thread_lengths[task.id].push(subTask);
        }
        if(task.parent_id){
            descendants.push(task.id);
            return this.buildThread(this.state.tasks[task.parent_id], descendants, i, thread_length);
        }
        else{
            if(task.length > this.state.first_task.length){
                this.state.first_task.length = task.length;
            }
        }
        return true;
    }

    buildLastChildren(){
        this.last_children = [];
        $.each(this.state.taskTree, function(id,v){
           this.getLastChildren(v);
        }.bind(this));
    }
    getLastChildren(task){
        if('undefined' == typeof this.threads[task.id] ){
            this.threads[task.id] = {'descendants':[]};
        }
        if(task.children && Object.size(task.children) > 0 ){
            Object.keys(task.children).map(function(childId) {
                var child = task.children[childId];
                this.getLastChildren(child);
            }, this);
        }
        else{
            this.last_children.push(task.id);
        }
    }    
    zoomIn(){
        var newRatio = this.state.length_ratio + 2;
        this.setState({
            length_ratio : newRatio
        })
        this.updateHeight();
    }
    zoomOut(){
        var newRatio = this.state.length_ratio - 2;
        if(newRatio < 1){
            newRatio = 1;
        }
        this.setState({
            length_ratio : newRatio
        })
        this.updateHeight();
    }
    fn() {
//console.log('parent')
    }
    reBuildThread(task, descendants, i, thread_length){
        var taskLength = parseInt(task.length);
        if( task.startBuffer > 0 ){
            taskLength += parseInt(task.startBuffer);
        }
        if( task.endBuffer > 0 ){
            taskLength += parseInt(task.endBuffer);
        }
        thread_length += parseInt(taskLength);
        if(this.isThreadStart(task)){
            $.each(descendants,function(k,v){
                var descTask = this.state.tasks[v];
                var descTaskLength = parseInt(descTask.length);
                if( descTask.startBuffer > 0 ){
                    //descTaskLength += parseInt(descTask.startBuffer);
                }
                if( descTask.endBuffer > 0 ){
                    //descTaskLength += parseInt(descTask.endBuffer);
                }
                this.state.tasks[task.id].descendant_thread_length[i] += parseInt(descTaskLength);
            }.bind(this))
//stuff...
            this.state.tasks[task.id].descendant_thread_length[i] += taskLength;
            var subTaskId = i,
                subTaskLength = thread_length;
            $.each(this.state.project_thread_lengths[task.id], function(k,v){
                if(subTaskId == v.id){
                    var subTaskStartBuffer = parseInt(this.state.tasks[v.id].startBuffer);
                    if( subTaskStartBuffer < 0 ){
                        subTaskStartBuffer = 0;
                    }
                    subTaskLength += subTaskStartBuffer;
                    var subTaskEndBuffer = parseInt(this.state.tasks[v.id].endBuffer);
                    if( subTaskEndBuffer < 0 ){
                        subTaskEndBuffer = 0;
                    }
                    //subTaskLength += subTaskEndBuffer;
                    this.state.project_thread_lengths[task.id][k].length = subTaskLength;

                }
            }.bind(this))
        }
        if(task.parent_id){
            return this.reBuildThread(this.state.tasks[task.parent_id], descendants, i, thread_length);
        }
        else{
            if(task.length > this.state.first_task.length){
                this.state.first_task.length = task.length;
            }
        }
        return true;

    }
    getAncestors(task, orig_id){
        if('undefined' == typeof this.state.tasks[orig_id].ancestors){
            this.state.tasks[orig_id].ancestors = [];
        }
        if(this.state.tasks[task.id].parent_id){
            var parent_id = this.state.tasks[task.id].parent_id,
                parent = this.state.tasks[parent_id];
            this.state.tasks[orig_id].ancestors.push(parent_id);
            if(parent.parent_id){
                this.getAncestors(parent, orig_id);
            }
        }
    }
    isThreadStart(task){
        if(task.hasSiblings > 0 ){
            return true;
        }
        if( task.children ){
            return true;
        }
        if( !task.parent_id  ){
            return true;
        }
        return false;
    }
    updateTask(task_id){
        this.buildTaskTree();
        //var tasks = this.state.tasks;
        //alert($('#task-length').val());
        var newLength = $('#task-length').val();
        this.state.tasks[task_id].length = newLength;
        $('[data-id="'+ task_id +'"] .length').html(newLength);
        this.setState({
            tasks
        });
    }
    handleUpdateEvent(){
        // fires when modal is saved
        var task_id = $('#task-update-id').val();
        this.updateTask(task_id);
    }
    handleEvent(task){
        // fires when modal is opened
    }
    handleTaskChange(taskId){
//alert(taskId);
    }
    handleEventVoid(e){
        this.updateHeight();
    }
    openThread(e){
        var $target = $(e.target),
            toggle = true;
        if($target.parent().hasClass('active-thread')){
            toggle = false;
        }
        $('.active-thread').removeClass('active-thread');
        if(toggle){
            $('body').addClass('task-open  thread-open');
            $target.parent().addClass('active-thread');
        }
        else{
            $('body').removeClass('task-open  thread-open');            
        }
    }
    renderTask(taskId){

        var children = "",
            task = this.state.tasks[taskId],
            hasChildren = 0,
            users = this.state.tasks[taskId].users,
            classes = "",
            numberChildren = 0;
            $.each(users,function(k,v){
//              console.log('ggggggggggggggggggggggggggggggggggggggggggggg:', v);
                classes += ' u-' + v.id;
            });
        if( task.children) {
            children = task.children;
            hasChildren = 1,
            numberChildren = Object.size(children);
        }
        var totalWidth = 14,
            nodeWidth = (task.length/totalWidth * 100),
            childWidth = 100 - nodeWidth,
            nodeHeight = 25,
            thread_length = "";
        if(task.thread_length){
            thread_length = task.thread_length;
        }
        if ( (hasChildren && numberChildren >= 2) || task.hasSiblings ) {
            if ( numberChildren > 1  || task.hasSiblings ){
                if(hasChildren){
                    return (
                          <div className="thread-container " key={task.id} data-task-id={task.id} data-weight={task.weight} >  
                            <div className="end-buffer" data-length={task.endBuffer} id={"end-buffer-" + task.id}></div><Task node={task} children={task.children}  id={task.id}  classes={classes} onVote={this.handleEvent} name={task.name} /><div className="start-buffer" data-length={task.startBuffer} id={"start-buffer-" + task.id}></div>
                            <div className="children" >{ this.renderTaskChildren(children) }</div>
                             <div className="thread-control" data-task-id={task.id} onClick={this.openThread}>
                            </div>
                          </div>
                    );
                }
                else{
                    return (
                          <div className="thread-container " key={task.id} data-task-id={task.id} data-weight={task.weight} >  
                            <div className="end-buffer" data-length={task.endBuffer} id={"end-buffer-" + task.id}></div><Task node={task} id={task.id}  onVote={this.handleEvent} classes={classes} name={task.name} /><div className="start-buffer" data-length={task.startBuffer} id={"start-buffer-" + task.id}></div>
                             <div className="thread-control" data-task-id={task.id} onClick={this.openThread}>
                            </div>
                          </div>
                    );

                }
            }
            else{
                if ( numberChildren == 1 ){
                    return (
                        <div>
                        <div className="end-buffer" data-length={task.endBuffer} id={"end-buffer-" + task.id}></div>
                            <Task node={task} children={task.children} onVote={this.handleEvent}  id={task.id}  name={task.name} />
                        <div className="start-buffer" data-length={task.startBuffer} id={"start-buffer-" + task.id}></div>
                        { this.renderTaskChildren(children) }
                        </div>
                    );            
                }
                else{
                    return (

                        <Task node={task} children={task.children} onVote={this.handleEvent}  id={task.id} name={task.name} />
                    );            
                }
            }
        }else{
            if(task.hasSiblings){
                nodeHeight = 100/task.hasSiblings;
            }
            if(task.hasSiblings){
                if ( hasChildren && numberChildren == 1 ){
                    return (<div>
                                                <div className="end-buffer" data-length={task.endBuffer} id={"end-buffer-" + task.id}></div>

                            <Task node={task} children={task.children} onVote={this.handleEvent}  id={task.id}  name={task.name} />     
                            <div className="start-buffer" data-length={task.startBuffer} id={"start-buffer-" + task.id}></div>
                            { this.renderTaskChildren(children) }
                            </div>
                    );
                }
                else{
                    return(
                    <div key={task.id}>
                        <div className="end-buffer" data-length={task.endBuffer} id={"end-buffer-" + task.id}></div>
                        <Task node={task} children={task.children} onVote={this.handleEvent} id={task.id} name={task.name} />
                        <div className="start-buffer" data-length={task.startBuffer} id={"start-buffer-" + task.id}></div>
                    </div>
                    );
                }
            }
            else{
                if ( hasChildren && numberChildren == 1 ){
                    return (<div className="thread-container" key={task.id} data-task-id={task.id} data-weight={task.weight} > 
                        <div className="end-buffer" data-length={task.endBuffer} id={"end-buffer-" + task.id}></div>
                            <Task node={task} children={task.children} onVote={this.handleEvent}  id={task.id}  name={task.name} />     
                    <div className="start-buffer" data-length={task.startBuffer} id={"start-buffer-" + task.id}></div>
                            { this.renderTaskChildren(children) }
                            </div>
                    );
                }
                else{
                    if(!task.parent_id){
                        return(
                        <div className="thread-container" key={task.id} data-task-id={task.id} data-weight={task.weight} >
                        <div className="end-buffer" data-length={task.endBuffer} id={"end-buffer-" + task.id}></div>
                            <Task node={task} children={task.children} onVote={this.handleEvent} id={task.id} name={task.name} />
                            <div className="start-buffer" data-length={task.startBuffer} id={"start-buffer-" + task.id}></div>
                        </div>
                        );
                    }
                    else{
                        return(
                        <div key={task.id}>
                        <div className="end-buffer" data-length={task.endBuffer} id={"end-buffer-" + task.id}></div>
                            <Task node={task} children={task.children} onVote={this.handleEvent} id={task.id} name={task.name} />
                        <div className="start-buffer" data-length={task.startBuffer} id={"start-buffer-" + task.id} ></div>
                        </div>
                        );

                    }
                }

            }
        } 
       
    }
    renderTaskChildren(children){
        let childnodes = null;
        let singleChildNodes = null;
        childnodes = Object.keys(children).map(function(childnodeId) {
           var singleChild = 0;
            if(Object.size(children) == 1){
                singleChild = 1;
            }
            var childnode = children[childnodeId];
            return this.renderTask(childnodeId);
        }, this);
        return childnodes;
    }
    render(){
        //
        ReactDOM.render(<TaskForm onUpdate={this.handleUpdateEvent}  />, document.getElementById('task-form'));
        $.each(this.state.tasks, function(k,v){
            this.getAncestors(v,v.id);
        }.bind(this));

        var taskTree = this.state.taskTree,
            app = (
            Object.keys(taskTree).map(function(taskId) {
                var task = this.state.tasks[taskId];
                return this.renderTask(taskId);
            }, this)
        )
//        console.log('aPP', app);
        return ( <div className='app-container'><div className='controls'><div onClick={this.zoomIn} className='zoom' id='zoom-in'>zoom in</div><div onClick={this.zoomOut} className='zoom' id='zoom-out'>zoom out</div></div><div id='calendar-container'></div>{ app }</div> );       
    }
    componentDidMount() {
//
        var $this = $(ReactDOM.findDOMNode(this)),
            $first_threads = $this.find('.thread-container');
            
        //  moving this 12/21 10:am to a data manipulation area
    //  alert('setdates could be...in builddates???');
        $.each($first_threads,function(k,v){
            var id = $(v).data('task-id'),
            thread_length = 0;
            this.setDates(this.state.tasks[id]);
        }.bind(this));
        
      $.each(this.last_children, function(i,id){
            var task = this.state.tasks[id],
                descendants = [],
                thread_length = 0;
            this.reBuildThread(task, descendants, id, thread_length);
        }.bind(this));
        this.updateHeight();
    }
    componentDidUpdate() {
        this.updateHeight();
    }
    // iterate UP the tree and identify start and end buffers for each task and thus thread
    setFinalThreadLengths($task){
            var thisStartBuffer = $task.startBuffer,
                children = $task.children;
            if( children ){
                /*
                $.each(this.state.project_thread_lengths[thisParent_id], function(a,b){
                    if(v == b.id){
                        this.state.project_thread_lengths[thisParent_id][a].startBuffer = thisStartBuffer;
                        this.state.project_thread_lengths[thisParent_id][a].realLength = this.state.project_thread_lengths[thisParent_id][a].length + thisStartBuffer;

                    }
                }.bind(this));
                */
            }


    }
    updateHeight(){
        //in project thread lengths we have the longest thread in days. we can get the end date???
        // .... 
        /*
need to calc and accomodate any task with a set date. 
1. an antecedent task cannot start until a preceding task has been closed/ended
2. 
        */
        // have to pass any task's end date from the following start date - the last task to the right has the riht end date
    //  alert('updateheight');
        var $this = $(ReactDOM.findDOMNode(this));
        var $first_threads = $this.find('.thread-container'),
            $longest_thread ="",
            thread_lengths = this.state.project_thread_lengths,
            greatest_thread_length = this.state.greatest_thread_length,
            vp_height = $(window).height(); 
        var length_ratio = this.state.length_ratio;
// DDO A PRELIM ITERATION OVER FIRST THREADS TO REBUILD WITH STARTBUFFER
        $.each($first_threads,function(k,v){

            var id = $(v).data('task-id'),
            thread_length = 0,
            thread_height_ratio = .3;
            if( $(v).data('weight') ){
                thread_height_ratio = $(v).data('weight')/100;
            }
           $(v).css('height',vp_height * thread_height_ratio);
           var $task =  this.state.tasks[id];
           var test_thread_length = thread_length;
           if(!$task.parent_id){
            test_thread_length = thread_length;
           }
            if( (test_thread_length  ) > (greatest_thread_length )){
                greatest_thread_length = (test_thread_length  ) * length_ratio;
                $longest_thread = $(v);
            }
            $(v).data('days',thread_length);
        }.bind(this));
        $($longest_thread).width(100*length_ratio+'%');
        $('.children').each(function(k,v){
            var parent_height = $(v).siblings('.task').height();
            var children_number = $(v).children('.children .thread-container').length,
            height = 1/children_number * parent_height + 'px';
        });
        this.state.greatest_thread_length = greatest_thread_length;
        var calLength = this.state.greatest_thread_length/length_ratio;
       
        var longest_thread_width = $('.project-container').width() * length_ratio;
        var parent_width = 0;
        var parent_days = 0;

        var unit_length_px = longest_thread_width / greatest_thread_length * length_ratio;
        var $tasks = $('.task');
        var $app = this;
        var start_date = new Date();


         $('.thread-container').each(function(c_key, container){
            var task_id = $(container).data('task-id'),
                thread_lengths = $app.state.project_thread_lengths[task_id];
                var container_days = 0;
            $.each(thread_lengths, function(s,d){
                if(d.length > container_days){
                    container_days = d;
                }
            });          
            
            $(container).data('total-days',container_days);
            var children = $(container).children('.children'),
                targets = $(children).children('.thread-container, div'),
                percent = 100;
                if( targets.length){
                    percent = 1/targets.length * 100 ;
                }
                targets.css('height',percent + '%');
           // $(container).find('.task:first-child').each(function(k,v){
               // var children_width = $longest_thread.width() - $(v).width() ;
               // $(v).next('.children .thread-container').width(children_width);
           // })
        });
        $.each($tasks,function(k,v){

            var $id = $(v).data('id'),
                task = $app.state.tasks[$id],
                length = task.length,
                startBuffer = task.startBuffer,
                endBuffer = task.endBuffer,
                realLength = length + startBuffer + endBuffer;
            if($app.state.tasks[$id].startDate){
                var task_start_date = new Date($app.state.tasks[$id].startDate);

                var compare = dates.compare(task_start_date,start_date);
                if( compare < 0){
                    start_date = task_start_date;
                }
            }

 var day_filler_length = 1; //tie to some variable (start adte)
            $(v).siblings('.day-filler').css('width', day_filler_length * unit_length_px);
            if(parent_width < 1){
                parent_width = longest_thread_width;
            }
            else{

                if( $(v).parent('.thread-container').length > 0 ){
                    parent_width = $(v).closest('.thread-container').width();
                }
                else{
                    parent_width = $(v).parentsUntil('.children').width();
                }
            }
            if(parent_days < 1){
                parent_days = longest_thread_width;
            }
            else{
                parent_days = $(v).closest('.thread-container').data('total-days');
            }
// $(v) is every task
            var startBufferLength = $(v).siblings('.start-buffer').first().data('length');
            if(startBufferLength){
                $(v).siblings('.start-buffer').first().css('width', (startBufferLength * unit_length_px / parent_width) * 100 + '%');
            }
            else{
                $(v).siblings('.start-buffer').first().remove();
            }
            var endBufferLength = $(v).siblings('.end-buffer').first().data('length');
            if(endBufferLength){
                $(v).siblings('.end-buffer').first().css('width', (endBufferLength * unit_length_px / parent_width).toFixed(2) * 100 + '%');
            }
            else{
                $(v).siblings('.end-buffer').first().remove();
            }
            if(task.closedDate && task.startDate){
                var task_start_date = new Date(task.startDate);
                var task_closed_date = new Date(task.closedDate);

                var compare = dateDiffInDays(task_start_date,task_closed_date);

                task.length = compare + 1; // (+ 1 ) is for inlcusive reckoning
            }

           $(v).css('width', (task.length * unit_length_px / parent_width) * 100 + '%');

           $(v).next('.start-buffer').css('width', (startBuffer * unit_length_px / parent_width) * 100 + '%');
           $(v).next('.end-buffer').css('width', (endBuffer * unit_length_px / parent_width) * 100 + '%');
            var child_length_px = parent_width - (realLength * unit_length_px); 
            var thread_container_width = child_length_px/parent_width * 100 +"%"; 
            $(v).siblings('.children').width(thread_container_width);
            var number_of_threads = $(v).next('.children').children('.thread-container').length;
        });
        ReactDOM.render(<Calendar today={this.state.today} length={calLength} startDate={start_date} length_ratio={this.state.length_ratio}/>, document.getElementById('calendar-container'));
    

    }
}

class Task extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        var task = this.props.node;
//        console.log('task render:',this.props.node);
        this.state = {
            userLength : task.userLength,
            length: task.length,
            thread_length: task.thread_length,
            realLength : task.realLength
        }
    }
    handleClick(){
        this.props.onVote(this);
        var toggle = true;
        if( $('body').hasClass('task-open') && $('body').hasClass('task-edit') ){
            toggle = false;
        }
        $('.active-thread').removeClass('active-thread');
        if(toggle){
            $('body').addClass('task-open  task-edit');
            $target.parent().addClass('active-thread');
        }
        else{
            $('body').removeClass('task-open  task-edit');            
        }

        $('body').addClass('task-open  task-edit');
         $('#task-update-id').val(this.props.id);
    }
    render() {
        var extra = "";        
        var classes = "task " + this.props.classes;
        return(
            <div className={classes} data-target="#updateTask" onClick={this.handleClick} data-id={this.props.id} data-startBuffer={this.props.node.startBuffer} data-endBuffer={this.props.node.endBuffer} data-thread-length={this.props.thread_length} data-days={this.state.realLength} style={{ backgroundColor:'#63b9bc'}}><div className="task-description">{this.props.name} #{this.props.id} : <span className="length">(P: {this.state.userLength} ) r: {this.state.length}</span> days</div></div>
        )
    }
}

class TaskForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: ''};
    
 this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }
   handleClick(){
//alert('handleClick');
        this.props.onUpdate(this);
    }
  handleSubmit(event) {
    event.preventDefault();

//console.log('A name was submitted: ' + event);
        var id = this.props.id;
//alert(id);
 //   console.log('on submitted',this);
  }

  render() {
    return (
     <div>
       <input 
        type="hidden"
        id="task-update-id"

       />
       <input
        id="task-length"
        type="text"
        value={this.props.value}
        onChange={() => this.setState({value: event.target.value})}
      />
      <button onClick={this.handleClick}>save</button>
     </div>
    );
  }
}
class Day extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }
    handleClick(){
        console.log('button clicked as FORM UPDATE');
    }
    render() {
        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
          "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];

        var compare = dates.compare(this.props.today,this.props.date),
            className = "day showDate",
            month = "";
        if(!compare){
            className += " currentDay"
        }
        //this.props.date.getMonth() + 1
        var html = this.props.date.getDate();
        //console.log('this props:' , this.props);
        if(1 == this.props.date.getDate() || !this.props.iter ){
            month =  monthNames[this.props.date.getMonth()];
        }
        var day = this.props.date.getDay(); // 0 == sunday
        //console.log("datttttttttttttttttttttttttttte:" + this.props.date + "--" + this.props.today + ":compare:" + compare);
        return (
            <div className='day-container' style={this.props.style}>
                <div className="monthName">{month}</div>
                <div className={className} >{html}
                <span className="dayName">{this.props.date.toLocaleDateString( "en-US",{weekday:'short'})}</span>
                </div>
            </div>
        );
    }
}
class Month extends React.Component {
  constructor(props) {
    super(props);
    this.calRef = React.createRef();
  console.log('month props:',this.props);
    this.state = {
        length: this.props.length,
        length_ratio: this.props.length_ratio
    };
    this.handleClick = this.handleClick.bind(this);
  }
  componentWillReceiveProps(props) {
    this.setState({ length_ratio: props.length_ratio })
  }
  handleClick(){
        console.log('button clicked as FORM UPDATE');
    }
    componentDidUpdate() {
       // this.setState({length_ratio: newLength });
        var newLength = $('#task-length').val();
        if( newLength != this.state.length){
            this.setState({length: newLength });
        }
    }    
  render() {
    var lengthCount = 0;
    var rows = [];
    var length_ratio = this.state.length_ratio;
    var numrows = this.props.length;
    var width = 1/numrows * 100 ;
    const dayStyle = {
        width: width + '%',
      //  borderRight: '1px solid red',
        display: 'inline-block'
    };
console.log("MONTH:", this.props);
    var date = new Date(this.props.startDate);
    var isStart = 0;
    for (var i = 0; i < numrows; i++) {
        if( i == (numrows-1) ){

            isStart=1;
        }
        rows.push(<Day today={this.props.today} key={i} style={dayStyle} date={date} isStart={isStart} iter={i}/>);
        date = new Date( date.addDays(1));
    }
    rows.reverse();
    const monthStyle = {
        width: (100 * this.state.length_ratio)  + '%',
        display: 'inline-block'
    };
    return (
     <div className="month" ref={this.calRef} style={monthStyle}>{rows}
     </div>
    );
  }
}

class Calendar extends React.Component {
  constructor(props) {
    super(props);
    this.calRef = React.createRef();
  console.log('calendar props:',this.props);
    this.state = {

        length: this.props.length,
        length_ratio: this.props.length_ratio
    };
    this.handleClick = this.handleClick.bind(this);
  }
  componentWillReceiveProps(props) {
    this.setState({ length_ratio: props.length_ratio })
  }
  handleClick(){
        console.log('button clicked as FORM UPDATE');
    }
    componentDidUpdate() {
       // this.setState({length_ratio: newLength });
        var newLength = $('#task-length').val();
        if( newLength != this.state.length){
            this.setState({length: newLength });
           // console.log('this length is component updateq:' ,this);
        }
    }    
  render() {
    var lengthCount = 0;
var rows = [];
var length_ratio = this.state.length_ratio;
           // console.log('in calendar render:', this);
var numrows = this.props.length;

var width = 1/numrows * 100 ;
const calStyle = {
    width: width + '%',
  //  borderRight: '1px solid red',
    display: 'inline-block'
};
//get day one

var date = new Date(this.props.startDate);
var isStart = 0;
for (var i = 0; i < numrows; i++) {
    if( i == (numrows-1) ){

        isStart=1;
    }
    if(isStart){
    // note: we add a key prop here to allow react to uniquely identify each
    // element in this array. see: https://reactjs.org/docs/lists-and-keys.html
   // console.log(date);
        rows.push(<Month today={this.props.today} key={i} style={calStyle} date={date} isStart={isStart} iter={i} length={this.props.length} length_ratio={this.state.length_ratio}/>);
    }
    date = new Date( date.addDays(1));
}
rows.reverse();
const calendarStyle = {
    width: (100 * this.state.length_ratio)  + '%',
    display: 'inline-block'
};
    return (
     <div className="calendar" ref={this.calRef} style={calendarStyle}>{rows}
     </div>
    );
  }
}
Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}
Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};
var dates = {
    convert:function(d) {
        // Converts the date in d to a date-object. The input can be:
        //   a date object: returned without modification
        //  an array      : Interpreted as [year,month,day]. NOTE: month is 0-11.
        //   a number     : Interpreted as number of milliseconds
        //                  since 1 Jan 1970 (a timestamp) 
        //   a string     : Any format supported by the javascript engine, like
        //                  "YYYY/MM/DD", "MM/DD/YYYY", "Jan 31 2009" etc.
        //  an object     : Interpreted as an object with year, month and date
        //                  attributes.  **NOTE** month is 0-11.
        return (
            d.constructor === Date ? d :
            d.constructor === Array ? new Date(d[0],d[1],d[2]) :
            d.constructor === Number ? new Date(d) :
            d.constructor === String ? new Date(d) :
            typeof d === "object" ? new Date(d.year,d.month,d.date) :
            NaN
        );
    },
    compare:function(a,b) {
        // Compare two dates (could be of any type supported by the convert
        // function above) and returns:
        //  -1 : if a < b
        //   0 : if a = b
        //   1 : if a > b
        // NaN : if a or b is an illegal date
        // NOTE: The code inside isFinite does an assignment (=).
        return (
            isFinite(a=this.convert(a).valueOf()) &&
            isFinite(b=this.convert(b).valueOf()) ?
            (a>b)-(a<b) :
            NaN
        );
    },
    inRange:function(d,start,end) {
        // Checks if date in d is between dates in start and end.
        // Returns a boolean or NaN:
        //    true  : if d is between start and end (inclusive)
        //    false : if d is before start or after end
        //    NaN   : if one or more of the dates is illegal.
        // NOTE: The code inside isFinite does an assignment (=).
       return (
            isFinite(d=this.convert(d).valueOf()) &&
            isFinite(start=this.convert(start).valueOf()) &&
            isFinite(end=this.convert(end).valueOf()) ?
            start <= d && d <= end :
            NaN
        );
    }
}
function isFunction(functionToCheck) {
 return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}
const _MS_PER_DAY = 1000 * 60 * 60 * 24;

// a and b are javascript Date objects
function dateDiffInDays(a, b) {
  // Discard the time and time-zone information.
  if(a.getMonth && b.getMonth){
      const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

     return Math.floor((utc2 - utc1) / _MS_PER_DAY);
    }
    else{
        console.log('dateDiffInDays one is not a Date object');
    }
}

