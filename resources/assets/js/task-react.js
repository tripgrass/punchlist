import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {attachDragRange, makeNewThread,canSaveTask,closeSidebar, manageMoveTaskToggle, dragRange, moveRange, newTaskRangeInput, setBubble, toggleTaskGroupOwner, isTaskAssigned,canUserEditTask,isUserTaskOwner,isUserProjectOwner, manageTaskMove,manageUserAssign,manageGroupAssign,manageOwnerAssign,testMessage,toggleEditMode,openEditTask,openSettings, saveProject,setCardState,getCardState, updateTaskCard, openEndDate, openStartDate, closeEndDate, closeStartDate} from './utilities.js';


Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};
if ( document.getElementById('project-container')) {
    window.appVars = {};
//    test('test in react.js');

    var project_id = document.getElementById('project-container').dataset.project_id,
        activeTaskId = getParameterByName('task', window.location.search),
        tasks = {},
        proposedStartDate = '',
        partners = {},
        owner = {},
        breaks = [],
        taskTree = {},
        project_thread_lengths = {},
        currentUser = {},
        earliestStart = {};
    function ajaxCalls(api_token){
        if( api_token ){
            return [
                $.ajax({
                    url : '/api/userByToken',
                    type : 'POST',
                    data : {
                        'api_token' : api_token
                    },
                    dataType:'json',
                    success : function(data) {
                        console.log('getbytoken',data)
                        window.appVars.currentUser = currentUser = new User(data);
                    },
                    error : function(request,error){
                        currentUser = 0;
    //                    console.log("getbytoken;Request: "+JSON.stringify(request));
                    }
                }),
                $.ajax({
                    url : '/api/breaks?api_token=' + api_token,
                    type : 'POST',
                    data : {
                        'ownerType' : 'user',
                        'owner' : 'auth',
                        'types' : [
                        ]
                    },
                    dataType:'json',
                    success : function(data) {
                        for(let i = 0; i < data.length; i++){
                            var thisStartDate = new Date(data[i].startDate),
                                counter = getUnix(thisStartDate.setHours(0,0,0,0)),
                                thisEndDate = new Date(data[i].endDate),
                                endCounter = getUnix(thisEndDate.setHours(0,0,0,0));
                            breaks[ getUnix(thisStartDate.setHours(0,0,0,0) )] = {
                                date: thisStartDate,
                                id: data[i].id,
                                group_id: data[i].group_id,
                                user_id: data[i].user_id,
                                project_id: data[i].project_id
                            };
                            while( counter <= endCounter ){
                                counter += 86400;
                                var newDate = new Date(counter *1000 );
                                breaks[ getUnix(newDate.setHours(0,0,0,0) )] = {
                                    date: newDate,
                                    id: data[i].id,
                                    group_id: data[i].group_id,
                                    user_id: data[i].user_id,
                                    project_id: data[i].project_id
                                }
                            }
                        }
                    }
                })
            ];
        }
    }
    function buildApp(){

        
        var api_token = $("meta[name='api_token']").attr("content"); 
        //console.log('api_token',api_token);
        $.when.apply($,ajaxCalls(api_token)).then(function(){
            axios
                .get('/api/taskTree/' + project_id + '?api_token=' + api_token)
                .then(response => {
                    if(!window.appVars.currentUser){
                        window.appVars.currentUser =  currentUser = new User();
                    }
                    var startTime = performance.now()
                   // console.log(`START::::::::::::::::::; axios`);
                    if( !response.data.error ){
                        console.log('responseeeeeeeeeeeeeeee', response.data);
                        tasks = response.data.tasks;
                        taskTree = response.data.branch;
                        partners = response.data.project.partners;
                        owner = response.data.project.owner;
                        proposedStartDate = response.data.proposedStartDate;
                        project_thread_lengths = response.data.project_thread_lengths;
                        earliestStart = response.data.earliestStart;
                        window.appVars.project = response.data.project;
                        if(!earliestStart){
                            earliestStart = {};
                            earliestStart.date = "";
                            $('#setPivotLabel').show();
                        }
                        else{
//                            $('#setPivotLabel').hide();                        
                        }
                        var showProposedSetDate = 1,
                            newTaskId = null;
                        $('#app').removeClass('new_task');
                        $.each(tasks,function(k,task){
                            if(task.closedDate){
                                task.closedDate = new Date(task.closedDate);
                            }
                            if(task.startDate){
                                task.startDate = new Date(task.startDate);
                                showProposedSetDate = 0;
                            }
                            if( 'new_task' == task.type ){
                                newTaskId = task.id;
                                $('#app').addClass('new_task');
                            }
                        })
                        if(showProposedSetDate){
//                            $('#setPivotLabel').css('display', 'inline-block');
                            $('#setPivotLabel').show();
                        }
                        else{
                            $('#setPivotLabel').hide();
                        }

                        ReactDOM.render(<App />, document.getElementById('project-container'));
                        $('#project-container').fadeIn();
                        $( "#task-close-date" ).not('.datepickerBuilt').datepicker({
                            closeText: "Cancel",
                            dateFormat: "M d yy"
                        });
                        $( "#task-update-start-date" ).not('.datepickerBuilt').datepicker({
                            showButtonPanel: true,
                            closeText: "Cancel",
                            dateFormat: "M d yy",
                            clearBtn: true,
                            beforeShow: function(el,inst){
                                $(el).addClass('pickerOpen');
                            },
                            onClose: function(el,inst){
                                $(el).removeClass('pickerOpen');
                            }
                        });
                        $( "#task-update-real-start" ).not('.datepickerBuilt').datepicker({
                            closeText: "Cancel",
                            dateFormat: "M d yy"
                        });
                        $('.new-task-section.after').on('click', function(e){
                            var task_id = $(e.target).closest('.task').data('id'),
                                project_id = tasks[task_id].project_id;
                            addTask(project_id,task_id);
                        })  
                    }
                    else{
                        console.log('%c ' + response.data.errorMessage, 'color: red', response.data);
                    }
                    if( $('#app').hasClass('new_task') ){
                        var newTask = tasks[newTaskId];
                        toggleSidebar(newTask);
                    }
                    else{
                        if(activeTaskId && tasks[activeTaskId]){
                            var activeTask = tasks[activeTaskId];
                            toggleSidebar(activeTask);
                        }
                    }
                    $('#userSelect').change(function(){
                        var user_id = parseInt( $('#userSelect').find(":selected").val() );
                        $('.task').removeClass('activeUserFilter');
                        if(!user_id){
                            $('body').removeClass('filterByUser');
                        }
                        else{
                            $('body').addClass('filterByUser');
                        }
                        $.each( $('.task'), function(k,v){
                            var jsonUsers = $(v).data('users');
                            if( $.inArray(user_id,jsonUsers) != -1 ){
                                $(v).addClass('activeUserFilter');
                                console.log('this is in the user select iterate over tasks' + user_id + "|" + $.inArray(user_id,jsonUsers), jsonUsers);
                            }
                        } );
                    })
                    // THIS IS SO HACKY - AND IM NOT SURE WHY IT WORKS::::
                    $('.react-div-wrapper').css('display','inline');
                    var tourState = getParameterByName('tourState', window.location.search);
                    if(tourState){
                        tutorial.start(tourState);
                    }
                    $('#scrollLeft').on('click',function(){
                        scrollCalendarLeft();
                    })
                    $('#assignTask-toggle').on('click', function(){
                        if( $('.assign-task').hasClass('closed') ){
                            $('.assign-task').removeClass('closed');
                            $('.assign-task').find('.arrow').removeClass('down').addClass('up');
                            $('.assign-task').find('.label').html('Hide');

                        }
                        else{
                            $('.assign-task').addClass('closed');                        
                            $('.assign-task').find('.arrow').removeClass('up').addClass('down');
                            $('.assign-task').find('.label').html('Assign Task');
                        }
                    });
                    var userFilterMain = "";  

                    userFilterMain += "<div class='user-filter-groups-wrapper'><ul class='user-filter-group color-" + owner.id + "' data-group-id='" + owner.id + "'>";            
                    userFilterMain += "<li class='filter-group'><div class='group-label user-label' style='display:inline-block;'>Owner: " + owner.name + "</div><ul class='sub-group' style=''>";
                    //alert('line 236 check if owner is currentuser - only show users with a task assigned to them' );
                       // console.log('PROJECT USER:::::::::' , window.appVars.project_users );

                    Object.keys(owner.users).map(function(user_key) { 
                        Object.keys(window.appVars.project_users).map(function(project_user) {
                        })
                        var user = owner.users[user_key];
                       // userFilterMain += "<li class='filter-user color-" + user.id + "' data-user-id='" + user.id + "'><div class='user-label'>" + user.name + "</div></li>";
                   })
                    userFilterMain += "</ul></li></ul>";
                   // console.log('partners ......', partners);                
                    Object.keys(partners).map(function(partner_key) {
                        var partner = partners[partner_key];
                        var partnerClass = "";
                        if(partner.users.length <= 1){
                            partnerClass = " solo";
                        }
                        userFilterMain += "<ul class='user-filter-group color-" + partner.id + partnerClass + "' data-group-id='" + partner.id + "'>";            
                        userFilterMain += "<li class='filter-group'><div class='group-label user-label' style=''>" + partner.name + "</div><ul class='sub-group' style=''>";
                        if(partner.account.name != "Individual Trial" && (partner.users && partner.users.length > 0 )){
                           // console.log('line 252 check if currentuser is partner - sub user inclusion of members for partners is commented out for now');
                            Object.keys(partner.users).map(function(user_key) {
                                var user = partner.users[user_key];  

                                //userFilterMain += "<li class='filter-user color-" + user.id + "' data-user-id='" + user.id + "'><div class='user-label'>" + user.name + "</div></li>";
                           })
                        }
                        userFilterMain += "</ul></li></ul></div>";
                    }, this)
                    $('#userFilterMain #user-filters').html(userFilterMain);
                    toggleTaskGroupOwner();
                    $('.filter-group .group-label').on('click',function(e){
                        toggleTaskGroupOwner(e);
                    });
                    $('.filter-user').on('click',function(e){
                        $('body').removeClass (function (index, className) {
                            return (className.match (/user-\S+/g) || []).join(' ');
                        });
                        $('body').removeClass (function (index, className) {
                            return (className.match (/group-\S+/g) || []).join(' ');
                        });
                        $('.user-filter-group').removeClass('active');
                        $('.filter-user').removeClass('active');

                        if(!$(event.target).hasClass('filter-user')){
                            var $this = $(event.target).parent();
                        }
                        else{
                            var $this = $(event.target);
                        }
                        //console.log('clinck on target',$this);

                        var userId = $this.data('user-id');
                        if( $this.hasClass('active') ){
                            $('body').removeClass('user-' + userId);
                            $this.removeClass( 'active' );
                        }
                        else{
                            $('body').addClass('user-' + userId);
                            $this.addClass( 'active' );
                        }
                        if( $('.filter-user.active').length ){
                            $('body').addClass('active-user-filter');
                        }
                        else{
                            $('body').removeClass('active-user-filter');        
                        }

                    });
                   // var allPanels = $('#all-filters').slideUp();     
                    $('.filter-toggle .acc-tog').click(function() {
                        if ( !$(this).closest('#filter-wrapper').hasClass('active') ) {
                            allPanels.slideUp();
                            $('#all-filters').slideDown();
                            $('#all-filters').addClass('js-open');
                            $(this).closest('#filter-wrapper').addClass('active');
                            $('.acc-label').html('Hide Filters');
                        }
                        else {
                            $(this).closest('#filter-wrapper').removeClass('active');
                            $('#all-filters').removeClass('js-open').slideUp();
                            $('body').removeClass (function (index, className) {
                                return (className.match (/user-\S+/g) || []).join(' ');
                            });
                            $('body').removeClass (function (index, className) {
                                return (className.match (/group-\S+/g) || []).join(' ');
                            });
                            $('#all-filters').removeClass('active');
                            $('.filter-user').removeClass('active');
                            $('.acc-label').html('Show Filters');

                        }

                        return false;
                    });                    


                    $('.taskDay.blocked').on('click',function(e){
                        if( $('.filter-blocked-days').length > 0 ){
                            e.preventDefault();
                        }
                    });
                    $('.thread-container').on('click',function(e){
                        if( $('body' ).hasClass('add-new-task') ){
                            var target = $(e.target);
    console.log('threadContainer------>',target[0]);
                        }
                    });
                    $('.day-container').on('click',function(e){
                        var target = $(e.target).closest('.day-container');
                        var threadContainer = $(e.target).closest('.thread-container');
                        var unix = target.data('unix');
                        var block = breaks[unix];
                        var remove = false;
                        if( $('body' ).hasClass('add-new-task') ){
var date = new Date(unix * 1000); 
var newDate = date.toLocaleString('en-US', { timeZone: 'UTC' });                           
alert('clicked daycontainer' + newDate);
console.log('threadContainer', target[0]);
                        }
                        else{
                            if( $('.taskDay[data-unix="' + unix + '"]').hasClass('activeCalendarClick') ){
                                remove = true;
                            };
                            $('.taskDay.activeCalendarClick').removeClass('activeCalendarClick').removeClass('activeCalendarDblClick');
                            if(remove){
                                $('.taskDay[data-unix="' + unix + '"]').removeClass('activeCalendarClick');
                            }
                            else{
                                $('.taskDay[data-unix="' + unix + '"]').addClass('activeCalendarClick');                        
                            }
                        }
                    })
                    $('.day-container').on('dblclick',function(e){
                        var target = $(e.target).closest('.day-container');
                        var unix = target.data('unix');
                        var block = breaks[unix];
                       // console.log('unix' + unix, block);
                        $('.taskDay.activeCalendarClick').removeClass('activeCalendarClick').removeClass('activeCalendarDblClick');
                        $('.taskDay[data-unix="' + unix + '"]').addClass('activeCalendarDblClick');
                    })
                    $('.thread-container').click(function (e) {
                        if( $('body').hasClass('adding-task') ){
                            var $target = $(e.target);
                            if($target.hasClass('thread-container')){
                                 addTaskModal($target, e);
                            }
                        }
                    })
                    $('.task').click(function (e) {
                        if( $('body').hasClass('adding-task') ){
                            var $target = $(e.target).closest('.task');
                            if($target.hasClass('task')){
                                 addTaskModal($target,e);
                            }
                        }
                    });
                    $('.task').on({
                        mouseenter: function (e) {
                            var task = $(e.currentTarget),
                                endDate = task.data('enddate'),
                                endUnix =    new Date(endDate).getTime(),
                                startDate = task.data('startdate'),
                                startUnix =    new Date(startDate).getTime();
                            $('.day-container[data-unix="' + getUnix(endUnix) + '"]').addClass('task-end');
                            $('.day-container[data-unix="' + getUnix(startUnix) + '"]').addClass('task-start');
                        },
                        mouseleave: function (e) {
                            var task = $(e.currentTarget),
                                endDate = task.data('enddate'),
                                endUnix =    new Date(endDate).getTime(),
                                startDate = task.data('startdate'),
                                startUnix =    new Date(startDate).getTime();
                            $('.day-container[data-unix="' + getUnix(endUnix) + '"]').removeClass('task-end');
                            $('.day-container[data-unix="' + getUnix(startUnix) + '"]').removeClass('task-start');                            
                        }
                    })
                    $('#filter-blocked-days').on('click',function(){
                        if( $('#filter-blocked-days').hasClass('active' ) ){
                            $('#filter-blocked-days').removeClass('active' );
                            $('#calendar-wrapper').removeClass('filter-blocked-days');
                        }
                        else{
                            $('#filter-blocked-days').addClass('active' );                        
                            $('#calendar-wrapper').addClass('filter-blocked-days');
                        }
                    })
                    if($('body').hasClass('task-open')){
                        var activeId = $('body').data('active-task');
                        $('.task[data-id="' + activeId + '"]').addClass('activeSidebarTask');

                    }
                    var buttonWidth = $('#new-task').outerWidth();
                    $('#new-task').css('width',buttonWidth).data('button-width', buttonWidth);
                    
                    /*
let circle = document.getElementById('newTaskRange');
//let circle = document.getElementById('ball');
console.log('newtaskrange', newTaskRange)
                        const onMouseMove = (e) =>{
                          circle.style.left = (e.pageX + 10) + 'px';
                          circle.style.top = (e.pageY + 10) + 'px';
                          console.log('on the move', circle);
                        }
*/
                    document.addEventListener('mousemove', attachDragRange);
                    $('#new-task').on('click',function(e){      
                        if( 'undefined' == typeof my_dragging.offset0 ){
                            my_dragging.offset0 = $('#newTaskRange').offset();        
                            my_dragging.elem = $('.slidecontainer');
                        }
                        if( $('body').hasClass('add-new-task')){
//                            document.removeEventListener('mousemove', dragRange);

                            //document.removeEventListener('mousemove', onMouseMove);
                        }
                        else{
                            $('#new-task').css('width','0');
  //                          document.addEventListener('mousemove', dragRange);
                            //document.addEventListener('mousemove', onMouseMove);
                        } 
                        $('body').toggleClass('add-new-task');
                        //var relX = e.pageX - $(this).offset().left;
                        var relY =  $('#new-task').offset().top - $('#newTaskRange').offset().top;
                        $('.slidecontainer').css('top', relY + "px" );
                        // 1. get y position and move slide to y position
                        // 2. get x position and move thumb to x position
                        $('body').toggleClass('adding-new-task');
                        // sort of working not really
                    });
                    $('#add-task').on('click',function(){
                      //  console.log('tasktree',taskTree);
    for (const [key, value] of Object.entries(taskTree)) {
      //  console.log(typeof key);
            var task_id = parseInt(key),
                project_id = tasks[task_id].project_id;
            addTask(project_id,null,task_id);
     // console.log(`${key}: ${value}`);
    }                    
                            //var project_id = tasks[task_id].project_id;
                            //addTask(project_id,task_id);
                    })
                        $('.task-has-started').on('click',function(e){
var $target = $(e.currentTarget),
    taskId = $target.find('input[name="task-parent"]').val(),
    task = tasks[taskId];
$('.task-move-error .fade-out-html').html( task.name + ' is already closed.').fadeIn();
                        });
                        $('select[name="user-assign"]').on('change',function(e){
                            var selectedUserId = parseInt( $(e.currentTarget).val() );
                            if( !selectedUserId ){
                                $("#updateTask .dialog-wrapper, .activeSidebarTask").removeClass(function (index, className) {
                                    return (className.match (/(^|\s)user-\S+/g) || []).join(' ');
                                });
                            }
                            else{
                                $("#updateTask .dialog-wrapper, .activeSidebarTask").addClass('user-' +  selectedUserId);
                            }
                        })
                        $('select[name="group-assign"]').on('change',function(e){

                            var task_id = $('.activeSidebarTask').data('id'),
                                task = window.appVars.tasks[task_id];
                                    manageOwnerAssign( task , e);
                            
                        });
                        $('input[name="task-parent"]').not('.task-has-started').on('change',function(e){
                            $('.task').removeClass('new-parent');
                            $('.task-move-error .fade-out-html').fadeOut();
                            $('.make-new-thread').prop('checked', false);
                            var taskId = $(e.target).val(),
                                $targetTask = $('.task[data-id="' + taskId + '"]');
                                //console.log("TASSK", $targetTask[0]);
                                // get width of active

                                var activeTaskElement = $('.activeSidebarTask'),
                                    elWidth = activeTaskElement.width(),
                                    activeTaskElementID = $('.activeSidebarTask').data('id'),
                                    activeTask = tasks[activeTaskElementID];
                                $targetTask.addClass('new-parent');
                                var newLocationText = `New Location for ` + activeTask.name + `...`;
                                if( null == activeTask.name ){
                                    newLocationText = `New Task Location`;
                                }
                                $('#new-task-style').html(`
                                    <style>
                                        .new-parent:before{ 
                                            height:100%;
                                        }
                                        .new-parent:after{
                                            content: '` + newLocationText + `';
                                            height:100%;
                                        }
                                    </style>`
                                );
                                activeTaskElement.addClass('moving-inactive');
                                $('#threads-wrapper .children').each(function(k,v){
                                    var origHeight = $(v).data('original-height');
                                    //console.log('origHeight',origHeight);
                                    $(v).height(origHeight );
                                })

                        });                    
                    $('#VOIDadd-task').on('click',function(){
                        var thisTask = {};
                        thisTask.new = true;

                        toggleSidebar(thisTask);
                        if( $('body').hasClass('adding-task') ){
                            $('body').removeClass('adding-task');
                            $('#add-task').removeClass('active').html('Add Task');
                            $('#sidebar-add-task').hide();
                            $('#sidebar-update-task').show();
                        }
                        else{
    //                    $('#sidebar-update-task').hide();
                            $('#sidebar-update-task').show();
    //                    $('#sidebar-add-task').show();
                            $('#sidebar-add-task').hide();
                            $('body').addClass('adding-task').addClass('edit-task');
                            $('#add-task').addClass('active').html('Adding Task...');
                            $('#form-task-title').prop("disabled", false).addClass('edit-input');
                            $('#app').addClass('task-edit');
                           // $('#addTaskModal').modal({
                             //   'show':true,
                               // 'backdrop':false
                            //});
                        }
                    })
                    $('#addTaskModal').on('hidden.bs.modal', function (e) {
                        $('body').removeClass('adding-task').removeClass('adding-task-hover');
                        $('#add-task').removeClass('active').html('Add Task');
                        $('#add-task-message').html("<h4 >Click where you'd like to add a Task</h4>");
                    })
                    $.each( $('.task'), function(k,v){
                        var taskLength = $(v).outerWidth(true),
                            $descr = $(v).find('.task-description'),
                            descriptionLength = $descr.outerWidth(true);

    //console.log( 'taskLength',taskLength );                        
    //console.log( 'descr', $descr );                        
    //console.log( 'descriptionLength', descriptionLength );                        
                        if(taskLength > descriptionLength){
                            $descr.find('.description-text').css('visibility','visible').show();
                            $(v).removeClass('short-width');
                        }
                        else{
                            $(v).addClass('short-width');
                            $descr.find('.description-text').attr('visibility','hidden').hide();
                        }

                    } )
                    $('.task-description-inner').hover(
                        function(e){
                           // console.log('hover on task descr');
                            var $task = $(e.target).closest('.task');
                            $('.task').not($task).addClass('fadeOut');
                            $task.removeClass('fadeOut');
    //                        $(e.target).addClass('fadeOut');

                        },
                        function(e){
                            var $task = $(e.target).closest('.task');
                            $('.task').removeClass('fadeOut');
                        }                    
                    );
                    if( !$('#app').hasClass('timeline-built') ){
                        $('#app').addClass('timeline-built');
                        $('.make-new-thread').on('click',function(e){
                            makeNewThread(e);
                        })
                    }
                    var assign_wrapper = $('#move-task-wrapper').detach();
                    assign_wrapper.insertAfter( $('#sidebar-update-task .modal-close') );
        var endTime = performance.now()
    //console.log(`Call to axios end took ${endTime - startTime} milliseconds`)
                    $('.assign-parent-task-wrapper').on('click',function(e){
                        //console.log('$(e.currentTarget)',$(e.target));
                        if( $(e.target).hasClass('assign-parent-task-toggle') ){
                            manageMoveTaskToggle();
                            
                        }
                    })
                    $('#saveNewPosition').on('click',function(){
                        alert(1);
                    })
                });
            });

        // finish buildApp
    }
var startTime = performance.now()
//console.log(`START::::::::::::::::::; INITIAL PERFORMANCE`);

    buildApp();

    window.my_dragging = {};
    my_dragging.pageX0 = ( $('#new-task').offset().left + $('#new-task').outerWidth() + 10);
    my_dragging.pageY0 = ( $('#new-task').offset().top + 10 );
console.log('my_dragging', my_dragging);
    var endTime = performance.now()
console.log(`Call to buildall took ${endTime - startTime} milliseconds`)
    $("body").on('DOMSubtreeModified', "#updateListener", function() {
        var height = $('#project-container').height();
        $('#project-container').height(height);
        $('#project-container').fadeOut(300, function(){
            ReactDOM.unmountComponentAtNode(document.getElementById('project-container'));        
var startTime = performance.now()
//console.log(`2ND START::::::::::::::::::; INITIAL PERFORMANCE`);
            buildApp();
    var endTime = performance.now()
console.log(`Call to buildall took ${endTime - startTime} milliseconds`)
        });
    });
}
class User {
    constructor(user) {
        if( typeof user === 'object' ){
            Object.keys(user).map(function(key) {
                this[key] = user[key];
            },this)
            for(let i=0; i < this.teams.length; i++){
                if(this.current_group == this.teams[i].id){
                    this.currentGroupObj = this.teams[i];
                }
            }        
        }
        else{
            this.user_type = 'guest';
        }        
        console.log('usr:::::::::', this);
    }
    isGroupAdmin(group_id){
        if(!group_id){
            var testGroup = this.currentGroupObj;
        }
        else{
            for(let i=0; i < this.teams.length; i++){
                if(group_id == this.teams[i].id){
                    var testGroup = this.teams[i];
                }
            }        
        }
        // get current group:
        if(testGroup.admin_id == this.id){
            return true;
        }
        else{
            return false;
        }
    }
    isProjectGroupAdmin(project_id,group_id){
        if(!group_id){
            thisGroup = this.currentGroupObj;
        }
        else{
            // get current group:
            for(let i=0; i < this.teams.length; i++){
                if(group_id == this.teams[i].id){
                    var thisGroup = this.teams[i];
                }
            }
        }
        if( owner.id == thisGroup.id){
            if(this.isGroupAdmin(thisGroup.id) ){
                return true;
            }
        }
        
        return false;
    }
    canEditTask(task){
        if( 'guest' == this.user_type ){
            return false;
        }
        else{
  //          console.log('USER eval canuseredit', this);
            if(task.users && task.users.length > 0){
                if( this.isProjectGroupAdmin(task.project_id) ){
                    return true;
                }
                 for(let i=0; i < task.users.length; i++){
                    if(this.id == task.users[i].id){
                        return true;
                    }
                }
            }
            else{
                if( this.isProjectGroupAdmin(task.project_id) ){
                    return true;
                }
            }
            if(task.groups && task.groups.length > 0){
//            console.log('task groups eval canuseredit', task.groups);

                 for(let i=0; i < task.groups.length; i++){
                    if(this.current_group == task.groups[i].id){
                        return true;
                    }
                }
            }
            // by this point, we know the task is NOT assigned. check if user belongs to project owner
            // this treats the whole team as admin
            if( this.current_group == window.appVars.owner.account.group_id ){
                return true;
            }
        }
        return false;
    }
}
export default class App extends Component {
    // need to test::
    // 1. variation on paralell ends (two trees)
    constructor(props) {
        super(props);
        var today = new Date();
        today.setHours(0,0,0,0);
        var groups =  {};
        Object.assign(groups, partners);
        groups[owner.id] = owner;
        this.state = {
            project_id : document.getElementById('project-container').dataset.project_id,
            tasks: tasks,
            taskTree: taskTree,
            breaks:breaks,
            project_thread_lengths: {},
            greatest_thread_length: 0,
            length_ratio : 1,
            lastChildren : [],
            first_task : {},
            threadStarts : {},
            estStartDate : earliestStart.date,
            latestEnd : earliestStart.date, // this is calculated below
            timezone : earliestStart.timezone,
            targetDate : today,
            tourButtons : {1:0},
            users:{},
            owner:owner,
            partners:partners,
            groups: groups,
            currentUser: currentUser
        };
        console.log('APPP',this);
        this.fn = this.fn.bind(this)
        this.calendarRef = React.createRef();
        this.zoomIn = this.zoomIn.bind(this);
        this.zoomOut = this.zoomOut.bind(this);
        //this.setPivot = this.setPivot.bind(this);
        this.handleEvent = this.handleEvent.bind(this);
        this.handleUpdateEvent = this.handleUpdateEvent.bind(this);
        this.handleTaskChange = this.handleTaskChange.bind(this);
        this.ancestors = [];
        this.threads = {};
        this.openDays = [];
        this.state.scheduleStr = {};
        this.state.scheduledParentTasks = {};
        this.state.tasksInScheduleString = {};
        this.state.scheduleStringChildren = {};
        this.state.schedulePhaseCount = 1;

     //   this.project_start = "";
//console.log('need to buildout lengths including blocked days of noncontrolling threads');
// will need to get parent realStartDate
        this.buildAll();
    }
    buildAll(){

        this.getUsers();
        this.buildLastChildren();
        this.setStartDate();
        this.setTargetDate();
        // delete all refs to project_start??????????????????????????????????
        this.setEndDefiningThread();
/*
         setTimeout(function() {
            
            if( !$(".loading-container").hasClass('hidden') && !$('#app').hasClass('timeline-built') ){
                alert(2);
                location.reload();
            }
        }, 3000);
*/
        this.buildTaskTree();
        this.updateProjectMeta();
        this.buildSchedule();
        this.buildGenericDaysArray();
        $('.container').removeClass('loading');
        

        window.appVars.openDays = this.openDays;
        window.appVars.projectLength = this.state.greatest_thread_length;
        console.log('        window.appVars',        window.appVars);
    }
    updateProjectMeta(){
        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
          "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];

        var startDate = this.state.projectStartDate;
        var startString = " " + monthNames[startDate.getUTCMonth()] + " " + startDate.getUTCDate() + ", " + startDate.getUTCFullYear();
        var endDate = this.state.latestEnd;
        var endString = " " + monthNames[endDate.getUTCMonth()] + " " + endDate.getUTCDate() + ", " + endDate.getUTCFullYear();

        $('#project-start-date .value').html(startString);
        $('#project-end-date .value').html(endString);
    }
    getUsers(){
        var tasks = this.state.tasks;
        const keys = Object.keys(tasks)
        for (const key of keys) {
            if('undefined' != typeof tasks[key].users && tasks[key].users.length > 0){
                var taskUsers = tasks[key].users;
                taskUsers.forEach(function(user) {
                    this.state.users[user.id] = user;
                }, this);

            }
        }
        // does this include owner?
        window.appVars.project_users = this.state.users;        
    }
    buildLastChildren(){
        this.last_children = [];
        $.each(this.state.taskTree, function(id,v){
           this.getLastChildren(v);
        }.bind(this));
        window.appVars.last_children = this.last_children;
    }
    setStartDate(){
        var earliestTaskStartDate = this.getEarliestTaskStartDate();
        if(!earliestTaskStartDate){
            if(proposedStartDate){
                var definedStartDate = new Date(proposedStartDate);
            }
            else{
                var definedStartDate = $('#setPivot').val();
            }
            if(!definedStartDate){
                var definedStartDate = new Date();
            }
            else{
                var definedStartDate = new Date(definedStartDate);
            }
            definedStartDate.setHours(0,0,0,0);
            this.state.projectStartDate = definedStartDate;
        }
        else{
            this.state.projectStartDate = earliestTaskStartDate;
        }     

    }       
    getEarliestTaskStartDate(){
        // this assumes the thread start date has already been validated when it was input. start date
        // must be targetDate or earlier when it is entered.
        var earliestTaskStartDate = null;
        if(this.last_children && Object.size(this.last_children) > 0 ){
            Object.keys(this.last_children).map(function(i) {
                var taskId = this.last_children[i],
                    task = this.state.tasks[taskId],
                    taskStartDate = task.startDate;
                if(taskStartDate){
                    if(!earliestTaskStartDate){
                        earliestTaskStartDate = taskStartDate;
                    }
                    else{
                        if(taskStartDate < earliestTaskStartDate){
                            earliestTaskStartDate = taskStartDate;
                        }
                    }
                }
            }, this);
        }
        if(earliestTaskStartDate){
            return new Date(earliestTaskStartDate);
        }
    }
    setTargetDate(){ // targetDate is a testCurrentDay
        var definedTargetDate = $('#setPivot').val();
        if(!definedTargetDate){
            definedTargetDate = new Date();
        }
        else{
            definedTargetDate = new Date(definedTargetDate);
        }
        definedTargetDate.setHours(0,0,0,0);
        this.state.targetDate = definedTargetDate;
        var isValidTargetDate = 0;
        this.nonBlockedTargetDate = this.state.targetDate;
        while(!isValidTargetDate){
            if(this.isDayBlocked(this.nonBlockedTargetDate)){
                this.nonBlockedTargetDate = new Date( this.nonBlockedTargetDate.addDays(1) );
            }
            else{
                isValidTargetDate = 1;
            }
        }
    }
    setEndDefiningThread(setDates){
        // 1. get all last children and work up through thread (using blocked dates). if thread has a start date
        // use that, it is considered valid at time of entry. otherwise use this.project_start_date
        this.testThreadLengths = {},
            this.testThreadLengths.lengths = {};
            this.testThreadLengths.endDates = {};
        $.each(this.last_children, function(k,id){
            var task = this.state.tasks[id],
                thisTask = {};
                this.testThreadLengths.lengths[id] = 0,
                this.testThreadLengths.endDates[id] = 0;
            if(!task.startDate){
                // testStartDate is declared here first. It is for the enddefiningthreadtest
                var thisTaskStartDate = new Date(this.state.projectStartDate);
            }
            else{
                var thisTaskStartDate = new Date(task.startDate);
            }

            this.buildDatesUpThread(id,thisTaskStartDate,id, setDates);
        }.bind(this));
        this.latestThread = {};
        this.latestThread.id = "";
        this.latestThread.date = "";
        $.each(this.testThreadLengths.endDates, function(id,date){
            if(date > this.latestThread.date){
                this.latestThread.date = date;
                this.latestThread.id = id;
            }
        }.bind(this));
    }    
    scheduleUpThread( task ){
//        console.log('SHECDULE UP THREAD::: ' + task.id);
        var addToThread = true,
            count = 1,
            parent = null;
        if( task.parent_id ){
            if( !(task.parent_id in this.state.scheduledParentTasks) ){
                this.state.scheduledParentTasks[task.parent_id] = [];
            }
            var parent = tasks[ task.parent_id ];
            if( !(task.id in this.state.scheduledParentTasks[ parent.id ]) ){
                this.state.scheduledParentTasks[parent.id][ task.id ] = {};
            }
            // if parent has multiple children check if all have been accounted before moving  up the thread
            var traceUpThread = false;
            count = Object.keys( parent.children ).length;
            if( count > 1 ){
                if( !this.state.scheduleStringChildren.hasOwnProperty( parent.id ) ){
                    this.state.scheduleStringChildren[parent.id] = {};
                }
                this.state.scheduleStringChildren[parent.id][task.id] = task;
                traceUpThread = true;

                Object.keys(parent.children).forEach( function( child_id ) {
                    //console.log( 'this.state.scheduledParentTasks current PARENT' , parent.id );
                    //console.log( 'CHECK CHILDREN - CHILD::::' + child_id , parent.children[child_id] );
                    if( !( child_id in this.state.scheduledParentTasks[ parent.id ]) ){
                        traceUpThread = false;
                    }
                }, this);
            }
            else{
                traceUpThread = true;
            }
            if( !(task.id in this.state.tasksInScheduleString) && addToThread ){
                this.state.tasksInScheduleString[ task.id ] = {};
                //                this.state.scheduleStr += " --> " + task.id;
            }
        }
        else{
//            console.log("has no parent: " + task.id);

        }
        if( parent && this.state.scheduleStringChildren.hasOwnProperty( parent.id ) ){
            var sibling_count = Object.keys( this.state.scheduleStringChildren[parent.id] ).length;
        }
        else{
            var sibling_count = 1;
        }
//        console.log(task.id + " count: " + count + " ---- sibling_count: " + sibling_count);
        this.state.scheduleStr += "\n";
        if( count > 1 ){
            if( sibling_count == count ){
                var sibling_text = "";
                Object.keys( this.state.scheduleStringChildren[parent.id] ).forEach( function( child_id ) {
                        var siblingTaskName = tasks[child_id].name ? tasks[child_id].name : 'Task #' . child_id;
                        sibling_text += " " + siblingTaskName;
                }, this);
//console.log( 'this.state.scheduleStringChildren[parent.id]', this.state.scheduleStringChildren[parent.id]);
                this.state.scheduleStr += "<div class='phase-wrapper'><h2>Phase " + this.state.schedulePhaseCount + "</h2><div class='phase-description'>The following tasks (" + sibling_text + ") must be completed before moving to the next phase (" + parent.id + ") </div><ul>";
                Object.keys( this.state.scheduleStringChildren[parent.id] ).forEach( function( child_id ) {
  //                  console.log('gggggggggggggggggg: ' , child_id);
    //                console.log('OOOOOOOOOOOOOOOOOOOOOOO: ' , tasks[child_id].name);
                    if( tasks[child_id].name ){

                        var taskName = tasks[child_id].name;
                    }
                    else{
                        var taskName = "Task #" + child_id;
                    }

                    this.state.scheduleStr += "<li>" + taskName + " consisting of x for " + tasks[child_id].length + " business days </li>";
                }, this);
                this.state.scheduleStr += "</ul></div>";
                this.state.schedulePhaseCount++;
            }
        }
        else{
            var taskName = tasks[task.id].name ? tasks[task.id].name : 'Task #' + task.id;
            this.state.scheduleStr += "<div class='phase-wrapper'><h2>Phase " + this.state.schedulePhaseCount + "</h2><div class='contract-task-description'><span class='contract-task-name'>"  + tasks[task.id].name +  "</span> for <span class='contract-task-length'>" + tasks[task.id].length  + " business days.</span><div class='contract-task-description'>Task description</div></div></div>";
            this.state.schedulePhaseCount++;
 //           this.state.scheduleStr += " -->new task " + task.id;                    
        }
        if( traceUpThread ){
            this.state.scheduleStringChildren[parent.id] = {};
            this.scheduleUpThread( parent );
        }
    }
    buildSchedule(){
        this.state.scheduleStr = "";
        var lastChildren = this.state.lastChildren;
        Object.keys(lastChildren).forEach( function( key ) {
            var taskId = lastChildren[key];
            this.scheduleUpThread( tasks[taskId] );
//            console.log( 'last chidlren:::::::::::' + taskId, tasks[taskId] );
        }, this);
  //      console.log('this.state.scheduledParentTasks++++++',         this.state.scheduledParentTasks );
        /*
        Object.keys(this.state.scheduledParentTasks[866]).forEach( function( key ) {
            var taskId = this.state.scheduledParentTasks[866][key];
//            console.log( '866 --------------------:::::::::::' +  key + " " + taskId );
        }, this);
        */
//        console.log('this.state.scheduleStr' + this.state.scheduleStr);
        $('#schedule_wrapper .col-md-12').html( this.state.scheduleStr );
    }
    buildDatesUpThread(currentTaskId, currentStartDate, lastChildId, setDates, isControl){
        var currentTask = this.state.tasks[currentTaskId],
            workDaysUsed = 1,
            totalDaysUsed = 1,
            testDate = new Date(currentStartDate),
            closedDate = currentTask.closedDate ? new Date(currentTask.closedDate) : 0,
            proposedLengthInWorkDays = currentTask.length,
            blockedDays = [],
            user = currentTask.users[0],
            user_id = "",
            blockedDaysObject = {};

            if('undefined' != typeof user){
                user_id = user.id;
            }
            this.state.tasks[currentTask.id].endDateInDb = currentTask.closedDate;

            //set targetDate to a nonBlockedTargetDate
        var nonBlockedTargetDate = this.nonBlockedTargetDate;
        // check if currenstStartDate is set - if not, check if is a blocked da
        if(currentTask.startDate != currentStartDate){
            var isValidStartDate = 0;
            while(!isValidStartDate){
                if(this.isDayBlocked(testDate,user_id)){
                    totalDaysUsed++;
                    blockedDays.push( testDate );
                    blockedDaysObject[ getUnix(testDate.setHours(0,0,0,0) )] = testDate;

                    var testDate = new Date( testDate.addDays(1) );
                }
                else{
                    isValidStartDate = 1;
                }
            }
        }
        else{

            var testDate1 = new Date(currentTask.startDate);
            var testDate2 = new Date(currentStartDate);
        }


        if(currentTask.closedDate){

            totalDaysUsed = proposedLengthInWorkDays = dateDiffInDays( currentStartDate, closedDate) + 1;
            var dayCount = 0;
            while(dayCount <  totalDaysUsed){
                if(this.isDayBlocked(testDate,user_id)){
                    blockedDays.push(testDate); 

                    blockedDaysObject[ getUnix(testDate.setHours(0,0,0,0) )] = testDate;
                }
                dayCount++;
                var testDate = new Date( testDate.addDays(1) );
            }
        }
        else{

            //iterate over dates of task and add blocked days - BOOM
            // task closes on length OR "targetDate" (tday is project "center")  if has startDate and NO close date.
            while(workDaysUsed < proposedLengthInWorkDays){
                var testDate = new Date( testDate.addDays(1) );
                if(!this.isDayBlocked(testDate,user_id)){

                    workDaysUsed++;
                }
                else{
                    blockedDays.push(testDate);

                    blockedDaysObject[ getUnix(testDate.setHours(0,0,0,0) )] = testDate;

                }
                totalDaysUsed++;
            }
            if(testDate >= nonBlockedTargetDate){
                closedDate = testDate;
            }
            else{
                while(testDate <  nonBlockedTargetDate){
                    totalDaysUsed++;
                    if(this.isDayBlocked(testDate,user_id)){
                        blockedDays.push(testDate); 

                        blockedDaysObject[ getUnix(testDate.setHours(0,0,0,0) )] = testDate;
                    }
                    var testDate = new Date( testDate.addDays(1) );
                }
                closedDate = testDate;                
            }
        }

//console.log('testDate:',testDate);        
        this.testThreadLengths.lengths[lastChildId] += totalDaysUsed;
        this.testThreadLengths.endDates[lastChildId] = testDate;

     //   if(setDates){

            this.state.tasks[currentTask.id].realStartDate = currentStartDate;
            this.state.tasks[currentTask.id].realEndDate = closedDate;
            this.state.tasks[currentTask.id].build.startDate = currentStartDate;
            this.state.tasks[currentTask.id].build.endDate = closedDate;
            this.state.tasks[currentTask.id].realLength = totalDaysUsed;
            this.state.tasks[currentTask.id].blockedDays = blockedDays;
            this.state.tasks[currentTask.id].blockedDaysObject = blockedDaysObject;

       //    }
        if(currentTask.parent_id){
            var parent = this.state.tasks[currentTask.parent_id];
            if(!parent.startDate){
                var parentStartDate = new Date( closedDate.addDays(1) );
            }
            else{
                var parentStartDate = parent.startDate;                
            }
            this.buildDatesUpThread(currentTask.parent_id, parentStartDate, lastChildId, true, isControl);
        }
    }
    workUpThreadforGeneric( currentTask ){
//var taskStartInNumber = this.state.projectStartDate - currentTask.realStartDate;
//console.log( 'this.state.projectStartDate' ,this.state.projectStartDate );
console.log( 'currentTask.realStartDate' , currentTask.id );
var taskStartInNumber = Math.round((currentTask.realStartDate - this.state.projectStartDate) / (1000 * 60 * 60 * 24));
var taskEndInNumber = Math.round((currentTask.realEndDate - this.state.projectStartDate) / (1000 * 60 * 60 * 24)) ;
taskEndInNumber++;
        if('undefined' == typeof this.openDays[taskStartInNumber] ){
            this.openDays[taskStartInNumber] = {};
            this.openDays[taskStartInNumber].start = [];
            this.openDays[taskStartInNumber].end = [];            
            this.openDays[taskStartInNumber].start.push(currentTask.id);
        }
        else{
            if( !this.openDays[taskStartInNumber].start.includes(currentTask.id) ){
                this.openDays[taskStartInNumber].start.push(currentTask.id);
            }
        }
            if('undefined' == typeof this.openDays[taskEndInNumber] ){
                    this.openDays[taskEndInNumber] = {};
                    this.openDays[taskEndInNumber].start = [];            
                    this.openDays[taskEndInNumber].end = [];            
                    this.openDays[taskEndInNumber].end.push(currentTask.id);
            }
            else{
                if( !this.openDays[taskEndInNumber].end.includes(currentTask.id) ){
                    this.openDays[taskEndInNumber].end.push(currentTask.id);
                }
            }
        if(currentTask.parent_id){
            var parentTask = tasks[currentTask.parent_id];
            this.workUpThreadforGeneric( parentTask );
        }            

    }
    buildGenericDaysArray(){
        Object.keys(this.last_children).forEach( function( k, v ) {
            console.log('in buildgenreicdays TASKS::', tasks);
            var currentTask = tasks[this.last_children[k]];
            console.log('in buildgenreicdays TASKS::', currentTask);
            this.workUpThreadforGeneric( currentTask );
        }, this);

    }    
    isDayBlocked(day,user_id,group_id,project_id){
        var dayOfWeek = day.getDay(),
            blocked = false,
            checkDate = getUnix(day.setHours(0,0,0,0) ),
            thisBreak = this.state.breaks[ parseInt(checkDate) ];



      //  console.log('isdayblocked' +  checkDate, day);
        if(0 == dayOfWeek || 6 == dayOfWeek){
            blocked = true;
        }
        if('undefined' != typeof thisBreak){
            //alert('checing task ownershuip against break line 841');
           // if(user_id && (user_id == this.state.breaks[checkDate].user_id))
            if(user_id || group_id || project_id){
                    if( thisBreak.user_id == user_id){
                        blocked = true;
                    }
            }
            else{
                blocked = true;
            }
//            alert(1);
        }
        return blocked;
    }
    buildTaskTree(){
var startTime = performance.now()
console.log(`START bildup thread::::::::::::::::::; INITIAL PERFORMANCE`);

        var thread_lengths = this.state.project_thread_lengths;
        // build controlling thread first
        var controllingTask = this.state.tasks[this.latestThread.id],
            descendants = [];

            console.log('THIS------------->', this);
            console.log('controllingTask.startDate');
            console.log(controllingTask);
console.log( 'this.latestThread.id' );            
console.log( this.latestThread.id );            
console.log( 'this.state.tasks' );            
console.log( this.state.tasks );

        if( !controllingTask.startDate || 'undefined' == typeof controllingTask.startDate){
            var controllingTaskStartDate = this.state.projectStartDate;
        }
        else{
            var controllingTaskStartDate = new Date(controllingTask.startDate);
        }           

        this.buildDatesUpThread(controllingTask.id,controllingTaskStartDate,controllingTask.id,true,true);

        this.state.lastChildren.push(this.latestThread.id);

        this.buildDates(controllingTask, descendants, this.latestThread.id);
        $.each(this.last_children, function(k,id){
            if(id != this.latestThread.id){
                this.state.lastChildren.push(id);
                var task = this.state.tasks[id],
                    descendants = [];
                this.buildDates(task, descendants, id);
            }
        }.bind(this));
        $.each(this.state.threadStarts,function(id,v){
            this.setDates(this.state.tasks[id]);
        }.bind(this));
        // this sets the threadlengths:
        // START BUFFER SHOULD BE DEFINED HERE, SO.....ACCOUNT FOR IT?
        $.each(this.last_children, function(k,id){
            var task = this.state.tasks[id],
                descendants = [],
                thread_length = 0;
            this.threads[task.id].descendants = [];
            this.buildThread(task, descendants, id, thread_length);
        }.bind(this));
// 10/23 this loop seems excessive: review
        $.each(this.state.threadStarts, function(f,g){
            var id = f,
                thread_length = 0;
            $.each(thread_lengths[id],function(key, thisTask){
                if( (thisTask.realLength ) > thread_length){
                    thread_length = thisTask.realLength;
                }
            })
            var $task =  this.state.tasks[id],
                test_thread_length = thread_length;
            if(!$task.parent_id){
                test_thread_length = thread_length;
            }
            if( (test_thread_length  ) > (this.state.greatest_thread_length )){

                this.state.greatest_thread_length = (test_thread_length  ) * this.state.length_ratio;
            }
        }.bind(this));
    var endTime = performance.now()
console.log(`Call to buildupthreed took ${endTime - startTime} milliseconds`);


    }
    setEndDatesVoid(task_id, testDate = null, compare = null){
    }
    buildDates(task, descendants, i){
        var thisTask = this.state.tasks[task.id],
            projectStartDate = this.state.projectStartDate,
            projectTargetDate = this.state.targetDate,
            latest_descendant_end_date = new Date(this.state.projectStartDate);
        thisTask.userLength = thisTask.length;
        //set initial estEndDate:
        if(thisTask.startDate || thisTask.realStartDate){
            if(thisTask.realStartDate){ // set in buildDatesUpThread
                var startDate = new Date(thisTask.realStartDate);
            }
            else{
                var startDate = new Date(thisTask.startDate);
            }
            //check if startDate is targetDate or earlier - set to null if not
            //startCompare = dates.compare(targetDate, startDate); //negative if targetDate is earlier than startDate
            if(projectStartDate < startDate){               
                // cannot have a startDate after "targetDate"
               // thisTask.startDate = null;
            }
            else{
                if(thisTask.realEndDate){ // set in buildDatesUpThread
                    thisTask.estEndDate = new Date( startDate.addDays(thisTask.length -1)); //-1 inlcusive reckoning
                    thisTask.estEndDate = thisTask.realEndDate;
                }
                else{
                    thisTask.estEndDate = new Date( startDate.addDays(thisTask.length -1)); //-1 inlcusive reckoning
                }
            }
        }
        if(thisTask.estStartDate){
            var estStartDate = new Date( thisTask.estStartDate);
            thisTask.estEndDate = new Date( estStartDate.addDays(thisTask.length -1)); //-1 inlcusive reckoning
        }
        else{
            thisTask.estStartDate = new Date(this.state.projectStartDate);
        }
        //set initial estEndDate:
        if(thisTask.closedDate){
            //check if closedDate is later than targetDate - set to null if it is
            var closedCompare = dates.compare(thisTask.closedDate,projectTargetDate); //positive if targetDate is earlier than closedDate
            if(closedCompare > 0){
                // cannot have a startDate after "targetDate"
                var closedDate = thisTask.endDate = null;
            }
            else{
                thisTask.endDate = thisTask.estEndDate = thisTask.closedDate;

                thisTask.defined.endDate = thisTask.build.endDate = thisTask.calculated.endDate = thisTask.closedDate;
            }
        }
        $.each(descendants,function(k,v){
            var descEndDate = this.state.tasks[v].endDate,
                desc_task_end_date = new Date(descEndDate);
            var compare = dates.compare(desc_task_end_date,latest_descendant_end_date);
            if( compare > 0){
                latest_descendant_end_date = desc_task_end_date;
            }
            if(thisTask.startDate){
                thisTask.estStartDate = new Date(thisTask.startDate);
            }
            else{
                if(thisTask.estStartDate){
                    var earliestCompare = dates.compare(  latest_descendant_end_date, thisTask.estStartDate);
                    if(earliestCompare > 0){
                        thisTask.estStartDate = new Date(latest_descendant_end_date.addDays(1));
                        var endMinusStart = dateDiffInDays( thisTask.estStartDate, this.state.tasks[task.id].estEndDate);
                        if(endMinusStart <= 0){
                             this.state.tasks[task.id].estEndDate = thisTask.estStartDate.addDays(this.state.tasks[task.id].length - 1);
                             this.state.tasks[task.id].calculated.endDate = thisTask.estStartDate.addDays(this.state.tasks[task.id].length - 1);
                        }
                    }
                }
                else{ 
                    thisTask.estStartDate = new Date(latest_descendant_end_date.addDays(1));

                }
            }
            if(!thisTask.closedDate){
                if( dateDiffInDays( new Date(thisTask.estEndDate), latest_descendant_end_date)> 0){
                    var testStartDate = new Date( thisTask.estStartDate.addDays(thisTask.length -1 )),
                        testMinusStart = dateDiffInDays( thisTask.estStartDate, testStartDate ),
                        testMinusEnd = dateDiffInDays( thisTask.estEndDate, testStartDate );
                    if( (testMinusStart > 0 ) && ( testMinusEnd < 0)){
                        thisTask.estEndDate = new Date( thisTask.estStartDate.addDays(thisTask.length -1 ));
                    }
                }
            }
        }.bind(this))
        if(this.isThreadStart(task)){
            if( 'undefined' == typeof this.state.threadStarts[task.id]){
                this.state.threadStarts[task.id] = {'id' : task.id};
            }
        }
        if(!thisTask.closedDate){
            thisTask.endDate = thisTask.estEndDate;

            var compare = dates.compare(thisTask.endDate, this.state.targetDate);

            if( compare <= 0){
                thisTask.endDate = thisTask.estEndDate= this.state.targetDate;

                if( this.state.tasks[task.id].startDate ){
                    var setStartDate = new Date(this.state.tasks[task.id].startDate);
                }
                else if(this.state.tasks[task.id].estStartDate){
                    var setStartDate = new Date(this.state.tasks[task.id].estStartDate);
                }
                else{
                    var setStartDate = new Date(this.state.tasks[task.id].estStartDate);
                }
                this.state.tasks[task.id].length = 1 + dateDiffInDays( setStartDate, this.state.tasks[task.id].estEndDate);
                this.state.tasks[task.id].pending = true;
            }
        }
        else{
            thisTask.endDate = thisTask.closedDate;

            thisTask.build.endDate = thisTask.closedDate;

            thisTask.length = 1 + dateDiffInDays( new Date(this.state.tasks[task.id].startDate), new Date(this.state.tasks[task.id].closedDate));
        }
        if(!thisTask.endDate && thisTask.estStartDate){

            thisTask.estEndDate = new Date( thisTask.estStartDate.addDays(thisTask.length -1 ));
            if(!thisTask.endDate && thisTask.estEndDate){
                thisTask.endDate = new Date(thisTask.estEndDate);
            }
        }
        if(task.parent_id){
            descendants.push(task.id);
            return this.buildDates(this.state.tasks[task.parent_id], descendants, i);
        }
        return true;
    }
    setDates(task){
        if(this.state.tasks[task.id].length<1){
            this.state.tasks[task.id].length = 1;
        }
        if(task.parent_id){
            var parent = this.state.tasks[task.parent_id],
                this_end_at_parent = new Date(parent.estStartDate),
                length = this.state.tasks[task.id].length,
                startDate = 0;
            if(this.state.tasks[task.id].startDate ){ // original
                startDate = new Date(this.state.tasks[task.id].startDate); //original
                var startCompare = dates.compare(this.state.targetDate, startDate);
                if(startCompare < 0){
//                    startDate = this.state.tasks[task.id].startDate = null;
  //                  this.state.tasks[task.id].build.startDate = null;

                }
            }
            if(parent.realStartDate){
                this_end_at_parent = new Date(parent.realStartDate);
            }
            this.state.tasks[task.id].parent_start = this_end_at_parent;
            var parent_start_minus = this.state.tasks[task.id].parent_start_minus_length = this_end_at_parent.addDays(-length);
            if(startDate){
                if( dates.compare(parent_start_minus, startDate) > 0 ){
                    // check (estEnddate - length) if thats earlier than parent_start_minus,  
                    if(!this.state.tasks[task.id].startDate){
                        this.state.tasks[task.id].estStartDate = parent_start_minus;
                        this.state.tasks[task.id].calculated.startDate = parent_start_minus;
                    }
                    else{                       
                        this.state.tasks[task.id].estStartDate = this.state.tasks[task.id].startDate;   
                       // this.state.tasks[task.id].calculated.startDate = this.state.tasks[task.id].build.startDate;   
                    }
                }
            }
        }
        var startDiff = 0, closedDiff =0;
        this.state.tasks[task.id].startBuffer = 0;
        if(this.state.tasks[task.id].estStartDate && this.state.tasks[task.id].startDate){
            startDiff = dateDiffInDays( new Date(this.state.tasks[task.id].estStartDate), new Date(this.state.tasks[task.id].startDate));
            if(startDiff> 0 ){
                this.state.tasks[task.id].startBuffer = parseInt(startDiff); 
            }
            else{
                startDiff = 0;
            }
        } 
        else{
//        alert(task.id + " --> " + this.state.tasks[task.id].estStartDate + " - " + this.state.tasks[task.id].startDate);

        }
        if(!this.state.tasks[task.id].realLength){
            this.state.tasks[task.id].realLength = this.state.tasks[task.id].length;
        }
        this.state.tasks[task.id].endBuffer = 0;
        if(this.state.tasks[task.id].closedDate){
            var estClosedDate = new Date(this.state.tasks[task.id].closedDate); 
        }
        else{
            var estClosedDate = new Date(this.state.tasks[task.id].estEndDate);         
        }
        if(startDate && estClosedDate){
            if(task.parent_id){
                var parent = this.state.tasks[task.parent_id];

                var closedDate = new Date(this.state.tasks[task.id].closedDate); 
                if(parent.startDate){
                    var parentStartDate = new Date(parent.startDate); 
                }
                else{
                    var parentStartDate = new Date(parent.estStartDate);                
                }
                var closedDiff = dateDiffInDays( parentStartDate, closedDate);
            }
        }   
        //combine this with immeditaely above????
        if( task.parent_id ){
            var parent = this.state.tasks[task.parent_id];
            var parentStartDate = new Date(parent.build.startDate);
            if(task.build.endDate){
              var closedDiff = dateDiffInDays( task.build.endDate, parentStartDate) -1; // -1 for inclusive reckoning
                if(closedDiff>0){
                    this.state.tasks[task.id].endBuffer = closedDiff ;
                }
            }
        }
        else{
            if(task.build.endDate && this.state.latestEnd ){
              var closedDiff = dateDiffInDays( task.build.endDate, this.state.latestEnd) -1; // -1 for inclusive reckoning
                if(closedDiff>0){
                    this.state.tasks[task.id].endBuffer = closedDiff ;
                }
            }            
        }
        if("undefined" != typeof task.children ){
            $.each(task.children, function(k,v){
                var child = this.state.tasks[v.id];
                this.setDates(child);
            }.bind(this));
        }
    }
    buildThread(task, descendants, i, thread_length){
        //calendr dates here
        /*
        Compare thread length to ...? 
***NEED TO CALCULATE THE PROJECT END DATE - ONE THREAD COULD HAVE A LATER/DELAYED START**** 
        lastchidlren should have an endDate - either real or planned
        that date will be the "task open" date for the next. actual start date may be later. (lull can be planned)
        lull will be part of thread length
        have four values: task planned length, lull, buffer days, total task length

        then, need to add team down days (check task owner and check blocked days) -
        (say...team X has 11/15 & 11/16 off during this calendar calc, so add two buffers days to task length; buffer days push out calendar and new days need to be checked)

        */
        var stateTask = this.state.tasks[task.id];
        var taskLength = parseInt(task.length),
            taskRealLength = parseInt(task.realLength),
            taskStartBuffer = parseInt(task.startBuffer),
            taskEndBuffer = parseInt(task.endBuffer);
        if( 'undefined' != typeof taskStartBuffer && taskStartBuffer > 0){
            taskRealLength += taskStartBuffer;
        }
        if( 'undefined' != typeof taskEndBuffer && taskEndBuffer > 0){
            taskRealLength += taskEndBuffer;
        }
        thread_length += taskRealLength;//total task length = plaanned plus lull
        if('undefined' == typeof this.state.tasks[task.id].descendant_thread_length){
            this.state.tasks[task.id].descendant_thread_length = [];
        }
        if('undefined' == typeof this.state.tasks[task.id].descendant_thread_length[i]){
            this.state.tasks[task.id].descendant_thread_length[i] = 0;
        }
        if(this.isThreadStart(task)){
            $.each(descendants,function(k,v){
                this.state.tasks[task.id].descendant_thread_length[i] += parseInt(this.state.tasks[v].length);
                this.threads[task.id].descendants.push(descendants[k]);
            }.bind(this))
            this.state.tasks[task.id].descendant_thread_length[i] += task.length;
            if('undefined' == typeof this.state.project_thread_lengths[task.id]){
                this.state.project_thread_lengths[task.id] = [];
            }
            var subTask = {
                'id':i,
                'length':thread_length,
                'realLength':thread_length
            };
            this.state.project_thread_lengths[task.id].push(subTask);
        }
        if(task.parent_id){
            descendants.push(task.id);
            return this.buildThread(this.state.tasks[task.parent_id], descendants, i, thread_length);
        }
        else{
            if(task.length > this.state.first_task.length){
                this.state.first_task.length = task.length;
            }
        }
        // add blocked days
        if( task.realEndDate ){
            if( 'undefined' == typeof this.state.latestEnd){
                this.state.latestEnd = new Date(task.realEndDate);
            }
            else{
                if( task.realEndDate > this.state.latestEnd ){
                    this.state.latestEnd = task.realEndDate;
                }
            }
        } 
        return true;
    }

    getLastChildren(task){
        if('undefined' == typeof this.threads[task.id] ){
            this.threads[task.id] = {'descendants':[]};
        }
        if(task.children && Object.size(task.children) > 0 ){
            Object.keys(task.children).map(function(childId) {
                var child = task.children[childId];
                this.getLastChildren(child);
            }, this);
        }
        else{
            this.last_children.push(task.id);
        }
    }    
    zoomIn(){
        var newRatio = this.state.length_ratio + 2;
        this.setState({
            length_ratio : newRatio
        })
        this.updateHeight();
    }
    zoomOut(){
        var newRatio = this.state.length_ratio - 2;
        if(newRatio < 1){
            newRatio = 1;
        }
        this.setState({
            length_ratio : newRatio
        })
        this.updateHeight();
    }
    setPivot(){
        var date = new Date($('#setPivot').val());
        this.setState({targetDate:date});
        this.updateHeight();
    }
    fn() {
    };
    reBuildThread(task, descendants, i, thread_length){
        var taskLength = parseInt(task.length);
        if( task.startBuffer > 0 ){
            taskLength += parseInt(task.startBuffer);
        }
        if( task.endBuffer > 0 ){
            taskLength += parseInt(task.endBuffer);
        }
        thread_length += parseInt(taskLength);
        if(this.isThreadStart(task)){
            $.each(descendants,function(k,v){
                var descTask = this.state.tasks[v];
                var descTaskLength = parseInt(descTask.length);
                if( descTask.startBuffer > 0 ){
                    //descTaskLength += parseInt(descTask.startBuffer);
                }
                if( descTask.endBuffer > 0 ){
                    //descTaskLength += parseInt(descTask.endBuffer);
                }
                this.state.tasks[task.id].descendant_thread_length[i] += parseInt(descTaskLength);
            }.bind(this))
           // uncomment????? this.state.tasks[task.id].descendant_thread_length[i] += taskLength;
            var subTaskId = i,
                subTaskLength = thread_length;
            $.each(this.state.project_thread_lengths[task.id], function(k,v){
                if(subTaskId == v.id){
                    var subTaskStartBuffer = parseInt(this.state.tasks[v.id].startBuffer);
                    if( subTaskStartBuffer < 0 ){
                        subTaskStartBuffer = 0;
                    }
                    subTaskLength += subTaskStartBuffer;
                    var subTaskEndBuffer = parseInt(this.state.tasks[v.id].endBuffer);
                    if( subTaskEndBuffer < 0 ){
                        subTaskEndBuffer = 0;
                    }
                    //subTaskLength += subTaskEndBuffer;
                    this.state.project_thread_lengths[task.id][k].length = subTaskLength;

                }
            }.bind(this))
        }
        if(task.parent_id){
            // pass the date to parent
            return this.reBuildThread(this.state.tasks[task.parent_id], descendants, i, thread_length);
        }
        else{
            if(task.length > this.state.first_task.length){
                this.state.first_task.length = task.length;
            }
        }
        return true;

    }
    getAncestors(task, orig_id){
        if('undefined' == typeof this.state.tasks[orig_id].ancestors){
            this.state.tasks[orig_id].ancestors = [];
        }
        if(this.state.tasks[task.id].parent_id){
            var parent_id = this.state.tasks[task.id].parent_id,
                parent = this.state.tasks[parent_id];
            this.state.tasks[orig_id].ancestors.push(parent_id);
            if(parent.parent_id){
                this.getAncestors(parent, orig_id);
            }
        }
    }
    isThreadStart(task){
        if(task.hasSiblings > 0 ){
            return true;
        }
        if( task.children ){
            return true;
        }
        if( !task.parent_id  ){
            return true;
        }
        return false;
    }
    updateTask(task_id){
        var newLength = $('#task-length').val();
        this.state.tasks[task_id].length = newLength;
        $('[data-id="'+ task_id +'"] .length').html(newLength);
        $('[data-id="'+ task_id +'"]').data('days',newLength);
        this.buildAll();
    }
    validateTask(){
        var task_id = $('#task-update-id').val(),
            task_close_date = $('#task-close-date').val(),
            task_start_date = $('#task-update-start-date').val(),
            task_users = $('#users').val(),
            task_length = $('#task-length').val(),
            mssg = "",
            valid = 1;
        if(task_close_date && !task_start_date){
            mssg = "This task has a close date with no open date.";
        }
        if(valid){
            return true;
        }
        else{
            $('#task-error-message .message').html(mssg);
            return false;
        }
    }
    handleUpdateEvent(){
        // fires when modal is saved

        var task_id = $('#task-update-id').val(),
            task_close_date = $('#task-close-date').val(),
            task_start_date = $('#task-update-start-date').val(),
            task_users = $('#users').val(),
            task_length = $('#task-update-length').val(),
            task_name = $('#form-task-title').val(),
            task_description = $('#task-description-content').val(),
            task_owner = $("select[name='user-assign']").find(":selected").val(),
            task_parent_radio = $('input[name="task-parent"]:checked'),
            task_parent_id = task_parent_radio.val(),
            is_new_thread = task_parent_radio.siblings('.new-thread-wrapper').find('.make-new-thread').is(':checked'),
            task_group = $("select[name='group-assign']").find(":selected").val(),
            canEditTask = 0;
        // validate -- should be as function
        if( canUserEditTask() ){
            if(this.validateTask()){
                if( $('#saveTask').hasClass('active') ){
var obj = {
                      id: task_id,
                      length: task_length,
                      closedDate: task_close_date,
                      startDate: task_start_date,
                      users: task_users,
                      name: task_name,
                      description: task_description,
                      ownerUser: task_owner,
                      ownerGroup: task_group,
                      taskParentId: task_parent_id,
                      isNewThread: is_new_thread  
                        };
//                        console.log('pre save:', obj);
                    ;(async () => { const response = await axios.post('/api/taskUpdate', {
                      id: task_id,
                      length: task_length,
                      closedDate: task_close_date,
                      startDate: task_start_date,
                      users: task_users,
                      name: task_name,
                      description: task_description,
                      ownerUser: task_owner,
                      ownerGroup: task_group,
                      taskParentId: task_parent_id,
                      isNewThread: is_new_thread  
                        })
                    if( task_start_date){
                        $('#task-close-date').datepicker("option","minDate",task_start_date).attr('disabled',false);
                        $('#clear-close-date').removeClass('disabled');                        
                    }
                    else{
                        $('#task-close-date').attr('disabled',true);
                        $('#clear-close-date').removeClass('disabled');                        
                    }
// datepickers on rebuild were janky. destroy and recreate
var datePickers = [
            'task-close-date',
            'task-update-start-date',
            'task-update-real-start'
        ];
        $.each( datePickers, function(k,v){

            $( '#' + v ).datepicker( "destroy" );
//alert('96 this is adding datepicker elements to the timeline incorrectly');
           // $( '#' + v ).removeClass("hasDatepicker").removeAttr('id');
            console.log('destroy?' , $( '#' + v ) );
        })
        $('#ui-datepicker-div').remove();
        $('.calendar.month > td').remove();
                    $('#updateListener').html('in this ratio');
                    })()
                }

            }
        }
        else{
            alert('Cannot edit task');
        }
    }
    handleCloseDateEvent(){
        // fires when modal is saved
        var task_id = $('#task-update-id').val(),
            task_length = $('#task-length').val(),
            task_close_date = new Date();
            alert('use ajax to save new close date task-recat line 1516');
                    ;(async () => { const response = await axios.post('/api/taskUpdate', {
                      id: task_id,
                      closeOnly: true,
                      closedDate: task_close_date
                    })
                })()
    }
    handleDeleteTask(){
        // fires when modal is saved
//        closeSidebar();
        var task_id = $('body').data('active-task');
        ;(  async () => { const response = await axios.post('/api/taskDelete', {
              id: task_id
            })
            $('#app').removeClass('new_task').removeClass('task-edit-new');
            $('#updateTask').removeClass('new-task');
            $('#updateListener').html('in this ratio');
        })()
        $(".sidebar").animate({width: "0"});
        $(".sidebar").animate({opacity: "0"});
         setTimeout(function() { 
            toggleSidebar(null,true,true);
            $(".sidebar").animate({opacity: "1"});
        }, 1000);
    }
    handleEvent(task){
        // fires when modal is opened
    }
    handleTaskChange(taskId){
    }
    handleEventVoid(e){
        this.updateHeight();
    }
    openThreadVoid(e){
        var $target = $(e.target),
            toggle = true;
        if($target.parent().hasClass('active-thread')){
            toggle = false;
        }
        $('.active-thread').removeClass('active-thread');
        if(toggle){
            $('body').addClass('task-open  thread-open');
            $target.parent().addClass('active-thread');
        }
        else{
            $('body').removeClass('task-open  thread-open');            
        }
    }
    renderTask(taskId){

        var children = "",
            task = this.state.tasks[taskId],
            hasChildren = 0,
            users = this.state.tasks[taskId].users,
            classes = "",
            numberChildren = 0;
            $.each(users,function(k,v){
                classes += ' u-' + v.id;
            });
        if( task.children) {
            children = task.children;
            hasChildren = 1,
            numberChildren = Object.size(children);
        }
        var totalWidth = 14,
            nodeWidth = (task.length/totalWidth * 100),
            childWidth = 100 - nodeWidth,
            nodeHeight = 25,
            thread_length = "";
        if(task.thread_length){
            thread_length = task.thread_length;
        }
        // check startDate conditionals:
        //1. all previous tasks (children) must be closed or no children
        var allChildrenAreClosed = 1,
            latestChildClosedDate = "";
        Object.keys(children).map(function(childnodeId) {
            var childTask = this.state.tasks[childnodeId];
            if(!childTask.closedDate){
                allChildrenAreClosed = 0;
            }
            else{
                if(childTask.closedDate > latestChildClosedDate){
                    latestChildClosedDate = childTask.closedDate;
                }
            }
        }, this);
        if(allChildrenAreClosed){
            console.log('all chidlren are closed' , latestChildClosedDate);
        }
        var tour1 = "";
        if(!task.children && !this.state.tourButtons[1]){
            tour1 = (<div className="numberCircle" data-number="1"><span>1</span></div>);
            this.state.tourButtons[1] = 1;
        }
        if ( (hasChildren && numberChildren >= 2) || task.hasSiblings ) {
            if ( numberChildren > 1  || task.hasSiblings ){
                var endBufferId = "end-buffer-" + task.id,
                    startBufferId = "start-buffer-" + task.id;
                if(hasChildren){
                    this.state.tasks[taskId].render_option = 1;
                    return (
                          <div className="thread-container " key={task.id} data-task-id={task.id} data-weight={task.weight} >  
                            <div className="end-buffer" data-length={task.endBuffer} id={endBufferId}></div>
                            <Task users={this.state.users}  node={task} children={task.children}  nonBlockedTargetDate={this.nonBlockedTargetDate} id={task.id}  classes={classes} onVote={this.handleEvent} name={task.name} /><div className="start-buffer" data-length={task.startBuffer} id={startBufferId}></div>
                            <div className="children" >{ this.renderTaskChildren(children) }</div>
                             <div className="thread-control" data-task-id={task.id} onClick={this.openThread}>
                            </div>
                          </div>
                    );
                }
                else{
                    this.state.tasks[taskId].render_option = 2;                    
                    return (
                          <div className="thread-container option-two" key={task.id} data-task-id={task.id} data-weight={task.weight} >  
                            <div className="end-buffer" data-length={task.endBuffer} id={endBufferId}></div>
                            <Task users={this.state.users}  nonBlockedTargetDate={this.nonBlockedTargetDate} node={task} id={task.id}  onVote={this.handleEvent} classes={classes} name={task.name} /><div className="start-buffer" data-length={task.startBuffer} id={startBufferId}></div>
                             <div className="thread-control" data-task-id={task.id} onClick={this.openThread}>
                            </div>
                          </div>
                    );
                }
            }
            else{
                if ( numberChildren == 1 ){
                    this.state.tasks[taskId].render_option = 3;                    
                    return (
                        <div className='one-child'>
                        <div className="end-buffer" data-length={task.endBuffer} id={"end-buffer-" + task.id}></div>
                            <Task users={this.state.users}  users={this.state.users}  node={task} children={task.children} onVote={this.handleEvent}  id={task.id}  name={task.name} nonBlockedTargetDate={this.nonBlockedTargetDate}/>
                        <div className="start-buffer" data-length={task.startBuffer} id={"start-buffer-" + task.id}></div>
                        { this.renderTaskChildren(children) }
                        </div>
                    );            
                }
                else{
                    this.state.tasks[taskId].render_option = 4;

                    return (

                        <Task users={this.state.users}  node={task} children={task.children} onVote={this.handleEvent}  id={task.id} name={task.name} nonBlockedTargetDate={this.nonBlockedTargetDate}/>
                    );            
                }
            }
        }else{
            if(task.hasSiblings){
                nodeHeight = 100/task.hasSiblings;
            }
            if(task.hasSiblings){
                if ( hasChildren && numberChildren == 1 ){
                    this.state.tasks[taskId].render_option = 5;

                    return (<div className="siblings-and-child">
                                                <div className="end-buffer" data-length={task.endBuffer} id={"end-buffer-" + task.id}></div>

                            <Task users={this.state.users}  node={task} children={task.children} onVote={this.handleEvent}  id={task.id}  name={task.name} nonBlockedTargetDate={this.nonBlockedTargetDate}/>     
                            <div className="start-buffer" data-length={task.startBuffer} id={"start-buffer-" + task.id}></div>
                            { this.renderTaskChildren(children) }
                            </div>
                    );
                }
                else{
                    this.state.tasks[taskId].render_option = 6;

                    return(
                    <div key={task.id} className="react-div-wrapper">
                        <div className="end-buffer" data-length={task.endBuffer} id={"end-buffer-" + task.id}></div>
                        <Task users={this.state.users}  node={task} children={task.children} onVote={this.handleEvent} id={task.id} name={task.name} nonBlockedTargetDate={this.nonBlockedTargetDate} />
                        <div className="start-buffer" data-length={task.startBuffer} id={"start-buffer-" + task.id}></div>
                    </div>
                    );
                }
            }
            else{
                if ( hasChildren && numberChildren == 1 ){
                    this.state.tasks[taskId].render_option = 7;

                    return (<div className="thread-container" key={task.id} data-task-id={task.id} data-weight={task.weight} > 
                        <div className="end-buffer" data-length={task.endBuffer} id={"end-buffer-" + task.id}></div>
                            <Task users={this.state.users}  node={task} children={task.children} onVote={this.handleEvent}  id={task.id}  name={task.name} nonBlockedTargetDate={this.nonBlockedTargetDate}/>     
                    <div className="start-buffer" data-length={task.startBuffer} id={"start-buffer-" + task.id}></div>
                     <div className="children" >{ this.renderTaskChildren(children) }</div>
                            </div>
                    );
                }
                else{
                    if(!task.parent_id){
                        this.state.tasks[taskId].render_option = 8;

                        return(
                        <div className="thread-container 1125" key={task.id} data-task-id={task.id} data-weight={task.weight} >
                        <div className="end-buffer" data-length={task.endBuffer} id={"end-buffer-" + task.id}></div>
                           {tour2} <Task users={this.state.users}  node={task} children={task.children} onVote={this.handleEvent} id={task.id} name={task.name} nonBlockedTargetDate={this.nonBlockedTargetDate}/>
                            <div className="start-buffer" data-length={task.startBuffer} id={"start-buffer-" + task.id}></div>
                        </div>
                        );
                    }
                    else{
                        this.state.tasks[taskId].render_option = 9;

                        return(
                        <div key={task.id} className="react-div-wrapper 1134">
                        <div className="end-buffer" data-length={task.endBuffer} id={"end-buffer-" + task.id}></div>
                            <Task users={this.state.users}  node={task} children={task.children} onVote={this.handleEvent} id={task.id} name={task.name} nonBlockedTargetDate={this.nonBlockedTargetDate} />
                        <div className="start-buffer" data-length={task.startBuffer} id={"start-buffer-" + task.id} ></div>
                        {tour1}</div>
                        );

                    }
                }

            }
        } 
    }
    renderTaskChildren(children){
        let childnodes = null;
        let singleChildNodes = null;
        childnodes = Object.keys(children).map(function(childnodeId) {
           var singleChild = 0;
            if(Object.size(children) == 1){
                singleChild = 1;
            }
            var childnode = children[childnodeId];
            return this.renderTask(childnodeId);
        }, this);
        return childnodes;
    }
    render(){
        var state = this.state;
        $.each(this.state.tasks, function(k,v){
            this.getAncestors(v,v.id);
        }.bind(this));

        var taskTree = this.state.taskTree,
            app = (
            Object.keys(taskTree).map(function(taskId) {
                var task = this.state.tasks[taskId];
                return this.renderTask(taskId);
            }, this)
        )
        var filterStyle = "<style>";
        this.groupColorCount = 0;
        this.indivColorCount = 0;
        Object.keys(this.state.users).map(function(user_id) {
            var user = this.state.users[user_id];
                var color = "1ABC9C",
                rgbacolor100 = "(26,188,156,1)",
                rgbacolor60 = "(26,188,156,.6)",
                rgbacolor20 = "(26,188,156,.2)";
            if(1 == this.groupColorCount){
                color = "009bff";
                rgbacolor100 = "(0,155,255,1)";
                rgbacolor60 = "(0,155,255,.6)";
                rgbacolor20 = "(0,155,255,.2)";
            }
            if(2 == this.groupColorCount){
                color = "4BC0D9";
                rgbacolor100 = "(0,155,255,1)";
                rgbacolor60 = "(0,155,255,.6)";
                rgbacolor20 = "(0,155,255,.2)";
            }
            if(3 == this.groupColorCount){
                var color = "9DACFF",
                rgbacolor100 = "(231,78,78,1)",
                rgbacolor60 = "(231,78,78,.6)",
                rgbacolor20 = "(231,78,78,.2)";
            }
            if(4 == this.groupColorCount){
                color = "5C2751";
                rgbacolor100 = "(255,126,56,1)";
                rgbacolor60 = "(255,126,56,.6)";
                rgbacolor20 = "(255,126,56,.2)";
            }
            this.groupColorCount++;
            filterStyle += " body.user-" + user.id + " .task.user-" + user.id  + " .task-days-wrapper,#calendar-wrapper .activeSidebarTask.active-user.task.user-" + user.id  + " .task-days-wrapper, .filter-user.color-" + user.id  + " .legend-color-void { background-color: rgba" + rgbacolor100 + " !important;}";
            filterStyle += " body.user-" + user.id + " .task.user-" + user.id  + " .task-description{ color:white !important;}";
            //filterStyle += " .filter-user.active.color-" + user.id  + "{ background-color: rgba" + rgbacolor100 + ";}";
            filterStyle += ".dialog-wrapper.user-" + user.id + " .modal-content-top-header{ background-color: rgba" + rgbacolor100 + " !important;  }";
            filterStyle += ".dialog-wrapper.user-" + user.id + " .modal-title textarea{ color: #" + color + " !important;  }";
        }, this)
        if( !this.indivColorCount ){
            this.indivColorCount = 0;
        }
        Object.keys(this.state.groups).map(function(group_id) {
            var group = this.state.groups[group_id];
            var color = "800080";
            if(1 == this.indivColorCount){
                 color = "1ABC9C";
            }
            if(2 == this.indivColorCount){
                color = "ECA72C";
            }
            if(3 == this.indivColorCount){
                color = "EE5622";
            }
            if(4 == this.indivColorCount){
                color = "31263E";
            }
            this.indivColorCount++;
            filterStyle += ".dialog-wrapper.group-" + group.id + " .modal-content-top-header{ background-color: #" + color + " !important;  }";
            filterStyle += "#calendar-wrapper .activeSidebarTask.active-user.task.group-" + group.id  + " .task-days-wrapper { background-color: #" + color + " !important;}";

            filterStyle += ".dialog-wrapper.group-" + group.id + ", body.group-" + group.id + " #calendar-wrapper .task.group-" + group.id  + " .task-days-wrapper, .user-filter-group.color-" + group.id  + " .legend-color-void { background-color: #" + color + " }";
            filterStyle += " .user-filter-group.active.color-" + group.id  + ":after { background-color: #" + color + "; }";
        }, this)
        filterStyle += "</style>";
        return ( <div className='app-container'>
     <span id='app-data' data-start-date={this.state.estStartDate}></span><div dangerouslySetInnerHTML={{__html:filterStyle}}></div><div className='controls'><div onClick={this.zoomIn} className='zoom' id='zoom-in'>zoom in</div><div onClick={this.zoomOut} className='zoom' id='zoom-out'>zoom out</div></div><div id='calendar-container'></div>{ app }</div> );       
    }
    componentDidMount() {
        console.log('componentDidMount', this.state);
        window.appVars.tasks = this.state.tasks;
        window.appVars.owner = this.state.owner,
        window.appVars.currentUser = this.state.currentUser;
        ReactDOM.render(<TaskForm users={this.state.users} partners={this.state.partners} groups={this.state.groups} onUpdate={this.handleUpdateEvent} onCloseDateUpdate={this.handleCloseDateEvent} onDeleteUpdate={this.handleDeleteTask}  />, document.getElementById('task-form'));
        var $this = $(ReactDOM.findDOMNode(this)),
            $first_threads = $this.find('.thread-container');
        //  moving this 12/21 10:am to a data manipulation area
        $.each($first_threads,function(k,v){
            var id = $(v).data('task-id'),
            thread_length = 0;
            this.setDates(this.state.tasks[id]);
        }.bind(this));
      $.each(this.last_children, function(i,id){
            var task = this.state.tasks[id],
                descendants = [],
                thread_length = 0;
            this.reBuildThread(task, descendants, id, thread_length);
        }.bind(this));     
        this.updateHeight();
    }
    componentDidUpdate() {
        this.updateHeight();
    }
    // iterate UP the tree and identify start and end buffers for each task and thus thread
    setFinalThreadLengths($task){
            var thisStartBuffer = $task.startBuffer,
                children = $task.children;
            if( children ){
                /*
                $.each(this.state.project_thread_lengths[thisParent_id], function(a,b){
                    if(v == b.id){
                        this.state.project_thread_lengths[thisParent_id][a].startBuffer = thisStartBuffer;
                        this.state.project_thread_lengths[thisParent_id][a].realLength = this.state.project_thread_lengths[thisParent_id][a].length + thisStartBuffer;

                    }
                }.bind(this));
                */
            }
    }
    updateHeight(){

        //in project thread lengths we have the longest thread in days. we can get the end date???
        // .... 
        /*
need to calc and accomodate any task with a set date. 
1. an antecedent task cannot start until a preceding task has been closed/ended
2. 
        */
        // have to pass any task's end date from the following start date - the last task to the right has the riht end date
        var $this = $(ReactDOM.findDOMNode(this));
        var $first_threads = $this.find('.thread-container'),
            $longest_thread ="",
            thread_lengths = this.state.project_thread_lengths,
            greatest_thread_length = this.state.greatest_thread_length,
            vp_height = $(window).height(); 
        var length_ratio = this.state.length_ratio;
// DDO A PRELIM ITERATION OVER FIRST THREADS TO REBUILD WITH STARTBUFFER
        $.each($first_threads,function(k,v){
            var id = $(v).data('task-id'),
            thread_length = 0,

            thread_height_ratio = .3;
            var count = this.last_children.length;
            if( $(v).data('weight') ){
                thread_height_ratio = $(v).data('weight')/100;
            }
            console.log('vp_height' + vp_height);
            var thread_height = vp_height * thread_height_ratio;
            var min_last_child_height = 100;
            if( ( count * min_last_child_height) > thread_height ){
                thread_height = count * min_last_child_height;
            }
            console.log('thread height:'  + thread_height);
           $(v).css('height', thread_height);
           var $task =  this.state.tasks[id];
           var test_thread_length = thread_length;
           if(!$task.parent_id){
            test_thread_length = thread_length;
           }
            if( !$longest_thread ){
                $longest_thread = $(v);
            }
            if( (test_thread_length  ) > (greatest_thread_length )){
                greatest_thread_length = (test_thread_length  ) * length_ratio;
                $longest_thread = $(v);
            }
            $(v).data('days',thread_length);
        }.bind(this));
        $($longest_thread).width(100*length_ratio+'%');
        $('.children').each(function(k,v){
            var parent_height = $(v).siblings('.task').height();
            var children_number = $(v).children('.children .thread-container').length,
            height = 1/children_number * parent_height + 'px';
        });
        this.state.greatest_thread_length = greatest_thread_length;
        var calLength = this.state.greatest_thread_length/length_ratio;       
        var longest_thread_width = $('#project-container').width() * length_ratio;
        var parent_width = 0;
        console.log('SETTING PARENT WIDTH TO 0');
        var parent_days = 0;
        var unit_length_px = window.unit_length_px = longest_thread_width / greatest_thread_length * length_ratio;
        var $tasks = $('.task');
        var $app = this;
        var start_date = new Date(this.state.projectStartDate);
        $('.thread-container').each(function(c_key, container){
            var task_id = $(container).data('task-id'),
                thread_lengths = $app.state.project_thread_lengths[task_id];
                var container_days = 0;
            $.each(thread_lengths, function(s,d){
                if(d.length > container_days){
                    container_days = d.length;
                }
            });                      
            $(container).data('total-days',container_days);
            var children = $(container).children('.children'),
                targets = $(children).children('.thread-container, div'),
                percent = 100;
            if( targets.length){
                percent = 1/targets.length * 100 ;
            }
            targets.css('height',percent + '%');
        });
        var thisRef = this;
        $.each($tasks,function(k,v){
//console.log("parent_width at start  of loop  " + parent_width);

            var $id = $(v).data('id'),
                task = $app.state.tasks[$id],
                length = task.realLength,
                startBuffer = task.startBuffer,
                endBuffer = task.endBuffer,
                realLength = length + startBuffer + endBuffer;
            var threadLengths = thisRef.state.project_thread_lengths[$id];
            if(threadLengths){
                const keys = Object.keys(threadLengths)
                var longestLength = 0;
                for (const key of keys) {
                    if('undefined' != typeof threadLengths[key]){
                        if( longestLength < threadLengths[key].realLength){
                            longestLength = threadLengths[key].realLength;
                        }
                    }
                }
            }
            if($app.state.tasks[$id].startDate){
                var task_start_date = new Date($app.state.tasks[$id].startDate);
                var compare = dates.compare(task_start_date,start_date);
                if( compare < 0){
                    start_date = task_start_date;
                }
            }
            var day_filler_length = 1; //tie to some variable (start adte)
            $(v).siblings('.day-filler').css('width', day_filler_length * unit_length_px);
            if(parent_width < 1){
                parent_width = longest_thread_width;
//                console.log("set parent_width: " + 1966);
  //              console.log("parent_width =  " + parent_width);
            }
            else{
                if( $(v).parent('.thread-container').length > 0  ){
//                    alert(1);
                    parent_width = $(v).parent('.thread-container').width();
           //     console.log("set parent_width: " + 1972);
            //    console.log("v: ", $(v) );
             //   console.log("v closests thread-container: ", $(v).closest('.thread-container')[0] );
              //  console.log("v parent thread-container: ", $(v).parent('.thread-container')[0] );
               // console.log("v parent width: ", $(v).parent('.thread-container').width() );
                //console.log("parent_width =  " + parent_width);
                }
                else{
                    parent_width = $(v).parentsUntil('.children').width();
               //console.log("set parent_width: " + 1976);
                //console.log("parent_width =  " + parent_width);
                //console.log("v: ", $(v)[0] );
                //console.log("v childrem: ", $(v).parentsUntil('.children')[0] );
                //console.log("parent_width =  " + parent_width);
                }
            }
            if(parent_days < 1){
                parent_days = longest_thread_width;
            }
            else{
                parent_days = $(v).closest('.thread-container').data('total-days');
            }
            var startBufferLength = $(v).siblings('.start-buffer').first().data('length');
            var startBufferWidth = 0;
            if(startBufferLength){
                startBufferWidth = (startBufferLength * unit_length_px / parent_width) * 100;
                $(v).siblings('.start-buffer').first().css('width', startBufferWidth + '%');
            }
            else{
                if( startBuffer ){

                }
                else{
                    $(v).siblings('.start-buffer').first().remove();
                }
            }
            var endBufferLength = $(v).siblings('.end-buffer').first().data('length');
            var endBufferWidth = 0;
            if(endBufferLength){
                var endBufferWidth =  (endBufferLength * unit_length_px / (parent_width )) * 100;
               // console.log("end buffer::::::::::", task);
                
                $(v).siblings('.end-buffer').first().css('width', endBufferWidth + '%');
            }
            else{
                if( endBuffer ){
                    var endBufferWidth =  (endBuffer * unit_length_px / (parent_width )) * 100;
                    $(v).siblings('.end-buffer').first().css('width', endBufferWidth + '%');
                }
                else{
                    $(v).siblings('.end-buffer').first().remove();
                }
            }
            if(task.closedDate && task.startDate){
                var task_start_date = new Date(task.startDate);
                var task_closed_date = new Date(task.closedDate);
                var compare = dateDiffInDays(task_start_date,task_closed_date);
                var taskLength = compare+ 1; // (+ 1 ) is for inlcusive reckoning
            }
            else{
                var taskLength = task.realLength;
            }
            // what is this testRatio?????????????????
            var testRatio = (taskLength / longestLength) * 100;
            var bufferWidths = endBufferWidth + startBufferWidth;
            if(bufferWidths){

                var this_width = 100 - bufferWidths;
            }
            else{
                var this_width = (taskLength * unit_length_px / parent_width) * 100;
//console.log( 'task width : ', task);
//console.log( 'task length: ', taskLength);
//console.log( 'unit_length_px: ', unit_length_px);
//console.log( '2026 parent_width : ', parent_width);
                if(testRatio && 100 != testRatio ){
                    this_width = testRatio;
                }
    //if(this_width > 100 ){
    //       this_width = 100;
    //}
            }
           $(v).css('width', this_width + '%');
           $(v).next('.start-buffer').css('width', (startBuffer * unit_length_px / parent_width) * 100 + '%');
           $(v).next('.end-buffer').css('width', (endBuffer * unit_length_px / parent_width) * 100 + '%');
            var child_length_px = parent_width - (realLength * unit_length_px); 
//            var thread_container_width = child_length_px/parent_width * 100 +"%"; 
            var thread_container_width = (100 - this_width) +"%"; 
            $(v).siblings('.children').width(thread_container_width);
            var number_of_threads = $(v).next('.children').children('.thread-container').length;
//console.log("parent_width at end of loop  " + parent_width);

        }, thisRef);
        // ITERATE OVER END-BUFFERS?
        var mainId = Object.keys(this.state.taskTree)[0];
        // get length of all threads of main task
        var main_threads = this.state.tasks[mainId].descendant_thread_length;
        var main_greatest_length = 0;
        $.each(main_threads,function(_taskId,_taskLength){
            if(main_greatest_length < _taskLength){
                main_greatest_length = _taskLength;
            }
        })
        var project_end_date = new Date( start_date.addDays(main_greatest_length -1 ));
        var  mainTaskDiv = $('[data-id="' + mainId + '"]');
        mainTaskDiv.addClass('main-task').data('calc-end-date',project_end_date);
        this.state.tasks[mainId].calcEndDate = project_end_date;
        this.state.tasks[mainId].calculated.endDate = project_end_date;
        $.each(this.state.taskTree,function(_taskId,_task){
            this.setRealDate(_task);
       }.bind(this));
        // collect all blocked days
        var blockedDays = [];
        $.each(this.state.tasks,function(_taskId,_task){
            $.each(_task.blockedDays,function(_i,_blockedDay){
                blockedDays.push(_blockedDay);
            }.bind(blockedDays));
        }.bind(this));
//        console.log('BlockedDays:::',blockedDays);
        calLength = dateDiffInDays(start_date, this.latestThread.date) + 1;

        ReactDOM.render(<Calendar targetDate={this.state.targetDate} blockedDays={blockedDays} nonBlockedTargetDate={this.nonBlockedTargetDate} projectStartDate={this.state.projectStartDate} length={calLength} startDate={start_date} length_ratio={this.state.length_ratio}/>, document.getElementById('calendar-container'));
    }
    setRealDate(task){
        var stateTask = this.state.tasks[task.id];
        var startDate = new Date(stateTask.calcEndDate).addDays(-1*(stateTask.realLength -1 ));
        this.state.tasks[task.id].calcStartDate = startDate;
        this.state.tasks[task.id].calculated.startDate = startDate;
        if(task.children  ){
            $.each(task.children, function(childId,child){
                var stateChild = this.state.tasks[childId],
                    totalDays = stateChild.endBuffer + stateChild.realLength , //inlcusive reckoning
                    childEndDate = new Date(startDate).addDays(-1*(stateChild.endBuffer +1 )),
                    childStartDate = new Date(startDate).addDays(-1*(totalDays));
                    this.state.tasks[childId].calcStartDate = childStartDate;
                    this.state.tasks[childId].calcEndDate = childEndDate;
                    this.state.tasks[childId].calculated.startDate = childStartDate;
                    this.state.tasks[childId].calculated.endDate = childEndDate;
                    this.setRealDate(child);
            }.bind(this));
        }
    }
}
class Task extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        var task = this.props.node;
        this.state = {
            userLength : task.userLength,
            length: task.length,
            thread_length: task.thread_length,
            realLength : task.realLength,
            users : task.users
        }
    }
    handleClick(){
//        if($('#app').hasClass('project-edit')){
  //          console.log('in edit', this.props);
    //    }
      //  else{
console.log('HANDLE CLICK in edit', this.props);
            this.props.onVote(this);
            var thisTask = this.props.node;
            var taskId = this.props.node.id,
                activeId = $("#task-update-id").val();
            var taskDiv = $('[data-id="' + taskId + '"]');
            var mainTask = $('.main-task');
            var project_end_date = mainTask.data('calc-end-date');
            var totalDays = taskDiv.closest('.thread-container').data('total-days');
            var unitWidth = $('.day-container').width(),
            daysToTask = taskDiv.position().left / unitWidth;
            if( !$('#app').hasClass('new_task') ){
                toggleSidebar(thisTask);
            }
        //}
    }
    render() {
        var extra = "";        
        var classes = "task " + this.props.classes,
            thisTask = this.props.node,
            users = [],
            groups = [],
            children = [],
            parentId = thisTask.parent_id;
            console.log('thisTask;;;;;;;;;;;;;',thisTask);
        thisTask.users.forEach(function(user) {
            users.push(user.id);
            classes += " user-" + user.id;
        });
        if( 'new_task' == thisTask.type ){
            classes += " new";
        }
        thisTask.groups.forEach(function(group) {
//            console.log('------------------group',group);
            groups.push(group.id);
            classes += " group-" + group.id;
        });
        classes += " testetst"
        if('undefined' != typeof thisTask.children){
            Object.keys(thisTask.children).map(function(childTask_id){
                children.push( parseInt(childTask_id));
            })
        }
        if( !thisTask.closedDate ){
            if(thisTask.startDate){
                var startDate = thisTask.startDate;
            }
            if(thisTask.realStartDate){
                var startDate = thisTask.realStartDate;
            }
            if(startDate && startDate <= this.props.nonBlockedTargetDate){
                classes += " pending";
            }
        }
        var usersJSX = JSON.stringify(users);
        var groupsJSX = JSON.stringify(groups);
        var childrenJSX = JSON.stringify(children);
        var thisDate = new Date(thisTask.build.startDate);
        thisDate.setDate(thisDate.getDate() + (thisTask.realLength - 1)) ;
        var dayCount = 0;
        var taskDays = "";

        while(dayCount < thisTask.realLength){
            var thisDaysClass = "";
            var normalThisDate = thisDate;
            normalThisDate.setHours(0,0,0,0);
//            normalThisDate.setDate( normalThisDate.getDate() + 1);
            normalThisDate = getUnix(normalThisDate); 

            if('undefined' != typeof thisTask.blockedDaysObject ){
                if('undefined' != typeof thisTask.blockedDaysObject[normalThisDate] ){
                        thisDaysClass = " blocked";
                }
                for(let i = 0; i < thisTask.blockedDays.length; i++){
                    var blockedDay = thisTask.blockedDays[i];
                    blockedDay.setHours(0,0,0,0);

                    if( !(blockedDay < normalThisDate) && !(blockedDay > normalThisDate)){
                       // thisDaysClass = " blocked";
                    }
                }
            }
            var node = this.props.node,
                normalClosedDate = node.closedDate;
            if( normalClosedDate ){
                var closedDateUnix = getUnix( normalClosedDate.setHours(0,0,0,0) ); 
                if( closedDateUnix == normalThisDate){
                    thisDaysClass += " closedDate";
                }
            }
            var thisDayWidth = dayWidth;
            var thisStyle = "";
            if( dayCount == 0 || dayCount == (thisTask.realLength - 1 ) ){
//                thisDayWidth = 1 / thisTask.realLength * 50;
                if( dayCount == 0 ){
                    thisStyle = "margin-right:-10px";
                }
                if( dayCount == (thisTask.realLength - 1 ) ){
                    thisStyle = "margin-left:-10px";
                }
            }
        var dayWidth = 1 / (thisTask.realLength )* 100;
            var correction = ( 20 / thisTask.realLength );
            taskDays += "<div data-unix='" + normalThisDate + "' class='taskDay " + thisDaysClass + "' data-date='" + thisDate + "' style='width: calc(" + thisDayWidth + "% + " + correction + "px);" + thisStyle + "'></div>";
            thisDate.setDate(thisDate.getDate() - 1);
            dayCount++;
        }
        return(
            <div className={classes} data-startdate={startDate} data-enddate={thisTask.realEndDate} data-users={usersJSX} data-groups={groupsJSX} data-children={childrenJSX} data-parent={parentId} data-target="#updateTask" data-id={this.props.id} data-startbuffer={this.props.node.startBuffer} data-endbuffer={this.props.node.endBuffer} data-thread-length={this.props.thread_length} data-days={this.state.realLength} onClick={this.handleClick}><div className="task-description"   ><i className="tooltip-wrapper"><div className="top">{( this.props.name ? this.props.name : '#' + this.props.id ) }</div></i><div className="task-description-inner"><div className="description-text">{( this.props.name ? this.props.name : '#' + this.props.id ) } </div></div></div><div className="task-days-wrapper" dangerouslySetInnerHTML={{__html: taskDays}}></div><div className="taskEditor"><section className="new-task-section before"><div className="diamond-shape"><div className="item-count">+</div></div></section><section className="new-task-section after"><div className=" diamond-shape"><div className="item-count">+</div></div></section></div></div>
        )
    }
}

class TaskForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            users:props.users
        };
        this.handleClick = this.handleClick.bind(this);
        this.handleCloseDate = this.handleCloseDate.bind(this);
        this.handleDeleteTask = this.handleDeleteTask.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    openTaskEdit(){
        alert('react');
    }
    handleUserToggle(event){
        if(!$(event.target).hasClass('filter-user') || !$(event.target).hasClass('filter-group')){
            var $this = $(event.target).parent();
        }
        else{
            var $this = $(event.target);
        }
        var userId = $this.data('user-id');
        if( $this.hasClass('active') ){
            $('body').removeClass('user-' + userId);
            $this.removeClass( 'active' );
        }
        else{
            $('body').addClass('user-' + userId);
            $this.addClass( 'active' );
        }
        if( $('.filter-user.active').length ){
            $('body').addClass('active-user-filter');
        }
        else{
            $('body').removeClass('active-user-filter');        
        }
    }
    handleChange(event) {
        this.setState({value: event.target.value});
    }
    handleClick(){

        this.props.onUpdate(this);
    }
    handleCloseDate(){
        this.props.onCloseDateUpdate(this);
    }
    handleDeleteTask(){
         this.props.onDeleteUpdate(this);
    }
    handleSubmit(event) {
        event.preventDefault();
        var id = this.props.id;
    }
    componentDidMount(){
        $('.assign-task').appendTo('#project-partners');
//        $('#saveTask-wrapper').appendTo('#save-task-wrapper');
    }
  render() {
    var users = this.state.users;
    var groups = this.props.groups;
    groups[0] = {
        id : 0,
        name: 'Not assigned'
    }
        var userFilter = "<ul>";
       var addPartner = '<a class="anchor" data-toggle="modal" data-target="#partnerModal" >Add a Partner</a>';

        var taskAssign = "<div class='assignWrapper field-wrapper task-settings'><label class='label full-width'>Assign Task " +  addPartner + "</label>";
        var groupAssign = "<label>Assign to Group</label><select class='full-width' name='group-assign' id='group-assign'>";
        var userAssign = "<label id='user-assign-label'>Assign to User</label><select class='full-width' name='user-assign' id='user-assign'><option class='null-select' value='0'>Not Assigned</option>";
        Object.keys(groups).map(function(id) {
//             taskAssign += "<div class='radio-wrapper'><input type='radio' name='task-assign' id='task-assign-partner-" + id + "' data-type='group' value='" + id + "'><label class='partner-label' for='task-assign-partner-" + id + "'>" + groups[id].name + "</label></div>";
            var groupType = "individual";
            if( groups[id].account && groups[id].account.name ){
                if( "Individual Trial" == groups[id].account.name ){
                    groupType = "individual";                    
                }
                if( "Group Trial" == groups[id].account.name || "Group Partner" == groups[id].account.name ){
                    groupType = "group";                    
                }                
            }
            groupAssign += "<option value='" + id + "' data-type='" + groupType + "'>" + groups[id].name + "</option>";
            if( ( groups[id].account && "Individual Trial" != groups[id].account.name ) || !groups[id].account){

                if( groups[id].users){
                    Object.keys(groups[id].users).map(function(user_i) {
                        var user = groups[id].users[user_i];
                        userAssign += "<option data-type='user' data-group-id='" + id  + "' value='" + user.id + "' id='task-assign-user-" + user.id + "'>" + user.name + "</option>";
                    });
                };
            }
        })
        Object.keys(users).map(function(user_id) {
            var user = users[user_id];
            userFilter += "<li class='filter-user color-" + user.id + "' data-user-id='" + user.id + "'><div class='legend-color'></div><div class='user-label'>" + user.name + "</div></li>";
           
        }, this);

        userFilter += "</ul>";
        groupAssign += "</select>";
        userAssign += "</select>";
        taskAssign += groupAssign + userAssign + "</div>";
        var taskMove = `<div id="move-task-wrapper"><details class="col-lg-12 project-settings task-settings assign-parent-task-wrapper field-wrapper"`;
        if( $('#app').hasClass('new_task')){
            taskMove += ` open`;
        }
        var lastTask = ``;
        taskMove += `>
                    <summary class="assign-parent-task-toggle"></summary>
                    <div class="task-list"><div class="field-wrapper task-move-error"><div class="fade-out-html"></div></div>`;

        //see if there's a new task
        const reversedTasks = Object.keys(tasks).reverse(); 
        reversedTasks.forEach(key => {                   

            var task = tasks[key],
                taskName = tasks[key].name,
                taskClass = '';
            if( 'new_task' == tasks[key].type ){
                taskName = "Move to the End";
                lastTask = `<div class="radio-wrapper">
                            <div class="parent-wrapper">
                                 <label for="task-` + key + `">` + taskName + `</label>
                                <input type="radio" value="` + key + `" id="task-` + key + `" name="task-parent" />`;
                if( 'undefined' != typeof tasks[key].children ){
                    lastTask += `
                             <div class='new-thread-wrapper'>
                                 <input type="checkbox" id='make-new-thread-` + key + `' class="make-new-thread" value="` + key + `"><label for='make-new-thread-` + key + `'>Make a new Thread</label>
                            </div>`;
                }
                lastTask += `
                            </div>
                    </div>`;           

            }
            else{
                if( task.startDate ){
                    taskClass = ' task-has-started';
                }
                taskMove += `<div class="radio-wrapper ` + taskClass + `">
                            <div class="parent-wrapper">
                                 <label for="task-` + key + `" data-original-name="` + taskName +`">` + taskName + `</label>
                                <input type="radio" value="` + key + `" id="task-` + key + `" name="task-parent" class="` + taskClass + `" />`;
                if( 'undefined' != typeof tasks[key].children ){
                    taskMove += `
                             <div class='new-thread-wrapper'>
                                 <input type="checkbox" id='make-new-thread-` + key + `' class="make-new-thread" value="` + key + `"><label for='make-new-thread-` + key + `'>Make a new Thread</label>
                            </div>`;
                }
                taskMove += `
                            </div>
                    </div>`;           
            }
        }, this);
        taskMove += lastTask;
        taskMove += `</div></details></div>`;
    return (
        <div className="taskForm">
            <div id="task-form-wrapper" >
                <div className="button-wrapper">
                    <button className="close-task" onClick={this.handleCloseDate}>Mark Task Complete</button>
                </div>
                <div id="taskEditToggleWrapper">
                    <div className="row">
                        <div className="col-lg-12">
                        </div>
                    </div>
                    <div className="lowerTaskForm">
                        <div className="container-fluid">
                            <div className="row">
                                <div className="label-wrapper col-lg-5">
                                    <i className="far fa-question-circle tooltip-wrapper">
                                        <div className="top">
                                            <h3>Lorem Ipsum</h3>
                                            <ul>
                                            <li>Aliquam ac odio ut est</li>
                                            <li>Cras porttitor orci</li>
                                            </ul>
                                            <i></i>
                                        </div>
                                    </i>
                                    <label>Start Date: </label>
                                </div>
                                <div className="input-wrapper col-lg-7">
                                    <span className="input-error" id="startDate-error">start date</span>
                                </div>
                            </div>
                            <div className="row close-date-wrapper">
                                <div className="label-wrapper col-lg-5">
                                    <i className="far fa-question-circle tooltip-wrapper">
                                        <div className="top">
                                            <h3>Lorem Ipsum</h3>
                                            <ul>
                                            <li>Aliquam ac odio ut est</li>
                                            <li>Cras porttitor orci</li>
                                            </ul>
                                            <i></i>
                                        </div>
                                    </i>
                                    <label>Close Date: </label>
                                </div>
                                <div className="input-wrapper col-lg-7">
                                    <span className="input-error" id="close-date-error"></span>
                                </div>
                            </div>
                            <div className="field-wrapper row">
                                <div className="assign-task  col-lg-12">


                                    <div className="assign-task-inner inline" dangerouslySetInnerHTML={{__html: taskAssign}}>
                                    </div>
                                    
                                    <div className="field-wrapper project-settings task-settings col-lg-12 task-buttons" id="saveTask-wrapper">
                                        <button className="active" id="saveTask" title="Change a Task Option to Save" onClick={this.handleClick}>Save</button>

                                        <button className="active" id="deleteTask" title="Change a Task Option to Save" onClick={this.handleDeleteTask}>Delete</button>
                                        <button type="button" id="close-task" data-dismiss="modal" aria-label="Close">
                                            Cancel
                                        </button>
                                    </div>

                                </div>
                            </div>
                            <div className="row">
                                <div className="field-wrapper  col-lg-12">

                                   <div id="task-error-message"><div className="message"></div></div>
                                </div>
                            </div>
                            <div className="field-wrapper row">
                                <div className="col-lg-12">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="notes-wrapper"></div>

                <div className="upperTaskForm">
                    <input type="text" id="task-update-id" />
                    <br />
                    <label>Real Start Date  : </label>
                    <input id="task-update-real-start" type="text" />
                    <br />
                    <label>Proposed Start Date </label>
                    <input id="task-update-proposed-start" type="text" />
                    <br />
                    <label>Real Length </label>
                    <input id="task-update-realLength" type="text" />
                    <br />
                    <label>Proposed End Date </label>
                    <input id="task-update-proposed-end" type="text" />
                    <br />
                    <label>Real End Date </label>
                    <input id="task-update-real-end" type="text" />
                    <br />
                    <input id="task-length" type="text" value={this.props.value} onChange={() => this.setState({value: event.target.value})}/>
                </div>
            </div>
            <div className='user-filter-wrapper' onClick={this.handleUserToggle}  dangerouslySetInnerHTML={{__html: userFilter}}></div>
        </div>
    );
  }
}
class Day extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick(){
        console.log('button clicked as FORM UPDATE');
    }
    render() {
        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
          "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];
        var thisDay = new Date(this.props.date),
            compare = (this.props.projectStartDate - this.props.date),
            className = "day showDate",
            month = "",
            today = new Date();
            today.setHours(0,0,0,0);
        if(!compare){
            className += " startDay";
        }

        var targetCompare = (this.props.nonBlockedTargetDate - thisDay);
        if(!targetCompare){
            className += " targetDay";
        }
        var todayCompare = (today  - thisDay);
        if(!todayCompare){
            className += " currentDay";
        }

        //this.props.date.getMonth() + 1
        var html = this.props.date.getDate();
        if(1 == this.props.date.getDate() || !this.props.iter ){
            month =  monthNames[this.props.date.getMonth()];
        }
        month =  monthNames[this.props.date.getMonth()];
        var dayClass = "day-container ";
        if(1 == this.props.date.getDay() ){
            dayClass += " first-of-week";
        }
        var normalThisDay = thisDay;
        var thisDayUnix = getUnix( normalThisDay.setHours(0,0,0,0) );
        if( 'undefined' != typeof this.props.blockedDays[thisDayUnix] ){
            dayClass += " blocked"
        }
        dayClass += " " + this.props.date.toLocaleDateString( 'en-US',{weekday:'short'});
        return (
            <div className={dayClass} style={this.props.style} data-unix={thisDayUnix} data-day={this.props.date.getDay()} data-month={this.props.date.getMonth() + 1} data-month-start={this.props.monthStart} data-month-end={this.props.monthEnd}>
                <div className="monthName">{month}</div>
                <div className={className} >
                
                {html}
                <span className="dayName">{this.props.date.toLocaleDateString( "en-US",{weekday:'short'})}</span>
                </div>                
            </div>
        );
    }
}

class Calendar extends React.Component {
  constructor(props) {
    super(props);
    this.calRef = React.createRef();
    this.state = {
        length: this.props.length,
        length_ratio: this.props.length_ratio,
        targetDate : this.props.targetDate
    };
    this.handleClick = this.handleClick.bind(this);
  }
  componentWillReceiveProps(props) {
    this.setState({ length_ratio: props.length_ratio })
  }
  handleClick(){
        console.log('button clicked as FORM UPDATE');
    }
    handleDoubleClick(){
        alert(1);
    }
    componentDidMount(){
        wrapMonth();
        var widthCoeff = 1;
        if( $('body').hasClass('task-open') ){
            widthCoeff = 0.7;
        }
        scaleDates(widthCoeff,1);
        window.addEventListener("resize", scaleDates);
//this.props.length
        var range_length = this.props.length;
        //var range_length = 2;
        console.log( window.appVars.last_children );
      $('#calendar-container').append('<div class="slidecontainer"><input type="range" min="0" step="10" max="' + ( range_length * 10 ) + '" value="1" class="slider" id="newTaskRange" name="newTaskRange"><output for="newTaskRange" onforminput="" id="newTaskRangeBubble">Add Task Here</output></div>');
        Object.keys(window.appVars.last_children).forEach( function( k, v ) {
            var currentTask = tasks[window.appVars.last_children[k]];


            console.log('in buildgenreicdays TASKS::', currentTask);
        }, this);        
//          $('#calendar-container').append('<div class="slidecontainer"><input type="range" min="0" step="10" max="' + ( range_length * 10 ) + '" value="1" class="slider" id="newTaskRange"></div>');
     var bubble = $("#newTaskRangeBubble");
          $('#newTaskRange').on('input', function(e){
            newTaskRangeInput(e);
            var range = $('#newTaskRange');
          setBubble(range, bubble);
         })
        $('#newTaskRangeBubble').on('click', function(e){
            alert(1);
        })
    }
    componentDidUpdate() {
       // this.setState({length_ratio: newLength });
        window.addEventListener("resize", scaleDates);
        var newLength = $('#task-length').val();
        if( newLength != this.state.length){
            this.setState({length: newLength });
        }

    }    
  render() {
    var lengthCount = 0,
        rows = [],
        length_ratio = this.state.length_ratio,
        numrows = this.props.length,
        width = 1/numrows * 100,
        date = new Date(this.props.projectStartDate),
        isStart = 0,
        monthIter = 1;
        const calStyle = {
            width: width + '%',
            display: 'inline-block'
        };
        var blockedDays = {};
        for (var i = 0; i < this.props.blockedDays.length; i++) {
            blockedDays[getUnix(this.props.blockedDays[i])] = this.props.blockedDays[i];
        }
    for (var i = 0; i < numrows; i++) {
        var monthEnd = 0,
        monthStart = 0;

        if( i == (numrows-1) ){
            isStart=1;
            monthEnd = monthIter;
        }
        if(0 == i){
            monthStart = monthIter;
        }
        if(1 == date.getDate()){
            monthStart = monthIter;
        }
        var month = date.getMonth() + 1,
            year = date.getYear();
        var daysInMonth = new Date(year, month, 0).getDate();
        if(date.getDate() == (daysInMonth )){
            monthEnd = monthIter;
        }
        // note: we add a key prop here to allow react to uniquely identify each
        // element in this array. see: https://reactjs.org/docs/lists-and-keys.html

        rows.push(<Day onDoubleClick = {this.handleDoubleClick}  blockedDays={blockedDays} monthStart={monthStart} monthEnd={monthEnd} targetDate={this.props.targetDate} nonBlockedTargetDate={this.props.nonBlockedTargetDate} projectStartDate={this.props.projectStartDate} key={i} style={calStyle} date={date} isStart={isStart} iter={i}/>);
        date = new Date( date.addDays(1));
        if(monthEnd){
            monthIter++;
        }
    }
    rows.reverse();
    const calendarStyle = {
        width: (100 * this.state.length_ratio)  + '%',
        display: 'inline-block'
    };
    return ( <div className="calendar" ref={this.calRef} style={calendarStyle}>{rows}</div> );
  }
}
Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}
Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};
var dates = {
    convert:function(d) {
        // Converts the date in d to a date-object. The input can be:
        //   a date object: returned without modification
        //  an array      : Interpreted as [year,month,day]. NOTE: month is 0-11.
        //   a number     : Interpreted as number of milliseconds
        //                  since 1 Jan 1970 (a timestamp) 
        //   a string     : Any format supported by the javascript engine, like
        //                  "YYYY/MM/DD", "MM/DD/YYYY", "Jan 31 2009" etc.
        //  an object     : Interpreted as an object with year, month and date
        //                  attributes.  **NOTE** month is 0-11.
        return (
            d.constructor === Date ? d :
            d.constructor === Array ? new Date(d[0],d[1],d[2]) :
            d.constructor === Number ? new Date(d) :
            d.constructor === String ? new Date(d) :
            typeof d === "object" ? new Date(d.year,d.month,d.date) :
            NaN
        );
    },
    compare:function(a,b) {
        // Compare two dates (could be of any type supported by the convert
        // function above) and returns:
        //  -1 : if a < b
        //   0 : if a = b
        //   1 : if a > b
        // NaN : if a or b is an illegal date
        // NOTE: The code inside isFinite does an assignment (=).
        return (
            isFinite(a=this.convert(a).valueOf()) &&
            isFinite(b=this.convert(b).valueOf()) ?
            (a>b)-(a<b) :
            NaN
        );
    },
    inRange:function(d,start,end) {
        // Checks if date in d is between dates in start and end.
        // Returns a boolean or NaN:
        //    true  : if d is between start and end (inclusive)
        //    false : if d is before start or after end
        //    NaN   : if one or more of the dates is illegal.
        // NOTE: The code inside isFinite does an assignment (=).
       return (
            isFinite(d=this.convert(d).valueOf()) &&
            isFinite(start=this.convert(start).valueOf()) &&
            isFinite(end=this.convert(end).valueOf()) ?
            start <= d && d <= end :
            NaN
        );
    }
}

function getUnix(date){
    var offset = new Date().getTimezoneOffset() * 60000,
        utc = date - offset;
       // console.log(utc, new Date(utc));
        var out =     Math.round((new Date(utc)).getTime() / 1000);

    return out; 
}
function normalizeDate(date){
    //var localTime = date.getTime(),
      //  localOffset = date.getTimezoneOffset() * 60000,
        //utc = localTime + localOffset;
    return date.setHours(0,0,0,0);
}
function isFunction(functionToCheck) {
 return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}
const _MS_PER_DAY = 1000 * 60 * 60 * 24;

// a and b are javascript Date objects
function dateDiffInDays(a, b) {
  // Discard the time and time-zone information.
  if(a.getMonth && b.getMonth){
      const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

     return Math.floor((utc2 - utc1) / _MS_PER_DAY);
    }
    else{
        console.log('dateDiffInDays one is not a Date object');
    }
}
function addStartDate(){
    var unixDate = $('#start-date-button').data('start-date');
    $('.day-container').removeClass('task-start');
    if( $('#start-date-button').hasClass('active') ){
    $('#start-date-button').removeClass('active');
    $('.day-container[data-unix="' + unixDate + '"]').removeClass('task-start');
    }
    else{
    $('#start-date-button').addClass('active');                        
    $('.day-container[data-unix="' + unixDate + '"]').addClass('task-start');
    }

}
function addEndDate(){
    var unixDate = $('#end-date-button').data('end-date');
    //                    alert(unixDate);
    $('.day-container').removeClass('task-end');
    if( $('#end-date-button').hasClass('active') ){
        $('#end-date-button').removeClass('active');
        $('.day-container[data-unix="' + unixDate + '"]').removeClass('task-end');
    }
    else{
        $('#end-date-button').addClass('active');                        
        $('.day-container[data-unix="' + unixDate + '"]').addClass('task-end');
    }

}
function wrapMonth(){
    var totalDays = $('.day-container').length,
    $monthStarts = $("[data-month-start]").filter(function() {
        return  $(this).attr("data-month-start") > 0;
    });

    $.each($monthStarts,function(k,v){
        var monthNum = $(v).data('month');
        var monthClass = "month-centric",
            percentOfTotal = $('[data-month='+monthNum+']').length/totalDays * 100 + "%",
            dayWidth = 1/$('[data-month='+monthNum+']').length * 100;
        $('[data-month='+monthNum+']').wrapAll("<div class='month "+ monthClass + "' style='width:" + percentOfTotal + "; display:inline-block;'></div>");
        $('[data-month='+monthNum+']').css('width', dayWidth + '%');
    })

}
function setupProjectEditing(){

}
function setupTaskEditing(){

}
function addTask(project_id,parent_id, child_id){
    axios.post('/api/taskNew', {
        project_id: project_id,
        length: 3,
        parent_id: parent_id,
        child_id: child_id,
        name: '',
        type: 'new_task',
        is_new: true
    })
    .then(function (response){
        console.log('response',response);
        var task_id = response.data.task_id;
        var base_url = window.location.origin;
        window.location.replace(base_url + "/projects/" + project_id + "/timeline?task-edit-new&task=" + task_id);

        //$('#updateListener').html('in this ratio');
    })

}
function scrollCalendarLeft(){
    var halfWidth = parseInt($('#threads-wrapper').width()/2);
    $('#calendar-wrapper').scrollLeft( halfWidth );
}
function scaleDates(widthCoeff = null, ignoreDelay = 0){
    var containerWidth = $('#calendar-container').width(),
        width = ( $('body').width() * .95 ) - 80,
        totalDays = $('.day-container').length,
        delay = 600,
        dayWidth = 18;
    if(ignoreDelay){
        delay = 0;
    }

    // wrap cal if not already
    if( !$('#threads-wrapper')[0] ){
        $('.app-container > .thread-container').wrapAll( "<div id='threads-wrapper' />");        
        $('#threads-wrapper, #calendar-container').wrapAll( "<div id='calendar-wrapper' />");
        var threadsHeight = $('#threads-wrapper').outerHeight(),
            calHeight = $('#calendar-container').outerHeight(),
            totalHeight = parseInt(threadsHeight + calHeight + 200);
        $('#calendar-wrapper').height(totalHeight);
    }
    if(containerWidth < (totalDays * dayWidth) ){
       // $('#calendar-container .calendar').width( totalDays * dayWidth );
       // $('#threads-wrapper').width( totalDays * dayWidth );
    }
    if(widthCoeff){
        width = width * widthCoeff;
    }
    var ratio = totalDays/width * 1000,
        low = 30,
        mid = 40,
        high = 100000,
        calClass = "";
    var densities = {
        0 : 30,
        1 : 50,
        2 : 100
    }
    var i = 0,
        cont = 1;
    for (i = 0; i < Object.keys(densities).length; i++) {
        if(cont){
            var cur = densities[i];
            if(cur > ratio){
                var highRange = cur;
                cont = 0;
            }
            if('undefined' == typeof densities[i+1] ){
                var highRange = 'inf';
                var lowRange = densities[i];
                cont = 0;
            }
            if(i && !lowRange){
                if(i){
                    var lowRange = densities[i-1];
                }
                else{
                    var lowRange = 0;
                }
            }
        }
    }
    var cl =  $('body').attr("class").split(" "),
        newcl =[];
    for(var i=0;i<cl.length;i++){
        var r = cl[i].search(/density/);
        if(r !== 0)newcl[newcl.length] = cl[i];
    }
    $('body').removeClass().addClass(newcl.join(" "));  
    if('undefined' != highRange && 'undefined' != lowRange){
        var bodyClass = "density-" + lowRange + "-" + highRange;
        $('body').addClass(bodyClass);
    }
    if( 0 < ratio && ratio <= low ){
        calClass= "lowDensity";
         setTimeout( function() {
            $('#app').removeClass('midDensity').removeClass('highDensity')}
        , delay );
         $('body').addClass('density-0-30');    
    }
    if( low < ratio && ratio <= mid ){
        calClass= "midDensity";
        setTimeout( function() {
            $('#app').removeClass('lowDensity').removeClass('highDensity')
        }, delay );
    }
    if( mid < ratio && ratio < high ){
        calClass= "highDensity";
        setTimeout( function() {
            $('#app').removeClass('lowDensity').removeClass('midDensity')
        }, delay);
    }
    $('#ratio').html(ratio + "  " + calClass);
        setTimeout( function() {
            $('#app').addClass(calClass)
        }, delay);
}
    function cleanCalendar(){
        $('.day-container').removeClass('task-start').removeClass('task-end');
        $('.date-button').removeClass('active');
        $('#start-date-button').off( "click");

        $('#end-date-button').off( "click");
        $('.date-button .label,.date-button .value').removeClass('estimated');      
    }
function formatDate( dateInput ){
     const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
          "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];
    return monthNames[dateInput.getMonth()]  + " " + dateInput.getDate() + " " + dateInput.getFullYear();
}
function toggleSidebar(thisTask = null, justClose = null, justCloseButton = null){

    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
          "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];
        $('#task-form-wrapper').hide();
    var activeId = $("#task-update-id").val(),
        taskName = "",
        taskInputName = "",
        taskId = "",
        newTaskId = null,
        activeTaskElement = {},
        bodyTask = $('body').data('active-task'),
        currentUser = window.appVars.currentUser,
        projectOwnerGroup = window.appVars.owner;

console.log('thisssss in togglesidebar', window.appVars);
    cleanCalendar();


    if( !$('#app').hasClass('new_task') ){
        $('.task').removeClass('activeSidebarTask').removeClass('new-parent');
        $('input[name="task-parent"]').prop('checked', false);
    }
    if( !$('#app').hasClass('new_task') ){
        //$('.assign-parent-task-wrapper').removeAttr("open");
    }
    //$('#saveTask').removeClass('active');
    var isNewTask = 0;
    if( $('#app').hasClass('new_task') ){
        Object.keys(tasks).map(function(key) {
            if( 'new_task' == tasks[key].type ){
                newTaskId = key;
            }
        });
        var thisTask = tasks[newTaskId];
        isNewTask = 1;
    }
    $('#updateTask').removeClass( 'project-owner-admin' ).removeClass('task-owner-group');
    var sidebarOwnerClass = '';
    console.log('togglesidebar:::', thisTask);
    if( projectOwnerGroup ){
        $( projectOwnerGroup.users ).each(function(k,v){
console.log('for each owner group users::', v.id);
            if( currentUser.id == v.id ){
                sidebarOwnerClass = ' project-owner-admin';
            }
        })
    }
    $('#task-end-date-error-messages').removeClass('show');
    $('#task-start-date-error-messages').removeClass('show');

    $('#updateTask').addClass( sidebarOwnerClass );
    sidebarOwnerClass = "";
    // check if current user's group belongs to task group OR task users

    if(thisTask){
        $( thisTask.groups ).each(function(k,v){
//console.log('for each task group ::', v.id);
            if( currentUser.current_group == v.id ){
                sidebarOwnerClass += ' task-owner-group';
            }
        })
        $('#updateTask').addClass( sidebarOwnerClass );

        taskInputName = taskName = ( thisTask.name ? thisTask.name : '#' + thisTask.id);
        taskId = thisTask.id,
        activeTaskElement = $('.task[data-id="' + taskId + '"]');
            activeTaskElement.addClass('activeSidebarTask');
        $('#task-form-wrapper').show();
        $('#updateTask').removeClass('new-task');
        updateTaskCard(thisTask);

    }
    if( isNewTask ){
        taskInputName = "";
        $('#saveTask-wrapper').show();
    }
  /*
    $('body').removeClass (function (index, className) {
        return (className.match (/user-\S+/g) || []).join(' ');
    });
    $('body').removeClass (function (index, className) {
        return (className.match (/group-\S+/g) || []).join(' ');
    });
    */
    //$('.user-filter-group').removeClass('active');
    //$('.filter-user').removeClass('active');

    var openSidebar = true,
            toggle = true,
            showCloseTask = 0,
            $target = $(".task[data-id='" + taskId + "']");
        if( $('body').hasClass('task-open') && ( $('body').hasClass('task-edit') || $('body').hasClass('task-edit-new') ) ){
            openSidebar = false;
        }
        if($('body').hasClass('task-open') && activeId && taskId != activeId){
            toggle = false;
        }
        if(justClose){
            openSidebar = false;
        }
        if(justCloseButton){
            toggle = true;
        }
        $('.input-clear').on('click',function(e){
            var $target = $(e.target);
//            console.log('click target', $target);
            var $sibling = $target.siblings('input');
            if( "task-update-start-date" == $sibling.attr('id') ){
                setTimeout(function() { 
                    $('#task-close-date').val("");
                    $('#task-close-date').attr('disabled',true);
                    $('#clear-close-date').addClass('disabled');                        

                }, 50);
            }
            if( "task-close-date" == $sibling.attr('id') ){
                if(thisTask && thisTask.parent_id){

                }
//                alert(2);
                setTimeout(function() { 
                    $('#task-start-date-error-messages').html('');
                    $('#clear-start-date').removeClass('disabled');                        

                    $('#task-update-start-date').attr('disabled',false);
                }, 50);
            }
            $target.siblings('input').val("");
            $('#saveTask').addClass('active');
            $('#saveTask').attr('title','');            
        })
        $('#task-update-start-date').on('change', function(e){
            var val = $('#task-update-start-date').val();
            alert('update start' + val);
            if( val ){
                openEndDate('','3083');
            }
            else{
                closeEndDate('','3086');
            }

        })        
        $('#task-close-date').on('change', function(){
            alert('update end');
        })        
        $('.taskForm #users').on('change',function(){
            $('#saveTask').addClass('active');
            $('#saveTask').attr('title','');
        })       
        $('.taskForm input').on('change',function(){
            $('#saveTask').addClass('active');
            $('#saveTask').attr('title','');
        })

        $('.active-thread').removeClass('active-thread');
        var widthCoeff = 1;
        if(openSidebar){
            $('body').data('active-task', taskId);
                $('#task-edit-toggle').fadeIn();
            if(toggle){
                widthCoeff = 0.7;
                $("#project-container").animate({width: "60%"});

                $(".sidebar").animate({width: "40%"}, 500, function(){
                    $("#updateTask .modal-content").fadeIn("slow",function(){
                        $('body').addClass('task-open  ');
                        scaleDates(widthCoeff);

                    });
                });
                $('#close-task').on('click',function(){
                    toggleSidebar('',true,true);
                    $('body').removeClass('task-open').removeClass('task-edit');
                    var $btn = $('#settings-toggle button'),
                    allowEdit = 1;
                    $('#app').removeClass('project-edit');
                    $('.project-input').prop("disabled", true).removeClass('edit-input');
                    if( !$('#project-description').val() ){
                        //$('#project-description-wrapper').hide();
                    }
                })  

            }
            $target.parent().addClass('active-thread');
        }
        else{
            if(toggle){
                $('#task-edit-toggle').fadeOut();
                $('#close-task').unbind();
                $(".sidebar").animate({width: "0"});
                $("#updateTask .modal-content").fadeOut("slow",function(){
                    $("#project-container").animate({width: "100%"}, 100, function(){
                        $('body').removeClass('task-open').removeClass('task-edit').removeClass('open-user-select').data('active-task',0);
                        scaleDates(widthCoeff);
                    });
                });
            }
        }

        var startDate = new Date( $('#app-data').data('start-date')),
            taskStart = startDate.addDays();
        if(!justClose){
            $('body').addClass('task-open  ').data('active-task', taskId);
            $('#form-task-title').html(taskInputName).val(taskInputName);
            $('#form-task-title-reading').html(taskInputName);

            $('#task-update-id').val(taskId);
            $('#task-update-proposed-start').val();
            if(thisTask){
                update_start_date_text = "";
//1. all previous tasks (children) must be closed or no children
        var allChildrenAreClosed = 1,
            latestChildClosedDate = "";
            if( 'undefined' != typeof thisTask.children ){

                Object.keys(thisTask.children).map(function(childnodeId) {
                    var childTask = tasks[childnodeId];
                    if(!childTask.closedDate){
                        allChildrenAreClosed = 0;
                    }
                    else{
                        if(childTask.closedDate > latestChildClosedDate){
                            latestChildClosedDate = childTask.closedDate;
                        }
                    }
                }, this);
            }
        if(allChildrenAreClosed){
            openStartDate();
            //$('.close-task').show();
        }
        else{
           // $('.close-task').hide();
            closeStartDate('');
        }                
                if(thisTask.startDate){
                    var update_start_date = new Date(thisTask.startDate),
                        update_start_date_text =  monthNames[update_start_date.getMonth()]  + " " + update_start_date.getDate() + " " + update_start_date.getFullYear();
                    $('.close-date-wrapper').addClass('show');
                    openEndDate('','3185');
                    showCloseTask = 0;
                }
                else{
                    $('.close-date-wrapper').addClass('hide');
                    if( 'new_task' != thisTask.type){
                        //$('.task-error-messages.dates').html('These dates are estimates.'); 
                    }
                    closeEndDate('','line 3193');
                }
                //
                if( thisTask.parent_id ){
                    if( window.appVars.tasks[thisTask.parent_id] ){
                        var this_parent_task = window.appVars.tasks[thisTask.parent_id];
                        console.log( 'this parent tak::::::::::::::::', this_parent_task);
                        if( this_parent_task.startDate ){
                            closeEndDate('','line 3197');
                        }
                        else{
                            //openEndDate('','3204');
                        }
                    }
                }
                update_closed_date_text = "";
                console.log('closed date:::::::::::::::', thisTask);
                if(thisTask.closedDate){
                    var update_closed_date = new Date(thisTask.closedDate),
                        update_closed_date_text =  monthNames[update_closed_date.getMonth()]  + " " + update_closed_date.getDate() + " " + update_closed_date.getFullYear();
                        showCloseTask = 0;
                    $('#move-task-wrapper').addClass('disabled');    
                    $('#task-update-length').addClass('disabled').prop('disabled', true);
                    closeStartDate('Clear End Date to edit Start Date');                    
                }
                else{
                    var update_closed_date = "",
                        update_closed_date_text = "";
                    if( currentUser && currentUser.canEditTask( thisTask ) ){
                        showCloseTask = 1;
                    }
                    $('#move-task-wrapper').removeClass('disabled');
                    $('#task-update-length').removeClass('disabled').prop('disabled', false);                        

console.log('not sure why this was here like this????', thisTask);                        
                        /*
                    var update_closed_date = new Date(),
                        update_closed_date_text =  monthNames[update_closed_date.getMonth()]  + " " + update_closed_date.getDate() + " " + update_closed_date.getFullYear();
                    */
                }
                if(!thisTask.startDate){
                    showCloseTask = 0;
                }
                if( currentUser && currentUser.canEditTask( thisTask ) ){
                    $('#taskEditToggleWrapper').show();
                    $('#task-edit-toggle').show();
                }
                else{
                    $('#task-edit-toggle').hide();
                }
                if( showCloseTask ){
                    $('button.close-task').show();                                            
                }
                else{
                    $('button.close-task').hide();
                }

                // output summary
                var props = {},
                    timeProps = {},
                    status = 'open',
                    normalStartDate = '',
                    normalEndDate = {};
                
                // NOTES
                $('#notes-wrapper').html('');
                if( thisTask.notes && thisTask.notes.length > 0 ){
                    $('#notes-wrapper').html('<div id="notes-list-wrapper"></div><div id="notes-nav-wrapper"><div class="triangle up"></div><div class="note-counter"><div class="count-x">1</div> of <div class="count-of">' + thisTask.notes.length + '</div></div><div class="triangle down"></div></div>');
                    $('<ul/>', {
                        id: 'notes-list',
                        "class": 'class'
                    }).appendTo('#notes-list-wrapper');
                    $('#notes-nav-wrapper .triangle').on('click', function(e){
                        var order = $('.note.active').data('order'),
                            current = order,
                            moving = 0;

                        var target = $(e.target);
                        var direction = 'down';
                        if( target.hasClass('up') ){
                            direction = 'up';
                            if(0 != order){
                                moving = 1;
                            }
                        }
                        else{
                            if( ($('#notes-list li').length - 1) != order){
                                moving = 1;
                            }
                        }
                        var activeOffset = $('.note:first-child').offset(),
                            containerOffset = $('#notes-list-wrapper').offset();
                        if('up' == direction){
                            var offset = ( containerOffset.placement - activeOffset.placement) - $('.note.active').outerHeight();
                        }   
                        else{                
                            var offset = ( containerOffset.placement - activeOffset.placement) + $('.note.active').outerHeight();
                        }
                        if(moving){
                            if('up' == direction){
                                current--;                                        
                            }
                            else{
                                current++;
                            }
                            $('.note-counter .count-x').html(current + 1);
                            $('.note-counter .count-of').html( $('#notes-list li').length );
                            $('.note').removeClass('prevNote').removeClass('nextNote');
                            $('.note.active').addClass('prevNote').removeClass('active');
                            $('.note[data-order="' + current + '"]').addClass('active').removeClass('nextNote');
                            $('#notes-list').animate({
                                    scrollTop: offset
                                }, 
                                500, 
                                function(){
                                    var height = $('.note.active').height();
                                    $('#notes-list').height(height * 1.2);
                                }
                            );

                        }
                    })
                    for(var i=0; i<thisTask.notes.length; i++ ){
                        console.log('NOTE:::',thisTask.notes[i]);
                        var thisNote = thisTask.notes[i],
                            thisClass = " note",
                            overlayClass = "prevNote";                        
                        if(0 == i){
                            thisClass += " active";
                            overlayClass = "";
                        }
                        else{
                            overlayClass = "nextNote";
                        }
                        $('<li/>', {
                            id: 'note-' + thisNote.id,
                            "class": thisClass + ' ' + overlayClass,
                            "data-order" : i
                        }).appendTo('#notes-list');

                        $('<div/>', {
                            "class": 'note-overlay ',
                        }).appendTo('#note-' + thisNote.id );
                        var title = "";
                        if(thisNote.title){
                            title = thisNote.title;
                        }
                        $('<div/>', {
                            "class": 'note-title',
                            text : (i+1) + ': ' + title
                        }).appendTo('#note-' + thisNote.id );
                        $('<div/>', {
                            "class": 'note-content',
                            text : thisNote.content
                        }).appendTo('#note-' + thisNote.id );
                        if(0 == i){
//                            var height = $('.note.active').innerHeight();
  //                          $('#notes-list').height(height);
                        }
                     }

                     window.notes = {
                        current : 0,
                        offset : 0
                     }
                }
                // END NOTES
                //console.log('date compare close vs new',dates.compare(update_closed_date,today)); 
                $('#task-update-real-start').val(thisTask.calcStartDate);
                $('#task-update-real-end').val(thisTask.calcEndDate);
                $('#task-update-proposed-end').val(thisTask.endDate);
                $('#task-update-realLength').val(thisTask.realLength);
                $('#task-update-length').val(thisTask.userLength);
                $('#task-length-label').html('Estimated #Days: ');
                $('#task-length-value').html(' ' +  thisTask.userLength);
                $('#task-realLength-label').html('Expected Total Days: ' );
                $('#task-realLength-value').html(' ' + thisTask.realLength);
                $('#task-update-start-date').val(update_start_date_text);
                $('#task-close-date').val(update_closed_date_text);
                $("#task-owner-label-wrapper").attr('class','');
                $('.assign-parent-task-toggle').html('Move Task' );
                manageOwnerAssign( thisTask );

                $('#taskEdit').attr('href','/tasks/' + taskId + '/edit');
                // check authorization:
                //$('#taskEditToggleWrapper').hide();
                setTimeout(function(){
                    var height = $('.note.active').outerHeight();
                    $('#notes-list').height(height * 1.2);
                }, 1000); 

                function openTaskEdit(){
                    $('#task-form-wrapper .lowerTaskForm').addClass('open');
                    //$('#notes-wrapper').hide();
                    $( "#taskEditToggle" ).addClass('editOpen').html('Close Editor');
                    $( "#taskEditToggle" ).find('.arrow').removeClass('down').addClass('up');
                }
                $( "#taskEditToggle" ).on('click', function(){
                    var $taskEdit = $('#task-form-wrapper .lowerTaskForm');
                    if( $taskEdit.hasClass('open') ){
                       // $taskEdit.hide(200);
                        $taskEdit.removeClass('open');
                        //$('#notes-wrapper').show();
                        $( "#taskEditToggle" ).removeClass('editOpen').html('Open Editor');
                        $( "#taskEditToggle" ).find('.arrow').removeClass('up').addClass('down');
                    }
                    else{
                        //$taskEdit.show(200);
                        openTaskEdit();
                    }
                })
            }
        }
        if(thisTask){
            if(thisTask.build && thisTask.build.startDate){
                var key = "Start Date";
                var date = thisTask.build.startDate;
                var day = date.getDate();
                var monthIndex = date.getMonth();
                var year = date.getFullYear();
                normalStartDate = date;
                var startDate = monthNames[monthIndex] + ' ' + day + ', ' + year,
                    startClass = " estimated";
                if(thisTask.startDate){
                    key = 'Start Date';
                    startClass = "";
                }
                else{
                    key = 'Projected Start Date';
                    startDate = '<i class="estimated-date">' + startDate + '</i>';
                    status = 'future';
                }
                timeProps['start'] = {
                    'label' : key,
                    'value' : startDate,
                    'class' : startClass
                };
            }
            if(thisTask.build && thisTask.build.endDate){
                var key = "End Date";
                var date = thisTask.build.endDate;
                normalEndDate = date;
                var day = date.getDate();
                var monthIndex = date.getMonth();
                var year = date.getFullYear();
                var endDate = monthNames[monthIndex] + ' ' + day + ', ' + year,
                    endClass = " estimated"; 

                if(thisTask.closedDate){
                    key = 'End Date';
                    status = 'closed';
                    endClass = "";
                }
                else{
                    if('future' != status){
                        status = 'open';
                    }
                    key = 'Projected End Date';
                    endDate = '<i class="estimated-date">' + endDate + '</i>';                        
                }
                timeProps['end'] = {
                    'label' : key,                        
                    'value' : endDate,
                    'class' : endClass
                };
            }
            normalStartDate.setHours(0,0,0,0);
            var dpStartDate = formatDate( new Date(normalStartDate) );
            //            normalThisDate.setDate( normalThisDate.getDate() + 1);
            normalStartDate = getUnix(normalStartDate); 

            // startdate cannot be later than today - :
            var todayMaxDate = formatDate( new Date() );
            $('#task-update-start-date').datepicker("option","maxDate",todayMaxDate);


            normalEndDate.setHours(0,0,0,0);
            var dpEndDate = formatDate( new Date(normalEndDate) );
            //            normalThisDate.setDate( normalThisDate.getDate() + 1);
            normalEndDate = getUnix(normalEndDate); 

//            alert( normalStartDate);
                /*
            if(normalStartDate && typeof normalStartDate === 'function'){
                alert(3132);
                console.log('normalStartDate',normalStartDate);
                normalStartDate.setHours(0,0,0,0);
                var dpEndDate = formatDate( new Date(normalStartDate) );
                //            normalThisDate.setDate( normalThisDate.getDate() + 1);
                normalStartDate = getUnix(normalStartDate); 
            }
            if(normalEndDate && typeof normalEndDate === 'function'){
                normalEndDate.setHours(0,0,0,0);
                var dpEndDate = formatDate( new Date(normalEndDate) );
                //            normalThisDate.setDate( normalThisDate.getDate() + 1);
                normalEndDate = getUnix(normalEndDate); 
            }
            */
            if(timeProps['start']){
                $('#start-label').html(timeProps['start'].label).addClass(startClass).closest('#start-date-button').data('start-date', normalStartDate);
                $('#start-value').html(timeProps['start'].value).addClass(startClass);
            }
            if(timeProps['end']){
                $('#end-label').html(timeProps['end'].label).addClass(endClass).closest('#end-date-button').data('end-date', normalEndDate);
                $('#end-value').html(timeProps['end'].value).addClass(endClass);
            }
            if('undefined' != typeof dpEndDate && dpEndDate){
                $('#task-close-date').datepicker("option","maxDate",dpEndDate);
               // $('#task-close-date').val(dpEndDate);

//                $('#task-update-start-date').datepicker("option","maxDate",dpEndDate);
            }
            if('undefined' != typeof dpStartDate && dpStartDate){
 //                alert(new Date(normalStartDate));
              //  $('#task-update-start-date').datepicker("setDate", dpStartDate);
                $('#task-close-date').datepicker("option","minDate", dpStartDate);
            }


            $('#start-date-button').on('click', function (){
               // addStartDate();
            })
            $('#end-date-button').on('click', function (){
               // addEndDate();
            })
            if(thisTask.description){
                $('#task-description-content-view').html(thisTask.description);
                $('#task-description-content').html(thisTask.description).addClass('active');
                $('#task-description-label').addClass('active');
            }
            else{
                $('#task-description-content-view').html("");
                $('#task-description-content').html(thisTask.description).removeClass('active');
                $('#task-description-label').removeClass('active');
            }

        //1. all previous tasks (children) must be closed or no children
            /*
        var allChildrenAreClosed = 1,
            latestChildClosedDate = "";
            if( 'undefined' != typeof thisTask.children ){

                Object.keys(thisTask.children).map(function(childnodeId) {
                    var childTask = tasks[childnodeId];
                    if(!childTask.closedDate){
                        allChildrenAreClosed = 0;
                    }
                    else{
                        if(childTask.closedDate > latestChildClosedDate){
                            latestChildClosedDate = childTask.closedDate;
                        }
                    }
                }, this);
            }
        if(allChildrenAreClosed){
            alert(3497);
            openStartDate();
            //$('.close-task').show();
        }
        else{
            alert(3502);
           // $('.close-task').hide();
            closeStartDate();
        }
        */

//            console.log('parent',parent);

            var lengthProps = {};
            if(thisTask.blockedDays && thisTask.blockedDays.length){
                lengthProps['Task Length'] = {
                    'value' : ''
                }
            }
            lengthProps['Original Time Estimate'] = {
                'value' : thisTask.length + ' days'
            }
            if(thisTask.blockedDays && thisTask.blockedDays.length){
                lengthProps['Task Length'] = {
                    'key' : '<b>Task Length</b>',
                    'value' : '',
                    'class' : 'time-calc top'
                }
                lengthProps['Original Time Estimate'].class = 'time-calc';
                lengthProps['Blocked Days'] = {
                    'key' : 'Days Off',
                    'value' : thisTask.blockedDays.length + ' days',
                    'class' : 'time-calc'
                }
                if( 'open' == status ){

                }
                lengthProps['Total Length'] = {
                    'value' : thisTask.realLength + ' days',
                    'class' : 'time-calc total'
                }
            }

            $('#task-properties').html('');
            $('#length-properties').html('');
            var statusObj = {};
            if('open' == status){
                statusObj = {
                    'value' : "Open",
                    'class' : "status-open"
                }
            }
            if('closed' == status){
                statusObj = {
                    'value' : "Closed",
                    'class' : "status-closed"
                }
            }
            if('future' == status){
                statusObj = {
                    'value' : "Future",
                    'class' : "status-future"
                }
            }
            $('#form-task-title').focus();
            $('#task-status').removeClass('status-future').removeClass('status-closed').removeClass('status-open');
            $('#task-status').addClass(statusObj.class).html('Status: <span>' + statusObj.value + '</span>');
            Object.keys(props).map(function(key) {
                var prop = props[key],
                    propKey = ( prop.key ? prop.key : key + ":" ),
                    propClass = ( prop.class ? prop.class : ""),
                    propVal = (prop.value ? prop.value : "");
                if(prop){
                    $('<div class="row ' + propClass + '"><div class="col-lg-6">' + propKey + ' </div><div class="col-lg-6"> ' + propVal + '</div></div>').appendTo( $('#task-properties') );
                }
            })
            Object.keys(lengthProps).map(function(key) {
                var prop = lengthProps[key],
                    propKey = ( prop.key ? prop.key : key + ":" ),
                    propClass = ( prop.class ? prop.class : ""),
                    propVal = (prop.value ? prop.value : "");
                if(prop){
                    $('<div class="row ' + propClass + '"><div class="col-lg-8">' + propKey + ' </div><div class="col-lg-4"> ' + propVal + '</div></div>').appendTo( $('#length-properties') );
                }
            })  
            addStartDate();      
            addEndDate();
            setTimeout(
                function(){
                    window.autoExpand($('#task-description-content')[0])
                    window.autoExpand($('#form-task-title')[0])

                },
                1000
            );

        }
        //
        canSaveTask();
        $('textarea, input, select').on('input', function(k,v){
            canSaveTask();
        })

}
function truncateString(str, num) {
  if (str.length <= num) {
    return str
  }
  return str.slice(0, num) + '...'
}
function addTaskModal( $target , e){
    $('body').addClass('adding-task-hover');
    var childrenIds = $target.data('children'),
        parentId = $target.data('parent'),
        parent = tasks[parentId],
        thisId = $target.data('id'),
        thisTask = tasks[thisId];
        var buttonLength = 15;
        if($target.hasClass('task')){
            var width = $target.width();
            var clickX = e.pageX;
            var taskLeft = $target.offset().left;
            var project_id = $('#project').data('project_id');
            var taskRight = taskLeft + width;
            var position = 'on';
            if ( clickX < (taskLeft + 10) ){
                position = 'before';

            }
            else if ( clickX >= (taskRight - 10)) {
                position = 'after';
            }
            var output = "<div><h4 style='margin-bottom:20px'>Choose the location of the New Task</h4><div class='select-message'>Hover on the options below to see where you can place a new task</div></div>",
                locationCount = "";
            // if after - use parent
            if('after' == position){
                if(parent){
                    // check if parent has children
                    var selectText = "Add a new task <div class='task-diamond'></div> between <div class='task-select-child'>" + truncateString(thisTask.name, buttonLength) + "</div> and <div class='task-select-parent'>" + truncateString( parent.name, buttonLength) + "</div>";  
                output += "<div class='task-select' data-child='" + thisTask.id + "' data-parent='" + parent.id + "'><div class='select-option'> " + locationCount + "</div><div class='select-text'>" + selectText + "</div></div>"; 
                locationCount;
                //alert(locationCount);
                    var parentChildLength = Object.size(parent.children);
                    if( parentChildLength > 1){
                        const keys = Object.keys(parent.children);
                        for (const key of keys) {
                          console.log('key',key);
                            if(thisTask.id != key){
                                var child = tasks[key];
                    var selectText = "Add a new task <div class='task-diamond'></div> between <div class='task-select-child'>" + child.name + "</div> and <div class='task-select-parent'>" + parent.name + "</div>";  
                output += "<div class='task-select' data-child='" + child.id + "' data-parent='" + parent.id + "'><div class='select-option'>" + locationCount + "</div><div class='select-text'>" + selectText + "</div><button class='btn'>Add a new task</button></div>"; 
                locationCount++;
                            }
                        }

                    }
                    var selectText = "Add a new task <div class='task-diamond'></div> before <div class='task-select-parent'>" + parent.name + "</div>";  
                output += "<div class='task-select new-thread' data-parent='" + parent.id + "'><div class='select-option'> " + locationCount + "</div><div class='select-text'>" + selectText + "</div></div>"; 
                locationCount;
                }
                else{
                    var selectText = "After " + thisTask.name;                      
                output += "<div class='task-select' data-child='" + child.id + "' data-parent='" + parent.id + "'><div class='select-option'>" + locationCount + "</div><div class='select-text'>" + selectText + "</div><button class='btn'>Add Task</button></div>"; 
                locationCount++;
                }
                
            }
            // if after - use parent
            if('before' == position){
                if(childrenIds){
                    var index = 0;
                    for (index = 0; index < childrenIds.length; ++index) {
                    }
                    var selectText = "Between " + thisTask.name + " and " + parent.name;  
                    output += "<div class='task-select' data-child='' data-parent='" + parent.id + "'><div class='select-option'>1</div><div class='select-text'>" + selectText + "</div><button class='btn'>Add Task</button></div>"; 
                }
                else{
                    var selectText = "After " + thisTask.name;                      
                }
                
            }
        }
        $('#sidebar-add-task').html( output );

//        $('#add-task-message').html( output );
        $('#add-task-message .task-select').on('click', function(e){
        });
        $('#sidebar-add-task .task-select').hover(function(e){

            var $target = $(e.target),
            childId = $target.data('child'),
            parentId = $target.data('parent');
            $('.task[data-id="' + childId + '"]').addClass('adding-child');
            $('.task[data-id="' + parentId + '"]').addClass('adding-parent');

            if($target.hasClass('new-thread')){
                var $test = $('.task[data-id="' + parentId + '"]');
                $('.task[data-id="' + parentId + '"]').siblings('.children').addClass('adding-thread').height('50%');
                $('.task[data-id="' + parentId + '"]').addClass('adding-thread');
            }
        }).mouseleave(function(e){
            $('.task').removeClass('adding-thread');
            $('.task').siblings('.children').removeClass('adding-thread').height('100%');
            var $target = $(e.target),
            childId = $target.data('child'),
            parentId = $target.data('parent');
            $('.task').removeClass('adding-child').removeClass('adding-parent');
            if($target.hasClass('new-thread')){
                var $test = $('.task[data-id="' + parentId + '"]');
            }
        });
 
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}
function styleCalendar(){

}
var tutorial = {

    designer : {
        bannerTitle : "Traverse: A Designer's Project Calendar",
        bannerContent : "<div class='banner-content'><img src='/img/booking.svg'>This is the intro for the project calendar. It will excite and delight you. That's how thing go with a landing page.</div>", 
        states: {
            1:{
                placement: 'top',
                card_top: -80,
                card_left: -20,
                number_top: '0%',
                number_left: '0%',            
                pointer_top: '0%',
                pointer_left: '10%',
                pointer_direction : "down",
                header : "The Designer Tasks",
                content : 'What is going n?'

            },
            2:{
                placement: 'left',
                card_top: 0,
                card_left:50,
                number_top: '50%',
                number_left: '0%',            
                pointer_direction : "left",
                pointer_top: '16%',
                pointer_left: '17px',
                header : "Click the pencil to update the project and tasks",
                content : "this is content."
            },
            3:{
                card_top: 0,
                card_left:50,
                number_top: '0%',
                number_left: '0%',            
                pointer_direction : "left",
                pointer_top: '16%',
                pointer_left: '17px',
                header : "This is state 2",
                content : "this is content."
            }
        },
    },
    developer : {
        bannerTitle : "Traverse: A Developer's Project Calendar",
        bannerContent : "<div class='banner-content'><img src='/img/booking.svg'>This is the intro for the project calendar. It will excite and delight you. That's how thing go with a landing page.</div>", 
        states : {
            1:{
                placement: 'top',                
                card_top: -80,
                card_left: -20,
                number_top: '0%',
                number_left: '0%',            
                pointer_top: '0%',
                pointer_left: '10%',
                pointer_direction : "down",
                header : "The Developer Tasks...",
                content : 'What is going n?'

            },
            2:{
                placement: 'left',                
                card_top: 100,
                card_left:-70,
                number_top: '0%',
                number_left: '0%',            
                pointer_direction : "right",
                pointer_top: '16%',
                pointer_left: '17px',
                header : "Click the pencil to update the project and tasks",
                content : "this is content."
            },            
            3:{
                placement:'top',
                card_top: -80,
                card_left:-20,
                number_top: '0%',
                number_left: '0%',            
                pointer_direction : "left",
                pointer_top: '16%',
                pointer_left: '17px',
                header : "This is state 2",
                content : "this is content."
            }
        },
    },
    client : {
        bannerTitle : "Welcome to Traverse: Your Project Calendar",
        bannerContent : "<div class='banner-content'><img src='/img/booking.svg'>Hello - You were probably sent here by someone you're working on a project with. This is the intro for the project calendar. It will excite and delight you. That's how thing go with a landing page.</div>", 
        states : {
            1:{
                card_top: -80,
                card_left: -20,
                number_top: '0%',
                number_left: '0%',            
                pointer_top: '0%',
                pointer_left: '10%',
                pointer_direction : "down",
                header : "The Project's First Task",
                content : 'This kicks off the project. Double click the task block.'

            },
            2:{
                card_top: 0,
                card_left:50,
                number_top: '0%',
                number_left: '0%',            
                pointer_direction : "left",
                pointer_top: '16%',
                pointer_left: '17px',
                header : "Click the pencil to update the project and tasks",
                content : "this is content."
            },            
            3:{
                card_top: 0,
                card_left:50,
                number_top: '0%',
                number_left: '0%',            
                pointer_direction : "left",
                pointer_top: '16%',
                pointer_left: '17px',
                header : "This is state 2",
                content : "this is content."
            }
        }
    },
    positionPopup($number){
        this.current_state = $number.data('number');       
        var currentState = this[this.tourType].states[this.current_state];
        if(this.current_state){
            var placement = currentState.placement;
            if('left' == currentState.pointer_direction){
                this.pointLeft();
            }
            if('down' == currentState.pointer_direction){
                this.pointDown();
            }
            if('right' == currentState.pointer_direction){
                this.pointRight();
            }
            $('.tour').show();
            $('.tour .placement h3').html(currentState.header);
            $('.tour .placement p').html(currentState.content);
            
            $('.tour .placement').css('top', currentState.pointer_top);
            $('.tour .placement').css('margin-left', currentState.pointer_left);
        }
        else{
            $('.tour').hide();
        }
        var height = $('.tour.tooltip-wrapper .placement').height(),
            width = $('.tour.tooltip-wrapper .placement').width(),
            card_top = currentState.card_top,
            card_left = currentState.card_left
        if('left' == currentState.pointer_direction){
            card_top += height/2;
        }
        $('.tour').css('top', card_top);

        if('down' == currentState.pointer_direction){
            card_left -= width/2;
        }
        $('.tour').css('left', card_left);
        $('.numberCircle').removeClass('active');
        $number.addClass('active');
    },
    toggleTour(){
        if( $('#toggleTour').is(':visible') ){
            if( $('#app').hasClass('hide-project-tour') ){
                $('#editMode').addClass('active');
                $('#exitEdit').removeClass('active');
                $('#app').removeClass('project-edit');
                $('#app').addClass('project-tour').removeClass('hide-project-tour').removeClass('tour-intro');
                $('#toggleTour').html('Close Tour');
            }
            else{
                $('#app').removeClass('project-tour').addClass('hide-project-tour');
                $('#toggleTour').html('Show Tour');
            }
        }
        else{
            $('#app').removeClass('project-edit');
            $('#app').addClass('project-tour').removeClass('hide-project-tour').removeClass('tour-intro');
        }
    },
    beforeStateChange(){

//        alert('statechange');
    },
    runPopup: function( $target ){
        this.addPopup(  $target );
        this.checkState();
        this.positionPopup( $target );
    },
    previousState: function( $target ){

    },
    start: function(state = 0) {
        var show = 1;        
        if( userSettings && userSettings.hide_all_tours == 'hide' ){
            var show = 0;
        } 
        if( show ){
            $('#app').removeClass('project-edit');
            this.current_state = state;
            this.tourType = getParameterByName('tour', window.location.search);
            if(!this.tourType){
                this.tourType= 'designer';
            }
            if(this[this.tourType].bannerTitle){
               
                $('.tutorial-banner-wrapper').show();
                var buttonDiv = "<div id='buttonDiv'><a id='skipTour'>Skip the Tour</a><button id='startTutorial'>Start the Tour</button></div>";
                $('.tutorial-banner').html("<h1>" + this[this.tourType].bannerTitle + "</h1><div id='bannerContent'>" + this[this.tourType].bannerContent + "</div>" + buttonDiv);
            }
            this.checkState();
            if(state != 1){
                var $target = $('.numberCircle[data-number="' + state + '"]');
                this.runPopup($target);
            }
            else{
        //        var $target = $('.numberCircle[data-number="1"]');
      //          this.runPopup($target);            
            }
            $('#skipTour').click({_this: this},function(e){
                $('.tutorial-banner-wrapper').fadeOut("slow", function(){
                    var $target = $('.numberCircle[data-number="1"]'),
                        _this = e.data._this;
                    _this.current_state = 1;
                    $target.fadeIn(200);
                    $('#toggleTour').html('Show Tour');
                    _this.runPopup($target);
                    $('#app').addClass('hide-project-tour').removeClass('project-tour');
                });

            })
            $('#startTutorial').click({_this: this},function(e){
                $('.tutorial-banner-wrapper').fadeOut("slow", function(){
                    var $target = $('.numberCircle[data-number="1"]'),
                        _this = e.data._this;
                    $target.fadeIn(200);
                    _this.current_state = 1;
                    _this.runPopup($target);
                    _this.toggleTour();
                });

            })
            /*
            $('.numberCircle').click({_this: this},function(e){
                var _this = e.data._this,
                    $target = $(e.target);
                    if(!$target.hasClass('.numberCircle')){
                        $target = $target.closest('.numberCircle');
                    }
                    alert(4);
                    console.log('target on lcick',$target);
                if(!$target.hasClass('active')){
                    $('.tooltip-wrapper.tour').remove();
                    _this.current_state = $target.data('number');
                    _this.runPopup($target);
                }
            }).
    */
            //$('#app').addClass('project-tour');
            $('.arrow.left').click({_this: this},function(e){
                var _this = e.data._this;
                if(_this.current_state > 1){
                    _this.current_state--;
                }
                _this.checkState();
            })
            $('.arrow.right').click({_this: this},function(e){
                var _this = e.data._this;
                if(_this.current_state < 10){
                    _this.current_state++;
                }
                _this.checkState();
            });
            $('#toggleTour').click({_this: this},function(e){
                var _this = e.data._this;
                _this.toggleTour();
            })
        }
    },
    addPopup: function($circleNumber){
        console.log('___________________________________>>>>>>   circleNumber',$($circleNumber)[0]);
     //   if(_this.current_state > 1){
        var placement = this[this.tourType].states[this.current_state].placement;
        if( !placement ){
            placement = 'top';
        }
        console.log('placement:::::::::', placement);
        var $html = '<i class="active tour tooltip-wrapper"><div class="placement ' + placement + '"><i class="arrow left"></i><div class="content"><h3></h3><p></p><div id="nav_wrapper"><a class="previousState">Previous</a><a class="nextState">Next</a></div></div><i class="arrow right"></i><i class="pointer pointer-one"></i></div></i>';
        console.log('HTML:::::::::', $html);
        console.log('circleNumber:::::::::', $circleNumber);
        $circleNumber.find('i.tour').remove();
        $($html).appendTo($circleNumber);
        $circleNumber.find('.previousState').click({_this: this},function(e){
            e.stopPropagation();
            console.log('not toggling to the next circleNumber');
            var _this = e.data._this;
            var $target = $(e.target);
//            $('.numberCircle.active').remove();
            _this.beforeStateChange();
            console.log('target1::' , $target[0]);
            if(!$target.hasClass('.numberCircle')){
                $target = $($target).closest('.numberCircle');
            console.log('target2::' , $target);
            }
            var targetNumber = parseInt($target.data('number')),
                newTargetNumber = parseInt(targetNumber - 1),
                $newTarget = $('.numberCircle[data-number="' + newTargetNumber + '"]');
            _this.current_state = newTargetNumber;
            console.log('targetnumber::' + targetNumber);
            console.log('target on prevstate click::' + newTargetNumber);
            console.log( $newTarget.parent());
//            $('.tour').remove();
            _this.runPopup($newTarget);

        })
  //      console.log('nextstae',$('#nextState'));
        $circleNumber.find('.nextState').click({_this: this},function(e){
            e.stopPropagation();
            console.log('not toggling to the next circleNumber');
            var _this = e.data._this;
            var $target = $(e.target);
            $('.numberCircle.active').removeClass('active').hide();
            $('.tour.tooltip-wrapper.active').removeClass('active').hide();
            _this.beforeStateChange();
            console.log('target1::' , $target[0]);
            if(!$target.hasClass('.numberCircle')){
                $target = $($target).closest('.numberCircle');
            console.log('target2::' , $target);
            }
            var targetNumber = parseInt($target.data('number')),
                newTargetNumber = parseInt(targetNumber + 1),
                $newTarget = $('.numberCircle[data-number="' + newTargetNumber + '"]');
            _this.current_state = newTargetNumber;
            console.log('targetnumber::' + targetNumber);
            console.log('target on prevstate click::' + newTargetNumber);
            console.log( $newTarget.parent());
//            $('.tour').remove();
            _this.runPopup($newTarget);
        })

    },
    checkState: function(){
        var currentState = this[this.tourType].states[this.current_state];
    },
    pointLeft: function(){
        $('.tooltip-wrapper .placement').removeClass('point-right').removeClass('point-down').addClass('point-left');
    },
    pointRight: function(){
        $('.tooltip-wrapper .placement').removeClass('point-left').removeClass('point-down').addClass('point-right');
    },
    pointDown: function(){
        $('.tooltip-wrapper .placement').removeClass('point-right').removeClass('point-left').addClass('point-down');
    }
};
$( document ).ready(function() {
    // tutorial ::
    $(document).click(function(e) {
        if(!$(e.target).hasClass('numberCircle') && !$(e.target).parent().hasClass('numberCircle') && !$(e.target).hasClass('task')){
           // $('.tooltip-wrapper.tour').hide();
        }
    });
    // end tutorial
    $('#userToggle').on('click',function(e){
        e.preventDefault();
        $('body').addClass('open-user-select');
        toggleSidebar();
    })
$( "#setPivot" ).datepicker();
    $('#setPivotLabel').click(function(){
        $( "#setPivot" ).datepicker('show');
    });    
    $('#setPivot').change(function(){
        toggleSidebar(null,true);
        ReactDOM.unmountComponentAtNode(document.getElementById('project-container'));
        ReactDOM.render(<App />, document.getElementById('project-container'));
    })
    $('#updateTask .close').click(function(){
        toggleSidebar(null,true,true);
    })
    $('#updateTask .cancel-add-new').click(function(){
        alert('cancel taskreact  3757');
        toggleSidebar(null,true,true);
    })
    styleCalendar();
    $( window ).resize(function() {
        styleCalendar();
    });
    if( $('#app').hasClass('project-edit') ){
        toggleEditMode();
    }
})
