
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */
require('./bootstrap');

import $ from 'jquery';
window.$ = window.jQuery = $;

import 'jquery-ui/ui/widgets/datepicker.js';
import 'jquery-ui/ui/widgets/autocomplete.js';
import {testMessage,manageTaskMove, manageMoveTaskToggle,manageUserAssign,manageGroupAssign,manageOwnerAssign,toggleEditMode,openEditTask,openSettings,saveProject,toggleTaskEdit} from './utilities.js';
/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

require('./task-react'); //test module
require('./timeline-react');

require('../../../packages/joinery/groups/src/assets/js/groups-react');
require('../../../packages/joinery/projects/src/assets/js/projects-react');
require('../../../packages/joinery/tasks/src/assets/js/tasks-react');
//var Calendar = require('tui-calendar'); /* CommonJS */
//require("tui-calendar/dist/tui-calendar.css");

import Calendar from '@toast-ui/calendar';
import '@toast-ui/calendar/dist/toastui-calendar.min.css';

// If you use the default popups, use this.
require("tui-date-picker/dist/tui-date-picker.css");
require("tui-time-picker/dist/tui-time-picker.css");

    
 window.autoExpand = function (field) {
        // Reset field height
        field.style.height = 'inherit';
        var computed = window.getComputedStyle(field);
        // Calculate the height
        /*
        console.log('FIELD',field);
        console.log("parseInt(computed.getPropertyValue('border-top-width'), 10)", parseInt(computed.getPropertyValue('border-top-width') ) );
        console.log("computed.getPropertyValue('padding-top'), 10", computed.getPropertyValue('padding-top') );
        console.log( 'field.scrollHeight', field.scrollHeight);
        console.log("parseInt(computed.getPropertyValue('padding-bottom'), 10)", parseInt(computed.getPropertyValue('padding-bottom'), 10) );
        console.log("parseInt(computed.getPropertyValue('border-bottom-width'), 10)", parseInt(computed.getPropertyValue('border-bottom-width'), 10));
        */
        var height = parseInt(computed.getPropertyValue('border-top-width'), 10)
                    //+ parseInt(computed.getPropertyValue('padding-top'), 10)
                     + field.scrollHeight
                     + parseInt(computed.getPropertyValue('padding-bottom'), 10)
                     + parseInt(computed.getPropertyValue('border-bottom-width'), 10);
        field.style.height = (height   ) + 'px';

    };
    window.EventCounter = 0;
    window.createdEvents = [];
function updateBreak(calBreak){
	var event_id = event.id;
	for (let i = 0; i < window.createdEvents.length; i++) {
		console.log('iterating: ', window.createdEvents[i] );
		if( event.id == window.createdEvents[i].id ){
			event_id = window.createdEvents[i].laravel_id;
		}
	}		
	
	var schedule = {
		id: event_id
	}

	$.ajax({
		url : '/api/breaks/update?api_token=' + api_token,
		type : 'POST',
		data : calBreak,
		dataType:'json',
		success : function(data) {  
			console.log('success',data);
			console.log('success',data);
		},
		error : function(request,error){
			console.log('update date',calBreak);
			console.log('update request',request);
			console.log("Request: "+JSON.stringify(request));
		}
	});

}

function createBreak(calBreak, calendar){
	console.log('calBreak',calBreak);
	$.ajax({
		url : '/api/breaks/new?api_token=' + api_token,
		type : 'POST',
		data : calBreak,
		dataType:'json',
		success : function(data) {  
			var new_id = data.id;
    	var event_id = calBreak.id;
	    //calendar.createSchedules([schedule]);
//		calendar.updateEvent(event_id, 1, {
//		    id: new_id
//		});
		calBreak.id = new_id;
console.log('event_id:', event_id);
console.log('new-id:', new_id);
var block = $('[data-event-id=1]');
block.data('event-id', new_id);
console.log('block',block[0]);
console.log('block', block.data('event-id') );
			console.log('success - new break data',data);
			console.log('success - new break calbreak',calBreak);
			calendar.clearGridSelections();
			var createdEvent = {
				'id' : event_id,
				'laravel_id' : new_id

			};
	        window.createdEvents.push( createdEvent );
	    var schedule = {
	        id: new_id,
	        calendarId: '1',
	        title: data.title,
	        category: 'time',
	        dueDateClass: '',
	        start: calBreak.start,
	        end: calBreak.end
		}

	        	    calendar.createEvents([schedule]);
		
	        console.log( 'window.createdEvents', window.createdEvents  );
		},
		error : function(request,error){
			console.log("Request new break: "+JSON.stringify(request));
		}
	});
}
	var months = {
		0: 'Jan',
		1: 'Feb',
		2: 'Mar',
		3: 'Apr',
		4: 'May',
		5: 'Jun',
		6: 'Jul',
		7: 'Aug',
		8: 'Sep',
		9: 'Oct',
		10: 'Nov',
		11: 'Dec'
	}

function setRenderRange(centerDate){
	if(!centerDate){
		centerDate = new Date();
	}
	var month =	centerDate.getMonth(),
	year = centerDate.getFullYear();

	if (centerDate.getMonth() == 11) {
	    var next = new Date(centerDate.getFullYear() + 1, 0, 1);
	    var prev = new Date(centerDate.getFullYear(), centerDate.getMonth() - 1, 1);
	} else {
	    var next = new Date(centerDate.getFullYear(), centerDate.getMonth() + 1, 1);
	}			
    var prev = new Date(centerDate.getFullYear(), centerDate.getMonth() - 1, 1);
	var nextMonth = next.getMonth();
	var prevMonth = prev.getMonth();

$('.move-day[data-action="move-next"]').html(months[nextMonth] + ' ' + next.getFullYear() );	
$('.move-day[data-action="move-prev"]').html(months[prevMonth] + ' ' + prev.getFullYear() );	
	$('#renderRange').html( months[month] + ' ' + year);

}
function deleteSchedule(event, calendar){
	var api_token = $("meta[name='api_token']").attr("content");
	var event_id = event.id;
	for (let i = 0; i < window.createdEvents.length; i++) {
		console.log('iterating: ', window.createdEvents[i] );
		if( event.id == window.createdEvents[i].id ){
			event_id = window.createdEvents[i].laravel_id;
		}
	}		
	
	var schedule = {
		id: event_id
	}
	calendar.deleteEvent(event.id, event.calendarId);	
	$.ajax({
		url : '/api/breaks/delete?api_token=' + api_token,
		type : 'POST',
		data : schedule,
		success : function(data) {  
			console.log('success',data);
		},
		error : function(request,error){
			console.log("Request: "+JSON.stringify(request));
		}
	});

}
function createSchedule(event, calendar, update){
	window.EventCounter++;
    var eventID = window.EventCounter;
	if( update ){
	}
	var data = {};
    var startTime = event.start;
    var endTime = event.end;
    var isAllDay = event.isAllDay;
    var guide = event.guide;
    var triggerEventName = event.triggerEventName;
    var schedule;
	if(startTime > endTime){
		endTime = startTime;
	}
    if (triggerEventName === 'click') {
        // open writing simple schedule popup
//        schedule = {...};
    } else if (triggerEventName === 'dblclick') {
        // open writing detail schedule popup
  //      schedule = {...};
    }
    if( event.event ){
    	schedule = event.event;
		calBreak = {
			title : schedule.title,
			start : schedule.start.d.d,
			end : schedule.end.d.d
		};
			console.log('initial calBreak set::', calBreak);
		if( 'undefined' != typeof event.changes ){
			console.log('has changes' , event);
			if( 'undefined' != typeof event.changes.title ){
			    calBreak.title = event.changes.title; 
			}
			if( 'undefined' != typeof event.changes.start ){
			    calBreak.start = event.changes.start.d.d; 
			}
			if( 'undefined' != typeof event.changes.end ){
			    calBreak.end = event.changes.end.d.d; 
			}			
		}

			console.log('before send calBreak set::', calBreak);
		calendar.updateEvent(schedule.id, schedule.calendarId, {
		    title: calBreak.title,
		    start: calBreak.start,
		    end: calBreak.end
		});
	    data.id = schedule.id;
		data.schedule = calBreak; 
	    updateBreak(data);
    }    
    else{
	    schedule = {
	        id: eventID,
	        calendarId: '1',
	        title: event.title,
	        category: 'time',
	        dueDateClass: '',
	        start: startTime,
	        end: endTime
		}
	    // ajax to create break
	    var calBreak = schedule;
	    calBreak.project = ( 'null' == $('#cloned-break-project-filter option:selected').val() ) ? '' : $('#cloned-break-project-filter option:selected').val();
	    calBreak.breakType ='user';
	    calBreak.group = '';
	    if('clonedByGroup' == $('[name="clonedBreakType"]:checked').attr('id') ){
	    	calBreak.breakType = 'group';
	    	calBreak.user = ('null' == $('#cloned-break-user-filter option:selected').val() ) ? '' : $('#cloned-break-user-filter option:selected').val(); 
	    	calBreak.group = $('#cloned-break-current-group-id').val();
	    }
	    else{
	    	calBreak.user = $('#cloned-break-current-user-id').val();
	    	if(!calBreak.user){
		    	calBreak.user = $('#break-current-user-id').val();
	    	}
	    	calBreak.group = $('#cloned-break-current-group-id').val();
	    	if(!calBreak.group){
		    	calBreak.group = $('#break-current-group-id').val();
		    }
	    }
	    calBreak.public = 0;
	    if( $('#cloned-break-public').prop("checked") == true){
		    calBreak.public = 1;
	    }
	    calBreak.start = schedule.start.d.d; 
	    calBreak.end = schedule.end.d.d; 
	    console.log('calbreak on new creation', calBreak);
	    createBreak(calBreak,calendar);

	}

}
if('undefined' != typeof buildCalendar && buildCalendar){
    var api_token = $("meta[name='api_token']").attr("content");
	$.ajax({
		url : '/api/breaks?api_token=' + api_token,
		type : 'POST',
		data : {
			'ownerType' : 'user',
			'owner' : 'auth',
			'types' : [
			]
		},
		dataType:'json',
		success : function(data) {  
			var breaks = data;
console.log('breaks',breaks);
			const templates = {
			    popupIsAllDay: function() {
			      return 'All Day';
			    },
			    popupStateFree: function() {
			      return 'Free';
			    },
			    popupStateBusy: function() {
			      return 'Busy';
			    },
			    titlePlaceholder: function() {
			      return 'Name of Break ';
			    },
			    locationPlaceholder: function() {
			      return 'Location';
			    },
			    startDatePlaceholder: function() {
			      return 'Start date';
			    },
			    endDatePlaceholder: function() {
			      return 'End date';
			    },
			    popupSave: function() {
			      return 'Save';
			    },
			    popupUpdate: function() {
			      return 'Update';
			    },
			    popupDetailLocation: function(schedule) {
			    	console.log('schedule',schedule);
			      return 'Location : ' + schedule.location;
			    },
			    popupDetailUser: function(schedule) {
			      return 'User : ' + (schedule.attendees || []).join(', ');
			    },
			    popupDetailState: function(schedule) {
			    				    	console.log('schedule',schedule);

			      return 'State : ' + schedule.state || 'Busy';
			    },
			    popupDetailRepeat: function(schedule) {
			      return 'Repeat : ' + schedule.recurrenceRule;
			    },
			    popupDetailBody: function(schedule) {
			      return 'Body : ' + schedule.body;
			    },
			    popupEdit: function() {
			      return 'Edit';
			    },
			    popupDelete: function() {
			      return 'Delete';
			    },
				time: function(schedule) {
			      return schedule.title;
			    }			    
			}
			const voidtemplates = {
			    popupDetailDate: function(isAllDay, start, end) {
			      /*
			      var isSameDate = moment(start).isSame(end);
			      var endFormat = (isSameDate ? '' : 'YYYY.MM.DD ') + 'hh:mm a';

			      if (isAllDay) {
			        return moment(start).format('YYYY.MM.DD') + (isSameDate ? '' : ' - ' + moment(end).format('YYYY.MM.DD'));
			      }

			      return (moment(start).format('YYYY.MM.DD hh:mm a') + ' - ' + moment(end).format(endFormat));
			      */
			      return start;
			    }

			};
		  	var calTheme = {
//		  		'common.border': '0',
//		  		'common.backgroundColor': 'red'

		  	}
			var calendar = new Calendar('#calendar', {
				id: 1,
				name:'Breaks',
				 week: {
				    taskView: ['task'],
				    eventView: ['allday'],
				  },				
				defaultView: 'month',
 				useFormPopup: true,
        		useDetailPopup: true,
        		template: templates,
				theme: calTheme
			});
			$('.move-day').on('click',function(e){
				var thisAction = $(e.target).data('action');
				if('move-next' == thisAction){
					calendar.next();
				}
				else if('move-prev' == thisAction){
					calendar.prev();
					console.log(calendar._renderDate._date.getMonth());
				}
				else{
					calendar.setDate(new Date());
				}
				var thisDate = calendar._renderDate._date,
					month = thisDate.getMonth(),
					year = thisDate.getFullYear();
					setRenderRange(thisDate);
			});
			var schedules = [],
				bgColors = [
					'e7b83c',
					'1abc9c',
					'009bff',
					'9e5bd9',
					'ff7e38'
				];
			var colori = 0;
			for (var i = 0; i < breaks.length; i++) { 
				//console.log('BREAK', breaks[i].title);
				var color = bgColors[colori];
				if( 'undefined' == typeof bgColors[colori] ){
					colori = 0;
					var color = bgColors[colori];
				}
				else{
					colori++;
				}
			    var schedule = {
			        id: breaks[i].id,
			        calendarId: '1',
			        title: breaks[i].title,
			        //category: 'time',
			        //dueDateClass: '',
			        start: breaks[i].startDate,
			        end: breaks[i].endDate,
			        //isAllDay: true,
			       // bgColor: '#' + color,
			        //color: 'white'
			    };
				schedules.push(schedule);
			} 
			calendar.createEvents(schedules);
			var today = new Date();
			setRenderRange(today);
			window.calendarSetup = 0;
			
			$('#calendar').bind('DOMSubtreeModified', function(e) {
//				if( $(e.target).hasClass('tui-timepicker-minute') && $('.toastui-calendar-popup-container .toastui-calendar-form-container').length > 0 ){
				if( !window.calendarSetup && $(e.target).hasClass('tui-timepicker-minute') && $('.toastui-calendar-popup-container').length > 0 ){
					//check if popup is fully built
					console.log('dom change');
//					if( $('.toastui-calendar-button.toastui-calendar-dropdown-button.toastui-calendar-popup-section-item') ){
					if( $('.toastui-calendar-popup.button.toastui-calendar-popup-section-item') ){
			    		$('.toastui-calendar-hide').remove();
			    		$('.toastui-calendar-popup-section-item.toastui-calendar-popup-section-title input').data('has-default',1);
			    		if(!$('#toastui-calendar-section-title input').val()){
				    		$('#toastui-calendar-section-title input').val('Break');
				    	}
				    	if( !window.calendarSetup ){
				    		$('.toastui-calendar-popup-section-item.toastui-calendar-popup-section-title input').bind('focus',function(){
								if( $('.toastui-calendar-popup-section-item.toastui-calendar-popup-section-title input').data('has-default') ){
						    		$('.toastui-calendar-popup-section-item.toastui-calendar-popup-section-title input').val('');
						    		$('.toastui-calendar-popup-section-item.toastui-calendar-popup-section-title input').data('has-default',0);
						    		$('.toastui-calendar-popup-section-item.toastui-calendar-popup-section-title input').parent('.toastui-calendar-section-title').addClass('empty-input');
						    	}
						    	else if( !$('.toastui-calendar-popup-section-item.toastui-calendar-popup-section-title input').val() ){
						    		$('.toastui-calendar-popup-section-item.toastui-calendar-popup-section-title input').val('');
						    		$('.toastui-calendar-popup-section-item.toastui-calendar-popup-section-title input').parent('.toastui-calendar-section-title').addClass('empty-input');
						    	}
						    	else{
						    		$('.toastui-calendar-popup-section-item.toastui-calendar-popup-section-title input').parent('.toastui-calendar-section-title').removeClass('empty-input');
						    	}
				    		});	
				    		
				    		$('.toastui-calendar-floating-layer').bind('click',function(e){
				    			var $target = $(e.target);
				    			if( $(e.target).closest('.toastui-calendar-popup-container').length > 0 ){

				    			}
				    			else{
					    			if( $target.hasClass('toastui-calendar-floating-layer') ){
						    			$(e.target).remove();
					    			}
					    			else{
						    			if( $(e.target).closest('.toastui-calendar-popup-container').length > 0 ){
							    			$(e.target).closest('.toastui-calendar-floating-layer').css('display','none');
							    		}
					    			}
					    		}
					    		
				    		})
				    	
				    		$('.toastui-calendar-popup-section-item.toastui-calendar-popup-section-title input').bind('keypress',function(){
								if( !$('.toastui-calendar-popup-section-item.toastui-calendar-popup-section-title input').data('has-default') ){
							    	if( !$('.toastui-calendar-popup-section-item.toastui-calendar-popup-section-title input').val() ){
							    		$('.toastui-calendar-popup-section-item.toastui-calendar-popup-section-title input').val('');
							    		$('.toastui-calendar-popup-section-item.toastui-calendar-popup-section-title input').parent('.toastui-calendar-section-title').addClass('empty-input');
							    	}
							    	else{
							    		$('.toastui-calendar-popup-section-item.toastui-calendar-popup-section-title input').parent('.toastui-calendar-section-title').removeClass('empty-input');
							    	}
								}
				    		});
				    	}
						window.calendarSetup = 1;
			    		$('.toastui-calendar-popup-section-item.toastui-calendar-section-location').parent('.toastui-calendar-popup-section').hide();
			    		$('.toastui-calendar-popup-section.toastui-calendar-dropdown.toastui-calendar-close.toastui-calendar-section-state').hide();
			    		if( $('#traverse-custom-popup-title').length < 1 && $('.toastui-calendar-popup-container #cloned-break-filters').length < 1){
			    			console.log('clone the stuff');
				    		$('.toastui-calendar-popup .toastui-calendar-popup-container').prepend('<div id="traverse-custom-popup-title">Create Blocked Time</div>');
				    		var $filterClone = $('#break-filters').clone();
				    		//$('#break-filters').remove();
				    		$filterClone.attr('id','cloned-break-filters').show();
				    		
				    		$filterClone.find('#break-project-filter').attr('id','cloned-break-project-filter');
				    		$filterClone.find('#break-user-filter').attr('id','cloned-break-user-filter');
				    		$filterClone.find('#byUser').attr('id','clonedByUser');
				    		$filterClone.find('#byGroup').attr('id','clonedByGroup');
				    		$filterClone.find('#break-public').attr('id','cloned-break-public').attr('name','cloned-break-public');
				    		$filterClone.find('[for="break-public"]').attr('for','cloned-break-public');
				    		$filterClone.find('#break-user-filter-wrapper').attr('id','cloned-break-user-filter-wrapper');
				    		$filterClone.find('#break-current-user-id').attr('id','cloned-break-current-user-id').attr('name','cloned-break-current-user-id');
				    		$filterClone.find('#break-current-group-id').attr('id','cloned-break-current-group-id').attr('name','cloned-break-current-group-id');
				    		$filterClone.find('[for="byGroup"]').attr('for','clonedByGroup');
				    		$filterClone.find('[for="byUser"]').attr('for','clonedByUser');
				    		$filterClone.find('[name="breakType"]').attr('name','clonedBreakType').on('change',function(){
				    			var breakType = $filterClone.find('[name="clonedBreakType"]:checked').attr('id');
				    			if('clonedByGroup' == breakType){
				    				$('#cloned-break-user-filter-wrapper').show();
				    			}
				    			else{
				    				$('#cloned-break-user-filter-wrapper').hide();
				    				$('#cloned-break-user-filter').val('All Users');				    				
				    			}
				    		});
				    		$filterClone.insertAfter( $('.toastui-calendar-state-section') );
				    		
//				    		$('.toastui-calendar-popup .toastui-calendar-popup-container').append( $filterClone );
			    	//	$('#calendar').data('popup-modified',1);
				    	}
					}
		    	}
			});
			
			calendar.on('beforeCreateEvent', function(event) {
				console.log('HOOOK BEFORE CREATE:::::::::::',event);
				createSchedule(event, calendar);
			});	
			//calendar.deleteSchedule(schedule.id, schedule.calendarId);
			calendar.on('beforeUpdateEvent', function(event) {
				console.log('HOOOK UPDATE:::::::::::',event);
				createSchedule(event, calendar, true);
			});
			calendar.on('afterRenderEvent', function(event) {
				console.log('schedules', schedules);
				console.log('after render:::::::::::',event);
				// createSchedule(event, calendar, true);
			});				
			calendar.on('beforeDeleteEvent', function(event) {
				console.log('deleteschedule');
				deleteSchedule(event, calendar);
			});
		},
		error : function(request,error){
			console.log("Request: "+JSON.stringify(request));
		}
	});
}
jQuery( document ).ready(function($) {

	$('#save-project').on('click', function(){ 
		alert('click')
		saveProject();
	});
	$('.has-prompt').focus(function(e){
		$(e.target).siblings('.form-prompt').addClass('active');
	})
	$('.has-prompt').focusout(function(e){
		$(e.target).siblings('.form-prompt').removeClass('active');
	})

        $('#start-label, #end-label').on('click', function(e){
            var $target = $(e.target);
            console.log('click target', $target);
            if( 'end-label' == $target.attr('id') ){
                $('#task-start-date-error-messages').removeClass('show');
                $('#task-end-date-error-messages').toggleClass('show');
            }
            if( 'start-label' == $target.attr('id') ){
                $('#task-end-date-error-messages').removeClass('show');
                $('#task-start-date-error-messages').toggleClass('show');
            }
        })

	$('#searchExisting').change(function() {
	    var ischecked= $(this).is(':checked');
	    if(!ischecked){
	    	$('#existingGroup_wrapper').removeClass('active');
	    }
	    else{
	    	$('#existingGroup_wrapper').addClass('active');	    	
	    }
	}); 

	$('[name="accountToggle"]').on('change',function(){
		if($('#groupAccount').is(':checked')){
			$('#existingGroup_wrapper').show();
			$('#groupOption').show();
			$('#group').val('');
			$('#partnerGroup').val('');
			$('#accountType').val('group');
		}
		else{
			$('#existingGroup_wrapper').hide();
			$('#groupOption').hide();
			$('#existingGroup').prop("checked", true);
			$('#existingGroup_wrapper').hide();
			$('#newGroup_wrapper').hide();
			$('#accountType').val('individual');
		}
	})

	// expanding input::
	document.addEventListener('input', function (event) {
		if ( !$(event.target).hasClass('expanding-textarea') ){
			return;
		} 
		autoExpand(event.target);
	}, false);
	$.each($('.expanding-textarea').not('#form-task-title'), function(k,v){
		console.log('v',v);
		autoExpand(v);
	})
// end expanding input
	$('#partnerPrevious').on('change',function(){
		var val = $('#partnerPrevious').children("option:selected").val();
		$('#group_id').val(val);
	});
	$('[name="groupType"]').on('change',function(){
		if($('#newGroup').is(':checked')){
			$('#existingGroup_wrapper').hide();
			$('#newGroup_wrapper').show();
			$('#group').val('');
			$('#partnerGroup').val('');
			$('#group_id').val('');

			//$('#group').val('');
			//$('#accountType').val('group');
		}
		else{
			$('#existingGroup_wrapper').show();
			$('#newGroup_wrapper').hide();
//			$('#group_wrapper').hide();
//			$('#accountType').val('individual');
		}
	})
	$('[name="searchType"]').on('change',function(){
		if( $('#searchName').is(':checked') ){
			$('#adminEmail').hide();
			$('#partnerPrevious').hide();
			$('#partnerGroup').show();
			$('#adminEmail').val('');
			$('#partnerPrevious').val('null');
		}
		else if( $('#searchEmail').is(':checked') ){
			$('#adminEmail').show();
			$('#partnerGroup').hide();
			$('#partnerPrevious').hide();
			$('#partnerGroup').val('');
			$('#partnerPrevious').val('null');
		}
		else{
			$('#adminEmail').hide();
			$('#partnerGroup').hide();
			$('#partnerPrevious').show();
			$('#adminEmail').val('');
		}
	})
	$( function() {
		$( "#group" ).autocomplete({
			source: window.groups
		}).on( "autocompletechange", function( event, ui ) {
			console.log(event, ui);
			var val = "";
			if(ui.item){
				val = ui.item.id;
			}
			$("#group_id").val(val);
			console.log('val:'+ $('#group_id').val());
		});
	});

	$( function() {
		var project_id = $('#project').data('project_id');
		$( "#partnerGroup" ).autocomplete({
			source: function( request, response ) {
				$.ajax({
					url: "/api/team-complete/" + request.term + "/" + project_id,
					dataType: "json",
					data: {
						search: request.term
					},
					success: function( data ) {
						response( data );
					}
				});
			},
			select: function (event, ui) {
//				$('#group-label').html('Team: ' + ui.item.label ); // display the selected text	
				if('invited' == ui.item.value){
					$('#partnerGroup').val('');
					$('#group_id').val(''); 					
				}	
				else{		
					$('#partnerGroup').val(ui.item.label); // display the selected text
					$('#group_id').val(ui.item.value); // save selected id to input
				}
				return false;
			}
		});
	});
	if(openSettings){
		//openSettings();
	}
	$( "#task-edit-toggle" ).on('click', function(e){
		var $target = $(e.target), 
			task_id = $target.data('id'),
			task = window.appVars.tasks[task_id];
		manageOwnerAssign(task);
//     $('#user-assign').hide();                        

		toggleTaskEdit();
		openEditTask(e);
	})
	
	$( "#settings-toggle" ).on('click', function(e){
		openEditTask(e);
	})
	$('#rotate-screen').on('click', function(){
		if($('html').hasClass('rotate')){
			$('html').removeClass('rotate');
			$('#rotate-screen').html('Switch to Landscape Mode');
		}
		else{
			$('html').addClass('rotate');
			$('#rotate-screen').html('Switch to Portrait Mode');
		}
	})
	function detachPartner( e ){
	var api_token = $("meta[name='api_token']").attr("content"),
		project_id = $('#project').data('project_id'),
		group_id = $(e.target).data('group-id');
console.log('remove partner from project', project_id);
var dataObject = {
				'project_id' : project_id,
				'group_id' : group_id
			};
			console.log('dataObject::',dataObject);
		$.ajax({
			url : '/api/projects/detachPartner?api_token=' + api_token,
			type : 'POST',
			data : dataObject,
			dataType:'json',
			success : function(data) {              
				console.log('Data: ',data);
				$(e.target).parent('li').remove();
				var groupName = $('select[name="group-assign"] option[value="' + group_id + '"]').html();
				$('select[name="group-assign"] option[value="' + group_id + '"]').remove();
				$('select[name="user-assign"] option[data-group-id="' + group_id + '"]').remove();
				$('select#partnerPrevious').append($('<option>', {value: group_id, text: groupName}));
				if( $('ul.manage-partners li').length < 1 ){
					$('.existing-partners-wrapper').hide();
				}
			},
			error : function(request,error){
				console.log("Requestssss: "+JSON.stringify(request));
			}
		});		
	}
	$( ".manage-partners button.detachPartner" ).on('click', function(e){
		detachPartner(e);
	});
	function addPartner(){
		var api_token = $("meta[name='api_token']").attr("content"),
			$groupForm = $('#partnerModal'),
			project_id = $('#project').data('project_id');
var searchType = $('input[name="searchType"]:checked').attr('id');
			var groupType = "existing",
				group_id = $groupForm.find('#group_id').val(),
				admin_email = null;
				/*
		if( 1 != 1 ){
			var groupType = "existing",
				group_id = $groupForm.find('#group_id').val(),
				admin_email = null;
		}
		else{
			var groupType = "new",
				group_id = null,
				admin_email = $groupForm.find('#newGroup').val();
				// set invite data-allow-new to 0
				//$('button#projectPartner').data('allow-new',0);
			$.ajax({
				url: "/api/team-admin-complete/" + admin_email,
				dataType: "json",
				data: {
					search: admin_email
				},
				success: function( data ) {
					console.log( 'admin_email::', data );
				}
			});
		}
		
*/
		console.log( 'allow-new::', $('button#projectPartner'));
		var dataObject = {
					'project_id' : project_id,
					'group_id' : group_id,
					'groupType' : groupType,
					'admin_email' : admin_email
				};
				console.log('dataObject::',dataObject);
			$.ajax({
				url : '/api/projects/addPartner?api_token=' + api_token,
				type : 'POST',
				data : dataObject,
				dataType:'json',
				success : function(data) {              
					console.log('Data: ',data);
					$('.existing-partners-wrapper').show();
					var groupName = $('select#partnerPrevious option[value="' + group_id + '"]').html();
					$('ul.manage-partners').append($('<li><button title="Remove Partner From Project" class="delete-btn detachPartner" data-group-id="' + group_id + '">x</button>' + groupName + '</li>'));
					$( '.manage-partners button.detachPartner[data-group-id="' + group_id + '"]').on('click', function(e){
						detachPartner(e);
					});
					var data_group_type = 'group';
					if( data.group && data.group.account && ( 'Individual Trial' == data.group.account.name || 'Individual' == data.group.account.name ) ){
						data_group_type = 'individual';
					}
					$('select[name="group-assign"]').append($('<option data-type="' + data_group_type + '" value="' + group_id + '">' + groupName + '</option>' ));
					$( 'select[name="group-assign"] option[value="' + group_id + '"]').on('click', function(e){
						addPartner();
					});
					//add new group's users to user_assign select
					if( 'group' == data_group_type && data.group && data.group.users ){
						$.each( data.group.users, function(k,user){
							console.log('new group user::',user);
							$('select[name="user-assign"]').append($('<option data-group-id="' + group_id + '" value="' + user.id + '" id="task-assign-user-' + user.id + '" style="display:none">' + user.name + '</option>' ));
						})

					}
					$('select#partnerPrevious option[value="' + group_id + '"]').remove();
				},
				error : function(request,error){
					console.log("Requestssss: "+JSON.stringify(request));
				}
			});
		
	}
	$( "#partnerPrevious,#partnerGroup" ).on('change', function(){
		addPartner();
	});

	$( "#adminEmail" ).on('focus',function(){
		$('#partnerGroup').val('');
//		$('#group-label').html('Team: ');
	})
	$( "#partnerGroup" ).on('focus',function(){
		$('#adminEmail').val('');
//		$('#group-label').html('Team: ');
	})
	$('.tr-href').on('click',function(e){
		window.location.replace( $(this).data('href') );
	})
 $('#confirmDelete').on('show.bs.modal', function (e) {
    var message = $(e.relatedTarget).attr('data-message');
    var title = $(e.relatedTarget).attr('data-title');
    var form = $(e.relatedTarget).closest('form');
    $(this).find('.modal-body p').text(message);
    $(this).find('.modal-title').text(title);
    $(this).find('.modal-footer #confirm').data('form', form);
  });
  $('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
      $(this).data('form').submit();
  });
	$( function() {
		$( "#adminEmail" ).autocomplete({
			source: function( request, response ) {
				$.ajax({
					url: "/api/team-admin-complete/" + request.term,
					dataType: "json",
					data: {
						search: request.term
					},
					success: function( data ) {
						response( data );
					}
				});
			},
			select: function (event, ui) {
				//$('#group-label').html('Team: ' + ui.item.label ); // display the selected text
				$('#adminEmail').val(ui.item.label); // display the selected text
				$('#group_id').val(ui.item.value); // save selected id to input
				return false;
			}
		});
	});

});

