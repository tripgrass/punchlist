import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class Timeline extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tasks: []
        };
        axios
            .post('/api/taskTree')
            .then(response => {
                console.log('from submit', response);
                this.setState({
                     tasks: [...response.data.data]
                })
            });
        this.handleSearch = this.handleSearch.bind(this);            
    }
    handleSearch(e){
        e.preventDefault();
        var _searchTerm = $('#task_search_box').val();
         axios
            .post('api/search-tasks', {
                searchTerm: _searchTerm 
              })
            .then(response => {
                console.log('from search submit', response);
                this.setState({
                     tasks: [...response.data]
                })
            });

    }
    render() {
        return (
        <div className="project-container">
            <div className="deliverable-container">
                <div className="first-tasks" style={{ width:'71.5%' }}>
                    <div className="thread-container" id="thread-two" style={{height:'33%'}} data-days="14">
                        <div className="task" id="task-3" style={{width:'60%', backgroundColor:'teal'}}>
                            6 days - Task Three
                        </div>
                        <div className="task" id="task-2" style={{width:'40%', backgroundColor:'red'}}>
                            4 days - Task Four
                        </div>
                        <div className="thread-control">
                            T-2
                        </div>
                    </div>
                    <div className="thread-container" id="thread-three" style={{height:'33%'}}>
                        <div className="task" id="task-3" style={{width:'40%', backgroundColor:'lime'}}>
                            4 days - Task Five
                        </div>
                        <div className="thread-container" id="thread-four" style={{height:'50%', width:'60%'}} data-days="6">
                            <div className="task" id="task-3" style={{width:'100%', backgroundColor:'teal'}}>
                                6 days - Task Six
                            </div>
                            <div className="thread-control">
                                T-4
                            </div>

                        </div>
                        <div className="thread-container" id="thread-four" style={{width:'60%', height:'50%'}} data-days="6">
                            <div className="task" id="task-2" style={{width:'100%', backgroundColor:'red'}}>
                                6 days - Task 7
                            </div>
                        </div>
                    </div>
                    <div className="thread-container" id="thread-one" data-days="2" style={{height:'33%', width:'20%'}}>
                        <div className="task" id="task-1">
                         2 Days   
                        </div>
                        <div className="thread-control">
                            T-1
                        </div>
                    </div>

                </div>
                <div className="last-task" id="task-32" style={{width:'28.5%'}}>
                    One - 4 days
                </div>
            </div>
            <div className="deliverable-container">
            </div>
        </div>
        );
    }
}

if (document.getElementById('voidtimeline-container')) {
    ReactDOM.render(<Timeline />, document.getElementById('timeline-container'));
}
