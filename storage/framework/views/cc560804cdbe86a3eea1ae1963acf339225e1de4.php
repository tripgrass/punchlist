<div class="modal fade modal-danger" id="confirmDelete" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    Delete Project
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">close</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    Are you sure you want to delete this project?
                </p>
            </div>
            <div class="modal-footer">
                <?php echo Form::button('Cancel', array('class' => 'btn btn-light pull-left', 'type' => 'button', 'data-dismiss' => 'modal' )); ?>

                <?php echo Form::button('Confirm', array('class' => 'btn btn-danger pull-right btn-flat', 'type' => 'button', 'id' => 'confirm' )); ?>

            </div>
        </div>
    </div>
</div>
<?php /**PATH /home/vagrant/Code/punchlist/packages/joinery/projects/src/views/modals/modal-delete.blade.php ENDPATH**/ ?>