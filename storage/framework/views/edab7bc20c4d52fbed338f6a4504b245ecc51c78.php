

<?php $__env->startSection('template_title'); ?>
    <?php echo app('translator')->getFromJson('projects::projects.showing-all-groups'); ?> 
<?php $__env->stopSection(); ?>
<?php $__env->startSection('template_linked_css'); ?>
    <?php if(config('projects.enabledDatatablesJs')): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo e(config('projects.datatablesCssCDN')); ?>">
    <?php endif; ?>
    <?php if(config('projects.fontAwesomeEnabled')): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo e(config('projects.fontAwesomeCdn')); ?>">
    <?php endif; ?>
    <?php echo $__env->make('projects::partials.styles', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('projects::partials.bs-visibility-css', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="container">
        <?php if(config('projects.enablePackageBootstapAlerts')): ?>
            <div class="row">
                <div class="col-sm-12">
                  <?php  @include('projects::partials.form-status') ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                               Showing all Projects
                            </span>

                            <div class="btn-project pull-right btn-project-xs">
                                <?php if(config('projects.softDeletedEnabled')): ?>
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i>
                                        <span class="sr-only">
                                            <?php echo app('translator')->getFromJson('projects::projects.groups-menu-alt'); ?>
                                        </span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="<?php echo e(route('groups.create')); ?>">
                                                <?php if(config('projects.fontAwesomeEnabled')): ?>
                                                    <i class="fa fa-fw fa-group-plus" aria-hidden="true"></i>
                                                <?php endif; ?>
                                               New Project
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/groups/deleted">
                                                <?php if(config('projects.fontAwesomeEnabled')): ?>
                                                    <i class="fa fa-fw fa-project" aria-hidden="true"></i>
                                                <?php endif; ?>
                                                <?php echo app('translator')->getFromJson('projects::projects.show-deleted-groups'); ?>
                                            </a>
                                        </li>
                                    </ul>
                                <?php else: ?>
                                    <a href="/projects/create" class="btn btn-default btn-sm pull-right" data-toggle="tooltip" data-placement="left" title="<?php echo app('translator')->getFromJson('projects::projects.tooltips.create-new'); ?>">
                                        <?php if(config('projects.fontAwesomeEnabled')): ?>
                                            <i class="fa fa-fw fa-group-plus" aria-hidden="true"></i>
                                        <?php endif; ?>
                                        New Project
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" id="project-card-body">
                    </div>

                </div>
            </div>
        </div>
    </div>
    <?php echo $__env->make('projects::modals/modal-delete', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('template_scripts'); ?>
    <?php echo $__env->make('projects::scripts/csrf-token', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php if((count($projects) > config('projects.datatablesJsStartCount')) && config('projects.enabledDatatablesJs')): ?>
        <?php echo $__env->make('projects::scripts.datatables', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>
    <?php echo $__env->make('projects::scripts.delete-modal-script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('projects::scripts.save-modal-script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php if(config('projects.tooltipsEnabled')): ?>
        <?php echo $__env->make('projects::scripts.tooltips', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/Code/punchlist/packages/joinery/projects/src/views/show-projects.blade.php ENDPATH**/ ?>