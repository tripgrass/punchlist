

<script type="text/javascript">
console.log('dletemodal-script should be run by react');
  $('#confirmDelete').on('show.bs.modal', function (e) {
    var message = $(e.relatedTarget).attr('data-message');
    var title = $(e.relatedTarget).attr('data-title');
    var form = $(e.relatedTarget).closest('form');
    $(this).find('.modal-body p').text(message);
    $(this).find('.modal-title').text(title);
    $(this).find('.modal-footer #confirm').data('form', form);
  });
  $('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
    $(this).data('form').find('input[name="_token"]').val(csrf_token);
    var project_id = $(this).data('form').find('input[name="project_id"]').val();
    $(this).data('form').attr('action', '/projects/' + project_id );

      $(this).data('form').submit();
  });
</script><?php /**PATH /home/vagrant/Code/punchlist/packages/joinery/projects/src/views/scripts/delete-modal-script.blade.php ENDPATH**/ ?>