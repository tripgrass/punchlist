<?php if( $user ) : ?>
	<?php $current_group = Joinery\Groups\Group::find($user->current_group); 
	$groupsAlreadyAttachedToProject = [];
	$manageGroups = [];
	foreach( $project->groups as $attachedGroup ){
		$groupsAlreadyAttachedToProject[] = $attachedGroup->id;
		if( $current_group->id != $attachedGroup->id){
			$manageGroups[] = $attachedGroup;
		}
	}
	?>
	<div class="modal fade" id="partnerModal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document" style="width:750px; max-width:750px">
			<div class="modal-content">
				<div class="modal-body">
					<div class="row">
						<div class="col-lg-1 offset-lg-11">
							<button type="button" class="close" data-dismiss="modal" style="margin-right:20px"><span aria-hidden="true">&times;</span></button>
						</div>
					</div>

					<div class="form-group-options row" id="groupOption" >
						<div class="col-md-12"><h3 style="text-align:center;">Add A Partner to this Project</h3>
							<div class="btn-group pull-right btn-group-xs">
                                    <a href="/groups/create?addToProject=<?php echo e($project->id); ?>" class="model-add-new pull-right" data-toggle="tooltip" data-placement="left" title="<?php echo app('translator')->getFromJson('groups::groups.tooltips.create-new'); ?>">
                                        Create New Partner
                                    </a>
                            </div>
                        </div>


                    	<?php if (1 == 1 ) : ?>
						<div class="col-md-12" style="">
									<div class="row">
										<div class="col-md-12" style=" margin-bottom:30px;margin-top:10px;">
											<?php if( count($current_group->teams) > 0 ) : ?>
												<div class="custom-control custom-radio custom-control-inline">
													<input type="radio" id="searchPrevious" name="searchType" class="custom-control-input" checked>
													<label class="custom-control-label" for="searchPrevious">Choose From Your Partners</label>
												</div>
											<?php endif; ?>
											<div class="custom-control custom-radio custom-control-inline">
												<input type="radio" id="searchName" name="searchType" class="custom-control-input">
												<label class="custom-control-label" for="searchName">Search by Team Name</label>
											</div>
											<div class="custom-control custom-radio custom-control-inline">
												<input type="radio" id="searchEmail" name="searchType" class="custom-control-input">
												<label class="custom-control-label" for="searchEmail">Search by Admin Email</label>
											</div>
										</div>
									</div>
									<div class="row" style="margin-top:10px; margin-bottom:10px; font-weight:bold;">
										<div class="col-lg-2" id="group-label" style="height:30px;">Team: </div>

										<div class="col-md-10">
											<?php 
												$partnerOptions = "";
												if( count($current_group->teams) > 0 ){
														foreach($current_group->teams as $team){
															if( !in_array( $team->id, $groupsAlreadyAttachedToProject) ){
																$partnerOptions .= '<option value="' . $team->id . '">' . $team->name . '</option>';
															}
														}
												}
											?>
											<?php if( $partnerOptions ) : ?>
												<select id="partnerPrevious" class="form-control" name="partnerPrevious" style="width:100%">
													<option value="null">Choose a Partner</option>
													<?php echo $partnerOptions; ?>
												</select>
											<?php else: ?>
												All your existing Partners are already attached to this Project 
											<?php endif; ?>
											<input id="partnerGroup" type="text" class="form-control" name="partnerGroup" value=""  placeholder="Type to find existing team" style="width:100%; display:none;">
											<input id="adminEmail" type="text" class="form-control" name="adminEmail" placeholder="Type to find existing admin email" style="width:100%; display:none">
										</div>
									</div>
								</div>
								
								<input id="group_id" type="hidden" name="group_id">
					<?php endif; ?>
					</div>
									<?php if( count( $manageGroups ) < 1 ){  $wrapperDisplay = "display:none"; }else{ $wrapperDisplay = ""; }; ?>
										<div class="row existing-partners-wrapper" style="<?php echo $wrapperDisplay; ?>">
											<div class="col-md-12" style="margin-bottom:30px;margin-top:50px; padding-top:14px;border-top:solid 2px #d0d0d0">
												<h4>Partners Attached to Project : <?php echo $project->name; ?></h4>
												<ul class="manage-partners">
													<?php foreach($manageGroups as $manageGroup): ?>
														<li>
															<button title="Remove Partner From Project" class="delete-btn detachPartner" data-group-id="<?php echo $manageGroup->id; ?>">x</button><?php echo $manageGroup->name; ?></li>
													<?php endforeach; ?>
												</ul>
										</div>
									</div>

				<div class="row">
						<div class="col-md-12">
							
							<div class="col-lg-1 offset-lg-10">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>   <!--/modal-fade-->     
	<script>
		<?php 
			$groups = Joinery\Groups\Group::get();
			$options = [];
			foreach($groups as $group){
				$groupObj = new stdClass();
				$groupObj->label = $group->name;
				$groupObj->value = $group->name;
				$groupObj->id = $group->id;
				$options[] = $groupObj;
			}
			// see app.js for autocomplete -- need to make an ajax of this
			echo "window.groups = " . json_encode($options) . ";";
		?>    
	</script>
	<style>
	  .ui-widget-content{
		z-index:999999;
	  }
	</style>
<?php endif; ?><?php /**PATH C:\wamp64\www\punchlist.test\resources\views/modals/project-partner.blade.php ENDPATH**/ ?>