<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <?php if(session('status')): ?>
                        <div class="alert alert-danger" role="alert">
                            <?php echo e(session('status')); ?>


                        </div>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <?php if( 'timelines' == session('list') ) : ?> 
                    <div class="card-header">Your Timelines</div>

                    <div class="card-body">
                        <?php 
                            $user = \Auth::user();
                            $projects = [];
                            if($user){
                                $projects = Joinery\Projects\Project::whereHas('groups', function($q) use ($user){
                                    $q->where('group_id', '=',$user->current_group);
                                })->get();
                            }
                        ?>
                        <?php $__currentLoopData = $projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $project): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <a class="dropdown-item" href="/projects/<?php echo e($project->id); ?>/timeline">
                                        <?php echo e($project->name); ?>

                                    </a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\punchlist.test\resources\views/permission.blade.php ENDPATH**/ ?>