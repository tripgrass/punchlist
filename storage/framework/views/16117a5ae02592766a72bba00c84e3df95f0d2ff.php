<script type="text/javascript">
    $(function () {
        var is_touch_device = 'ontouchstart' in document.documentElement;
        if (!is_touch_device) {
            $('[data-toggle="tooltip"]').tooltip();
        }
    });
</script>
<?php /**PATH /home/vagrant/Code/punchlist/packages/joinery/groups/src/views/scripts/tooltips.blade.php ENDPATH**/ ?>