

<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><?php echo e(__('Register')); ?></div>

                <div class="card-body">
                    <form method="POST" action="<?php echo e(route('register')); ?>" aria-label="<?php echo e(__('Register')); ?>">
                        <?php echo csrf_field(); ?>
                        <div class="form-group row">
                            <div class="col-md-4"></div>
                            <div class="col-md-6">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="individualAccount" name="accountToggle" class="custom-control-input" checked>
                                    <label class="custom-control-label" for="individualAccount">Individual</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="groupAccount" name="accountToggle" class="custom-control-input">
                                    <label class="custom-control-label" for="groupAccount">Team</label>
                                </div>
                                <input id="accountType" type="hidden" name="accountType" value="individual">
                            </div>

                        </div>
                        <div class="form-group-options row" id="groupOption" style="display:none; margin-bottom:10px;">
                            <div class="col-md-12"><hr></div>
                            <div class="col-md-4"></div>
                            <div class="col-md-6">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="existingGroup" name="groupType" class="custom-control-input" checked>
                                    <label class="custom-control-label" for="existingGroup">Existing Team</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="newGroup" name="groupType" class="custom-control-input">
                                    <label class="custom-control-label" for="newGroup">New Team</label>
                                </div>
                            </div>

                        </div>
                        <div class="form-group row" id="existingGroup_wrapper" style="display:none">
                            <label for="group" class="col-md-4 col-form-label text-md-right"><?php echo e(__('Existing Team/Org Name')); ?></label>
                            <div class="col-md-6">
                                <input id="group" type="text" class="form-control<?php echo e($errors->has('group') ? ' is-invalid' : ''); ?>" name="group" value="<?php echo e(old('group')); ?>"  placeholder="Type to find existing team">
                                <input id="group_id" type="hidden" name="group_id">
                                <?php if($errors->has('group')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('group')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group row" id="newGroup_wrapper" style="display:none">
                            <label for="newGroup" class="col-md-4 col-form-label text-md-right"><?php echo e(__('New Team/Org Name')); ?></label>
                            <div class="col-md-6">
                                <input id="newGroup" type="text" class="form-control<?php echo e($errors->has('newGroup') ? ' is-invalid' : ''); ?>" name="newGroup" value="<?php echo e(old('newGroup')); ?>"  placeholder="Team Name">
                                <?php if($errors->has('newGroup')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('newGroup')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>                        
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right"><?php echo e(__('Username')); ?></label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control<?php echo e($errors->has('name') ? ' is-invalid' : ''); ?>" name="name" value="<?php echo e(old('name')); ?>" required autofocus>

                                <?php if($errors->has('name')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('name')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right"><?php echo e(__('E-Mail Address')); ?></label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email" value="<?php echo e(old('email')); ?>" required>

                                <?php if($errors->has('email')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right"><?php echo e(__('Password')); ?></label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" name="password" required>

                                <?php if($errors->has('password')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right"><?php echo e(__('Confirm Password')); ?></label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    <?php echo e(__('Register')); ?>

                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script>
    <?php 
        $groups = Joinery\Groups\Group::get();
        $options = [];
        foreach($groups as $group){
            $groupObj = new stdClass();
            $groupObj->label = $group->name;
            $groupObj->value = $group->name;
            $groupObj->id = $group->id;
            $options[] = $groupObj;
        }
        // see app.js for autocomplete -- need to make an ajax of this
        echo "window.groups = " . json_encode($options) . ";";
    ?>    
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/Code/punchlist/resources/views/auth/register.blade.php ENDPATH**/ ?>