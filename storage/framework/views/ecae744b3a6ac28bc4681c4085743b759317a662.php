<?php 
    use App\User;

    $edit = 0;
    $title = "Create New Group";
    $btn_text = "Create New Group";
    $form_name = "";
    $form_admin_email = "";
    $form_id = "NULL";
    if( isset($group) && is_object($group)){
        $edit = 1;
        $title = "Edit Group";
        $btn_text = "Update Group";
        $form_name = $group->name;
        $form_id = $group->id;
//        print_r($group);
    }
    $associate = 0;
    
    if (Auth::check()) {
        $id = Auth::user()->id;
        $user = User::find($id);
//        print_r($user->groups);
        $creating_group_id = $user->groups[0]->id;
        $associate = 1 ;
    }
?>
    <div class="container">
        <?php if(config('groups.enablePackageBootstapAlerts')): ?>
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <?php 
//                    @include('groups::partials.form-status')
                ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">
                            <?php echo e($title); ?>

                            <div class="pull-right">
                                <a href="/groups" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="left" title="<?php echo app('translator')->getFromJson('Back to Groups'); ?>">
                                    <?php if(config('groups.fontAwesomeEnabled')): ?>
                                        <i class="fas fa-fw fa-reply-all" aria-hidden="true"></i>
                                    <?php endif; ?>
                                   Back to Groups
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <?php echo Form::open(array('route' => 'groups.store', 'method' => 'POST', 'role' => 'form', 'class' => 'needs-validation')); ?>

                            <?php echo Form::hidden('id', $form_id, array('id' => 'id')); ?>

                            <?php if($associate): ?>
                                <?php echo Form::hidden('creating_group_id', $creating_group_id, array('id' => 'creating_group_id')); ?>

                            <?php endif; ?>
                            <?php echo csrf_field(); ?>

                            <div class="form-group has-feedback row <?php echo e($errors->has('email') ? ' has-error ' : ''); ?>">
                                <?php if(config('groups.fontAwesomeEnabled')): ?>
                                    <?php echo Form::label('name', 'Group Name', array('class' => 'col-md-3 control-label'));; ?>

                                <?php endif; ?>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <?php echo Form::text('name', $form_name, array('id' => 'name', 'class' => 'form-control', 'placeholder' => 'Name')); ?>

                                    </div>
                                    <?php if($errors->has('name')): ?>
                                        <span class="help-block">
                                            <strong><?php echo e($errors->first('name')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group has-feedback row <?php echo e($errors->has('admin_email') ? ' has-error ' : ''); ?>">
                                <div class="col-md-9">
                                    <div class="input-group">
                                        add a toggle between email and users "" (or default with existing users list/search and have button for add new)
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback row <?php echo e($errors->has('admin_email') ? ' has-error ' : ''); ?>">
                                <?php if(config('groups.fontAwesomeEnabled')): ?>
                                    <?php echo Form::label('admin_email', 'Admin Email', array('class' => 'col-md-3 control-label'));; ?>

                                <?php endif; ?>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <?php echo Form::text('admin_email', $form_admin_email, array('id' => 'admin_email', 'class' => 'form-control', 'placeholder' => 'admin@yourclient.com')); ?>

                                    </div>
                                    <?php if($errors->has('admin_email')): ?>
                                        <span class="help-block">
                                            <strong><?php echo e($errors->first('admin_email')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group has-feedback row <?php echo e($errors->has('email') ? ' has-error ' : ''); ?>">
                                <?php if(config('groups.fontAwesomeEnabled')): ?>
                                    <?php echo Form::label('users', 'Group Members', array('class' => 'col-md-3 control-label'));; ?>

                                <?php endif; ?>
                                <div class="col-md-9">
                                    <div class="input-group">
<?php 
$select_users = [];
foreach($users as $user){
    $select_users[$user->id] = $user->name;
}
$selected_users = [];
if(isset($group) && isset($group->users)){
    foreach( $group->users as $user){
        $selected_users[$user->id] = $user->id;
    }
}
?>
<?php echo e(Form::select('users',$select_users,$selected_users,array('multiple'=>'multiple','name'=>'users[]', 'id' => 'users', 'class' => 'form-control'))); ?>

                                    </div>
                                    <?php if($errors->has('users')): ?>
                                        <span class="help-block">
                                            <strong><?php echo e($errors->first('users')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <?php echo Form::button( $btn_text, array('class' => 'btn btn-success margin-bottom-1 mb-1 float-right','type' => 'submit' )); ?>

                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php /**PATH /home/vagrant/Code/punchlist/resources/views/forms/new_group.blade.php ENDPATH**/ ?>