<?php $__env->startSection('template_linked_css'); ?>
    <?php if(config('projects.enabledDatatablesJs')): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo e(config('projects.datatablesCssCDN')); ?>">
    <?php endif; ?>
    <?php if(config('projects.fontAwesomeEnabled')): ?>
        <link rel="stylesheets" type="text/css" href="<?php echo e(config('projects.fontAwesomeCdn')); ?>">
    <?php endif; ?>
    <?php echo $__env->make('projects::partials.bs-visibility-css', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<?php $__env->stopSection(); ?>

<?php 
        $h1 = "Put your projects on autopilot.";
        $p = "Create and manage self-adjusting timelines for your contracts.";
    if(is_array($_SERVER) && array_key_exists('HTTP_REFERER', $_SERVER) ){
        //echo $_SERVER['HTTP_REFERER'];
        $h1 = "Put your projects on autopilot.";
        $p = "Create and manage self-adjusting timelines for your contracts.";
    }
?>
<?php $__env->startSection('content'); ?>
        <div id="hero">
            <h1><?php echo $h1; ?></h1>
            <p><?php echo $p; ?></p>
            <button>Sign Up</button>
        </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make(config('projects.ProjectsBladeExtended'), \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/Code/punchlist/resources/views/welcome.blade.php ENDPATH**/ ?>