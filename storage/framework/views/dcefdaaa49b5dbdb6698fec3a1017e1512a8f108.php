<?php if( $user ) : ?>
	<div class="modal fade" id="addTaskModal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document" style="width:60%; max-width:90%">
			<div class="modal-content">
				<div class="modal-body">
					<div class="row">
						<div class="col-lg-1 offset-lg-11">
							<button type="button" class="close" data-dismiss="modal" style="margin-right:20px"><span aria-hidden="true">&times;</span></button>
						</div>
					</div>
					<div class="form-add-task row" id="" >
						<div class="col-md-12" style="text-align:center; padding-top:25px; padding-bottom:35px" id="add-task-message">
							<h4 >
								Click where you'd like to add a Task 								
							</h4>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							
							<div class="col-lg-1 offset-lg-10">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>   <!--/modal-fade-->     
<?php endif; ?><?php /**PATH C:\wamp64\www\punchlist.test\resources\views/modals/add-task.blade.php ENDPATH**/ ?>