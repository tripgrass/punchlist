<script type="text/javascript">
    $(function () {
        var is_touch_device = 'ontouchstart' in document.documentElement;
        if (!is_touch_device) {
            $('[data-toggle="tooltip"]').tooltip();
        }
    });
</script>
<?php /**PATH /home/vagrant/Code/punchlist/resources/views/vendor/laravelusers/scripts/tooltips.blade.php ENDPATH**/ ?>