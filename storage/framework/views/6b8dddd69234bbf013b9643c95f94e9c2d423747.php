<script>
    $( document ).ready(function() {
        var cardTitle = $('#card_title');
        var groupsTable = $('#groups_table');
        var resultsContainer = $('#search_results');
        var groupsCount = $('#group_count');
        var clearSearchTrigger = $('.clear-search');
        var searchform = $('#search_groups');
        var searchformInput = $('#group_search_box');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        console.log(searchform);
        searchform.submit(function(e) {
            e.preventDefault();
            resultsContainer.html('');
            groupsTable.hide();
            clearSearchTrigger.show();
            let noResulsHtml = '<tr>' +
                                '<td><?php echo app('translator')->getFromJson("groups::groups.search.no-results"); ?></td>' +
                                '<td></td>' +
                                '<td class="hidden-xs"></td>' +
                                '<td class="hidden-sm hidden-xs"></td>' +
                                '<td class="hidden-sm hidden-xs hidden-md"></td>' +
                                '<td class="hidden-sm hidden-xs hidden-md"></td>' +
                                '<td></td>' +
                                '<td></td>' +
                                '<td></td>' +
                                '</tr>';

            $.ajax({
                type:'POST',
                url: "api/search-groups",
                data: searchform.serialize(),
                success: function (result) {
                    let jsonData = JSON.parse(result);
                    if (jsonData.length != 0) {
                        $.each(jsonData, function(index, val) {
                            let rolesHtml = '';
                            let roleClass = '';
                            let showCellHtml = '<a class="btn btn-sm btn-success btn-block" href="groups/' + val.id + '" data-toggle="tooltip" title="<?php echo app('translator')->getFromJson("groups::groups.tooltips.show"); ?>"><?php echo app('translator')->getFromJson("groups::groups.buttons.show"); ?></a>';
                            let editCellHtml = '<a class="btn btn-sm btn-info btn-block" href="groups/' + val.id + '/edit" data-toggle="tooltip" title="<?php echo app('translator')->getFromJson("groups::groups.tooltips.edit"); ?>"><?php echo app('translator')->getFromJson("groups::groups.buttons.edit"); ?></a>';
                            let deleteCellHtml = '<form method="POST" action="http://laravel.local/groups/'+ val.id +'" accept-charset="UTF-8" data-toggle="tooltip" title="Delete">' +
                                    '<?php echo Form::hidden("_method", "DELETE"); ?>' +
                                    '<?php echo csrf_field(); ?>' +
                                    '<button class="btn btn-danger btn-sm" type="button" style="width: 100%;" data-toggle="modal" data-target="#confirmDelete" data-title="Delete User" data-message="<?php echo app('translator')->getFromJson("groups::modals.delete_group_message", ["group" => "'+val.name+'"]); ?>">' +
                                        '<?php echo app('translator')->getFromJson("groups::groups.buttons.delete"); ?>' +
                                    '</button>' +
                                '</form>';

                            resultsContainer.append('<tr>' +
                                '<td>' + val.id + '</td>' +
                                '<td>' + val.name + '</td>' +
                                '<td class="hidden-sm hidden-xs hidden-md">' + val.created_at + '</td>' +
                                '<td class="hidden-sm hidden-xs hidden-md">' + val.updated_at + '</td>' +
                                '<td>' + deleteCellHtml + '</td>' +
                                '<td>' + showCellHtml + '</td>' +
                                '<td>' + editCellHtml + '</td>' +
                            '</tr>');
                        });
                    } else {
                        resultsContainer.append(noResulsHtml);
                    };
                    groupsCount.html(jsonData.length + " <?php echo app('translator')->getFromJson('groups::groups.search.found-footer'); ?>");
                    cardTitle.html("<?php echo app('translator')->getFromJson('groups::groups.search.title'); ?>");
                },
                error: function (response, status, error) {
                    if (response.status === 422) {
                        resultsContainer.append(noResulsHtml);
                        groupsCount.html(0 + " <?php echo app('translator')->getFromJson('groups::groups.search.found-footer'); ?>");
                        cardTitle.html("<?php echo app('translator')->getFromJson('groups::groups.search.title'); ?>");
                    };
                },
            });
        });
        searchformInput.keyup(function(event) {
            if ($('#group_search_box').val() != '') {
                clearSearchTrigger.show();
            } else {
                clearSearchTrigger.hide();
                resultsContainer.html('');
                groupsTable.show();
                cardTitle.html("<?php echo app('translator')->getFromJson('groups::groups.showing-all-groups'); ?>");
                groupsCount.html("<?php echo e(trans_choice('groups::groups.groups-table.caption', 1, ['groupscount' => $groups->count()])); ?>");
            };
        });
        clearSearchTrigger.click(function(e) {
            e.preventDefault();
            clearSearchTrigger.hide();
            groupsTable.show();
            resultsContainer.html('');
            searchformInput.val('');
            cardTitle.html("<?php echo app('translator')->getFromJson('groups::groups.showing-all-groups'); ?>");
            groupsCount.html("<?php echo e(trans_choice('groups::groups.groups-table.caption', 1, ['groupscount' => $groups->count()])); ?>");
        });
    });
</script>
<?php /**PATH /home/vagrant/Code/punchlist/packages/joinery/groups/src/views/scripts/search-groups.blade.php ENDPATH**/ ?>