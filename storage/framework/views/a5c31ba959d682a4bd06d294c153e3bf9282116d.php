<?php $__env->startSection('template_title'); ?>
    <?php echo app('translator')->getFromJson('groups::groups.showing-all-users'); ?> <?php $__env->stopSection(); ?>

<?php $__env->startSection('template_linked_css'); ?>
    <?php if(config('groups.enabledDatatablesJs')): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo e(config('groups.datatablesCssCDN')); ?>">
    <?php endif; ?>
    <?php if(config('groups.fontAwesomeEnabled')): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo e(config('groups.fontAwesomeCdn')); ?>">
    <?php endif; ?>
    <?php echo $__env->make('groups::partials.styles', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('groups::partials.bs-visibility-css', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="container">
        <?php if(config('groups.enablePackageBootstapAlerts')): ?>
            <div class="row">
                <div class="col-sm-12">
                  <?php  @include('groups::partials.form-status') ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                               Manage Your Partners
                            </span>

                            <div class="btn-group pull-right btn-group-xs">
                                <?php if(config('groups.softDeletedEnabled')): ?>
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i>
                                        <span class="sr-only">
                                            <?php echo app('translator')->getFromJson('groups::groups.users-menu-alt'); ?>
                                        </span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="<?php echo e(route('users.create')); ?>">
                                                <?php if(config('groups.fontAwesomeEnabled')): ?>
                                                    <i class="fa fa-fw fa-user-plus" aria-hidden="true"></i>
                                                <?php endif; ?>
                                               New Group
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/users/deleted">
                                                <?php if(config('groups.fontAwesomeEnabled')): ?>
                                                    <i class="fa fa-fw fa-group" aria-hidden="true"></i>
                                                <?php endif; ?>
                                                <?php echo app('translator')->getFromJson('groups::groups.show-deleted-users'); ?>
                                            </a>
                                        </li>
                                    </ul>
                                <?php else: ?>
                                    <a href="/groups/create" class="model-add-new pull-right" data-toggle="tooltip" data-placement="left" title="<?php echo app('translator')->getFromJson('groups::groups.tooltips.create-new'); ?>">
                                        Add New Partner
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" id="card-body">
                        
                    </div>

                </div>
            </div>
        </div>
    </div>
    <?php echo $__env->make('groups::modals/modal-delete', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('template_scripts'); ?>
    <?php echo $__env->make('groups::scripts/csrf-token', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php if((count($groups) > config('groups.datatablesJsStartCount')) && config('groups.enabledDatatablesJs')): ?>
        <?php echo $__env->make('groups::scripts.datatables', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>
    <?php echo $__env->make('groups::scripts.delete-modal-script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('groups::scripts.save-modal-script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php if(config('groups.tooltipsEnabled')): ?>
        <?php echo $__env->make('groups::scripts.tooltips', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>
    <?php if(config('groups.enableSearchGroups')): ?>
        <?php echo $__env->make('groups::scripts.search-groups', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\punchlist.test\packages\joinery\groups\src/views/show-groups.blade.php ENDPATH**/ ?>