<?php if( $user ) : ?>
	<?php $current_group = Joinery\Groups\Group::find($user->current_group); ?>
	<div class="modal fade" id="partnerModal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document" style="width:750px; max-width:750px">
			<div class="modal-content">
				<div class="modal-body">
					<div class="row">
						<div class="col-lg-1 offset-lg-11">
							<button type="button" class="close" data-dismiss="modal" style="margin-right:20px"><span aria-hidden="true">&times;</span></button>
						</div>
					</div>

					<div class="form-group-options row" id="groupOption" >
						<div class="col-md-12"><h3 style="text-align:center;">Add A Project Partner</h3></div>
						<div class="col-md-12" style="">
							<ul  class="nav nav-pills">
								<li><a class="active" href="#newGroup_wrapper" data-toggle="tab">Invite a New Team</a></li>
								<li><a href="#existingGroup_wrapper" data-toggle="tab">Search Registered Teams</a></li>
							</ul>
	<div class="tab-content clearfix">
								<div class="tab-pane " id="existingGroup_wrapper">
									<div class="row">
										<div class="col-md-12" style=" margin-bottom:30px;margin-top:10px;">
											<?php if( count($current_group->teams) > 0 ) : ?>
												<div class="custom-control custom-radio custom-control-inline">
													<input type="radio" id="searchPrevious" name="searchType" class="custom-control-input" checked>
													<label class="custom-control-label" for="searchPrevious">Choose Previous Partner</label>
												</div>
											<?php endif; ?>
											<div class="custom-control custom-radio custom-control-inline">
												<input type="radio" id="searchName" name="searchType" class="custom-control-input">
												<label class="custom-control-label" for="searchName">Search by Team Name</label>
											</div>
											<div class="custom-control custom-radio custom-control-inline">
												<input type="radio" id="searchEmail" name="searchType" class="custom-control-input">
												<label class="custom-control-label" for="searchEmail">Search by Admin Email</label>
											</div>
										</div>
									</div>
									<div class="row" style="margin-top:10px; margin-bottom:10px; font-weight:bold;">
										<div class="col-lg-2" id="group-label" style="height:30px;">Team: </div>

										<div class="col-md-10">
											<?php if( count($current_group->teams) > 0 ) : ?>
												<select id="partnerPrevious" class="form-control" name="partnerPrevious" style="width:100%">
													<option value="null">Choose a Partner</option>
													<?php foreach($current_group->teams as $team) : ?>
														<option value="<?php echo $team->id; ?>"><?php echo $team->name; ?></option>
													<?php endforeach; ?>
												</select>
											<?php endif; ?>
											<input id="partnerGroup" type="text" class="form-control" name="partnerGroup" value=""  placeholder="Type to find existing team" style="width:100%; display:none;">
											<input id="adminEmail" type="text" class="form-control" name="adminEmail" placeholder="Type to find existing admin email" style="width:100%; display:none">
										</div>
									</div>
								</div>
								<div class="tab-pane active" id="newGroup_wrapper" >
									<div class="form-group row" id="newGroup_wrapper" style="">
										<label for="newGroup" class="col-md-5 col-form-label text-md-right"><?php echo e(__('New Team/Org Contact Email')); ?></label>
										<div class="col-md-7">
											<input id="newGroup" style="width:100%" type="text" class="form-control" name="newGroup"  placeholder="admin@newteam.com">
										</div>
									</div>
								</div>
								<input id="group_id" type="hidden" name="group_id">
								<div class="row">
									<div class="col-lg-1 offset-lg-10" >
								<button type="button" class="btn" id="projectPartner" data-allow-new="1">Invite</button>
									</div>
								</div>
							</div>						
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							
							<div class="col-lg-1 offset-lg-10">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>   <!--/modal-fade-->     
	<script>
		<?php 
			$groups = Joinery\Groups\Group::get();
			$options = [];
			foreach($groups as $group){
				$groupObj = new stdClass();
				$groupObj->label = $group->name;
				$groupObj->value = $group->name;
				$groupObj->id = $group->id;
				$options[] = $groupObj;
			}
			// see app.js for autocomplete -- need to make an ajax of this
			echo "window.groups = " . json_encode($options) . ";";
		?>    
	</script>
	<style>
	  .ui-widget-content{
		z-index:999999;
	  }
	</style>
<?php endif; ?><?php /**PATH /home/vagrant/Code/punchlist/resources/views/modals/project-partner.blade.php ENDPATH**/ ?>