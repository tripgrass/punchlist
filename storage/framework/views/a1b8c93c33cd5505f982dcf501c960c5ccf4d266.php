<?php $__env->startSection('template_title'); ?>
    <?php echo app('translator')->getFromJson('groups::groups.showing-group', ['name' => $group->name]); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('template_linked_css'); ?>
    <?php if(config('groups.enabledDatatablesJs')): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo e(config('groups.datatablesCssCDN')); ?>">
    <?php endif; ?>
    <?php if(config('groups.fontAwesomeEnabled')): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo e(config('groups.fontAwesomeCdn')); ?>">
    <?php endif; ?>
    <?php echo $__env->make('groups::partials.styles', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('groups::partials.bs-visibility-css', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="container">
        <?php if(config('groups.enablePackageBootstapAlerts')): ?>
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <?php echo $__env->make('groups::partials.form-status', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">
                            <?php echo app('translator')->getFromJson('groups::groups.showing-group-title', ['name' => $group->name]); ?>
                            <div class="float-right">
                                <a href="<?php echo e(route('groups')); ?>" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="left" title="<?php echo app('translator')->getFromJson('groups::groups.tooltips.back-groups'); ?>">
                                    <?php if(config('groups.fontAwesomeEnabled')): ?>
                                        <i class="fas fa-fw fa-reply-all" aria-hidden="true"></i>
                                    <?php endif; ?>
                                    <?php echo app('translator')->getFromJson('groups::groups.buttons.back-to-groups'); ?>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <h4 class="text-muted text-center">
                            <?php echo e($group->name); ?>

                        </h4>
                        <?php if($group->email): ?>
                            <p class="text-center" data-toggle="tooltip" data-placement="top" title="<?php echo app('translator')->getFromJson('groups::groups.tooltips.email-group', ['group' => $group->email]); ?>">
                                <?php echo e(Html::mailto($group->email, $group->email)); ?>

                            </p>
                        <?php endif; ?>
                        <div class="row mb-4">
                            <div class="col-3 offset-3 col-sm-4 offset-sm-2 col-md-4 offset-md-2 col-lg-3 offset-lg-3">
                                <a href="/groups/<?php echo e($group->id); ?>/edit" class="btn btn-block btn-md btn-warning">
                                    <?php echo app('translator')->getFromJson('groups::groups.buttons.edit-group'); ?>
                                </a>
                            </div>
                            <div class="col-3 col-sm-4 col-md-4 col-lg-3">
                                <?php echo Form::open(array('url' => 'groups/' . $group->id, 'class' => 'form-inline')); ?>

                                    <?php echo Form::hidden('_method', 'DELETE'); ?>

                                    <?php echo Form::hidden('group_id', $group->id); ?>

                                    <?php echo Form::button(trans('groups::groups.buttons.delete-group'), array('class' => 'btn btn-danger btn-md btn-block','type' => 'button', 'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete User', 'data-message' => 'Are you sure you want to delete this group?')); ?>

                                <?php echo Form::close(); ?>

                            </div>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-4 col-sm-3">
                                        <strong>
                                            <?php echo app('translator')->getFromJson('groups::groups.show-group.id'); ?>
                                        </strong>
                                    </div>
                                    <div class="col-8 col-sm-9">
                                        <?php echo e($group->id); ?>

                                    </div>
                                </div>
                            </li>
                            <?php if($group->name): ?>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-4 col-sm-3">
                                            <strong>
                                                <?php echo app('translator')->getFromJson('groups::groups.show-group.name'); ?>
                                            </strong>
                                        </div>
                                        <div class="col-8 col-sm-9">
                                            <?php echo e($group->name); ?>

                                        </div>
                                    </div>
                                </li>
                            <?php endif; ?>
                            <?php if($group->email): ?>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-12 col-sm-3">
                                            <strong>
                                                <?php echo app('translator')->getFromJson('groups::groups.show-group.email'); ?>
                                            </strong>
                                        </div>
                                        <div class="col-12 col-sm-9">
                                            <?php echo e($group->email); ?>

                                        </div>
                                    </div>
                                </li>
                            <?php endif; ?>
                            <?php if($group->created_at): ?>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-4 col-sm-3">
                                            <strong>
                                                <?php echo app('translator')->getFromJson('groups::groups.show-group.created'); ?>
                                            </strong>
                                        </div>
                                        <div class="col-8 col-sm-9">
                                            <?php echo e($group->created_at); ?>

                                        </div>
                                    </div>
                                </li>
                            <?php endif; ?>
                            <?php if($group->updated_at): ?>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-4 col-sm-3">
                                            <strong>
                                                <?php echo app('translator')->getFromJson('groups::groups.show-group.updated'); ?>
                                            </strong>
                                        </div>
                                        <div class="col-8 col-sm-9">
                                            <?php echo e($group->updated_at); ?>

                                        </div>
                                    </div>
                                </li>
                            <?php endif; ?>
                            <?php if($group->users): ?>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-4 col-sm-3">
                                            <strong>
                                                <?php echo app('translator')->getFromJson('groups::groups.show-group.users'); ?>
                                            </strong>
                                        </div>
                                        <div class="col-8 col-sm-9">
                                            <ul>
                                             <?php $__currentLoopData = $group->users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li><a href=<?php echo e(URL::to('users/' . $user->id)); ?>><?php echo e($user->name); ?></a></li>
                                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo $__env->make('groups::modals.modal-delete', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('template_scripts'); ?>
    <?php echo $__env->make('groups::scripts/csrf-token', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('groups::scripts.delete-modal-script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <?php echo $__env->make('groups::scripts.delete-modal-script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php if(config('groups.tooltipsEnabled')): ?>
        <?php echo $__env->make('groups::scripts.tooltips', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make(config('groups.GroupsBladeExtended'), \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/Code/punchlist/packages/joinery/groups/src/views/show-group.blade.php ENDPATH**/ ?>