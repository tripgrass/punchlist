

<?php $__env->startSection('template_title'); ?>
    <?php echo app('translator')->getFromJson('tasks::tasks.showing-all-projects'); ?> <?php $__env->stopSection(); ?>

<?php $__env->startSection('template_linked_css'); ?>
    <?php if(config('tasks.enabledDatatablesJs')): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo e(config('tasks.datatablesCssCDN')); ?>">
    <?php endif; ?>
    <?php if(config('tasks.fontAwesomeEnabled')): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo e(config('tasks.fontAwesomeCdn')); ?>">
    <?php endif; ?>
    <?php echo $__env->make('tasks::partials.styles', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('tasks::partials.bs-visibility-css', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="container">
        <?php if(config('tasks.enablePackageBootstapAlerts')): ?>
            <div class="row">
                <div class="col-sm-12">
                  <?php  @include('tasks::partials.form-status') ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                               Showing all Tasks
                            </span>

                            <div class="btn-task pull-right btn-task-xs">
                                <?php if(config('tasks.softDeletedEnabled')): ?>
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i>
                                        <span class="sr-only">
                                            <?php echo app('translator')->getFromJson('tasks::tasks.projects-menu-alt'); ?>
                                        </span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="<?php echo e(route('projects.create')); ?>">
                                                <?php if(config('tasks.fontAwesomeEnabled')): ?>
                                                    <i class="fa fa-fw fa-project-plus" aria-hidden="true"></i>
                                                <?php endif; ?>
                                               New Task
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/projects/deleted">
                                                <?php if(config('tasks.fontAwesomeEnabled')): ?>
                                                    <i class="fa fa-fw fa-task" aria-hidden="true"></i>
                                                <?php endif; ?>
                                                <?php echo app('translator')->getFromJson('tasks::tasks.show-deleted-projects'); ?>
                                            </a>
                                        </li>
                                    </ul>
                                <?php else: ?>
                                    <a href="/tasks/create" class="btn btn-default btn-sm pull-right" data-toggle="tooltip" data-placement="left" title="<?php echo app('translator')->getFromJson('tasks::tasks.tooltips.create-new'); ?>">
                                        <?php if(config('tasks.fontAwesomeEnabled')): ?>
                                            <i class="fa fa-fw fa-project-plus" aria-hidden="true"></i>
                                        <?php endif; ?>
                                        New Task
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" id="tasks-card-body" data-projectId=<?php echo e($project_id); ?>>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <?php echo $__env->make('tasks::modals/modal-delete', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('template_scripts'); ?>
    <?php echo $__env->make('tasks::scripts/csrf-token', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php if((count($tasks) > config('tasks.datatablesJsStartCount')) && config('tasks.enabledDatatablesJs')): ?>
        <?php echo $__env->make('tasks::scripts.datatables', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>
    <?php echo $__env->make('tasks::scripts.delete-modal-script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('tasks::scripts.save-modal-script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php if(config('tasks.tooltipsEnabled')): ?>
        <?php echo $__env->make('tasks::scripts.tooltips', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make(config('projects.ProjectsBladeExtended'), \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/Code/punchlist/packages/joinery/tasks/src/views/show-tasks.blade.php ENDPATH**/ ?>