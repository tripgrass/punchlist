<?php $__env->startSection('template_title'); ?>
    <?php echo app('translator')->getFromJson('projects::projects.showing-project', ['name' => $project->name]); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('template_linked_css'); ?>
    <?php if(config('projects.enabledDatatablesJs')): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo e(config('projects.datatablesCssCDN')); ?>">
    <?php endif; ?>
    <?php if(config('projects.fontAwesomeEnabled')): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo e(config('projects.fontAwesomeCdn')); ?>">
    <?php endif; ?>
    <?php echo $__env->make('projects::partials.styles', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('projects::partials.bs-visibility-css', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="container">
        <?php if(config('projects.enablePackageBootstapAlerts')): ?>
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <?php echo $__env->make('projects::partials.form-status', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">
                            <?php echo app('translator')->getFromJson('projects::projects.showing-project-title', ['name' => $project->name]); ?>
                            <div class="float-right">
                                <a href="<?php echo e(route('projects')); ?>" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="left" title="<?php echo app('translator')->getFromJson('projects::projects.tooltips.back-projects'); ?>">
                                    <?php if(config('projects.fontAwesomeEnabled')): ?>
                                        <i class="fas fa-fw fa-reply-all" aria-hidden="true"></i>
                                    <?php endif; ?>
                                    <?php echo app('translator')->getFromJson('projects::projects.buttons.back-to-projects'); ?>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <h4 class="text-muted text-center">
                            <?php echo e($project->name); ?>

                        </h4>
                        <?php if($project->email): ?>
                            <p class="text-center" data-toggle="tooltip" data-placement="top" title="<?php echo app('translator')->getFromJson('projects::projects.tooltips.email-project', ['project' => $project->email]); ?>">
                                <?php echo e(Html::mailto($project->email, $project->email)); ?>

                            </p>
                        <?php endif; ?>
                        <div class="row mb-4">
                            <div class="col-3 offset-3 col-sm-4 offset-sm-2 col-md-4 offset-md-2 col-lg-3 offset-lg-3">
                                <a href="/projects/<?php echo e($project->id); ?>/edit" class="btn btn-block btn-md btn-warning">
                                    <?php echo app('translator')->getFromJson('projects::projects.buttons.edit-project'); ?>
                                </a>
                            </div>
                            <div class="col-3 col-sm-4 col-md-4 col-lg-3">
                                <?php echo Form::open(array('url' => 'projects/' . $project->id, 'class' => 'form-inline')); ?>

                                    <?php echo Form::hidden('_method', 'DELETE'); ?>

                                    <?php echo Form::hidden('project_id', $project->id); ?>

                                    <?php echo Form::button(trans('projects::projects.buttons.delete-project'), array('class' => 'btn btn-danger btn-md btn-block','type' => 'button', 'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete Group', 'data-message' => 'Are you sure you want to delete this project?')); ?>

                                <?php echo Form::close(); ?>

                            </div>
                        </div>
                        <ul class="list-project list-project-flush">
                            <li class="list-project-item">
                                <div class="row">
                                    <div class="col-4 col-sm-3">
                                        <strong>
                                            <?php echo app('translator')->getFromJson('projects::projects.show-project.id'); ?>
                                        </strong>
                                    </div>
                                    <div class="col-8 col-sm-9">
                                        <?php echo e($project->id); ?>

                                    </div>
                                </div>
                            </li>
                            <?php if($project->name): ?>
                                <li class="list-project-item">
                                    <div class="row">
                                        <div class="col-4 col-sm-3">
                                            <strong>
                                                <?php echo app('translator')->getFromJson('projects::projects.show-project.name'); ?>
                                            </strong>
                                        </div>
                                        <div class="col-8 col-sm-9">
                                            <?php echo e($project->name); ?>

                                        </div>
                                    </div>
                                </li>
                            <?php endif; ?>
                            <?php if($project->email): ?>
                                <li class="list-project-item">
                                    <div class="row">
                                        <div class="col-12 col-sm-3">
                                            <strong>
                                                <?php echo app('translator')->getFromJson('projects::projects.show-project.email'); ?>
                                            </strong>
                                        </div>
                                        <div class="col-12 col-sm-9">
                                            <?php echo e($project->email); ?>

                                        </div>
                                    </div>
                                </li>
                            <?php endif; ?>
                            <?php if($project->created_at): ?>
                                <li class="list-project-item">
                                    <div class="row">
                                        <div class="col-4 col-sm-3">
                                            <strong>
                                                <?php echo app('translator')->getFromJson('projects::projects.show-project.created'); ?>
                                            </strong>
                                        </div>
                                        <div class="col-8 col-sm-9">
                                            <?php echo e($project->created_at); ?>

                                        </div>
                                    </div>
                                </li>
                            <?php endif; ?>
                            <?php if($project->updated_at): ?>
                                <li class="list-project-item">
                                    <div class="row">
                                        <div class="col-4 col-sm-3">
                                            <strong>
                                                <?php echo app('translator')->getFromJson('projects::projects.show-project.updated'); ?>
                                            </strong>
                                        </div>
                                        <div class="col-8 col-sm-9">
                                            <?php echo e($project->updated_at); ?>

                                        </div>
                                    </div>
                                </li>
                            <?php endif; ?>
                            <?php if($project->groups): ?>
                                <li class="list-project-item">
                                    <div class="row">
                                        <div class="col-4 col-sm-3">
                                            <strong>
                                                <?php echo app('translator')->getFromJson('projects::projects.show-project.groups'); ?>
                                            </strong>
                                        </div>
                                        <div class="col-8 col-sm-9">
                                            <ul>
                                             <?php $__currentLoopData = $project->groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li><a href=<?php echo e(URL::to('groups/' . $group->id)); ?>><?php echo e($group->name); ?></a></li>
                                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo $__env->make('projects::modals.modal-delete', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('template_scripts'); ?>
    <?php echo $__env->make('projects::scripts/csrf-token', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('projects::scripts.delete-modal-script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <?php echo $__env->make('projects::scripts.delete-modal-script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php if(config('projects.tooltipsEnabled')): ?>
        <?php echo $__env->make('projects::scripts.tooltips', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make(config('projects.ProjectsBladeExtended'), \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/Code/punchlist/packages/joinery/projects/src/views/show-project.blade.php ENDPATH**/ ?>