<script type="text/javascript">
    $(function () {
        var is_touch_device = 'ontouchstart' in document.documentElement;
        if (!is_touch_device) {
            $('[data-toggle="tooltip"]').tooltip();
        }
    });
</script>
<?php /**PATH C:\wamp64\www\punchlist.test\resources\views/vendor/laravelusers/scripts/tooltips.blade.php ENDPATH**/ ?>