<?php $__env->startSection('template_title'); ?>
    <?php echo app('translator')->getFromJson('laravelusers::laravelusers.showing-all-users'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('class', 'listview-users'); ?>
<?php $__env->startSection('template_linked_css'); ?>
    <?php if(config('laravelusers.enabledDatatablesJs')): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo e(config('laravelusers.datatablesCssCDN')); ?>">
    <?php endif; ?>
    <?php if(config('laravelusers.fontAwesomeEnabled')): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo e(config('laravelusers.fontAwesomeCdn')); ?>">
    <?php endif; ?>
    <?php echo $__env->make('laravelusers::partials.styles', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('laravelusers::partials.bs-visibility-css', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php
    $current_user = \Auth::user();
    $current_group = Joinery\Groups\Group::find($current_user->current_group);

?>
<?php $__env->startSection('content'); ?>
    <div class="container">
        <?php if(config('laravelusers.enablePackageBootstapAlerts')): ?>
            <div class="row">
                <div class="col-sm-12">
                    <?php echo $__env->make('laravelusers::partials.form-status', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                Manage the  Team
                            </span>

                            <div class="btn-group pull-right btn-group-xs">
                                    <a href="<?php echo e(route('users.create')); ?>" class="btn btn-default btn-sm pull-right" data-toggle="tooltip" data-placement="left" title="<?php echo app('translator')->getFromJson('laravelusers::laravelusers.tooltips.create-new'); ?>">
                                        <?php echo app('translator')->getFromJson('+ Add New User'); ?>
                                    </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">

                        <div class="table-responsive users-table">
                            <table class="table table-striped table-sm data-table">
                                <caption id="user_count">
                                    <?php echo e(trans_choice('laravelusers::laravelusers.users-table.caption', 1, ['userscount' => $users->count()])); ?>

                                </caption>
                                <thead class="thead">
                                    <tr>
                                        <th><?php echo app('translator')->getFromJson('laravelusers::laravelusers.users-table.name'); ?></th>
                                        <th class="hidden-xs"><?php echo app('translator')->getFromJson('laravelusers::laravelusers.users-table.email'); ?></th>
                                        <th class="">Role</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody  id="users_table">
                                    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                        <?php
                                            $group_user = $current_group->users->where('id',$user->id)->first();
                                            $group_role = $group_user->pivot->role;

                                        ?>
                                        <tr class="tr-href" data-href="<?php echo e(URL::to('users/' . $user->id . '/edit')); ?>">

                                            <td><?php echo e($user->name); ?></td>
                                            <td class="hidden-xs"><?php echo e($user->email); ?></td>
                                            <td class="hidden-xs"><?php echo ucfirst($group_role); ?></td>
                                            <td><a href="<?php echo e(URL::to('users/' . $user->id . '/edit')); ?>">Edit</a></td>

                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                                <?php if(config('laravelusers.enableSearchUsers')): ?>
                                    <tbody id="search_results"></tbody>
                                <?php endif; ?>
                            </table>


                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <?php echo $__env->make('laravelusers::modals.modal-delete', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('template_scripts'); ?>
    <?php if((count($users) > config('laravelusers.datatablesJsStartCount')) && config('laravelusers.enabledDatatablesJs')): ?>
        <?php echo $__env->make('laravelusers::scripts.datatables', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>
    <?php echo $__env->make('laravelusers::scripts.delete-modal-script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('laravelusers::scripts.save-modal-script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php if(config('laravelusers.tooltipsEnabled')): ?>
        <?php echo $__env->make('laravelusers::scripts.tooltips', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>
    <?php if(config('laravelusers.enableSearchUsers')): ?>
        <?php echo $__env->make('laravelusers::scripts.search-users', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\punchlist.test\resources\views/vendor/laravelusers/usersmanagement/show-users.blade.php ENDPATH**/ ?>