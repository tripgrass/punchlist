<div class="sidebar fade modal-danger" id="updateTask" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true" tabindex="-1">
                <div class="thread-info">
      
    </div>

    <div class="dialog-wrapper">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-content-top-header"></div>
                <div class="modal-header">
                    <div class="container" id="sidebar-add-task" style="display:none"></div>
                    <div class="container" id="sidebar-update-task">
                        <div class="task-header">
                            <div class="row project-settings" id="save-task-wrapper">
                            </div>
                            <div class="row">
                                <h2 class="modal-title" style="padding-left:0">
                                    <textarea rows="1" class="expanding-textarea no-border project-input" name="form-task-title" id="form-task-title" style="width:100%" disabled=""></textarea>                                    
                                </h2>
                                <div class="modal-close" style="text-align:right">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        <span class="sr-only">close</span>
                                    </button>
                                </div>
                            </div>
                        <div id="task-description">
                            <div class="row">                           
                                <div class="col-lg-12 no-gutter" >
                                    <textarea rows="1" class="expanding-textarea no-border project-input" name="task-description-content" id="task-description-content" style="width:100%" disabled=""></textarea>                                    
                                </div>
                            </div>
                        </div>                            
                            <div class="row">
                                <div class="col-lg-12 task-owner-label-wrapper-wrapper">
                                    <div id="task-owner-label-wrapper">
                                        <i>Assigned to:</i>
                                        <div id="task-owner-label" style='display:inline-block'></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row task-status-wrapper">
                                <div class="col-lg-5" id="task-status">Status: Open</div>
                            </div>
                        </div>
                        <div id="task-schedule">
                            <div class="row">
                                <div class="col-lg-6" style="text-align:right">
                                    <div id="task-length-label">Estimated Length: 5 days</div>
                                </div>
                                <div class="col-lg-6" style="text-align:right">
                                    <div id="task-realLength-label">Estimated Length: 5 days</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 date-button" id="start-date-button">
                                    <div id="start-label" class="label">
                                    </div>
                                    <div id="start-value"  class="value">
                                    </div>
                                    <input id="task-update-start-date" type="text" class="hidden-input" />
                                    <i class="input-clear fas fa-times-circle hidden-input" title="Clear Date"></i>

                                </div>
                                <div class="col-lg-6 date-button" id="end-date-button">
                                    <div id="end-label"  class="label">
                                    </div>
                                    <div id="end-value" class="value">
                                    </div>
                                   <input id="task-close-date" class="hidden-input" type="text" />
                                    <i class="input-clear fas fa-times-circle hidden-input" title="Clear Date"></i>

                                </div>
                            </div>    
                            <div class="row field-wrapper project-settings">
                                <div class="label-wrapper col-lg-6">
                                    <label>Task Length: </label>
                                </div>
                                <div class="input-wrapper col-lg-6">
                                    <input id="task-update-length" type="text"  />
                                </div>
                            </div>
                            <div class="row field-wrapper project-settings" id="project-partners">
                            </div>
                        </div>


                        <div id="task-properties">
                            test
                                                        
                        </div>
                        <div id="length-properties"></div>
                                <div class="col-lg-12">
                                    <div id="task-settings-toggle" class="anchor" style="   ">Edit Task</div>
                                </div>
                    </div>
                    
                </div>
                <div class="modal-body" id="task-form">
                </div>
            </div>
        </div>
    </div>
</div>
<?php /**PATH /home/vagrant/Code/punchlist/resources/views/modals/task-update.blade.php ENDPATH**/ ?>