

<?php $__env->startSection('content'); ?>
<?php

$user = Auth::user();

$current_group = Joinery\Groups\Group::find($user->current_group);
/*
if($current_group->isSolo()){
//    print_r($current_group);
}
$group = Joinery\Groups\Group::find(1);
$teamBreaks = $group->breaks('','',$user);
*/
?>
<div class="container breaks-layout">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h3>Manage Your Blocked Days</h3>
                </div>
                <div class="card-body">
                    <?php if(session('status')): ?>
                        <div class="alert alert-success" role="alert">
                            <?php echo e(session('status')); ?>

                        </div>
                    <?php endif; ?>
                    <?php
                    $user = Auth::user();
                    ?>
    
                    <div id="break-filters" style="display:nne">
                        <!--
                        <div>
                            <input type="checkbox" name="break-public" id="break-public" checked>
                            <label for="break-public">Partner Teams Can View</label>
                        </div>
                    -->
                        <div>
                        <input type="hidden" id="break-current-user-id" name="break-current-user-id" value="<?php echo $user->id; ?>">  
                        <input type="hidden" id="break-current-group-id" name="break-current-group-id" value="<?php echo $current_group->id; ?>">   <?php if($current_group->isGroup() && $user->isGroupAdmin() ) : ?>
                                <label for='break-filter'>Assign Blocked Day(s) to: </label>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="byUser" name="breakType" class="custom-control-input" checked>
                                    <label class="custom-control-label" for="byUser"><?php echo $user->name; ?></label>



                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="byGroup" name="breakType" class="custom-control-input">
                                    <label class="custom-control-label" for="byGroup"><?php echo $current_group->name; ?></label>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div>
                            <label for="break-project-filter">Assign Blocked Day(s) to a Project: </label>
                            <select id="break-project-filter" name="break-project-filter">
                                <option value="null">All Projects</option>
                                <?php foreach($user->projects() as $project): ?>
                                    <option value="<?php echo $project->id; ?>"><?php echo $project->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div id="break-user-filter-wrapper" style="display:none">
                            <label for="break-project-filter">Assign Blocked Day(s) to a User: </label>
                            <select id="break-user-filter" style="">
                                <option value='null'>All Users</option>
                                <?php foreach($current_group->users as $member): ?>
                                    <option value="<?php echo $member->id; ?>"><?php echo $member->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                     <div id="menu">
                          <span id="menu-navi">
                            <a class=" move-day" data-action="move-today">Today</a>
                            <button type="button" class="btn btn-default btn-sm move-day" data-action="move-prev">
                              <i class="calendar-icon ic-arrow-line-left" data-action="move-prev"></i>
                            </button>
                          <span id="renderRange" class="render-range"></span>
                            <button type="button" class="btn btn-default btn-sm move-day" data-action="move-next">
                              <i class="calendar-icon ic-arrow-line-right" data-action="move-next"></i>
                            </button>
                          </span>
                        </div>                
                    <div id="calendar"></div>
        </div>
        </div>
    </div>
    
</div>

<script type="text/javascript">
    var buildCalendar = 1;

</script>
<style>
    .tui-full-calendar-icon.tui-full-calendar-ic-date{
        background-color:red;
        background: url('/img/calendar.svg');
    }
    .tui-full-calendar-near-month-day{
       // display: flex;
       // justify-content: center;
       // align-items: center;
    }
    .tui-full-calendar-weekday-schedule{
        //opacity: 0.5;
    }

</style>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/Code/punchlist/resources/views/breaks.blade.php ENDPATH**/ ?>