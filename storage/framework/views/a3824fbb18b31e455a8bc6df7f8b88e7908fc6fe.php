<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <p>You belong to multiple Teams. Select the Team you'd like to work in right now.</p> 
                </div>

                <div class="card-body">
                    <?php if(session('status')): ?>
                        <div class="alert alert-success" role="alert">
                            <?php echo e(session('status')); ?>

                        </div>
                    <?php endif; ?>
                    <?php $user = \Auth::user();
                        if($user->groups): ?>
                        <ul class="current-group-select">
                        <?php foreach($user->groups as $group): ?>
                            <li><a href="/updateCurrentGroup/<?php echo $group->id; ?>"><?php echo $group->name; ?></a></li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/Code/punchlist/resources/views/current-group.blade.php ENDPATH**/ ?>