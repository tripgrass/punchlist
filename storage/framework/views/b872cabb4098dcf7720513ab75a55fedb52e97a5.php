<?php
/*
use Jasny\SSO\NotAttachedException;
require_once __DIR__ . '/../../../vendor/autoload.php';

$broker = new Jasny\SSO\Broker(getenv('SSO_SERVER_URL'), getenv('SSO_BROKER_ID'), getenv('SSO_BROKER_SECRET'));
$broker->attach(true);
try {
    if (!empty($_GET['logout'])) {
        $broker->logout();
    } elseif ($broker->getUserInfo() || ($_SERVER['REQUEST_METHOD'] == 'POST' && $broker->login($_POST['username'], $_POST['password']))) {
        echo"ppppppppppp";
        print_r($broker->getUserInfo());
      //  header("Location: index.php", true, 303);
       // exit;
    }
echo "lllllllllll";
    if ($_SERVER['REQUEST_METHOD'] == 'POST') $errmsg = "Login failed";
} catch (NotAttachedException $e) {
    header('Location: ' . $_SERVER['REQUEST_URI']);
    exit;
} catch (Jasny\SSO\Exception $e) {
    $errmsg = $e->getMessage();
}
*/

?>


<?php $__env->startSection('content'); ?>

<div class="vertical-center">
        <div class="page-title-wrapper" style="border-bottom:solid 1px grey">
            <div class="container" >
                <div class="row" style="">
                    <div class="col-lg-12" style="">
                        <h2 class="project-name" style="display:none;"></h2>
                        <div class="textarea-wrapper"> 
                            <textarea rows="1" class='expanding-textarea no-border project-input' disabled name='project-name' id='project-name'>Login</textarea>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <form method="POST" action="<?php echo e(route('login')); ?>" aria-label="<?php echo e(__('Login')); ?>">
                            <?php echo csrf_field(); ?>

                            <div class="form-group row">
                                <label for="email" class="col-sm-4 col-form-label text-md-right"><?php echo e(__('E-Mail Address')); ?></label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email" value="<?php echo e(old('email')); ?>" required autofocus>

                                    <?php if($errors->has('email')): ?>
                                        <span class="invalid-feedback" role="alert">
                                            <strong><?php echo e($errors->first('email')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right"><?php echo e(__('Password')); ?></label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" name="password" required>

                                    <?php if($errors->has('password')): ?>
                                        <span class="invalid-feedback" role="alert">
                                            <strong><?php echo e($errors->first('password')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">

            <div class="custom-control custom-switch"> 
                <input type="checkbox" 
                       class="custom-control-input" 
                       id="customSwitch1" name="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>/> 
                <label class="custom-control-label"
                       for="customSwitch1"> <?php echo e(__('Remember Me')); ?></label> 
            </div> 
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="traverse-button">
                                        <?php echo e(__('Login')); ?>

                                    </button>

                                    <a class="btn btn-link" href="<?php echo e(route('password.request')); ?>">
                                        <?php echo e(__('Forgot Your Password?')); ?>

                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\tripgrass\Projects\punchlist\resources\views/auth/login.blade.php ENDPATH**/ ?>