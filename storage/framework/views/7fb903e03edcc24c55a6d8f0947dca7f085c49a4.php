

<?php $__env->startSection('template_title'); ?>
    <?php echo app('translator')->getFromJson('projects::projects.showing-project', ['name' => $project->name]); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('template_linked_css'); ?>
    <?php if(config('projects.enabledDatatablesJs')): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo e(config('projects.datatablesCssCDN')); ?>">
    <?php endif; ?>
    <?php if(config('projects.fontAwesomeEnabled')): ?>
        <link rel="stylesheets" type="text/css" href="<?php echo e(config('projects.fontAwesomeCdn')); ?>">
    <?php endif; ?>
    <?php echo $__env->make('projects::partials.bs-visibility-css', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php 
$user = Auth::user();
$openSettings = 1;
$canEditProject = 0;
if($user && $user->projectPermission($project) ){

}
/*
test
    $thisGroupsProjects = DB::table('projects')
        ->join('group_project', function($join) use($user) {
            $join->on('projects.id', '=', 'group_project.project_id')
                 ->where(function($query) {
                        $query->where('group_project.isSecondary', NULL)
                            ->orWhere('group_project.isSecondary', 0);
                    })
                 ->where('group_project.group_id', $user->current_group);
            })
        ->get();
    $projectsInArray = [];
    foreach($thisGroupsProjects as $proj){
        $projectsInArray[] = $proj->project_id;
    }

    $projects = DB::table('projects')
        ->select(
            'groups.name as group_name', 
            'groups.id as group_id', 
            'projects.name as project_name',
            'projects.id as project_id',
            'isAlias',
            'alias_id',
            'approved',
            'approvedDate',
            'approvedBy_id',
            'isSecondary',
            'invited',
            'invitedDate',
            'accepted',
            'acceptedDate'
        )
        ->join('group_project', function($join) use($user, $projectsInArray) {
            $join->on('projects.id', '=', 'group_project.project_id')
                 ->where(function($query) {
                        $query->where('group_project.isSecondary', 1);
                    })
                 ->whereIn('group_project.project_id', $projectsInArray);
            })
        ->join('groups', 'group_project.group_id', '=', 'groups.id')
        ->get();
        $partners = [];
        foreach($projects as $proj ){
print_r($proj);            
            $group_id = $proj->group_id;
            $project_id = $proj->project_id;
            $partners[$group_id]['projects'][$project_id] = $proj;
            $partners[$group_id]['group']['name'] = $proj->group_name;
            $partners[$group_id]['group']['isAlias'] = $proj->isAlias;
            $partners[$group_id]['group']['alias_id'] = $proj->alias_id;
        }
        print_r($partners);

*/

//print_r($user->teams); ?>
    <script>
        var openSettings = <?php echo $openSettings; ?>;
    </script>
    <style>
        @media  screen and (min-width: 320px) and (max-width: 767px) and (orientation: portrait) {
  html.rotate {
    transform: rotate(-90deg);
    transform-origin: left top;
    width: 100vh;
    height: 100vw;
    overflow-x: hidden;
    position: absolute;
    top: 100%;
    left: 0;
  }
}
        .tutorial-banner{
//            background-image: url('/img/inspection.svg');
         //   background-image: url('/img/booking.svg');
           //   background-repeat: no-repeat;
          //  background-size: 85% 80%;
          //  background-position: 30px 30px;            
        }
        </style>
        <div style="border-bottom:solid 1px grey">
            <div class="container" >
                <div class="row" style="">
                    <div class="col-lg-8" style="">
                        <h2 class="project-name" style="display:none;"></h2>
                            <div class="textarea-wrapper"> 
                                <textarea rows="1" class='expanding-textarea no-border project-input' disabled name='project-name' id='project-name'><?php echo e($project->name); ?></textarea>
                            </div>
                        <div class="numberCircle" data-number="2"><span>2</span></div>
                    </div>
                    <div class="col-lg-4" style="text-align:right">
                        <button id="toggleTour" class="button" >Close Tour</button>
                        <?php if( $user && !$project->inhouse ) : ?>
                            <button class="add-button empty" data-toggle="modal" data-target="#partnerModal" style="display:inline-block; margin-right:10px;"> Partner</button>
                        <?php endif; ?>  
                        <?php if($user): ?>
                            <button class="add-button empty" id='add-task'>Task</button>
                        <?php endif; ?>
                        <button class='show-breaks'></button>
                        <div class="" id="filter-wrapper">
                            <div class=" filter-toggle">
                                <div class='acc-tog'><button class='show-filter'></button></div>
                            </div>
                        </div>
                        <div id="settings-toggle" style="display:inline-block">
                            <button class="edit-button"></button>
                        </div>
                   </div>
                </div> 
            </div>
        </div>
    <div class="container" style="max-width:95%; ">
        <div class="row mobile-rotate">
            <div class="col-lg-12">
                <button id="rotate-screen">Switch to Landscape Mode</button>
            </div>
        </div>
        <div id="project-width" class="project-meta container-fluid">
                <div class="row" id="project-data">
                    <div class="col-lg-4">
                        <div id="project-description-wrapper" <?php if( !$project->description ){ echo 'style="display:none;"'; } ?> > 

                            <input type="textarea" disabled value="<?php echo $project->description; ?>" name='project-description' id="project-description" class="no-border project-input" style="" placeholder="Project Description">
                        </div>
                    </div>
                    <div class="col-lg-8" style="text-align:right">
                        <div class="project-filters" style="display:inline-block;">
                            <div class="pj-inner" id="project-functions">
                                <div class="traverse-button grey-button" id='setPivotLabel'>Set Proposed Start Date
                                    <input  type='text' name='setPivot'  id='setPivot'/>
                                </div>
                                <button id="filter-blocked-days" class="filter-button">Show Blocked Days</button>
                                <a class="anchor btn btn-sm btn-inline-block" data-toggle="modal" data-target="#contractModal" >View Contract</a>
                                <div class="" id="all-filters" style="display:none;">
                                    <div class="" style="display:none;" >

                                        <div class="traverse-button grey-button" id="setPivotLabel" style="display: inline-block;">Set Proposed Start Date
                                            <input type="text" name="setPivot" id="setPivot" class="hasDatepicker">
                                        </div>
                                    </div>
                                    <div class="" >
                                        <div id="userFilterMain" class="user-filter-wrapper">
                                            <div id="user-filters"></div>
                                        </div>

                                    </div>
                                </div>
                                <div class="row" style="display:none">
                                    <div class="col-lg-12">
                                        <div id="dev-help">
                                            <div id="ratio" style=" "></div>
                                            <div id="updateListener"></div>
                                        <a id="scrollLeft" style=" ">Left</a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            <div class="project-settings col-lg-12" style="display:none">
                <div class="pj-inner">
                    <div class="project-settings-inner fluid-container">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="field-wrapper">
                                    <input type="checkbox" name="project-public" id="project-public" <?php if( $project->isPublic ){ echo "checked"; }?>>
                                    <label for="project-public">Make Project Public</label>
                                </div>
                                <div class="field-wrapper">
                                    <input type="checkbox" name="project-contract-public" id="project-contract-public" <?php if( $project->contractIsPublic ){ echo "checked"; }?>>
                                    <label for="project-contract-public">Make Contract Public</label>
                                </div>
                            </div>
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>
                        <div class="row">                    
                            <div class="col-lg-12">
                                    <a class="traverse-button grey-button">Set Project Permissions</a>
                            </div>
                            <div class="col-lg-8">

                            </div>
                            <div class="col-lg-4">
                                <?php if($project->partners) : ?>
                                    <ul class="partner-list">
                                        <?php foreach($project->partners as $partner) : ?>
                                            <?php 
                                                $status = "";
                                                if($partner->pivot->invited && !$partner->pivot->accepted){
                                                    $invitedDate = Carbon::parse($partner->pivot->invitedDate);
                                                    $status = " | Invited on " . $invitedDate->format('M d Y'); ?>
                                                    <li><?php echo $partner->name . $status; ?></li>
                                            <?php } ?>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </div>
                        </div><!--/row-->               
                    </div>
                </div>
            </div>
                        <hr style="margin-bottom:0">

        </div>
        <div class="project-container" id="project-container" data-project_id="<?php echo e($project->id); ?>" style="width:100%">
        </div>
        <div id="test-container" data-project_id="<?php echo e($project->id); ?>"></div>
        <div id="project" data-project_id="<?php echo e($project->id); ?>"></div>
    <?php echo $__env->make('modals.add-task', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('modals.task-update', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('modals.project-partner', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('modals.contract', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    

<?php $__env->stopSection(); ?>
<?php $__env->startSection('template_scripts'); ?>
    <?php echo $__env->make('projects::scripts/csrf-token', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('projects::scripts.delete-modal-script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <?php echo $__env->make('projects::scripts.delete-modal-script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php if(config('projects.tooltipsEnabled')): ?>
        <?php echo $__env->make('projects::scripts.tooltips', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>
<script type="text/javascript">
console.log('dletemodal-script should be run by react');
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make(config('projects.ProjectsBladeExtended'), \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/Code/punchlist/resources/views/projects/timeline.blade.php ENDPATH**/ ?>