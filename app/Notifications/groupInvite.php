<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use joinery\projects\Project;
use joinery\groups\Group;

class groupInvite extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Project $project, Group $partner, Group $current_group, $inviteCode)
    {
        $this->project = $project;
        $this->partner = $partner;
        $this->current_group = $current_group;
        $this->inviteCode = $inviteCode;
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $projectName = $this->project->name;
        $admin = $this->current_group->admin;
        return (new MailMessage)
                    ->line('Hello - ' . $admin->name . ' of ' . $this->current_group->name . ' would like to share the Traverse Project Calendar with you for ' . $projectName)
                    ->action('Join the Calendar', url('/project/' . $this->project->id .'/invite/' . $this->inviteCode))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
