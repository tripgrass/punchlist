<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = [
        'name','group_id','monthlyFee','active'
    ];

    /**
     * The groups that a user belongs to.
     */
    public function group()
    {
        return $this->hasOne('App\Group');
    }    
}
