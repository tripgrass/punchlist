<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Break extends Model
{
    protected $fillable = [
        'title','owner_id','project_id','active'
    ];

}
