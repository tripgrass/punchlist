<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Joinery\Groups\Group;
use App\Block;
use Laravel\Cashier\Billable;
use DB;

class User extends Authenticatable
{
	use Notifiable, Billable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'email', 'password','api_token'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	protected $appends = array('teams','invites'  );

	/**
	 * The groups that a user belongs to.
	 */
	
	public function groups()
	{
		return $this->belongsToMany('Joinery\Groups\Group');
	} 
	public function blocks()
	{
		return $this->belongsToMany('App\Block');
	} 

	public function isGroupAdmin($group = null){
		if(!$group){
			$group = Group::find($this->current_group);
		}
		$admin = $group->users()->where('isAdmin','1')->where('user_id',$this->id)->get();
		if($admin){
			return true;
		}
		return false;
	}

	public function projectPermission($project){
		if($project){
			$project_groupIds = $project->groups->pluck('id')->toArray();
			if(in_array($this->current_group, $project_groupIds)){
				return true;
			}
			return false;
		}
		return true;
	}
	public function projects(){
	  $projects = [];
	  $group = Group::find($this->current_group);
//print_r($group->projects);
	  return $group->projects;
	}
	public function getInvitesAttribute()
	{
		$rows = DB::table('group_project')
					 ->where('group_id', '=', $this->current_group)
					 ->where('invited', '=', 1)
					 ->where('accepted', '=', 0)
					 ->get();
		$output = [];
		if($rows){
			foreach($rows as $row){
				$project = Project::find($row->project_id);
//				error_log(print_r($project,true));
				if( $project ){
					$output[] = [
						'project_id' => $project->id,
						'project_name' => $project->name,
						'inviteCode' => $row->inviteCode,
						'invitedDate' => $row->invitedDate
					];
				}
			}
		}
		return $output;
	}
	public function getTeamsAttribute()
	{
		$currentGroup = Group::find($this->current_group);
		$output = [];
		if($currentGroup){
			$output[] = [
				'name' => $currentGroup->name,
				'id' => $currentGroup->id,
				'admin_id' => $currentGroup->adminid
			];
			if($currentGroup->teams){
				foreach($currentGroup->teams as $team){
					$output[] = [
						'name' => $team->name,
						'id' => $team->id,
						'admin_id' => $team->adminid
					];
				}
			}
		}
		else{
  //          return [];
		}
		return $output;
	} 
	public function breaks($startDate = null, $endDate = null){
	  $breaks = Block::
				  where('ownerType','user')
				  ->where('owner_id',$this->id)
				  ->get();
	  return $breaks;
	   // echo "in breakds";
	}   
}
