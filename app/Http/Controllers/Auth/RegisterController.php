<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Account;
use Joinery\Groups\Group;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Str;
use Carbon\Carbon;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/projects/new/timeline';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
//            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'api_token' => Str::random(60)
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['email'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'api_token' => Str::random(60)
         ]);
        $createGroup = 1;
        if(array_key_exists('accountType',$data)){
            if('individual' == $data['accountType']){
                $groupName = $data['email'];
                $accountType = 'Individual Trial';
            }
            elseif('group' == $data['accountType']){
                $accountType = 'Group Trial';
                if( array_key_exists('group_id',$data) ){
                    if( $data['group_id'] ){
                        $createGroup = 0;
                        $group_id = $data['group_id'];
                    }
                    else{
                        $groupName = $data['newGroup'];
                    }
                }
            }
        }
        if( $createGroup ){
            $group = Group::create([
                'name' => $groupName
            ]);

            $user->groups()->attach($group->id,[
                'role' => 'admin',
                'isAdmin' => 1

            ]);
            $user->current_group = $group->id;
            $account = Account::create([
                'name' => $accountType,
                'monthlyFee' => '9.99',
                'group_id' => $group->id,
                'active' => 1
            ]);
        }
        elseif( $group_id ){
            $group = Group::find($group_id); 
            $user->groups()->attach($group_id, 
                [
                    'role' => 'request',
                    'request' => '1',
                    'requestedOn' => Carbon::now()
                ]
            );
        }
        $user->save();
        return $user;

    }
}
