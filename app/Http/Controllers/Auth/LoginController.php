<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\MyBroker;
use App\User;
use Joinery\Projects\Project;
use Illuminate\Http\Request;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        logout as performLogout;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/projects/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
    }

    public function logout(Request $request)
    {
        $this->performLogout($request);
        return redirect('/login');
    }  

    protected function redirectPath()
    {
        $user = \Auth::user();
        if( $user ){
            $projects = Project::whereHas('groups', function($q) use ($user){
                $q->where('group_id', '=',$user->current_group);
            })->get();

        }
        if( count($projects)>0 ){
            if( $user->currentproject ){
                return '/projects/' . $user->currentproject . '/timeline';
            }
            // lookup most recent project
            return '/projects';
        }
        else{
            return '/projects/new/timeline';
        }
    }    

}
