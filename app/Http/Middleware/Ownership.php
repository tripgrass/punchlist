<?php

namespace App\Http\Middleware;
use Joinery\Projects\Project;
use Joinery\Groups\Group;
use Illuminate\Support\Facades\DB;
use Closure;
use Route;

class Ownership
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $project_id = $request->route('id');
        if( !$project_id && $request->id ){
            $project_id = $request->id;
        }
        $project = Project::find($project_id);
        $user = \Auth::user();
        if( "new" != $project_id && !$project && $project_id){
            $permission_alert = "That Project no longer exists";
            return redirect('/permissions')->with(['status' => $permission_alert,'list' => 'timelines']);
        }
        if( !$project_id && 'projects' == Route::currentRouteName()){
            return $next($request);
        }
        if( $user && !$user->current_group){
                $requesteds = DB::table('group_user')->where('request', '1')->get();
                $permission_alert = "";
                if( $requesteds && !$user->groups){
                    $groups = [];
                    foreach($requesteds as $req){
                        $group = Group::find($req->group_id);
                        $groups[] = $group->name;
                    }
                    if(count($groups) > 1){
                        $permission_alert = "You are currently waiting on approval from the following Teams: ". 
                            implode(",",$groups) .".";
                    }
                    if(count($groups) == 1){
                        $permission_alert = "You are currently waiting on approval from ". $groups[0] . ".";
                    }
                }
                else{
                    // redirect to select group
                    $permission_alert = "";
                    session_start();
                    $_SESSION['requestedUrl'] = \Request::getRequestUri();
                    return redirect('/current-group')->with('status', $permission_alert);
                }
                return redirect('/home')->with('status', $permission_alert);            
        }
        if($user || ($project && $project->isPublic)){
            if('new' == $project_id || ( $project  && ( $project->isPublic || $user->projectPermission($project))) ){
                return $next($request);
            }
            else{
                $currentGroup = Group::find($user->current_group);
                $permission_alert = "You're working in the ". $currentGroup->name." group - which doesn't have access to ". $project->name;
                return redirect('/permissions')->with(['status' => $permission_alert,'list' => 'timelines']);
            }
        }
        return redirect()->intended('/login');

        //return $next($request);
        //echo "not your project";
    }
}
