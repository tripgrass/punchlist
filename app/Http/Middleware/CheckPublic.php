<?php

namespace App\Http\Middleware;
use Joinery\Projects\Project;

use Closure;

class CheckPublic
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
        if ($request->id) {
            $project = Project::find( $request->id );
            if('new' == $request->id || $project->isPublic){
                return $next($request);
            }
        }


        return $next($request);    
    }
}
