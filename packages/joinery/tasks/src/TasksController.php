<?php

namespace Joinery\Tasks;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Validator;
use App\User;
use joinery\projects\Project;
use joinery\tasks\Task;
use Carbon\Carbon;


class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $pagintaionEnabled = config('tasks.enablePagination');
        if ($pagintaionEnabled) {
           // $groups = config('groups.defaultGroupModel')::paginate(config('groups.paginateListSize'));
        } else {
            $tasks = config('tasks.defaultTaskModel')::where('project_id','=',$id)->paginate(2);
        }
        $data = [
            'project_id' => $id,
            'tasks'             => $tasks,
            'pagintaionEnabled' => $pagintaionEnabled,
        ];
        return view(config('tasks.showTasksBlade'), $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // must filter by owner
        $projects = Project::all(); 
        $projects_select = [];
        foreach($projects as $project){
            $projects_select[$project->id] = $project->name;
        }

        $tasks = Task::all(); 
        $tasks_select = [];
        $tasks_select[0] = "None";
        foreach($tasks as $task){
            $tasks_select[$task->id] = $task->name;
        }
        $users = User::all();
        $data = [
            'tasks_select' => $tasks_select,
            'projects_select' => $projects_select,
            'users' => $users
        ];
        return view(config('tasks.createTaskBlade'), $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( 'NULL' == $request->id || !$request->id){
            $task = new Task;
//            echo "ppp";
        }
        else{
            $task = Task::find($request->id);   
            if( 'new_task' == $task->type ){
                Log::error('is NEW RASK' . $task->id );
            }         
            else{
                Log::error('is existing task' . $task->id );

            }
        }
        if($request->startDate){
            $startDateCarbon = Carbon::createFromTimestamp(strtotime($request->startDate))->startOfDay(); 
            $startDate = $startDateCarbon->toDateTimeString();
            $task->startDate = $startDate;
        }
        /*
        if($request->startDate){
            $estEndDateCarbon = $startDateCarbon->addDays($request->length);
            $estEndDate = $estEndDateCarbon->toDateTimeString();
            $task->estEndDate = $estEndDate;
        }
        */
        if($request->closedDate){
            // cannot be in the future
            $closedDateCarbon = Carbon::createFromTimestamp(strtotime($request->closedDate))->startOfDay(); 
            $closedDate = $closedDateCarbon->toDateTimeString();
            $task->closedDate = $closedDateCarbon;
        }
//dd(10);
       // echo  $startDate;
  //      print_r($task);
    //    dd(9);
        $task->name = $request->name;
        $task->description = $request->description;
        $task->length = $request->length;
        $task->parent_id = $request->parent_id;
        $task->project_id = $request->project_id;
        $task->type = $request->type;

        $task->save();
        if(isset($request->users)){
            $task->users()->sync($request->users);
        }
 
       if(isset($request->projects)){
            $task->projects()->sync($request->projects);
	    }
        if( $request->child_id ){
            $child_task = Task::find($request->child_id);
            $child_task->parent_id = $task->id;
            $child_task->save();
        }
        if( $request->is_new ){
            return response()->json([
                    'task_id' => $task->id
                ]);
        }
        else{
            return redirect('/projects/'.$task->project_id.'/tasks'); 
        }
   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function apiStore(Request $request)
    {
//        \Log::error( print_r($request->all(), true) );

        $isNew = false;
        $savingNew = 0;
        if( 'NULL' == $request->id || !$request->id ){
            $task = new Task;
            $isNew = true;
        }
        else{
            $task = Task::find($request->id);  
            if( $task ){
                if( ( $request->taskParentId && ($request->taskParentId != $task->parent_id)) || $request->isNewThread ){

                    if( $request->taskParentId != $task->parent_id  ){
                        $revise_orig_child_task =  Task::where('parent_id','=',$task->id)->first();
                        if( $revise_orig_child_task ){
                            $revise_orig_child_task->parent_id = $task->parent_id;
                            $revise_orig_child_task->save();
                        }
                    }
                    if( !$request->isNewThread ){
                        // get children of existing parent and swap parent id for moved task
                        $child_tasks = Task::where('parent_id','=',$request->taskParentId)->get();
                        foreach( $child_tasks as $child_task_arr){
                            $child_task = Task::find($child_task_arr->id);
                            if( $task->id ){
                                $child_task->parent_id = $task->id;
                                $child_task->save();
                            }
                        }        
                    }
                }                
                if( 'new_task' == $task->type  ){
                    $savingNew = 1;

                    $revise_orig_child_task =  Task::where('parent_id','=',$task->id)->first();
                    if( $revise_orig_child_task ){
            \Log::error( 'print_r($revise_orig_child_task, true) ');
            \Log::error( print_r($revise_orig_child_task, true) );

                        $revise_orig_child_task->parent_id = null;
                        $revise_orig_child_task->save();

                        // get children of existing parent and swap parent id for moved task
                        $child_tasks = Task::where('parent_id','=',$request->taskParentId)->get();
                        foreach( $child_tasks as $child_task_arr){
                            $child_task = Task::find($child_task_arr->id);
                            if( $task->id ){
                                $child_task->parent_id = $task->id;
                                $child_task->save();
                            }
                        }        
                    }
                    else{
                        \Log::error('ha sno parent');
                    }

                }
            }
        }
        if( $request->closeOnly ){
            $closedDateCarbon = Carbon::now()->startOfDay(); 
            $closedDate = $closedDateCarbon->toDateTimeString();
            $task->closedDate = $closedDateCarbon;
            $task->save();
        }
        else{            
            $task->startDate = null;
            if($request->startDate){
                $startDateCarbon = Carbon::createFromTimestamp(strtotime($request->startDate))->startOfDay(); 
                $startDate = $startDateCarbon->toDateTimeString();
                $task->startDate = $startDate;
            }
            $task->closedDate = null;
            if($request->closedDate){
                // cannot be in the future
                $closedDateCarbon = Carbon::createFromTimestamp(strtotime($request->closedDate))->startOfDay(); 
                $closedDate = $closedDateCarbon->toDateTimeString();
                $task->closedDate = $closedDateCarbon;
            }
            $task->name = ($request->name ? $request->name : $task->name);
            $task->description = ($request->description ? $request->description : $task->description);
            $task->length = ($request->length ? $request->length : $task->length);
            /*
            if( $request->isNewThread ){
                //tie all of its children to its parent
                $child_tasks = Task::where('parent_id','=',$task->id)->get();
                foreach( $child_tasks as $child_task_arr){
                    $child_task = Task::find($child_task_arr->id);
                    if( $task->id ){
                        $child_task->parent_id = $task->parent_id;
                        //$child_task->save();
                    }
                }        
            }
            */
            if( $request->taskParentId && $request->taskParentId != $task->id ){
                $task->parent_id = $request->taskParentId;
            }
            if( $savingNew ){
                $task->type = "";
            }
            $task->save();
            $task->users()->sync([]);
            $task->groups()->sync([]);
            if(isset($request->ownerUser)){
                $task->users()->sync($request->ownerUser);
            }
            if(isset($request->ownerGroup)){
                $task->groups()->sync($request->ownerGroup);
            }
     
     
           if(isset($request->projects)){
      //          $task->projects()->sync($request->projects);
            }
        }
    //    return redirect('/projects/'.$task->project_id.'/tasks'); 
   }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if( $id ){
            $task = Task::find($id);            
        }
  //      echo "--".$id;
//        print_r($task->notes);
        $data = [
            'task'             => $task,
            'pagintaionEnabled' => 0
        ];
        return view('tasks::show-task', $data); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = config('tasks.defaultTaskModel')::findOrFail($id);
   		$projects = Project::all();

        $projects_select = [];
        foreach($projects as $project){
            $projects_select[$project->id] = $project->name;
        }

        $tasks = Task::all(); 
        $tasks_select = [];
        $tasks_select[0] = "None";
        foreach($tasks as $_task){
            $tasks_select[$_task->id] = $_task->name;
        }
        $users = User::all();

        $data = [
            'tasks_select' => $tasks_select,
			'projects' => $projects,
            'projects_select' => $projects_select,
            'task'          => $task,
            'users' => $users
        ];
        return view(config('tasks.editIndividualTaskBlade'))->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteTask(Request $request)
    {
        $this_task_id = $request->id;
        if( 'NULL' == $this_task_id || !$this_task_id ){
            $task = new Task;
        }
        else{
            $task = Task::find($this_task_id);         
        }
        $child_tasks = Task::where('parent_id','=',$this_task_id)->get();
        foreach( $child_tasks as $child_task_arr){
            $child_task = Task::find($child_task_arr->id);
            if( $this_task_id ){
                $child_task->parent_id = $task->parent_id;
                $child_task->save();
            }
        }
        Task::destroy( $this_task_id );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Task::destroy($id);
        $tasks = Task::all();
        $data = [
            'tasks'             => $tasks,
            'pagintaionEnabled' => 0
        ];
        return redirect('/tasks');
    }

   /**
     * Method to search the users.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $searchTerm = $request->input('searchTerm');
        /*
        $searchRules = [
            'group_search_box' => 'required|string|max:255',
        ];
        $searchMessages = [
            'group_search_box.required' => 'Search term is required',
            'group_search_box.string'   => 'Search term has invalid characters',
            'group_search_box.max'      => 'Search term has too many characters - 255 allowed',
        ];
        $validator = Validator::make($request->all(), $searchRules, $searchMessages);

        if ($validator->fails()) {
            return response()->json([
                json_encode($validator),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
*/
         $results = config('tasks.defaultTaskModel')::where('id', 'like', $searchTerm.'%')
                            ->orWhere('name', 'like', '%'.$searchTerm.'%')->get();
      
    return response()->json($results);
    }    
}
