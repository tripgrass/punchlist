<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Laravel Tasks Blades Language Lines
    |--------------------------------------------------------------------------
    */

    'showing-all-tasks'     => 'Showing All Tasks',
    'tasks-menu-alt'        => 'Show Tasks Management Menu',
    'create-new-task'       => 'Create New Task',
    'show-deleted-tasks'    => 'Show Deleted Task',
    'editing-task'          => 'Editing Task :name',
    'showing-task'          => 'Showing Task :name',
    'showing-task-title'    => ':name\'s Information',

    'tasks-table' => [
        'caption'   => '{1} :taskscount task total|[2,*] :taskscount total tasks',
        'id'        => 'ID',
        'name'      => 'Name',
        'email'     => 'Email',
        'role'      => 'Role',
        'created'   => 'Created',
        'updated'   => 'Updated',
        'actions'   => 'Actions',
        'updated'   => 'Updated',
    ],

    'buttons' => [
        'create-new'    => '<span class="hidden-xs hidden-sm">New Task</span>',
        'delete'        => '<i class="far fa-trash-alt fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Delete</span><span class="hidden-xs hidden-sm hidden-md"> Task</span>',
        'show'          => '<i class="fas fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Show</span><span class="hidden-xs hidden-sm hidden-md"> Task</span>',
        'edit'          => '<i class="fas fa-pencil-alt fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Edit</span><span class="hidden-xs hidden-sm hidden-md"> Task</span>',
        'back-to-tasks' => '<span class="hidden-sm hidden-xs">Back to </span><span class="hidden-xs">Tasks</span>',
        'back-to-task'  => 'Back  <span class="hidden-xs">to Task</span>',
        'delete-task'   => '<i class="far fa-trash-alt fa-fw" aria-hidden="true"></i>  <span class="hidden-xs">Delete</span><span class="hidden-xs"> Task</span>',
        'edit-task'     => '<i class="fas fa-pencil-alt fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Edit</span><span class="hidden-xs"> Task</span>',
    ],

    'tooltips' => [
        'delete'        => 'Delete',
        'show'          => 'Show',
        'edit'          => 'Edit',
        'create-new'    => 'Create New Task',
        'back-tasks'    => 'Back to tasks',
        'email-task'    => 'Email :task',
        'submit-search' => 'Submit Tasks Search',
        'clear-search'  => 'Clear Search Results',
    ],

    'messages' => [
        'taskNameTaken'          => 'Taskname is taken',
        'taskNameRequired'       => 'Taskname is required',
        'fNameRequired'          => 'First Name is required',
        'lNameRequired'          => 'Last Name is required',
        'emailRequired'          => 'Email is required',
        'emailInvalid'           => 'Email is invalid',
        'passwordRequired'       => 'Password is required',
        'PasswordMin'            => 'Password needs to have at least 6 characters',
        'PasswordMax'            => 'Password maximum length is 20 characters',
        'captchaRequire'         => 'Captcha is required',
        'CaptchaWrong'           => 'Wrong captcha, please try again.',
        'roleRequired'           => 'Task role is required.',
        'task-creation-success'  => 'Successfully created task!',
        'update-task-success'    => 'Successfully updated task!',
        'delete-success'         => 'Successfully deleted the task!',
        'cannot-delete-yourself' => 'You cannot delete yourself!',
    ],

    'show-task' => [
        'id'                => 'Task ID',
        'name'              => 'Taskname',
        'email'             => '<span class="hidden-xs">Task </span>Email',
        'role'              => 'Task Role',
        'created'           => 'Created <span class="hidden-xs">at</span>',
        'updated'           => 'Updated <span class="hidden-xs">at</span>',
        'users'             => 'Users',
        'labelRole'         => 'Task Role',
        'labelAccessLevel'  => '<span class="hidden-xs">Task</span> Access Level|<span class="hidden-xs">Task</span> Access Levels',
    ],

    'search'  => [
        'title'         => 'Showing Search Results',
        'found-footer'  => ' Record(s) found',
        'no-results'    => 'No Results',
    ],
];
