import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class Task extends Component {
    constructor(props) {
        super(props);
        this.state = {
            prevUrl : "",
            nextUrl : "",
            tasks: []
        };
        if ( document.getElementById('tasks-card-body')) {
            var project_id = document.getElementById('tasks-card-body').dataset.projectid;
            axios
                .post('/api/projects/' + project_id + '/tasks')
                .then(response => {
                    console.log('from submit', response);
                    this.setState({
                        prevUrl: response.data.prev_page_url,
                        nextUrl: response.data.next_page_url,
                         tasks: [...response.data.data]
                    })
                });
            this.handlePrev = this.handlePrev.bind(this);            
            this.handleNext = this.handleNext.bind(this);            
            this.handleSearch = this.handleSearch.bind(this);            
        }
    }
    handleSearch(e){
        e.preventDefault();
        var _searchTerm = $('#task_search_box').val();
         axios
            .post('api/search-tasks', {
                searchTerm: _searchTerm 
              })
            .then(response => {
                console.log('from search submit', response);
                this.setState({
                    prevUrl: response.data.prev_page_url,
                    nextUrl: response.data.next_page_url,
                     tasks: [...response.data]
                })
            });

    }
    handlePrev(){
     axios
            .post(this.state.prevUrl)
            .then(response => {
                this.setState({
                    prevUrl: response.data.prev_page_url,
                    nextUrl: response.data.next_page_url,
                     tasks: [...response.data.data]
                })
            });

    }
    handleNext(){
         axios
            .post(this.state.nextUrl)
            .then(response => {
                console.log('from handle next', response);
                this.setState({
                    prevUrl: response.data.prev_page_url,
                    nextUrl: response.data.next_page_url,
                     tasks: [...response.data.data]
                })
            });
    }
    render() {
        return (
            <div>
                <form method="POST" action="/search-tasks" accept-charset="UTF-8" role="form" className="needs-validation" id="search_tasks" _lpchecked="1">
                    <div className="input-group mb-3">
                        <input id="task_search_box" onChange={this.handleSearch} className="form-control" placeholder="" aria-label="tasks::forms.search-tasks-ph" name="task_search_box" type="text"></input>
                        <div className="input-group-append">
                            <a href="#" className="btn btn-warning clear-search" data-toggle="tooltip" title="Clear Search Results">
                                <i className="fas fa-times" aria-hidden="true"></i>
                               <span className="sr-only">Clear Search Results</span>
                            </a>
                            <button className="btn btn-secondary"  data-toggle="tooltip" data-placement="bottom" title="Submit Tasks Search"><i className="fas fa-search" aria-hidden="true"></i> <span className="sr-only"> Submit Tasks Search </span></button>
                        </div>
                    </div>
                </form>            
                <div className="table-responsive tasks-table">
                    <table className="table table-striped table-sm data-table">
                        <caption id="task_count"></caption>
                        <thead className="thead">
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Parent Taskssss</th>
                                <th className="hidden-sm hidden-xs hidden-md">Created</th>
                                <th className="hidden-sm hidden-xs hidden-md">Updated</th>
                                <th className="no-search no-sort"></th>
                                <th className="no-search no-sort"></th>
                                <th className="no-search no-sort"></th>
                            </tr>
                        </thead>
                        <tbody id="task_table">
                            { this.state.tasks.map(task => (
                                 <tr key={ task.id}>
                                    <td>{ task.id}</td>
                                    <td>{ task.name}</td>
                                    <td>{ task.parent ? task.parent.name : "" }</td>
                                    <td className="hidden-sm hidden-xs hidden-md">{ task.created_at}</td>
                                    <td className="hidden-sm hidden-xs hidden-md">{ task.updated_at}</td>
                                    <td>
                                        <a className="btn btn-sm btn-success btn-block" href={"/tasks/" + task.id } data-toggle="tooltip">
                                            View Task
                                        </a>
                                    </td>
                                    <td>
                                    <form method="POST" accept-charset="UTF-8" className="" data-toggle="tooltip" title="Delete">
                                    <input name="_token" type="hidden" value=""></input>
                                                                        <input name="_method" type="hidden" value="DELETE"></input>
                                                                        <input name="task_id" type="hidden" value={ task.id}></input>
                                                                        <button className="btn btn-danger btn-sm" style={{width: '100%'}}  type="button"  data-toggle="modal" data-target="#confirmDelete" data-title="Delete Group" data-message="Are you sure you want to delete this group?">Delete</button>
                                                                    </form>
                                    </td>
                                    <td>
                                        <a className="btn btn-sm btn-info btn-block" href={"/tasks/" + task.id + "/edit"} data-toggle="tooltip">
                                            Edit Task
                                        </a>
                                    </td>
                                </tr>))
                            } 
                        </tbody>
                        <tbody id="search_results"></tbody>
                    </table>
                </div>
                <div className="paginated-links">
                    <span onClick={this.handlePrev}>Prev</span><span onClick={this.handleNext}>Next</span>
                </div>
            </div>
        );
    }
}
if (document.getElementById('tasks-card-body')) {

    ReactDOM.render(<Task />, document.getElementById('tasks-card-body'));
}
