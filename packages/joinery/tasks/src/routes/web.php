<?php
use Joinery\Tasks\Task;
use Joinery\Projects\Project;

Route::group(['middleware' => 'web', 'namespace' => 'joinery\tasks'], function () {
    Route::resource('tasks', 'TasksController', [
        'names' => [
            'index'   => 'tasks',
            'destroy' => 'task.destroy',
        ],
    ]);
});

?>