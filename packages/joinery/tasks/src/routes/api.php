<?php

use Illuminate\Http\Request;
use Joinery\Tasks\Task;
use Joinery\Projects\Project;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('api/projects/{id}/tasks', function($id, Request $request){
	$response = Task::where('project_id','=',$id)->paginate(15);
    foreach( $response as $task ){
       $task->parent = new Task(); 
       if( $task->parent_id ){
            $task->parent = Task::find($task->parent_id); 
        }
        $task->project = new Project();
        if( $task->project_id ){
            $task->project = Project::find($task->project_id); 
        }
    }
	return response()->json($response);

});
Route::post('api/taskNew', 'joinery\tasks\TasksController@store');
Route::post('api/taskUpdate', 'joinery\tasks\TasksController@apiStore');
Route::post('api/taskDelete', 'joinery\tasks\TasksController@deleteTask');
Route::post('api/search-tasks', 'joinery\tasks\TasksController@search');
