<?php

namespace joinery\tasks;

use Illuminate\Support\ServiceProvider;

class TasksServiceProvider extends ServiceProvider
{
    private $_packageTag = 'tasks';

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom( __DIR__.'/routes/api.php');
        $this->loadRoutesFrom( __DIR__.'/routes/web.php');
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        $this->loadTranslationsFrom(__DIR__.'/lang/', $this->_packageTag);
        $this->publishes([
            __DIR__.'/assets/js' => public_path('vendor/joinery'),
            __DIR__.'/assets/sass' => public_path('vendor/joinery'),
        ], 'public');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Joinery\Tasks\TasksController');
        $this->app->make('Joinery\Tasks\Task');
        $this->loadViewsFrom(__DIR__.'/views', 'tasks');
        $this->mergeConfigFrom(__DIR__.'/config/'.$this->_packageTag.'.php', $this->_packageTag);
        $this->app->singleton(joinery\tasks\TasksController\TasksController::class, function () {
            return new TasksController();
        });
        $this->app->alias(TasksController::class, 'tasks');
    }
}
