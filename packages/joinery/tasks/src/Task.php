<?php

namespace joinery\tasks;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{


	/**
	 * The project that the task belongs to.
	 */
	public function project()
	{
		return $this->belongsTo('App\Project');
	}    

   /**
	 * The users assigned to a task.
	 */
	public function users()
	{
		return $this->belongsToMany('App\User');
	}

   /**
	 * The users assigned to a task.
	 */
	public function groups()
	{
		return $this->belongsToMany('Joinery\Groups\Group');
	}

	public function notes(){
		return $this->hasMany('App\Note')->orderBy('created_at', 'ASC');
	}
	
	public function breaks($startDate = null, $endDate = null){
	  $breaks = Block::
				  where('ownerType','user')
				  ->where('owner_id',$this->id)
				  ->get();
	  return $breaks;
	   // echo "in breakds";
	}   
}
