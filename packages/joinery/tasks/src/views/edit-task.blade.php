@extends(config('tasks.GroupsBladeExtended'))

@section('template_title')
  @lang('tasks::tasks.editing-project', ['name' => $project->name])
@endsection

@section('template_linked_css')
    @if(config('tasks.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('tasks.datatablesCssCDN') }}">
    @endif
    @if(config('tasks.fontAwesomeEnabled'))
        <link rel="stylesheet" type="text/css" href="{{ config('tasks.fontAwesomeCdn') }}">
    @endif
    @include('tasks::partials.styles')
    @include('tasks::partials.bs-visibility-css')
@endsection

@section('content')
    <div class="container">
        @if(config('tasks.enablePackageBootstapAlerts'))
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    @include('tasks::partials.form-status')
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">
                            @lang('tasks::tasks.editing-task', ['name' => $task->name])
                            <div class="pull-right">
                                <a href="{{ route('projects') }}" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="top" title="@lang('tasks::tasks.tooltips.back-projects')">
                                    @if(config('tasks.fontAwesomeEnabled'))
                                        <i class="fas fa-fw fa-reply-all" aria-hidden="true"></i>
                                    @endif
                                    @lang('tasks::tasks.buttons.back-to-projects')
                                </a>
                                <a href="{{ url('/projects/' . $project->id) }}" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="left" title="@lang('tasks::tasks.tooltips.back-projects')">
                                    @if(config('tasks.fontAwesomeEnabled'))
                                        <i class="fas fa-fw fa-reply" aria-hidden="true"></i>
                                    @endif
                                    @lang('tasks::tasks.buttons.back-to-project')
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        {!! Form::open(array('route' => ['projects.update', $project->id], 'method' => 'PUT', 'role' => 'form', 'class' => 'needs-validation')) !!}
                            {!! csrf_field() !!}
                            <div class="form-task has-feedback row {{ $errors->has('name') ? ' has-error ' : '' }}">
                                @if(config('tasks.fontAwesomeEnabled'))
                                    {!! Form::label('name', trans('tasks::forms.create_project_label_projectname'), array('class' => 'col-md-3 control-label')); !!}
                                @endif
                                <div class="col-md-9">
                                    <div class="input-task">
                                        {!! Form::text('name', $project->name, array('id' => 'name', 'class' => 'form-control', 'placeholder' => trans('tasks::forms.create_project_ph_projectname'))) !!}
                                        <div class="input-task-append">
                                            <label class="input-task-text" for="name">
                                                @if(config('tasks.fontAwesomeEnabled'))
                                                    <i class="fa fa-fw {{ trans('tasks::forms.create_project_icon_projectname') }}" aria-hidden="true"></i>
                                                @else
                                                    @lang('tasks::forms.create_project_label_projectname')
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-task has-feedback row {{ $errors->has('email') ? ' has-error ' : '' }}">
                                @if(config('tasks.fontAwesomeEnabled'))
                                    {!! Form::label('email', trans('tasks::forms.create_project_label_email'), array('class' => 'col-md-3 control-label')); !!}
                                @endif
                                <div class="col-md-9">
                                    <div class="input-task">
                                        {!! Form::text('email', $project->email, array('id' => 'email', 'class' => 'form-control', 'placeholder' => trans('tasks::forms.create_project_ph_email'))) !!}
                                        <div class="input-task-append">
                                            <label for="email" class="input-task-text">
                                                @if(config('tasks.fontAwesomeEnabled'))
                                                    <i class="fa fa-fw {{ trans('tasks::forms.create_project_icon_email') }}" aria-hidden="true"></i>
                                                @else
                                                    @lang('tasks::forms.create_project_label_email')
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="pw-change-container">
                                <div class="form-task has-feedback row {{ $errors->has('password') ? ' has-error ' : '' }}">
                                    @if(config('tasks.fontAwesomeEnabled'))
                                        {!! Form::label('password', trans('tasks::forms.create_project_label_password'), array('class' => 'col-md-3 control-label')); !!}
                                    @endif
                                    <div class="col-md-9">
                                        <div class="input-task">
                                            {!! Form::password('password', array('id' => 'password', 'class' => 'form-control ', 'placeholder' => trans('tasks::forms.create_project_ph_password'))) !!}
                                            <div class="input-task-append">
                                                <label class="input-task-text" for="password">
                                                    @if(config('tasks.fontAwesomeEnabled'))
                                                        <i class="fa fa-fw {{ trans('tasks::forms.create_project_icon_password') }}" aria-hidden="true"></i>
                                                    @else
                                                        @lang('tasks::forms.create_project_label_password')
                                                    @endif
                                                </label>
                                            </div>
                                        </div>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-task has-feedback row {{ $errors->has('password_confirmation') ? ' has-error ' : '' }}">
                                    @if(config('tasks.fontAwesomeEnabled'))
                                        {!! Form::label('password_confirmation', trans('tasks::forms.create_project_label_pw_confirmation'), array('class' => 'col-md-3 control-label')); !!}
                                    @endif
                                    <div class="col-md-9">
                                        <div class="input-task">
                                            {!! Form::password('password_confirmation', array('id' => 'password_confirmation', 'class' => 'form-control', 'placeholder' => trans('tasks::forms.create_project_ph_pw_confirmation'))) !!}
                                            <div class="input-task-append">
                                                <label class="input-task-text" for="password_confirmation">
                                                    @if(config('tasks.fontAwesomeEnabled'))
                                                        <i class="fa fa-fw {{ trans('tasks::forms.create_project_icon_pw_confirmation') }}" aria-hidden="true"></i>
                                                    @else
                                                        @lang('tasks::forms.create_project_label_pw_confirmation')
                                                    @endif
                                                </label>
                                            </div>
                                        </div>
                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-6 mb-2">
                                    <a href="#" class="btn btn-outline-secondary btn-block btn-change-pw mt-3" title="@lang('tasks::forms.change-pw')">
                                        <i class="fa fa-fw fa-lock" aria-hidden="true"></i>
                                        <span></span> @lang('tasks::forms.change-pw')
                                    </a>
                                </div>
                                <div class="col-12 col-sm-6">
                                    {!! Form::button(trans('tasks::forms.save-changes'), array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2 btn-save','type' => 'button', 'data-toggle' => 'modal', 'data-target' => '#confirmSave', 'data-title' => trans('tasks::modals.edit_project__modal_text_confirm_title'), 'data-message' => trans('tasks::modals.edit_project__modal_text_confirm_message'))) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('tasks::modals.modal-save')
    @include('tasks::modals.modal-delete')

@endsection

@section('template_scripts')
    @include('tasks::scripts/csrf-token')
    @include('tasks::scripts.delete-modal-script')
    @include('tasks::scripts.save-modal-script')
    @include('tasks::scripts.check-changed')
    @if(config('tasks.tooltipsEnabled'))
        @include('tasks::scripts.tooltips')
    @endif
@endsection

