@extends('tasks::layouts.app')

@section('template_title')
    @lang('tasks::tasks.create-new-project')
@endsection

@section('template_linked_css')
    @if(config('tasks.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('tasks.datatablesCssCDN') }}">
    @endif
    @if(config('tasks.fontAwesomeEnabled'))
        <link rel="stylesheet" type="text/css" href="{{ config('tasks.fontAwesomeCdn') }}">
    @endif
    @include('tasks::partials.styles')
    @include('tasks::partials.bs-visibility-css')
@endsection

@section('content')
<?php 


    $edit = 0;
    $title = "Create New Task";
    $btn_text = "Create New Task";
    $form_name = "";
    $form_id = "NULL";
    $form_type = "calendar";
    $form_length = "";
    $form_project_id = "";
    $form_parent_id = "";
    $form_description = "";
    $form_startDate = "";
    $form_closedDate = "";
    $form_estStartDate = "";    
    if( isset($task) && is_object($task)){
        $edit = 1;
        $title = "Edit Task";
        $btn_text = "Update Task";
        $form_name = $task->name;
        $form_id = $task->id;
        $form_type = $task->type;
        $form_length = $task->length;
        $form_startDate = $task->startDate;
        $form_closedDate = $task->closedDate;
        $form_estStartDate = $task->estStartDate;
        $form_project_id = $task->project_id;
        $form_parent_id = $task->parent_id;
        $form_description = $task->description;
//        print_r($task);
    }
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <div class="container">
        @if(config('tasks.enablePackageBootstapAlerts'))
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <?php 
//                    @include('tasks::partials.form-status')
                ?>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">
                            {{ $title }}
                            <div class="pull-right">
                                <a href="/tasks" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="left" title="@lang('Back to Tasks')">
                                    @if(config('tasks.fontAwesomeEnabled'))
                                        <i class="fas fa-fw fa-reply-all" aria-hidden="true"></i>
                                    @endif
                                   Back to Tasks
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        {!! Form::open(array('route' => 'tasks.store', 'method' => 'POST', 'role' => 'form', 'class' => 'needs-validation')) !!}
                            {!! Form::hidden('id', $form_id, array('id' => 'id')) !!}
                            {!! csrf_field() !!}
                            <div class="form-group has-feedback  {{ $errors->has('name') ? ' has-error ' : '' }}">
                                <div class="col-md-12">
                                    @if(config('tasks.fontAwesomeEnabled'))
                                        {!! Form::label('name', 'Task Name', array('class' => ' control-label')); !!}
                                    @endif
                                    <div class="input-task">
                                        {!! Form::text('name', $form_name, array('id' => 'name', 'class' => 'form-control', 'placeholder' => 'Name')) !!}
                                    </div>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback  {{ $errors->has('description') ? ' has-error ' : '' }}">
                                <div class="col-md-12">
                                @if(config('tasks.fontAwesomeEnabled'))
                                    {!! Form::label('description', 'Task Description', array('class' => ' control-label')); !!}
                                @endif
                                    <div class="input-task">
                                        {!! Form::text('description', $form_description, array('id' => 'description', 'class' => 'form-control', 'placeholder' => 'Description')) !!}
                                    </div>
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group has-feedback row {{ $errors->has('users') ? ' has-error ' : '' }}">
                                    @if(config('projects.fontAwesomeEnabled'))
                                        {!! Form::label('users', 'Assign Task to Users', array('class' => 'col-md-3 control-label')); !!}
                                    @endif
                                    <div class="col-md-9">
                                        <div class="input-project">
    <?php 
    $select_users = [];
    foreach($users as $user){
        $select_users[$user->id] = $user->name;
    }
    $selected_users = [];
    if(isset($task) && isset($task->users)){
        foreach( $task->users as $user){
            $selected_users[$user->id] = $user->id;
        }
    }
    ?>
    {{Form::select('users',$select_users,$selected_users,array('multiple'=>'multiple','name'=>'users[]', 'id' => 'users', 'class' => 'form-control'))}}
                                        </div>
                                        @if ($errors->has('users'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('users') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group has-feedback  {{ $errors->has('project_id') ? ' has-error ' : '' }}">
                                    <div class="col-md-9">
                                        @if(config('tasks.fontAwesomeEnabled'))
                                            {!! Form::label('project_id', 'Project', array('class' => 'control-label')); !!}
                                        @endif
                                        <div class="input-task">
                                            {{Form::select('project_id',$projects_select,$form_project_id,array('name'=>'project_id', 'id' => 'project_id', 'class' => 'form-control'))}}
                                        </div>
                                        @if ($errors->has('project_id'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('project_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6 form-group has-feedback  {{ $errors->has('type') ? ' has-error ' : '' }}">
                                    <div class="col-md-9">
                                        @if(config('tasks.fontAwesomeEnabled'))
                                            {!! Form::label('type', 'Task Calendar Type', array('class' => 'control-label')); !!}
                                        @endif
                                        <div class="input-task">
                                            {{Form::select('type',['calendar'=>'Calendar Days','business'=>'Business Days'],$form_type,array('name'=>'type', 'id' => 'type', 'class' => 'form-control'))}}
                                        </div>
                                        @if ($errors->has('type'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('type') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group has-feedback  {{ $errors->has('length') ? ' has-error ' : '' }}">
                                    <div class="col-md-9">
                                        @if(config('tasks.fontAwesomeEnabled'))
                                            {!! Form::label('length', 'Task Length (in days)', array('class' => 'control-label')); !!}
                                        @endif
                                        <div class="input-task">
                                            {!! Form::text('length', $form_length, array('id' => 'length', 'class' => 'form-control', 'placeholder' => '')) !!}
                                        </div>
                                        @if ($errors->has('length'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('length') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6 form-group has-feedback {{ $errors->has('parent_id') ? ' has-error ' : '' }}">
                                    <div class="col-md-12">
                                        @if(config('tasks.fontAwesomeEnabled'))
                                            {!! Form::label('parent_id', 'Subsequent Task', array('class' => 'control-label')); !!}
                                        @endif
                                        <div class="input-task">
                                            {{Form::select('parent_id',$tasks_select,$form_parent_id,array('name'=>'parent_id', 'id' => 'parent_id', 'class' => 'form-control'))}}
                                        </div>
                                        @if ($errors->has('parent_id'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('parent_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group has-feedback {{ $errors->has('startDate') ? ' has-error ' : '' }}">
                                    @if(config('tasks.fontAwesomeEnabled'))
                                        {!! Form::label('startDate', 'Start Date', array('class' => 'col-md-4 control-label')); !!}
                                    @endif
                                    <div class="col-md-8">
                                        <div class="input-task">
                                            {!! Form::text('startDate', $form_startDate, array('id' => 'startDate', 'class' => 'form-control', 'placeholder' => '')) !!}
                                        </div>
                                        @if ($errors->has('startDate'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('startDate') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6 form-group has-feedback {{ $errors->has('closedDate') ? ' has-error ' : '' }}">
                                    @if(config('tasks.fontAwesomeEnabled'))
                                        {!! Form::label('closedDate', 'Closed Date', array('class' => 'col-md-4 control-label')); !!}
                                    @endif
                                    <div class="col-md-8">
                                        <div class="input-task">
                                            {!! Form::text('closedDate', $form_closedDate, array('id' => 'closedDate', 'class' => 'form-control', 'placeholder' => '')) !!}
                                        </div>
                                        @if ($errors->has('closedDate'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('closedDate') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            {!! Form::button( $btn_text, array('class' => 'btn btn-success margin-bottom-1 mb-1 float-right','type' => 'submit' )) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('template_scripts')
    @if(config('tasks.tooltipsEnabled'))
        @include('tasks::scripts.tooltips')
    @endif
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript">
    $( function() {
        $( "#startDate" ).datepicker();
        $( "#closedDate" ).datepicker({
            maxDate:"0"
        });
    });
  </script>
@endsection
