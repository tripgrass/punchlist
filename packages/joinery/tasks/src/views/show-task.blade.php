@extends(config('tasks.TasksBladeExtended'))

@section('template_title')
    @lang('tasks::tasks.showing-task', ['name' => $task->name])
@endsection

@section('template_linked_css')
    @if(config('tasks.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('tasks.datatablesCssCDN') }}">
    @endif
    @if(config('tasks.fontAwesomeEnabled'))
        <link rel="stylesheet" type="text/css" href="{{ config('tasks.fontAwesomeCdn') }}">
    @endif
    @include('tasks::partials.styles')
    @include('tasks::partials.bs-visibility-css')
@endsection

@section('content')
    <div class="container">
        @if(config('tasks.enablePackageBootstapAlerts'))
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    @include('tasks::partials.form-status')
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">
                            @lang('tasks::tasks.showing-task-title', ['name' => $task->name])
                            <div class="float-right">
                                <a href="{{ route('tasks') }}" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="left" title="@lang('tasks::tasks.tooltips.back-tasks')">
                                    @if(config('tasks.fontAwesomeEnabled'))
                                        <i class="fas fa-fw fa-reply-all" aria-hidden="true"></i>
                                    @endif
                                    @lang('tasks::tasks.buttons.back-to-tasks')
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <h4 class="text-muted text-center">
                            {{ $task->name }}
                        </h4>
                        @if($task->email)
                            <p class="text-center" data-toggle="tooltip" data-placement="top" title="@lang('tasks::tasks.tooltips.email-task', ['task' => $task->email])">
                                {{ Html::mailto($task->email, $task->email) }}
                            </p>
                        @endif
                        <div class="row mb-4">
                            <div class="col-3 offset-3 col-sm-4 offset-sm-2 col-md-4 offset-md-2 col-lg-3 offset-lg-3">
                                <a href="/tasks/{{$task->id}}/edit" class="btn btn-block btn-md btn-warning">
                                    @lang('tasks::tasks.buttons.edit-task')
                                </a>
                            </div>
                            <div class="col-3 col-sm-4 col-md-4 col-lg-3">
                                {!! Form::open(array('url' => 'tasks/' . $task->id, 'class' => 'form-inline')) !!}
                                    {!! Form::hidden('_method', 'DELETE') !!}
                                    {!! Form::hidden('task_id', $task->id) !!}
                                    {!! Form::button(trans('tasks::tasks.buttons.delete-task'), array('class' => 'btn btn-danger btn-md btn-block','type' => 'button', 'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete Project', 'data-message' => 'Are you sure you want to delete this task?')) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <ul class="list-task list-task-flush">
                            <li class="list-task-item">
                                <div class="row">
                                    <div class="col-4 col-sm-3">
                                        <strong>
                                            @lang('tasks::tasks.show-task.id')
                                        </strong>
                                    </div>
                                    <div class="col-8 col-sm-9">
                                        {{ $task->id }}
                                    </div>
                                </div>
                            </li>
                            @if ($task->name)
                                <li class="list-task-item">
                                    <div class="row">
                                        <div class="col-4 col-sm-3">
                                            <strong>
                                                @lang('tasks::tasks.show-task.name')
                                            </strong>
                                        </div>
                                        <div class="col-8 col-sm-9">
                                            {{ $task->name }}
                                        </div>
                                    </div>
                                </li>
                            @endif
                            @if ($task->email)
                                <li class="list-task-item">
                                    <div class="row">
                                        <div class="col-12 col-sm-3">
                                            <strong>
                                                @lang('tasks::tasks.show-task.email')
                                            </strong>
                                        </div>
                                        <div class="col-12 col-sm-9">
                                            {{ $task->email }}
                                        </div>
                                    </div>
                                </li>
                            @endif
                            @if ($task->created_at)
                                <li class="list-task-item">
                                    <div class="row">
                                        <div class="col-4 col-sm-3">
                                            <strong>
                                                @lang('tasks::tasks.show-task.created')
                                            </strong>
                                        </div>
                                        <div class="col-8 col-sm-9">
                                            {{ $task->created_at }}
                                        </div>
                                    </div>
                                </li>
                            @endif
                            @if ($task->updated_at)
                                <li class="list-task-item">
                                    <div class="row">
                                        <div class="col-4 col-sm-3">
                                            <strong>
                                                @lang('tasks::tasks.show-task.updated')
                                            </strong>
                                        </div>
                                        <div class="col-8 col-sm-9">
                                            {{ $task->updated_at }}
                                        </div>
                                    </div>
                                </li>
                            @endif
                            @if ($task->projects)
                                <li class="list-task-item">
                                    <div class="row">
                                        <div class="col-4 col-sm-3">
                                            <strong>
                                                @lang('tasks::tasks.show-task.projects')
                                            </strong>
                                        </div>
                                        <div class="col-8 col-sm-9">
                                            <ul>
                                             @foreach($task->projects as $project)
                                                <li><a href={{ URL::to('projects/' . $project->id) }}>{{ $project->name }}</a></li>
                                             @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('tasks::modals.modal-delete')
@endsection
@section('template_scripts')
    @include('tasks::scripts/csrf-token')
    @include('tasks::scripts.delete-modal-script')

    @include('tasks::scripts.delete-modal-script')
    @if(config('tasks.tooltipsEnabled'))
        @include('tasks::scripts.tooltips')
    @endif
@endsection
