@extends(config('projects.ProjectsBladeExtended'))

@section('template_title')
    @lang('tasks::tasks.showing-all-projects') @endsection

@section('template_linked_css')
    @if(config('tasks.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('tasks.datatablesCssCDN') }}">
    @endif
    @if(config('tasks.fontAwesomeEnabled'))
        <link rel="stylesheet" type="text/css" href="{{ config('tasks.fontAwesomeCdn') }}">
    @endif
    @include('tasks::partials.styles')
    @include('tasks::partials.bs-visibility-css')
@endsection

@section('content')
    <div class="container">
        @if(config('tasks.enablePackageBootstapAlerts'))
            <div class="row">
                <div class="col-sm-12">
                  <?php  @include('tasks::partials.form-status') ?>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                               Showing all Tasks
                            </span>

                            <div class="btn-task pull-right btn-task-xs">
                                @if(config('tasks.softDeletedEnabled'))
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i>
                                        <span class="sr-only">
                                            @lang('tasks::tasks.projects-menu-alt')
                                        </span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ route('projects.create') }}">
                                                @if(config('tasks.fontAwesomeEnabled'))
                                                    <i class="fa fa-fw fa-project-plus" aria-hidden="true"></i>
                                                @endif
                                               New Task
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/projects/deleted">
                                                @if(config('tasks.fontAwesomeEnabled'))
                                                    <i class="fa fa-fw fa-task" aria-hidden="true"></i>
                                                @endif
                                                @lang('tasks::tasks.show-deleted-projects')
                                            </a>
                                        </li>
                                    </ul>
                                @else
                                    <a href="/tasks/create" class="btn btn-default btn-sm pull-right" data-toggle="tooltip" data-placement="left" title="@lang('tasks::tasks.tooltips.create-new')">
                                        @if(config('tasks.fontAwesomeEnabled'))
                                            <i class="fa fa-fw fa-project-plus" aria-hidden="true"></i>
                                        @endif
                                        New Task
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-body" id="tasks-card-body" data-projectId={{$project_id}}>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @include('tasks::modals/modal-delete')

@endsection
@section('template_scripts')
    @include('tasks::scripts/csrf-token')
    @if ((count($tasks) > config('tasks.datatablesJsStartCount')) && config('tasks.enabledDatatablesJs'))
        @include('tasks::scripts.datatables')
    @endif
    @include('tasks::scripts.delete-modal-script')
    @include('tasks::scripts.save-modal-script')
    @if(config('tasks.tooltipsEnabled'))
        @include('tasks::scripts.tooltips')
    @endif
@endsection
