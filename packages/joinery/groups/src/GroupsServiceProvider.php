<?php

namespace joinery\groups;

use Illuminate\Support\ServiceProvider;

class GroupsServiceProvider extends ServiceProvider
{
    private $_packageTag = 'groups';

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom( __DIR__.'/routes/api.php');
        $this->loadRoutesFrom( __DIR__.'/routes/web.php');
//        include __DIR__.'/routes/api.php';
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        $this->loadTranslationsFrom(__DIR__.'/lang/', $this->_packageTag);
        $this->publishes([
            __DIR__.'/assets/js' => public_path('vendor/joinery'),
            __DIR__.'/assets/sass' => public_path('vendor/joinery'),
        ], 'public');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Joinery\Groups\GroupsController');
        $this->app->make('Joinery\Groups\Group');
        $this->loadViewsFrom(__DIR__.'/views', 'groups');
        $this->mergeConfigFrom(__DIR__.'/config/'.$this->_packageTag.'.php', $this->_packageTag);
        $this->app->singleton(joinery\groups\GroupsController\GroupsController::class, function () {
            return new GroupsController();
        });
        $this->app->alias(GroupsController::class, 'groups');
    }
}
