import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class Example extends Component {
    constructor(props) {
        super(props);
        this.state = {
            prevUrl : "",
            nextUrl : "",
            groups: []
        };
        var api_token = $("meta[name='api_token']").attr("content"); 
        axios
            .post('/api/groups'  + '?api_token=' + api_token)
            .then(response => {
                console.log('from handle submit', response.data.data);
                var groups = [];
                Object.keys(response.data.data).map(function(key) {
                    var projects = [];
                    var group = response.data.data[key];
                    Object.keys(group.projects).map(function(projectKey) {
                        projects.push( group.projects[projectKey] );
                    });
                    group.projects = projects;
                    groups.push( group );
                });

                this.setState({
                    prevUrl: response.data.prev_page_url,
                    nextUrl: response.data.next_page_url,
                     groups: groups
                })
                console.log('ttttthis',this);
            });
        this.handlePrev = this.handlePrev.bind(this);            
        this.handleNext = this.handleNext.bind(this);            
        this.handleSearch = this.handleSearch.bind(this);            
    }
    handleSearch(e){
        e.preventDefault();
        var _searchTerm = $('#group_search_box').val();
         axios
            .post('api/search-groups', {
                searchTerm: _searchTerm 
              })
            .then(response => {
                console.log('from search submit', response);
                this.setState({
                    prevUrl: response.data.prev_page_url,
                    nextUrl: response.data.next_page_url,
                     groups: [...response.data]
                })
            });

    }
    handlePrev(){
     axios
            .post(this.state.prevUrl)
            .then(response => {
                this.setState({
                    prevUrl: response.data.prev_page_url,
                    nextUrl: response.data.next_page_url,
                     groups: [...response.data.data]
                })
            });

    }
    handleNext(){
         axios
            .post(this.state.nextUrl)
            .then(response => {
                this.setState({
                    prevUrl: response.data.prev_page_url,
                    nextUrl: response.data.next_page_url,
                     groups: [...response.data.data]
                })
            });
    }
    render() {
        console.log('this state', this.state);
        this.state.groups.map(function(item, i){
            console.log('test', item);
            console.log('test', i);
        })
          return (
            <div>
                <form method="POST" action="http://punchlist.local/search-groups" acceptCharset="UTF-8" role="form" className="needs-validation" id="search_groups" _lpchecked="1">
                    <div className="input-group mb-3">
                        <input id="group_search_box" onChange={this.handleSearch} className="form-control" placeholder="" aria-label="groups::forms.search-groups-ph" name="group_search_box" type="text"></input>
                        <div className="input-group-append">
                            <a href="#" className="btn btn-warning clear-search" data-toggle="tooltip" title="Clear Search Results">
                                <i className="fas fa-times" aria-hidden="true"></i>
                               <span className="sr-only">Clear Search Results</span>
                            </a>
                            <button className="btn btn-secondary"  data-toggle="tooltip" data-placement="bottom" title="Submit Groups Search"><i className="fas fa-search" aria-hidden="true"></i> <span className="sr-only"> Submit Groups Search </span></button>
                        </div>
                    </div>
                </form>            
                <div className="table-responsive users-table">
                    <table className="table table-striped table-sm data-table">
                        <caption id="user_count"></caption>
                        <thead className="thead">
                                    <tr>
                                        <th>Group Name</th>
                                        <th>Your Group Alias</th>
                                        <th>Admin</th>
                                        <th></th>
                                    </tr>                        
                        </thead>
                        <tbody id="users_table">
                            { this.state.groups.map(group => (
                                 <tr key={group.group.id}>
                                    <td>{group.group.name}</td>
                                    <td>{group.group.obj.aliasName}</td>
                                    <td>{group.group.obj.admin.name}</td>
                                    <td>
                                        <a className="btn btn-sm btn-inline-block" href={"/groups/" + group.group.id + "/edit"} data-toggle="tooltip">
                                            Edit Partner
                                        </a>
                                    </td>
                                </tr>))
                            } 
                        </tbody>
                        <tbody id="search_results"></tbody>
                    </table>
                </div>
            </div>
        );
    }
}
if (document.getElementById('card-body')) {
    ReactDOM.render(<Example />, document.getElementById('card-body'));
}
