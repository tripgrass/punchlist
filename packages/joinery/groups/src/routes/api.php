<?php

use Illuminate\Http\Request;
use Joinery\Groups\Group;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->post('api/groups', function(Request $request){
	// filter group by current user
//$response = Group::paginate(50);
	$user = Auth::user();
	// get all projects that current group OWNS
    $thisGroupsProjects = DB::table('projects')
        ->join('group_project', function($join) use($user) {
            $join->on('projects.id', '=', 'group_project.project_id')
                 ->where(function($query) {
                        $query->where('group_project.isSecondary', NULL)
                            ->orWhere('group_project.isSecondary', 0);
                    })
                 ->where('group_project.group_id', $user->current_group);
            })
        ->get();
    $projectsInArray = [];
    foreach($thisGroupsProjects as $proj){
        $projectsInArray[] = $proj->project_id;
    }
    $projects = DB::table('projects')
        ->select(
            'groups.name as group_name', 
            'groups.id as group_id', 
            'projects.name as project_name',
            'projects.id as project_id',
            'isAlias',
            'alias_id',
            'approved',
            'approvedDate',
            'approvedBy_id',
            'isSecondary',
            'invited',
            'invitedDate',
            'accepted',
            'acceptedDate'
        )
        ->join('group_project', function($join) use($user, $projectsInArray) {
            $join->on('projects.id', '=', 'group_project.project_id')
                 ->where(function($query) {
                        $query->where('group_project.isSecondary', 1);
                    })
                 ->whereIn('group_project.project_id', $projectsInArray);
            })
        ->join('groups', 'group_project.group_id', '=', 'groups.id')
        ->get();
        $partners = [];
        foreach($projects as $proj ){
            $group_id = $proj->group_id;
$group = Group::where('id',$group_id)->first();
$group->admin;
            $project_id = $proj->project_id;
            $partners[$group_id]['group']['obj'] = $group;
            $partners[$group_id]['projects'][$project_id] = $proj;
            $partners[$group_id]['group']['id'] = $proj->group_id;
            $partners[$group_id]['group']['name'] = $proj->group_name;
            $partners[$group_id]['group']['isAlias'] = $proj->isAlias;
            $partners[$group_id]['group']['alias_id'] = $proj->alias_id;
        }
        $data['data'] = $partners;
	return response()->json($data);

});
Route::post('api/search-groups', 'joinery\groups\GroupsController@search');
