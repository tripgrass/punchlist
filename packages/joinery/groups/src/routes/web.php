<?php
use Joinery\Groups\Group;
use Illuminate\Http\Request;
use App\User;
use App\Account;
use Joinery\Projects\Project;


Route::group(['middleware' => 'web', 'namespace' => 'joinery\groups'], function () {
    Route::resource('groups', 'GroupsController', [
        'names' => [
            'index'   => 'groups',
            'destroy' => 'group.destroy',
        ],
    ]);
    Route::post('/groups/detachPartner', function(Request $request){
        $user = Auth::user();
        $current_group = Group::find($user->current_group);
        $project = Project::find($request->project_id);
        $project->groups()->detach($request->detach_group_id);
    });
    Route::post('/groups/addPartner', function(Request $request){
        $user = Auth::user();
        //User::where('api_token', $request->api_token)->first();
        $current_group = Group::find($user->current_group);
        // check if user has auth on project
        $project = Project::find($request->project_id);
        if( 'new' == $request->groupType ){
            $email = $request->newGroup;
            if(!$email){
                $email = $request->email;
            }
            $user = User::where('email','=',$email)->first();
            if(!$user){
                $rules = [
                    'email'                 => 'required|email|max:255'
                ];

                $messages = [
                    'email.required'      => trans('Email is required'),
                    'email.email'         => trans('Invalid email')
                ];

                $validator = Validator::make($request->all(), $rules, $messages);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                }


                $user = User::create([
                    'name' => $email,
                    'email' => $email,
                    'password' => Hash::make(rand()),
                    'isAlias' => 1,
                ]);

                $accountType = 'Group Partner';
                if( $request->alias_name ){
                    $name = $request->alias_name;
                }
                else{
                    $name = $email;
                }
                $group = Group::create([
                    'aliasName' => $name,
                    'name' => $name,
                    'isAlias' => 1,
                ]);
                $current_group->teams()->attach($group->id,[]);

                foreach($request->projects as $project_id){
                    $attach_project = Project::find($project_id);

                    $attach_project->groups()->attach($group->id,[
                        'isSecondary' => '1',
                        'invited' => 1,
                        'invitedDate' => Carbon::now()
                    ]);

                }
                $user->groups()->attach($group->id,[
                    'role' => 'admin',
                    'isAdmin' => 1
                ]);
                $user->current_group = $group->id;
                $account = Account::create([
                    'name' => $accountType,
                    'monthlyFee' => '0',
                    'group_id' => $group->id,
                    'active' => 1
                ]);
                $user->save();
            }
        }
        else{
            if('edit' == $request->group_type ){
                $partner = Group::find($request->group_id);

                $partner->aliasName = $request->alias_name;
                if( $partner->approved && $request->edited_by_owner && $partner->admin ){
                    // check if 
                    $admin = $partner->admin;
                }
                if( !$partner->approved && $partner->admin ){
                    $rules = [
                        'email'                 => 'required|email|max:255'
                    ];

                    $messages = [
                        'email.required'      => trans('Email is required'),
                        'email.email'         => trans('Invalid email')
                    ];

                    $validator = Validator::make($request->all(), $rules, $messages);

                    if ($validator->fails()) {
                        return back()->withErrors($validator)->withInput();
                    }


                    $admin = $partner->admin;
                    $admin->email = $request->email;
                    $admin->save();
                }
                //$partner->projects()->detach();
                foreach($request->projects as $project_id){

                    $attach_project = Project::find($project_id);
//echo "-->".$project_id."<br>";
//print_r($attach_project);
//die;
                    $attach_project->groups()->attach($partner->id,[
                        'isSecondary' => '1',
                        'invited' => 1,
                        'invitedDate' => Carbon::now()
                    ]);

                }

                $partner->save();
            }
            else{
                if( $request->group_id ){
                    $partner = Group::find($request->group_id);
                    $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
                    $inviteCode = substr(str_shuffle($permitted_chars), 0, 12);
                    $project->groups()->attach($request->group_id,[
                        'isSecondary' => '1',
                        'invited' => 1,
                        'invitedDate' => Carbon::now(),
                        'inviteCode' => $inviteCode
                    ]);
                    $current_group->teams()->attach($partner->id,[]);
                    $partner->admin->notify(new groupInvite($project,$partner,$current_group, $inviteCode));
                }
                else if( $request->admin_email){
                    // invite USER by email
                    $email = $request->admin_email;
                    $user = User::where('email','=',$email)->first();
                    if(!$user){
                        $user = User::create([
                            'name' => $email,
                            'email' => $email,
                            'password' => Hash::make(rand()),
                        ]);
                        $accountType = 'Group Partner';
                        $group = Group::create([
                            'name' => $email
                        ]);
                        $user->groups()->attach($group_id,[
                            'role' => 'admin',
                            'isAdmin' => 1
                        ]);
                        $user->current_group = $group->id;
                        $account = Account::create([
                            'name' => $accountType,
                            'monthlyFee' => '0',
                            'group_id' => $group->id,
                            'active' => 1
                        ]);
                        $project->groups()->attach($group->id,[
                            'isSecondary' => '1',
                            'invited' => 1,
                            'invitedDate' => Carbon::now()
                        ]);

                        $user->save();

                        //create NEW user from email
                    }
                }
            }

        }
         return redirect('/groups');

    //    return response()->json($request->all() );

      //return response()->json($response);
    });

});

//Route::post('/groups/new', 'joinery\groups\GroupsController@store');
//Route::delete('groups/{id}','GroupsController@destroy');
/*Route::middleware(['web', 'auth'])->group(function () {
    Route::post('search-groups', 'joinery\groups\GroupsController@search')->name('search-groups');
});
*/
?>