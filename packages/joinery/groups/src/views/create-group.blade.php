@extends(config('groups.GroupsBladeExtended'))

@section('template_title')
    @lang('groups::groups.create-new-user')
@endsection

@section('template_linked_css')
    @if(config('groups.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('groups.datatablesCssCDN') }}">
    @endif
    @if(config('groups.fontAwesomeEnabled'))
        <link rel="stylesheet" type="text/css" href="{{ config('groups.fontAwesomeCdn') }}">
    @endif
    @include('groups::partials.styles')
    @include('groups::partials.bs-visibility-css')
@endsection

@section('content')
@include('forms.new_group')
@endsection

@section('template_scripts')
    @if(config('groups.tooltipsEnabled'))
        @include('groups::scripts.tooltips')
    @endif
@endsection
