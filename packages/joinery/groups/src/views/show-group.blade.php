@extends(config('groups.GroupsBladeExtended'))

@section('template_title')
    @lang('groups::groups.showing-group', ['name' => $group->name])
@endsection

@section('template_linked_css')
    @if(config('groups.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('groups.datatablesCssCDN') }}">
    @endif
    @if(config('groups.fontAwesomeEnabled'))
        <link rel="stylesheet" type="text/css" href="{{ config('groups.fontAwesomeCdn') }}">
    @endif
    @include('groups::partials.styles')
    @include('groups::partials.bs-visibility-css')
@endsection

@section('content')
    <div class="container">
        @if(config('groups.enablePackageBootstapAlerts'))
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    @include('groups::partials.form-status')
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">
                            @lang('groups::groups.showing-group-title', ['name' => $group->name])
                            <div class="float-right">
                                <a href="{{ route('groups') }}" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="left" title="@lang('groups::groups.tooltips.back-groups')">
                                    @if(config('groups.fontAwesomeEnabled'))
                                        <i class="fas fa-fw fa-reply-all" aria-hidden="true"></i>
                                    @endif
                                    @lang('groups::groups.buttons.back-to-groups')
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <h4 class="text-muted text-center">
                            {{ $group->name }}
                        </h4>
                        @if($group->email)
                            <p class="text-center" data-toggle="tooltip" data-placement="top" title="@lang('groups::groups.tooltips.email-group', ['group' => $group->email])">
                                {{ Html::mailto($group->email, $group->email) }}
                            </p>
                        @endif
                        <div class="row mb-4">
                            <div class="col-3 offset-3 col-sm-4 offset-sm-2 col-md-4 offset-md-2 col-lg-3 offset-lg-3">
                                <a href="/groups/{{$group->id}}/edit" class="btn btn-block btn-md btn-warning">
                                    @lang('groups::groups.buttons.edit-group')
                                </a>
                            </div>
                            <div class="col-3 col-sm-4 col-md-4 col-lg-3">
                                {!! Form::open(array('url' => 'groups/' . $group->id, 'class' => 'form-inline')) !!}
                                    {!! Form::hidden('_method', 'DELETE') !!}
                                    {!! Form::hidden('group_id', $group->id) !!}
                                    {!! Form::button(trans('groups::groups.buttons.delete-group'), array('class' => 'btn btn-danger btn-md btn-block','type' => 'button', 'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete User', 'data-message' => 'Are you sure you want to delete this group?')) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-4 col-sm-3">
                                        <strong>
                                            @lang('groups::groups.show-group.id')
                                        </strong>
                                    </div>
                                    <div class="col-8 col-sm-9">
                                        {{ $group->id }}
                                    </div>
                                </div>
                            </li>
                            @if ($group->name)
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-4 col-sm-3">
                                            <strong>
                                                @lang('groups::groups.show-group.name')
                                            </strong>
                                        </div>
                                        <div class="col-8 col-sm-9">
                                            {{ $group->name }}
                                        </div>
                                    </div>
                                </li>
                            @endif
                            @if ($group->email)
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-12 col-sm-3">
                                            <strong>
                                                @lang('groups::groups.show-group.email')
                                            </strong>
                                        </div>
                                        <div class="col-12 col-sm-9">
                                            {{ $group->email }}
                                        </div>
                                    </div>
                                </li>
                            @endif
                            @if ($group->created_at)
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-4 col-sm-3">
                                            <strong>
                                                @lang('groups::groups.show-group.created')
                                            </strong>
                                        </div>
                                        <div class="col-8 col-sm-9">
                                            {{ $group->created_at }}
                                        </div>
                                    </div>
                                </li>
                            @endif
                            @if ($group->updated_at)
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-4 col-sm-3">
                                            <strong>
                                                @lang('groups::groups.show-group.updated')
                                            </strong>
                                        </div>
                                        <div class="col-8 col-sm-9">
                                            {{ $group->updated_at }}
                                        </div>
                                    </div>
                                </li>
                            @endif
                            @if ($group->users)
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-4 col-sm-3">
                                            <strong>
                                                @lang('groups::groups.show-group.users')
                                            </strong>
                                        </div>
                                        <div class="col-8 col-sm-9">
                                            <ul>
                                             @foreach($group->users as $user)
                                                <li><a href={{ URL::to('users/' . $user->id) }}>{{ $user->name }}</a></li>
                                             @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('groups::modals.modal-delete')
@endsection
@section('template_scripts')
    @include('groups::scripts/csrf-token')
    @include('groups::scripts.delete-modal-script')

    @include('groups::scripts.delete-modal-script')
    @if(config('groups.tooltipsEnabled'))
        @include('groups::scripts.tooltips')
    @endif
@endsection
