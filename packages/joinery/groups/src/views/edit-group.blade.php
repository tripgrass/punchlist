@extends(config('groups.GroupsBladeExtended'))

@section('template_title')
  @lang('groups::groups.editing-user', ['name' => $user->name])
@endsection

@section('template_linked_css')
    @if(config('groups.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('groups.datatablesCssCDN') }}">
    @endif
    @if(config('groups.fontAwesomeEnabled'))
        <link rel="stylesheet" type="text/css" href="{{ config('groups.fontAwesomeCdn') }}">
    @endif
    @include('groups::partials.styles')
    @include('groups::partials.bs-visibility-css')
@endsection

@section('content')
    <div class="container">
        @if(config('groups.enablePackageBootstapAlerts'))
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    @include('groups::partials.form-status')
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">
                            @lang('groups::groups.editing-group', ['name' => $group->name])
                            <div class="pull-right">
                                <a href="{{ route('users') }}" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="top" title="@lang('groups::groups.tooltips.back-users')">
                                    @if(config('groups.fontAwesomeEnabled'))
                                        <i class="fas fa-fw fa-reply-all" aria-hidden="true"></i>
                                    @endif
                                    @lang('groups::groups.buttons.back-to-users')
                                </a>
                                <a href="{{ url('/users/' . $user->id) }}" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="left" title="@lang('groups::groups.tooltips.back-users')">
                                    @if(config('groups.fontAwesomeEnabled'))
                                        <i class="fas fa-fw fa-reply" aria-hidden="true"></i>
                                    @endif
                                    @lang('groups::groups.buttons.back-to-user')
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        {!! Form::open(array('route' => ['groups.update', $group->id], 'method' => 'PUT', 'role' => 'form', 'class' => 'needs-validation')) !!}
                            {!! csrf_field() !!}
                            <div class="form-group has-feedback row {{ $errors->has('name') ? ' has-error ' : '' }}">
                                @if(config('groups.fontAwesomeEnabled'))
                                    {!! Form::label('name', trans('groups::forms.create_user_label_username'), array('class' => 'col-md-3 control-label')); !!}
                                @endif
                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('name', $user->name, array('id' => 'name', 'class' => 'form-control', 'placeholder' => trans('groups::forms.create_user_ph_username'))) !!}
                                        <div class="input-group-append">
                                            <label class="input-group-text" for="name">
                                                @if(config('groups.fontAwesomeEnabled'))
                                                    <i class="fa fa-fw {{ trans('groups::forms.create_user_icon_username') }}" aria-hidden="true"></i>
                                                @else
                                                    @lang('groups::forms.create_user_label_username')
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback row {{ $errors->has('email') ? ' has-error ' : '' }}">
                                @if(config('groups.fontAwesomeEnabled'))
                                    {!! Form::label('email', trans('groups::forms.create_user_label_email'), array('class' => 'col-md-3 control-label')); !!}
                                @endif
                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('email', $user->email, array('id' => 'email', 'class' => 'form-control', 'placeholder' => trans('groups::forms.create_user_ph_email'))) !!}
                                        <div class="input-group-append">
                                            <label for="email" class="input-group-text">
                                                @if(config('groups.fontAwesomeEnabled'))
                                                    <i class="fa fa-fw {{ trans('groups::forms.create_user_icon_email') }}" aria-hidden="true"></i>
                                                @else
                                                    @lang('groups::forms.create_user_label_email')
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            @if($rolesEnabled)
                                <div class="form-group has-feedback row {{ $errors->has('role') ? ' has-error ' : '' }}">
                                    @if(config('groups.fontAwesomeEnabled'))
                                        {!! Form::label('role', trans('groups::forms.create_user_label_role'), array('class' => 'col-md-3 control-label')); !!}
                                    @endif
                                    <div class="col-md-9">
                                    <div class="input-group">
                                        <select class="custom-select form-control" name="role" id="role">
                                            <option value="">{{ trans('groups::forms.create_user_ph_role') }}</option>
                                            @if ($roles)
                                                @foreach($roles as $role)
                                                    <option value="{{ $role->id }}" {{ $currentRole->id == $role->id ? 'selected="selected"' : '' }}>{{ $role->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <div class="input-group-append">
                                            <label class="input-group-text" for="role">
                                                @if(config('groups.fontAwesomeEnabled'))
                                                    <i class="{{ trans('groups::forms.create_user_icon_role') }}" aria-hidden="true"></i>
                                                @else
                                                    @lang('groups::forms.create_user_label_username')
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                    @if ($errors->has('role'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('role') }}</strong>
                                        </span>
                                    @endif
                                    </div>
                                </div>
                            @endif
                            <div class="pw-change-container">
                                <div class="form-group has-feedback row {{ $errors->has('password') ? ' has-error ' : '' }}">
                                    @if(config('groups.fontAwesomeEnabled'))
                                        {!! Form::label('password', trans('groups::forms.create_user_label_password'), array('class' => 'col-md-3 control-label')); !!}
                                    @endif
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            {!! Form::password('password', array('id' => 'password', 'class' => 'form-control ', 'placeholder' => trans('groups::forms.create_user_ph_password'))) !!}
                                            <div class="input-group-append">
                                                <label class="input-group-text" for="password">
                                                    @if(config('groups.fontAwesomeEnabled'))
                                                        <i class="fa fa-fw {{ trans('groups::forms.create_user_icon_password') }}" aria-hidden="true"></i>
                                                    @else
                                                        @lang('groups::forms.create_user_label_password')
                                                    @endif
                                                </label>
                                            </div>
                                        </div>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group has-feedback row {{ $errors->has('password_confirmation') ? ' has-error ' : '' }}">
                                    @if(config('groups.fontAwesomeEnabled'))
                                        {!! Form::label('password_confirmation', trans('groups::forms.create_user_label_pw_confirmation'), array('class' => 'col-md-3 control-label')); !!}
                                    @endif
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            {!! Form::password('password_confirmation', array('id' => 'password_confirmation', 'class' => 'form-control', 'placeholder' => trans('groups::forms.create_user_ph_pw_confirmation'))) !!}
                                            <div class="input-group-append">
                                                <label class="input-group-text" for="password_confirmation">
                                                    @if(config('groups.fontAwesomeEnabled'))
                                                        <i class="fa fa-fw {{ trans('groups::forms.create_user_icon_pw_confirmation') }}" aria-hidden="true"></i>
                                                    @else
                                                        @lang('groups::forms.create_user_label_pw_confirmation')
                                                    @endif
                                                </label>
                                            </div>
                                        </div>
                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-6 mb-2">
                                    <a href="#" class="btn btn-outline-secondary btn-block btn-change-pw mt-3" title="@lang('groups::forms.change-pw')">
                                        <i class="fa fa-fw fa-lock" aria-hidden="true"></i>
                                        <span></span> @lang('groups::forms.change-pw')
                                    </a>
                                </div>
                                <div class="col-12 col-sm-6">
                                    {!! Form::button(trans('groups::forms.save-changes'), array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2 btn-save','type' => 'button', 'data-toggle' => 'modal', 'data-target' => '#confirmSave', 'data-title' => trans('groups::modals.edit_user__modal_text_confirm_title'), 'data-message' => trans('groups::modals.edit_user__modal_text_confirm_message'))) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('groups::modals.modal-save')
    @include('groups::modals.modal-delete')

@endsection

@section('template_scripts')
    @include('groups::scripts/csrf-token')
    @include('groups::scripts.delete-modal-script')
    @include('groups::scripts.save-modal-script')
    @include('groups::scripts.check-changed')
    @if(config('groups.tooltipsEnabled'))
        @include('groups::scripts.tooltips')
    @endif
@endsection

