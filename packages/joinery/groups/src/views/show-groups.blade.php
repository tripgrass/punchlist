@extends('layouts.app')

@section('template_title')
    @lang('groups::groups.showing-all-users') @endsection

@section('template_linked_css')
    @if(config('groups.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('groups.datatablesCssCDN') }}">
    @endif
    @if(config('groups.fontAwesomeEnabled'))
        <link rel="stylesheet" type="text/css" href="{{ config('groups.fontAwesomeCdn') }}">
    @endif
    @include('groups::partials.styles')
    @include('groups::partials.bs-visibility-css')
@endsection

@section('content')
    <div class="container">
        @if(config('groups.enablePackageBootstapAlerts'))
            <div class="row">
                <div class="col-sm-12">
                  <?php  @include('groups::partials.form-status') ?>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                               Manage Your Partners
                            </span>

                            <div class="btn-group pull-right btn-group-xs">
                                @if(config('groups.softDeletedEnabled'))
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i>
                                        <span class="sr-only">
                                            @lang('groups::groups.users-menu-alt')
                                        </span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ route('users.create') }}">
                                                @if(config('groups.fontAwesomeEnabled'))
                                                    <i class="fa fa-fw fa-user-plus" aria-hidden="true"></i>
                                                @endif
                                               New Group
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/users/deleted">
                                                @if(config('groups.fontAwesomeEnabled'))
                                                    <i class="fa fa-fw fa-group" aria-hidden="true"></i>
                                                @endif
                                                @lang('groups::groups.show-deleted-users')
                                            </a>
                                        </li>
                                    </ul>
                                @else
                                    <a href="/groups/create" class="model-add-new pull-right" data-toggle="tooltip" data-placement="left" title="@lang('groups::groups.tooltips.create-new')">
                                        Add New Partner
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-body" id="card-body">
                        
                    </div>

                </div>
            </div>
        </div>
    </div>
    @include('groups::modals/modal-delete')

@endsection
@section('template_scripts')
    @include('groups::scripts/csrf-token')
    @if ((count($groups) > config('groups.datatablesJsStartCount')) && config('groups.enabledDatatablesJs'))
        @include('groups::scripts.datatables')
    @endif
    @include('groups::scripts.delete-modal-script')
    @include('groups::scripts.save-modal-script')
    @if(config('groups.tooltipsEnabled'))
        @include('groups::scripts.tooltips')
    @endif
    @if(config('groups.enableSearchGroups'))
        @include('groups::scripts.search-groups')
    @endif


@endsection
