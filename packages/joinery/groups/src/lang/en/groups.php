<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Laravel Groups Blades Language Lines
    |--------------------------------------------------------------------------
    */

    'showing-all-groups'     => 'Showing All Groups',
    'groups-menu-alt'        => 'Show Groups Management Menu',
    'create-new-group'       => 'Create New Group',
    'show-deleted-groups'    => 'Show Deleted Group',
    'editing-group'          => 'Editing Group :name',
    'showing-group'          => 'Showing Group :name',
    'showing-group-title'    => ':name\'s Information',

    'groups-table' => [
        'caption'   => '{1} :groupscount group total|[2,*] :groupscount total groups',
        'id'        => 'ID',
        'name'      => 'Name',
        'email'     => 'Email',
        'role'      => 'Role',
        'created'   => 'Created',
        'updated'   => 'Updated',
        'actions'   => 'Actions',
        'updated'   => 'Updated',
    ],

    'buttons' => [
        'create-new'    => '<span class="hidden-xs hidden-sm">New Group</span>',
        'delete'        => '<i class="far fa-trash-alt fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Delete</span><span class="hidden-xs hidden-sm hidden-md"> Group</span>',
        'show'          => '<i class="fas fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Show</span><span class="hidden-xs hidden-sm hidden-md"> Group</span>',
        'edit'          => '<i class="fas fa-pencil-alt fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Edit</span><span class="hidden-xs hidden-sm hidden-md"> Group</span>',
        'back-to-groups' => '<span class="hidden-sm hidden-xs">Back to </span><span class="hidden-xs">Groups</span>',
        'back-to-group'  => 'Back  <span class="hidden-xs">to Group</span>',
        'delete-group'   => '<i class="far fa-trash-alt fa-fw" aria-hidden="true"></i>  <span class="hidden-xs">Delete</span><span class="hidden-xs"> Group</span>',
        'edit-group'     => '<i class="fas fa-pencil-alt fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Edit</span><span class="hidden-xs"> Group</span>',
    ],

    'tooltips' => [
        'delete'        => 'Delete',
        'show'          => 'Show',
        'edit'          => 'Edit',
        'create-new'    => 'Create New Group',
        'back-groups'    => 'Back to groups',
        'email-group'    => 'Email :group',
        'submit-search' => 'Submit Groups Search',
        'clear-search'  => 'Clear Search Results',
    ],

    'messages' => [
        'groupNameTaken'          => 'Groupname is taken',
        'groupNameRequired'       => 'Groupname is required',
        'fNameRequired'          => 'First Name is required',
        'lNameRequired'          => 'Last Name is required',
        'emailRequired'          => 'Email is required',
        'emailInvalid'           => 'Email is invalid',
        'passwordRequired'       => 'Password is required',
        'PasswordMin'            => 'Password needs to have at least 6 characters',
        'PasswordMax'            => 'Password maximum length is 20 characters',
        'captchaRequire'         => 'Captcha is required',
        'CaptchaWrong'           => 'Wrong captcha, please try again.',
        'roleRequired'           => 'Group role is required.',
        'group-creation-success'  => 'Successfully created group!',
        'update-group-success'    => 'Successfully updated group!',
        'delete-success'         => 'Successfully deleted the group!',
        'cannot-delete-yourself' => 'You cannot delete yourself!',
    ],

    'show-group' => [
        'id'                => 'Group ID',
        'name'              => 'Groupname',
        'email'             => '<span class="hidden-xs">Group </span>Email',
        'role'              => 'Group Role',
        'created'           => 'Created <span class="hidden-xs">at</span>',
        'updated'           => 'Updated <span class="hidden-xs">at</span>',
        'users'             => 'Users',
        'labelRole'         => 'Group Role',
        'labelAccessLevel'  => '<span class="hidden-xs">Group</span> Access Level|<span class="hidden-xs">Group</span> Access Levels',
    ],

    'search'  => [
        'title'         => 'Showing Search Results',
        'found-footer'  => ' Record(s) found',
        'no-results'    => 'No Results',
    ],
];
