<?php

namespace Joinery\Groups;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Http\Response;
use Validator;
use Hash;

class GroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pagintaionEnabled = config('groups.enablePagination');
        if ($pagintaionEnabled) {
           // $groups = config('groups.defaultGroupModel')::paginate(config('groups.paginateListSize'));
        } else {
            $groups = config('groups.defaultGroupModel')::paginate(50);
        }

        $data = [
            'groups'             => $groups,
            'pagintaionEnabled' => $pagintaionEnabled,
        ];
        return view(config('groups.showGroupsBlade'), $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$users = User::all();
		$data = [
			'users' => $users
		];
        return view(config('groups.createGroupBlade'))->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    
        if( 'NULL' == $request->id ){
            $users = [];
            // check if is alias
            
            if($admin_email = $request->admin_email){

                $user = User::create([
                    'name' => $admin_email,
                    'email' => $admin_email,
                    'password' => Hash::make(str_random(10))
                ]);
                $users = [$user->id];
            }
            $groupCore = new Group;
            $groupCore->name = $request->name;
            $groupCore->save(); // add user admin
            $groupCore->users()->sync($users);

        // create two new groups - a core and an alias
            $group = new Group;
            $group->name = $request->name;
            $group->parent_id = $groupCore->id;
            $group->save();
            // if this is being created by 
            //creted_group_id
            $group->teams()->sync([$request->creating_group_id]);
        }
        else{
            $group = Group::find($request->id);            
            $group->name = $request->name;
            $group->save();
        }
       if(isset($request->users)){
            $group->users()->sync($request->users);
	    }
        $groups = Group::all();
        $data = [
            'groups'             => $groups,
            'pagintaionEnabled' => 0
        ];
        return view('groups::show-groups', $data); 
   }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if( $id ){
            $group = Group::find($id);            
        }
        $data = [
            'group'             => $group,
            'pagintaionEnabled' => 0
        ];
        return view('groups::show-group', $data); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = config('groups.defaultGroupModel')::findOrFail($id);
        $group->teams();
   		$users = User::all();
		$data = [
			'users' => $users,
            'group'          => $group,
        ];
        return view(config('groups.editIndividualGroupBlade'))->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Group::destroy($id);
        $groups = Group::all();
        $data = [
            'groups'             => $groups,
            'pagintaionEnabled' => 0
        ];
        return redirect('/groups');
    }

   /**
     * Method to search the users.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $searchTerm = $request->input('searchTerm');
        /*
        $searchRules = [
            'group_search_box' => 'required|string|max:255',
        ];
        $searchMessages = [
            'group_search_box.required' => 'Search term is required',
            'group_search_box.string'   => 'Search term has invalid characters',
            'group_search_box.max'      => 'Search term has too many characters - 255 allowed',
        ];
        $validator = Validator::make($request->all(), $searchRules, $searchMessages);

        if ($validator->fails()) {
            return response()->json([
                json_encode($validator),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
*/
         $results = config('groups.defaultGroupModel')::where('id', 'like', $searchTerm.'%')
                            ->orWhere('name', 'like', '%'.$searchTerm.'%')->get();
                        //    print_r($results);

      
    return response()->json($results);
    }    
}
