<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Laravel-groups setting
    |--------------------------------------------------------------------------
    */

    // The parent blade file
    'GroupsBladeExtended'     => 'layouts.app',

    // Enable Soft Deletes - Not yet setup - on the roadmap.
    'softDeletedEnabled'            => false,

    // Laravel Default Group Model
    'defaultGroupModel'              => 'Joinery\Groups\Group',

    // Use the provided blade templates or extend to your own templates.
    'showGroupsBlade'                => 'groups::show-groups',
    'createGroupBlade'               => 'groups::create-group',
    'showIndividualGroupBlade'       => 'groups::show-group',
    'editIndividualGroupBlade'       => 'groups::create-group',

    // Use Package Bootstrap Flash Alerts
    'enablePackageBootstapAlerts'   => true,

    // Groups List Pagination
    'enablePagination'              => false,
    'paginateListSize'              => 25,

    // Enable Search Groups- Uses jQuery Ajax
    'enableSearchGroups'             => true,

    // Groups List JS DataTables - not recommended use with pagination
    'enabledDatatablesJs'           => false,
    'datatablesJsStartCount'        => 25,
    'datatablesCssCDN'              => 'https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css',
    'datatablesJsCDN'               => 'https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js',
    'datatablesJsPresetCDN'         => 'https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js',

    // Bootstrap Tooltips
    'tooltipsEnabled'               => true,
    'enableBootstrapPopperJsCdn'    => true,
    'bootstrapPopperJsCdn'          => 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js',

    // Icons
    'fontAwesomeEnabled'            => true,
    'fontAwesomeCdn'                => 'https://use.fontawesome.com/releases/v5.0.6/css/all.css',

    // Extended blade options for packages app.blade.php
    'enableBootstrapCssCdn'         => true,
    'bootstrapCssCdn'               => 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',

    'enableAppCss'                  => true,
    'appCssPublicFile'              => 'css/app.css',

    'enablePackageCss'              => true,
    'packageCss'                    => '/css/joinery-groups.css',

    'enableBootstrapJsCdn'          => true,
    'bootstrapJsCdn'                => 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js',

    'enableAppJs'                   => true,
    'appJsPublicFile'               => 'js/app.js',

    'enablejQueryCdn'               => true,
    'jQueryCdn'                     => 'https://code.jquery.com/jquery-3.3.1.min.js',

];
