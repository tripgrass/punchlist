<?php

namespace joinery\groups;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Account;
use App\Block;
use DB;

//use Joinery\Group;

class Group extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'aliasName',
        'isAlias'
    ];
    protected $appends = array('admin','adminid');

    protected $accountNamesForIndividual = [
            'Individual Trial',
            'Individual'
        ];

    
    /**
     * The users that belong to the group.
     */
    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot('role','isAdmin');
    }

    public function projects()
    {
        return $this->belongsToMany('joinery\projects\Project')->withPivot('isSecondary', 'invited','invitedDate','accepted','acceptedDate','inviteCode');
    }

    /**
     * The groups that a user belongs to.
     */
    public function account()
    {
        return $this->hasOne('App\Account');
    }    

    public function isGroup(  ){
        // is being used in blades and controllers - need to distinguish; if is blade, no status.
        if( $this->account && $this->account->name ){
            if(in_array($this->account->name,$this->accountNamesForIndividual)){
                return false;
            }
            else{
                return true;
            }
        }
        else{

        }
        /*
        else{
            $permission_alert = "This Group does not appear to have an account. ";
            return redirect('/home')->with('status', $permission_alert);            
        }
        */
    }
    public function isSolo(){
        if(property_exists($this, 'account') && in_array($this->account->name,$this->accountNamesForIndividual)){
            return true;
        }
        else{
            return false;
        }
    }

	public function teams()
    {
		return $this->belongsToMany(Group::class, 'group_team', 'group_id', 'team_id');
    }   
    public function getAdminAttribute()
    {
        $user = $this->users()->where('isAdmin','1')->first();
        return $user;
    }
    public function getAdminidAttribute()
    {
        $user_row = DB::table('group_user')->where('isAdmin','1')->where('group_id',$this->id)->first();
        if($user_row){ 
            return $user_row->user_id;
        }
    }
    public function breaks($startDate = null, $endDate = null,$viewer, $user = null){
        $auth = $this->users()->where('user_id',$viewer->id)->first();
        if( $viewer ){
            // check if viewer belongs to group
            if( $auth ){
                $breaks = Block::
                    where('group_id',$this->id)
                    ->get();            
                   // print_r($breaks);
            }
            else{
                $breaks = Block::
                    where('group_id',$this->id)
                    ->where('public',1)
                    ->get();                            
            }
        }
        else{
            $breaks = Block::
                where('project_id',$this->id)
                ->get();            
        }
        return $breaks;
    }   
}
