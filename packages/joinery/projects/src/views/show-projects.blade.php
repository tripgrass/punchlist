@extends('layouts.app')

@section('template_title')
    @lang('projects::projects.showing-all-groups') 
@endsection
@section('template_linked_css')
    @if(config('projects.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('projects.datatablesCssCDN') }}">
    @endif
    @if(config('projects.fontAwesomeEnabled'))
        <link rel="stylesheet" type="text/css" href="{{ config('projects.fontAwesomeCdn') }}">
    @endif
    @include('projects::partials.styles')
    @include('projects::partials.bs-visibility-css')
@endsection

@section('content')
    <div class="container">
        @if(config('projects.enablePackageBootstapAlerts'))
            <div class="row">
                <div class="col-sm-12">
                  <?php  @include('projects::partials.form-status') ?>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                               Showing all Projects
                            </span>

                            <div class="btn-project pull-right btn-project-xs">
                                @if(config('projects.softDeletedEnabled'))
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i>
                                        <span class="sr-only">
                                            @lang('projects::projects.groups-menu-alt')
                                        </span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ route('groups.create') }}">
                                                @if(config('projects.fontAwesomeEnabled'))
                                                    <i class="fa fa-fw fa-group-plus" aria-hidden="true"></i>
                                                @endif
                                               New Project
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/groups/deleted">
                                                @if(config('projects.fontAwesomeEnabled'))
                                                    <i class="fa fa-fw fa-project" aria-hidden="true"></i>
                                                @endif
                                                @lang('projects::projects.show-deleted-groups')
                                            </a>
                                        </li>
                                    </ul>
                                @else
                                    <a href="/projects/create" class="btn btn-default btn-sm pull-right" data-toggle="tooltip" data-placement="left" title="@lang('projects::projects.tooltips.create-new')">
                                        @if(config('projects.fontAwesomeEnabled'))
                                            <i class="fa fa-fw fa-group-plus" aria-hidden="true"></i>
                                        @endif
                                        New Project
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-body" id="project-card-body">
                    </div>

                </div>
            </div>
        </div>
    </div>
    @include('projects::modals/modal-delete')

@endsection
@section('template_scripts')
    @include('projects::scripts/csrf-token')
    @if ((count($projects) > config('projects.datatablesJsStartCount')) && config('projects.enabledDatatablesJs'))
        @include('projects::scripts.datatables')
    @endif
    @include('projects::scripts.delete-modal-script')
    @include('projects::scripts.save-modal-script')
    @if(config('projects.tooltipsEnabled'))
        @include('projects::scripts.tooltips')
    @endif


@endsection
