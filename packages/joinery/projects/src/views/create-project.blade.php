@extends('projects::layouts.app')

@section('template_title')
    @lang('projects::projects.create-new-group')
@endsection

@section('template_linked_css')
    @if(config('projects.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('projects.datatablesCssCDN') }}">
    @endif
    @if(config('projects.fontAwesomeEnabled'))
        <link rel="stylesheet" type="text/css" href="{{ config('projects.fontAwesomeCdn') }}">
    @endif
    @include('projects::partials.styles')
    @include('projects::partials.bs-visibility-css')
@endsection

@section('content')
<?php 
    $edit = 0;
    $title = "Create New Project";
    $btn_text = "Create New Project";
    $form_name = "";
    $form_id = "NULL";
    if( isset($project) && is_object($project)){
        $edit = 1;
        $title = "Edit Project";
        $btn_text = "Update Project";
        $form_name = $project->name;
        $form_id = $project->id;
//        print_r($project);
    }
?>
    <div class="container">
        @if(config('projects.enablePackageBootstapAlerts'))
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <?php 
//                    @include('projects::partials.form-status')
                ?>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">
                            {{ $title }}
                            <div class="pull-right">
                                <a href="/projects" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="left" title="@lang('Back to Projects')">
                                    @if(config('projects.fontAwesomeEnabled'))
                                        <i class="fas fa-fw fa-reply-all" aria-hidden="true"></i>
                                    @endif
                                   Back to Projects
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        {!! Form::open(array('route' => 'projects.store', 'method' => 'POST', 'role' => 'form', 'class' => 'needs-validation')) !!}
                            {!! Form::hidden('id', $form_id, array('id' => 'id')) !!}
                            {!! csrf_field() !!}
                            <div class="form-group has-feedback row {{ $errors->has('email') ? ' has-error ' : '' }}">
                                @if(config('projects.fontAwesomeEnabled'))
                                    {!! Form::label('name', 'Project Name', array('class' => 'col-md-3 control-label')); !!}
                                @endif
                                <div class="col-md-9">
                                    <div class="input-project">
                                        {!! Form::text('name', $form_name, array('id' => 'name', 'class' => 'form-control', 'placeholder' => 'Name')) !!}
                                    </div>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback row {{ $errors->has('email') ? ' has-error ' : '' }}">
                                @if(config('projects.fontAwesomeEnabled'))
                                    {!! Form::label('groups', 'Project Members', array('class' => 'col-md-3 control-label')); !!}
                                @endif
                                <div class="col-md-9">
                                    <div class="input-project">
<?php 
$select_groups = [];
foreach($groups as $group){
    $select_groups[$group->id] = $group->name;
}
$selected_groups = [];
if(isset($project) && isset($project->groups)){
    foreach( $project->groups as $group){
        $selected_groups[$group->id] = $group->id;
    }
}
?>
{{Form::select('groups',$select_groups,$selected_groups,array('multiple'=>'multiple','name'=>'groups[]', 'id' => 'groups', 'class' => 'form-control'))}}
                                    </div>
                                    @if ($errors->has('groups'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('groups') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            {!! Form::button( $btn_text, array('class' => 'btn btn-success margin-bottom-1 mb-1 float-right','type' => 'submit' )) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('template_scripts')
    @if(config('projects.tooltipsEnabled'))
        @include('projects::scripts.tooltips')
    @endif
@endsection
