@extends(config('projects.GroupsBladeExtended'))

@section('template_title')
  @lang('projects::projects.editing-group', ['name' => $group->name])
@endsection

@section('template_linked_css')
    @if(config('projects.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('projects.datatablesCssCDN') }}">
    @endif
    @if(config('projects.fontAwesomeEnabled'))
        <link rel="stylesheet" type="text/css" href="{{ config('projects.fontAwesomeCdn') }}">
    @endif
    @include('projects::partials.styles')
    @include('projects::partials.bs-visibility-css')
@endsection

@section('content')
    <div class="container">
        @if(config('projects.enablePackageBootstapAlerts'))
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    @include('projects::partials.form-status')
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">
                            @lang('projects::projects.editing-project', ['name' => $project->name])
                            <div class="pull-right">
                                <a href="{{ route('groups') }}" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="top" title="@lang('projects::projects.tooltips.back-groups')">
                                    @if(config('projects.fontAwesomeEnabled'))
                                        <i class="fas fa-fw fa-reply-all" aria-hidden="true"></i>
                                    @endif
                                    @lang('projects::projects.buttons.back-to-groups')
                                </a>
                                <a href="{{ url('/groups/' . $group->id) }}" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="left" title="@lang('projects::projects.tooltips.back-groups')">
                                    @if(config('projects.fontAwesomeEnabled'))
                                        <i class="fas fa-fw fa-reply" aria-hidden="true"></i>
                                    @endif
                                    @lang('projects::projects.buttons.back-to-group')
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        {!! Form::open(array('route' => ['groups.update', $group->id], 'method' => 'PUT', 'role' => 'form', 'class' => 'needs-validation')) !!}
                            {!! csrf_field() !!}
                            <div class="form-project has-feedback row {{ $errors->has('name') ? ' has-error ' : '' }}">
                                @if(config('projects.fontAwesomeEnabled'))
                                    {!! Form::label('name', trans('projects::forms.create_group_label_groupname'), array('class' => 'col-md-3 control-label')); !!}
                                @endif
                                <div class="col-md-9">
                                    <div class="input-project">
                                        {!! Form::text('name', $group->name, array('id' => 'name', 'class' => 'form-control', 'placeholder' => trans('projects::forms.create_group_ph_groupname'))) !!}
                                        <div class="input-project-append">
                                            <label class="input-project-text" for="name">
                                                @if(config('projects.fontAwesomeEnabled'))
                                                    <i class="fa fa-fw {{ trans('projects::forms.create_group_icon_groupname') }}" aria-hidden="true"></i>
                                                @else
                                                    @lang('projects::forms.create_group_label_groupname')
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-project has-feedback row {{ $errors->has('email') ? ' has-error ' : '' }}">
                                @if(config('projects.fontAwesomeEnabled'))
                                    {!! Form::label('email', trans('projects::forms.create_group_label_email'), array('class' => 'col-md-3 control-label')); !!}
                                @endif
                                <div class="col-md-9">
                                    <div class="input-project">
                                        {!! Form::text('email', $group->email, array('id' => 'email', 'class' => 'form-control', 'placeholder' => trans('projects::forms.create_group_ph_email'))) !!}
                                        <div class="input-project-append">
                                            <label for="email" class="input-project-text">
                                                @if(config('projects.fontAwesomeEnabled'))
                                                    <i class="fa fa-fw {{ trans('projects::forms.create_group_icon_email') }}" aria-hidden="true"></i>
                                                @else
                                                    @lang('projects::forms.create_group_label_email')
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="pw-change-container">
                                <div class="form-project has-feedback row {{ $errors->has('password') ? ' has-error ' : '' }}">
                                    @if(config('projects.fontAwesomeEnabled'))
                                        {!! Form::label('password', trans('projects::forms.create_group_label_password'), array('class' => 'col-md-3 control-label')); !!}
                                    @endif
                                    <div class="col-md-9">
                                        <div class="input-project">
                                            {!! Form::password('password', array('id' => 'password', 'class' => 'form-control ', 'placeholder' => trans('projects::forms.create_group_ph_password'))) !!}
                                            <div class="input-project-append">
                                                <label class="input-project-text" for="password">
                                                    @if(config('projects.fontAwesomeEnabled'))
                                                        <i class="fa fa-fw {{ trans('projects::forms.create_group_icon_password') }}" aria-hidden="true"></i>
                                                    @else
                                                        @lang('projects::forms.create_group_label_password')
                                                    @endif
                                                </label>
                                            </div>
                                        </div>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-project has-feedback row {{ $errors->has('password_confirmation') ? ' has-error ' : '' }}">
                                    @if(config('projects.fontAwesomeEnabled'))
                                        {!! Form::label('password_confirmation', trans('projects::forms.create_group_label_pw_confirmation'), array('class' => 'col-md-3 control-label')); !!}
                                    @endif
                                    <div class="col-md-9">
                                        <div class="input-project">
                                            {!! Form::password('password_confirmation', array('id' => 'password_confirmation', 'class' => 'form-control', 'placeholder' => trans('projects::forms.create_group_ph_pw_confirmation'))) !!}
                                            <div class="input-project-append">
                                                <label class="input-project-text" for="password_confirmation">
                                                    @if(config('projects.fontAwesomeEnabled'))
                                                        <i class="fa fa-fw {{ trans('projects::forms.create_group_icon_pw_confirmation') }}" aria-hidden="true"></i>
                                                    @else
                                                        @lang('projects::forms.create_group_label_pw_confirmation')
                                                    @endif
                                                </label>
                                            </div>
                                        </div>
                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-6 mb-2">
                                    <a href="#" class="btn btn-outline-secondary btn-block btn-change-pw mt-3" title="@lang('projects::forms.change-pw')">
                                        <i class="fa fa-fw fa-lock" aria-hidden="true"></i>
                                        <span></span> @lang('projects::forms.change-pw')
                                    </a>
                                </div>
                                <div class="col-12 col-sm-6">
                                    {!! Form::button(trans('projects::forms.save-changes'), array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2 btn-save','type' => 'button', 'data-toggle' => 'modal', 'data-target' => '#confirmSave', 'data-title' => trans('projects::modals.edit_group__modal_text_confirm_title'), 'data-message' => trans('projects::modals.edit_group__modal_text_confirm_message'))) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('projects::modals.modal-save')
    @include('projects::modals.modal-delete')

@endsection

@section('template_scripts')
    @include('projects::scripts/csrf-token')
    @include('projects::scripts.delete-modal-script')
    @include('projects::scripts.save-modal-script')
    @include('projects::scripts.check-changed')
    @if(config('projects.tooltipsEnabled'))
        @include('projects::scripts.tooltips')
    @endif
@endsection

