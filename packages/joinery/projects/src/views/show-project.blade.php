@extends(config('projects.ProjectsBladeExtended'))

@section('template_title')
    @lang('projects::projects.showing-project', ['name' => $project->name])
@endsection

@section('template_linked_css')
    @if(config('projects.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('projects.datatablesCssCDN') }}">
    @endif
    @if(config('projects.fontAwesomeEnabled'))
        <link rel="stylesheet" type="text/css" href="{{ config('projects.fontAwesomeCdn') }}">
    @endif
    @include('projects::partials.styles')
    @include('projects::partials.bs-visibility-css')
@endsection

@section('content')
    <div class="container">
        @if(config('projects.enablePackageBootstapAlerts'))
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    @include('projects::partials.form-status')
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">
                            @lang('projects::projects.showing-project-title', ['name' => $project->name])
                            <div class="float-right">
                                <a href="{{ route('projects') }}" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="left" title="@lang('projects::projects.tooltips.back-projects')">
                                    @if(config('projects.fontAwesomeEnabled'))
                                        <i class="fas fa-fw fa-reply-all" aria-hidden="true"></i>
                                    @endif
                                    @lang('projects::projects.buttons.back-to-projects')
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <h4 class="text-muted text-center">
                            {{ $project->name }}
                        </h4>
                        @if($project->email)
                            <p class="text-center" data-toggle="tooltip" data-placement="top" title="@lang('projects::projects.tooltips.email-project', ['project' => $project->email])">
                                {{ Html::mailto($project->email, $project->email) }}
                            </p>
                        @endif
                        <div class="row mb-4">
                            <div class="col-3 offset-3 col-sm-4 offset-sm-2 col-md-4 offset-md-2 col-lg-3 offset-lg-3">
                                <a href="/projects/{{$project->id}}/edit" class="btn btn-block btn-md btn-warning">
                                    @lang('projects::projects.buttons.edit-project')
                                </a>
                            </div>
                            <div class="col-3 col-sm-4 col-md-4 col-lg-3">
                                {!! Form::open(array('url' => 'projects/' . $project->id, 'class' => 'form-inline')) !!}
                                    {!! Form::hidden('_method', 'DELETE') !!}
                                    {!! Form::hidden('project_id', $project->id) !!}
                                    {!! Form::button(trans('projects::projects.buttons.delete-project'), array('class' => 'btn btn-danger btn-md btn-block','type' => 'button', 'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete Group', 'data-message' => 'Are you sure you want to delete this project?')) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <ul class="list-project list-project-flush">
                            <li class="list-project-item">
                                <div class="row">
                                    <div class="col-4 col-sm-3">
                                        <strong>
                                            @lang('projects::projects.show-project.id')
                                        </strong>
                                    </div>
                                    <div class="col-8 col-sm-9">
                                        {{ $project->id }}
                                    </div>
                                </div>
                            </li>
                            @if ($project->name)
                                <li class="list-project-item">
                                    <div class="row">
                                        <div class="col-4 col-sm-3">
                                            <strong>
                                                @lang('projects::projects.show-project.name')
                                            </strong>
                                        </div>
                                        <div class="col-8 col-sm-9">
                                            {{ $project->name }}
                                        </div>
                                    </div>
                                </li>
                            @endif
                            @if ($project->email)
                                <li class="list-project-item">
                                    <div class="row">
                                        <div class="col-12 col-sm-3">
                                            <strong>
                                                @lang('projects::projects.show-project.email')
                                            </strong>
                                        </div>
                                        <div class="col-12 col-sm-9">
                                            {{ $project->email }}
                                        </div>
                                    </div>
                                </li>
                            @endif
                            @if ($project->created_at)
                                <li class="list-project-item">
                                    <div class="row">
                                        <div class="col-4 col-sm-3">
                                            <strong>
                                                @lang('projects::projects.show-project.created')
                                            </strong>
                                        </div>
                                        <div class="col-8 col-sm-9">
                                            {{ $project->created_at }}
                                        </div>
                                    </div>
                                </li>
                            @endif
                            @if ($project->updated_at)
                                <li class="list-project-item">
                                    <div class="row">
                                        <div class="col-4 col-sm-3">
                                            <strong>
                                                @lang('projects::projects.show-project.updated')
                                            </strong>
                                        </div>
                                        <div class="col-8 col-sm-9">
                                            {{ $project->updated_at }}
                                        </div>
                                    </div>
                                </li>
                            @endif
                            @if ($project->groups)
                                <li class="list-project-item">
                                    <div class="row">
                                        <div class="col-4 col-sm-3">
                                            <strong>
                                                @lang('projects::projects.show-project.groups')
                                            </strong>
                                        </div>
                                        <div class="col-8 col-sm-9">
                                            <ul>
                                             @foreach($project->groups as $group)
                                                <li><a href={{ URL::to('groups/' . $group->id) }}>{{ $group->name }}</a></li>
                                             @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('projects::modals.modal-delete')
@endsection
@section('template_scripts')
    @include('projects::scripts/csrf-token')
    @include('projects::scripts.delete-modal-script')

    @include('projects::scripts.delete-modal-script')
    @if(config('projects.tooltipsEnabled'))
        @include('projects::scripts.tooltips')
    @endif
@endsection
