<?php

namespace joinery\projects;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Joinery\Tasks\Task;
use Carbon\Carbon;
use App\User;
use App\Block;
use DB;
use Joinery\Groups\Group;

class Project extends Model
{
	protected $appends = array('taskTree','easyTree','partners','owner');

	public function __construct(array $attributes = array())
	{
	    parent::__construct($attributes);
	}	

    /**
     * The groups that belong to the group.
     */
    public function groups()
    {
        return $this->belongsToMany('joinery\groups\Group')->withPivot('isSecondary', 'invited','invitedDate','accepted','acceptedDate','inviteCode');
    }

    /**
     * The tasks that belong to the project.
     */
    public function tasks()
    {
        return $this->hasMany('joinery\tasks\Task');
    }

	public function buildTree( &$elements, $parentId = 0) {
	    $branch = array();

	    foreach ($elements as &$element) {

	        if ($element['parent_id'] == $parentId) {
	            $children = $this->buildTree($elements, $element['id']);
	            if ($children) {
	                $element['children'] = $children;
	            }
	            $branch[$element['id']] = $element;
	            unset($element);
	        }
	    }
	    return $branch;
	}
	public function isThreadStart($task){
		$task['threadStart'] = 0;
        if($task['hasSiblings'] > 0 ){
        	$task['threadStart'] = 1;
            return $task;
        }
        // has NO siblings:
        if( array_key_exists('children', $task) && count($task['children']) > 1 ){
        	$task['threadStart'] = 1;
            return $task;
        }
        return $task;
    }
	public function buildEasyTree( &$elements, $parentId = 0, $earliestStart = 0) {
	    $branch = array();

	    foreach ($elements as &$element) {
	    	$element->users;
	    	//$element->notes;
	    	$element->groups;

	    	// clean up: (issue with parent task deleting but not cascading)
	    	if( $element->parent_id ){
		    	$task_check = Task::where('id', $element->parent_id)->where('project_id', $element->project_id)->first();
		    	if( !$task_check ){
		    		$element->parent_id = NULL;
		    		$element->save();
		    	}
		    }
	    	$element['defined'] = new \stdClass();
	    	$element['calculated'] = new \stdClass();
	    	$element['build'] = new \stdClass();
	    	$tasks[$element->id] = $element;
	    	$tasks[$element->id]['closedDate'] = $element->closedDate;
	    	$newElement = [];
	    	$branchElement = [];
	    	// starts with parent
	    	$timezone = 'America/Phoenix';
			$tasks[$element->id]['estEndDate']= "";			        
			$newElement['estEndDate'] = "";

	    	if( $element->startDate > 0 ){
				$startDate = new Carbon($element->startDate, $timezone);

				if(!$earliestStart  ){
					$earliestStart = $startDate;
				}
				elseif( $startDate->lt($earliestStart) ){
					$earliestStart = $startDate;
				}
          	}
			elseif($this->proposedStartDate && !$element->startDate ){
				//$element->startDate = $startDate = $earliestStart = $this->proposedStartDate;
			}
	        if ($element['parent_id'] == $parentId) {
                $newElement['name'] = $element->name;
                $newElement['parent_id'] = $element->parent_id;
                $siblingCount = Task::where('parent_id','=',$element->parent_id)->count();
                $newElement['hasSiblings'] = 0;
                if( $siblingCount > 1 ){
	                $newElement['hasSiblings'] = $siblingCount;
	                $tasks[$element->id]['hasSiblings']= $siblingCount;
	            }
                $newElement['id'] = $element->id;
                $newElement['length'] = $element->length;
                $newElement['weight'] = $element->weight;

	            $easyTreeArray = $this->buildEasyTree($elements, $element['id'], $earliestStart);
	            $children = $easyTreeArray['branch'];
	            $branchElement['id'] = $element->id;
                $newElement['children'] = [];
	            if ($children) {
	            	foreach($children as $child){
			            $branchElement['children'][$child['id']] = $child;
	            	}
	                $newElement['children'] = $children;
	                $tasks[$element->id]['children']= $children;
	            }
	            else{
	            	$id = $element->id;
	            	$this->thread_lengths->$id = 0;
	            	$this->current_node = $element->id;
	            	$thread_lengths_array = $this->parent_recursion($element);
	            	$first_element_id = $thread_lengths_array['first_element_id'];
	            	$newElement['thread_length'] = $thread_lengths_array['thread_length'];	            	
	            }
	            $newElement = $this->isThreadStart($newElement);
	            $branch[$element['id']] = $branchElement;
	            unset($element);
	        }
	    }
	    return ['branch'=>$branch, 'tasks' => $tasks, 'earliestStart'=>$earliestStart, 'proposedStartDate' => $this->proposedStartDate];
	}
    public function getPartnersAttribute()
	{
		$partners = $this->groups()->wherePivot('isSecondary',1)->get();
		return $partners;
    }
    public function getOwnerAttribute()
	{
		$thisProject = $this;
		//$owner = $this->groups()->wherePivot('isSecondary',NULL)->first();

		$owner = DB::table('group_project')
                 ->where(function($query) {
                        $query->where('group_project.isSecondary', NULL)
                            ->orWhere('group_project.isSecondary', 0);
                    })
                 ->where('group_project.project_id', $thisProject->id)
        ->get();		

		if($owner){
			$ownerGroup = Group::find($owner[0]->group_id);
			$ownerGroup->account; 
 			$ownerGroup->users;
		}
		return $ownerGroup;
    }
    public function getTaskTreeAttribute()
	{
    	//return $this->buildTree($this->tasks);
    }
    public function getEasyTreeAttribute()
	{
		$this->thread_lengths = (object)[];
		$this->total_thread_lengths = [];

		$tasks = $this->tasks;
		if(count($tasks)>0){
			$earliestStart = 0;
			$parent_id = 0;
	    	$treeArray = $this->buildEasyTree($tasks,$parent_id);
	    	$partners = new \stdClass;

	    	if($this->partners){
	    		foreach($this->partners as $group){
		    			$partner = [
		    				'id' => $group->id,
		    				'name' => $group->name,
		    				'invited' => $group->pivot->invited,
		    				'accepted' => $group->accepted,
		    				'isSecondary' => $group->pivot->isSecondary,
		    				'users' => $group->users,
		    				'account' => $group->account
		    			];
		    			$partners->{$group->id} = $partner;
	    		}
	    	}
	    	$treeArray['project'] =
	    		[
	    			'id' => $this->id,
	    			'partners' => $partners,
	    			'owner' => $this->owner
	    		];
			return $treeArray;
	    }
 //   	print_r($treeArray);
//		print_r($this->thread_lengths);
	//	return $this->array_values_recursive($tree);
    }
    function parent_recursion($array, $first_element_id = 0){
    	$parent_task = Task::find($array->parent_id); 
        $current_node = $this->current_node;
       	$this->thread_lengths->$current_node += $array->length;
    	if($array['parent_id'] != 0){
    		$arr = $this->parent_recursion($parent_task, $first_element_id);
    		$first_element_id = $arr['first_element_id'];
    	}
    	else{
    		$first_element_id = $array->id;
    	}
    	$arr = ['thread_length'=>$this->thread_lengths->$current_node,'first_element_id'=>$first_element_id];
   		return $arr;
    }
	function array_values_recursive($array) {
		$flat = array();

		foreach($array as $value) {
			$id = $value['id'];
			echo "first round:". $id."<br>";
			if (is_array($value) && array_key_exists('children',$value) && count($value['children'])>0 ) {
				$existing_length = 0;
				if( array_key_exists($id, $flat)){
					$existing_length = $flat[$id];
				}
				echo "start child recursion:".$id."<br>";
				$flat[$id] = $this->array_values_recursive($value['children']);
				echo "has children:". $id."<br>";

			}
			else {
				$flat[$id] = $value;
			}
		}
		return $flat;
	}
	public function breaks($startDate = null, $endDate = null,$viewer = null, $user = null){
		// check if viewer owns the project
//		$viewer = User::find($viewer_id)->first();
		if( $viewer ){
			if( $viewer->projectPermission($this) ){
				$breaks = Block::
					where('project_id',$this->id)
					->get();			
			}
			else{
				$breaks = Block::
					where('project_id',$this->id)
					->where('public',1)
					->get();							
			}
		}
		else{
			$breaks = Block::
				where('project_id',$this->id)
				->get();			
		}
		return $breaks;
	}   

}
