import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class Project extends Component {
    constructor(props) {
        super(props);
        this.state = {
            prevUrl : "",
            nextUrl : "",
            projects: []
        };
        var api_token = $("meta[name='api_token']").attr("content");
        axios
            .post('/api/projects',{
                api_token: api_token
            })
            .then(response => {
//                console.log('from handle submit', response);
                this.setState({
                    prevUrl: response.data.prev_page_url,
                    nextUrl: response.data.next_page_url,
                     projects: [...response.data.data]
                })
            });
        this.handlePrev = this.handlePrev.bind(this);            
        this.handleNext = this.handleNext.bind(this);            
        this.handleSearch = this.handleSearch.bind(this);            
    }
    handleSearch(e){
        e.preventDefault();
        var _searchTerm = $('#project_search_box').val();
         axios
            .post('api/search-projects', {
                searchTerm: _searchTerm 
              })
            .then(response => {
                console.log('from search submit', response);
                this.setState({
                    prevUrl: response.data.prev_page_url,
                    nextUrl: response.data.next_page_url,
                     projects: [...response.data]
                })
            });

    }
    handlePrev(){
     axios
            .post(this.state.prevUrl)
            .then(response => {
                this.setState({
                    prevUrl: response.data.prev_page_url,
                    nextUrl: response.data.next_page_url,
                     projects: [...response.data.data]
                })
            });

    }
    handleNext(){
         axios
            .post(this.state.nextUrl)
            .then(response => {
                this.setState({
                    prevUrl: response.data.prev_page_url,
                    nextUrl: response.data.next_page_url,
                     projects: [...response.data.data]
                })
            });
    }
    render() {
        return (
            <div>
                <form method="POST" action="/search-projects" accept-charset="UTF-8" role="form" className="needs-validation" id="search_projects" _lpchecked="1">
                    <div className="input-group mb-3">
                        <input id="project_search_box" onChange={this.handleSearch} className="form-control" placeholder="" aria-label="projects::forms.search-projects-ph" name="project_search_box" type="text"></input>
                        <div className="input-group-append">
                            <a href="#" className="btn btn-warning clear-search" data-toggle="tooltip" title="Clear Search Results">
                                <i className="fas fa-times" aria-hidden="true"></i>
                               <span className="sr-only">Clear Search Results</span>
                            </a>
                            <button className="btn btn-secondary"  data-toggle="tooltip" data-placement="bottom" title="Submit Projects Search"><i className="fas fa-search" aria-hidden="true"></i> <span className="sr-only"> Submit Projects Search </span></button>
                        </div>
                    </div>
                </form>            
                <div className="table-responsive projects-table">
                    <table className="table table-striped table-sm data-table">
                        <caption id="project_count"></caption>
                        <thead className="thead">
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th className="hidden-sm hidden-xs hidden-md">Created</th>
                                <th className="hidden-sm hidden-xs hidden-md">Updated</th>
                                <th className="no-search no-sort"></th>
                                <th className="no-search no-sort"></th>
                                <th className="no-search no-sort"></th>
                            </tr>
                        </thead>
                        <tbody id="project_table">
                            { this.state.projects.map(project => (
                                 <tr key={ project.id}>
                                    <td>{ project.id}</td>
                                    <td>{ project.name}</td>
                                    <td className="hidden-sm hidden-xs hidden-md">{ project.created_at}</td>
                                    <td className="hidden-sm hidden-xs hidden-md">{ project.updated_at}</td>
                                    <td>
                                        <a className="btn btn-sm btn-success btn-block" href={"/projects/" + project.id + "/timeline"} data-toggle="tooltip">
                                            View Project
                                        </a>
                                    </td>
                                    <td>
                                    <form method="POST" accept-charset="UTF-8" className="" data-toggle="tooltip" title="Delete">
                                    <input name="_token" type="hidden" value=""></input>
                                                                        <input name="_method" type="hidden" value="DELETE"></input>
                                                                        <input name="project_id" type="hidden" value={ project.id}></input>
                                                                        <button className="btn btn-danger btn-sm" style={{width: '100%'}}  type="button"  data-toggle="modal" data-target="#confirmDelete" data-title="Delete Group" data-message="Are you sure you want to delete this group?">Delete</button>
                                                                    </form>
                                    </td>
                                    <td>
                                        <a className="btn btn-sm btn-info btn-block" href={"/projects/" + project.id + "/edit"} data-toggle="tooltip">
                                            Edit Project
                                        </a>
                                    </td>
                                    
                                    <td>
                                        <a className="btn btn-sm btn-info btn-block" href={"/projects/" + project.id + "/tasks"} data-toggle="tooltip">
                                            Tasks
                                        </a>
                                    </td>
                                </tr>))
                            } 
                        </tbody>
                        <tbody id="search_results"></tbody>
                    </table>
                </div>
                <div className="paginated-links">
                    <span onClick={this.handlePrev}>Prev</span><span onClick={this.handleNext}>Next</span>
                </div>
            </div>
        );
    }
}
if (document.getElementById('project-card-body')) {
    ReactDOM.render(<Project />, document.getElementById('project-card-body'));
}
