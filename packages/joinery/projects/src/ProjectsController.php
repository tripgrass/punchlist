<?php

namespace Joinery\Projects;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use joinery\groups\Group;
use joinery\projects\Project;



class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pagintaionEnabled = config('projects.enablePagination');
        if ($pagintaionEnabled) {
           // $groups = config('groups.defaultGroupModel')::paginate(config('groups.paginateListSize'));
        } else {
            $user = \Auth::user();
            $guest = 0;
            if( !$user ){
                $guest = 1;
            }
            $projects = [];
            $group = "";
            if($user){
                $projects = Project::whereHas('groups', function($q) use ($user){
                    $q->where('group_id', '=',$user->current_group);
                })->get();
            }
            else{
                $projects = config('projects.defaultProjectModel')::paginate(2);
            }
        }

        $data = [
            'projects'             => $projects,
            'pagintaionEnabled' => $pagintaionEnabled,
        ];
        return view(config('projects.showProjectsBlade'), $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $groups = Group::all();
        $data = [
            'groups'             => $groups
        ];
        return view(config('projects.createProjectBlade'), $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $save = 0;
        if( 'NULL' == $request->id ){
            $project = new Project;
            $save = 1;
        }
        else{
            $project = Project::find($request->id);            
        }
        if( $request->has('name' ) ){
            $project->name = $request->name;
            $save = 1;
        }
        $project->active = 1;
        if($save){
            $project->save();
        }
       if(isset($request->groups)){
            $project->groups()->sync($request->groups);
	    }
        $projects = Project::all();
        $data = [
            'projects'             => $projects,
            'pagintaionEnabled' => 0
        ];
        if( $request->has('redirect') ){
            if('timeline' == $request->redirect ){
                return redirect('/projects/' . $project->id .'/timeline');
            }
        }
        return view('projects::show-projects', $data); 

   }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if( $id ){
            $project = Project::find($id);            
        }
        $data = [
            'project'             => $project,
            'pagintaionEnabled' => 0
        ];
        return view('projects::show-project', $data); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = config('projects.defaultProjectModel')::findOrFail($id);

//$user->current_group = $group->id;

   		$groups = Group::all();
		$data = [
			'groups' => $groups,
            'project'          => $project,
        ];
        return view(config('projects.editIndividualProjectBlade'))->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Project::destroy($id);
        $projects = Project::all();
        $data = [
            'projects'             => $projects,
            'pagintaionEnabled' => 0
        ];
        return redirect('/projects');
    }

   /**
     * Method to search the users.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $searchTerm = $request->input('searchTerm');
        /*
        $searchRules = [
            'group_search_box' => 'required|string|max:255',
        ];
        $searchMessages = [
            'group_search_box.required' => 'Search term is required',
            'group_search_box.string'   => 'Search term has invalid characters',
            'group_search_box.max'      => 'Search term has too many characters - 255 allowed',
        ];
        $validator = Validator::make($request->all(), $searchRules, $searchMessages);

        if ($validator->fails()) {
            return response()->json([
                json_encode($validator),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
*/
         $results = config('projects.defaultProjectModel')::where('id', 'like', $searchTerm.'%')
                            ->orWhere('name', 'like', '%'.$searchTerm.'%')->get();
      
    return response()->json($results);
    }    
}
