<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Laravel Projects Blades Language Lines
    |--------------------------------------------------------------------------
    */

    'showing-all-projects'     => 'Showing All Projects',
    'projects-menu-alt'        => 'Show Projects Management Menu',
    'create-new-project'       => 'Create New Project',
    'show-deleted-projects'    => 'Show Deleted Project',
    'editing-project'          => 'Editing Project :name',
    'showing-project'          => 'Showing Project :name',
    'showing-project-title'    => ':name\'s Information',

    'projects-table' => [
        'caption'   => '{1} :projectscount project total|[2,*] :projectscount total projects',
        'id'        => 'ID',
        'name'      => 'Name',
        'email'     => 'Email',
        'role'      => 'Role',
        'created'   => 'Created',
        'updated'   => 'Updated',
        'actions'   => 'Actions',
        'updated'   => 'Updated',
    ],

    'buttons' => [
        'create-new'    => '<span class="hidden-xs hidden-sm">New Project</span>',
        'delete'        => '<i class="far fa-trash-alt fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Delete</span><span class="hidden-xs hidden-sm hidden-md"> Project</span>',
        'show'          => '<i class="fas fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Show</span><span class="hidden-xs hidden-sm hidden-md"> Project</span>',
        'edit'          => '<i class="fas fa-pencil-alt fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Edit</span><span class="hidden-xs hidden-sm hidden-md"> Project</span>',
        'back-to-projects' => '<span class="hidden-sm hidden-xs">Back to </span><span class="hidden-xs">Projects</span>',
        'back-to-project'  => 'Back  <span class="hidden-xs">to Project</span>',
        'delete-project'   => '<i class="far fa-trash-alt fa-fw" aria-hidden="true"></i>  <span class="hidden-xs">Delete</span><span class="hidden-xs"> Project</span>',
        'edit-project'     => '<i class="fas fa-pencil-alt fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Edit</span><span class="hidden-xs"> Project</span>',
    ],

    'tooltips' => [
        'delete'        => 'Delete',
        'show'          => 'Show',
        'edit'          => 'Edit',
        'create-new'    => 'Create New Project',
        'back-projects'    => 'Back to projects',
        'email-project'    => 'Email :project',
        'submit-search' => 'Submit Projects Search',
        'clear-search'  => 'Clear Search Results',
    ],

    'messages' => [
        'projectNameTaken'          => 'Projectname is taken',
        'projectNameRequired'       => 'Projectname is required',
        'fNameRequired'          => 'First Name is required',
        'lNameRequired'          => 'Last Name is required',
        'emailRequired'          => 'Email is required',
        'emailInvalid'           => 'Email is invalid',
        'passwordRequired'       => 'Password is required',
        'PasswordMin'            => 'Password needs to have at least 6 characters',
        'PasswordMax'            => 'Password maximum length is 20 characters',
        'captchaRequire'         => 'Captcha is required',
        'CaptchaWrong'           => 'Wrong captcha, please try again.',
        'roleRequired'           => 'Project role is required.',
        'project-creation-success'  => 'Successfully created project!',
        'update-project-success'    => 'Successfully updated project!',
        'delete-success'         => 'Successfully deleted the project!',
        'cannot-delete-yourself' => 'You cannot delete yourself!',
    ],

    'show-project' => [
        'id'                => 'Project ID',
        'name'              => 'Projectname',
        'email'             => '<span class="hidden-xs">Project </span>Email',
        'role'              => 'Project Role',
        'created'           => 'Created <span class="hidden-xs">at</span>',
        'updated'           => 'Updated <span class="hidden-xs">at</span>',
        'users'             => 'Users',
        'labelRole'         => 'Project Role',
        'labelAccessLevel'  => '<span class="hidden-xs">Project</span> Access Level|<span class="hidden-xs">Project</span> Access Levels',
    ],

    'search'  => [
        'title'         => 'Showing Search Results',
        'found-footer'  => ' Record(s) found',
        'no-results'    => 'No Results',
    ],
];
