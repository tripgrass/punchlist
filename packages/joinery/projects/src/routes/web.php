<?php
use Joinery\Projects\Project;
use Illuminate\Http\Request;
use joinery\tasks\Task;
use App\User;
use joinery\groups\Group;
 use App\Notifications\groupInvite;

//use DB;
Route::group(['middleware' => ['web','ownership','auth'	], 'namespace' => 'joinery\projects'], function () {
	Route::get('/projects/{id}/tasks', '\joinery\tasks\TasksController@index');
	Route::get('/projects/{id}/invite/{inviteCode}', function($id,$inviteCode){
		$user = \Auth::user();
        $row = DB::table('group_project')
             ->where('project_id', '=', $id)
             ->where('invited', '=', 1)
             ->where('accepted', '=', 0)
             ->where('inviteCode', '=', $inviteCode)
             ->first();
        if($row){
        	DB::table('group_project')
				->where('id', '=', $row->id)
				->update(
					[
						'accepted' => 1,
						'acceptedDate' => Carbon::now()					
					]
				);
        }
		echo $id."--";
		echo $inviteCode;
	});
});
Route::group(['middleware' => ['web','ownership','CheckPublic'	], 'namespace' => 'joinery\projects'], function () {
	Route::get('/projects/{id}/timeline', function($id){
		//die;

	    if( $id ){
	    	if('new' == $id){
				$project = new Project;
				$project->save();
				$project->name = "#" . $project->id;
				$project->active = 1;
				$project->save();
				$current_group = \Auth::user()->current_group;
	            $project->groups()->sync([$current_group]);

// create a single PARENT task :
	            $taskThree = new Task;
		        $taskThree->project_id = $project->id;
		        $taskThree->length = 2 + rand(1,4);
		        $taskThree->save();
		        $taskThree->name = "Task Three";
		        $taskThree->save();
// create a single task starting today that uses the previous as a parent:
	            $taskTwo = new Task;
		        $taskTwo->project_id = $project->id;
		        $taskTwo->parent_id = $taskThree->id;
		        $taskTwo->length = 2 + rand(2,6);
		        $taskTwo->save();
		        $taskTwo->name = "Task Two";
		        $taskTwo->save();
// create a single task starting today that uses the previous as a parent:
	            $taskOne = new Task;
		        $taskOne->project_id = $project->id;
		        $taskOne->parent_id = $taskTwo->id;
		        $taskOne->length = 2 + rand(2,6);
		        $taskOne->save();
		        $taskOne->name = "Task One";
		        $taskOne->save();

	            $taskStart = new Task;
	           // $taskStart->startDate = Carbon::now()->startOfDay(); 
		        $taskStart->project_id = $project->id;
		        $taskStart->parent_id = $taskOne->id;
		        $taskStart->length = 2 + rand(2,6);
		        $taskStart->save();
		        $taskStart->name = "Task Start";
		        $taskStart->save();

		        return redirect('/projects/' . $project->id .'/timeline?task-edit&task=' . $taskStart->id ."&tour=designer&tourState=1");
	    	}
	    	else{
		    	if(is_numeric($id)){

			        $project = Project::find($id);
				}
				else{
					$project = Project::where('url','=',$id)->first();
				}     
			}
	    }
	    if($project){
			$current_user = \Auth::user();
			if( $current_user ){
				$current_user->currentproject = $project->id;
				$current_user->save();
			}
		    $data = [
		        'project'             => $project,
		        'pagintaionEnabled' => 0
		    ];

		    return view('projects/timeline', $data); 
		}
	});
	Route::put('/projects/{id}', '\joinery\projects\ProjectsController@store');

    Route::resource('projects', 'ProjectsController', [
        'names' => [
            'index'   => 'projects',
            'destroy' => 'project.destroy',
        ],
    ]);
});

Route::middleware('auth:api')->post('api/projects/detachPartner', function(Request $request){
		$user = User::where('api_token', $request->api_token)->first();
Log::error( print_r($request->all(), true) );
Log::error( "print_r($user, true)" );
//Log::error( print_r($user, true) );

		if( $user->id != $request->group_id){
	        $project = Project::find($request->project_id);
	        $project->groups()->detach($request->group_id);
			return response()->json($request->all() );

	    }
    });
Route::middleware('auth:api')->post('api/projects/addPartner', function(Request $request){
Log::error( print_r($request->all(), true) );
 //	 Log::error('echo');
	$user = User::where('api_token', $request->api_token)->first();
	$current_group = Group::find($user->current_group);
	// check if user has auth on project
	$project = Project::find($request->project_id);
	$data_return =  new stdClass();
	$data_return->request = $request->all();
	if( 'new' == $request->groupType ){
		$email = $request->newGroup;
		if(!$email){
			$email = $request->admin_email;
		}
		$user = User::where('email','=',$email)->first();
		if(!$user){
	        $user = User::create([
	            'name' => $email,
	            'email' => $email,
	            'password' => Hash::make(rand()),
	            'isAlias' => 1,
	        ]);
	        $accountType = 'Group Partner';
	        $group = Group::create([
	            'name' => $email,
	            'isAlias' => 1,
	        ]);
	        $project->groups()->attach($group->id,[
	            'isSecondary' => '1',
	            'invited' => 1,
	            'invitedDate' => Carbon::now()
	        ]);
	        $user->groups()->attach($group->id,[
	            'role' => 'admin',
	            'isAdmin' => 1
	        ]);
			$user->current_group = $group->id;
	        $account = Account::create([
	            'name' => $accountType,
	            'monthlyFee' => '0',
	            'group_id' => $group->id,
	            'active' => 1
	        ]);
	        $user->save();
			$data_return->group = $group;

		}
	}
	else{
		if( $request->group_id ){
			// check if theyre already attached


			$partnerIsAlreadyAttached = 0;
			foreach( $project->groups as $attached_team ){
				if( $request->group_id == $attached_team->id ){
					$partnerIsAlreadyAttached = 1;
				}

			}
Log::error( print_r($current_group->teams, true) );
			if( !$partnerIsAlreadyAttached ){
Log::error( "gorup id:: " . $request->group_id );
				$partner = Group::find($request->group_id);
				$permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
				$inviteCode = substr(str_shuffle($permitted_chars), 0, 12);
		        $project->groups()->attach($request->group_id,[
		            'isSecondary' => '1',
		            'invited' => 1,
		            'invitedDate' => Carbon::now(),
		            'inviteCode' => $inviteCode
		        ]);
				//$project->groups()->attach($partner->id,[]);
	           // $partner->admin->notify(new groupInvite($project,$partner,$current_group, $inviteCode));
	        }
			$data_return->group = $partner;
		}
		else if( $request->admin_email){
			// invite USER by email
			$email = $request->admin_email;
			$user = User::where('email','=',$email)->first();
			if(!$user){
		        $user = User::create([
		            'name' => $email,
		            'email' => $email,
		            'password' => Hash::make(rand()),
		        ]);
		        $accountType = 'Group Partner';
		        $group = Group::create([
		            'name' => $email
		        ]);
		        $user->groups()->attach($group_id,[
		            'role' => 'admin',
		            'isAdmin' => 1
		        ]);
				$user->current_group = $group->id;
		        $account = Account::create([
		            'name' => $accountType,
		            'monthlyFee' => '0',
		            'group_id' => $group->id,
		            'active' => 1
		        ]);
		        $project->groups()->attach($group->id,[
		            'isSecondary' => '1',
		            'invited' => 1,
		            'invitedDate' => Carbon::now()
		        ]);

		        $user->save();
				$data_return->group = $group;
				//create NEW user from email
			}
		}

	}
	$data_return->group->account;
	$data_return->group->users;
	return response()->json($data_return );

//	return response()->json($response);
});

?>