<?php

use Illuminate\Http\Request;
use Joinery\Projects\Project;
use App\User;
use App\Account;
use joinery\groups\Group;
use App\Notifications\groupInvite;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->post('api/projects', function(Request $request){
	$user = User::where('api_token', $request->api_token)->first();
    $guest = 0;
    if( !$user ){
        $guest = 1;
    }
    $projects = [];
    $group = "";
    if($user){
        $response = Project::whereHas('groups', function($q) use ($user){
            $q->where('group_id', '=',$user->current_group);
        })->paginate(5);
     }
     else{
		$response = Project::whereHas('groups', function($q) {
	//		$q->where('group_id', '=','1');
		})->paginate(5);

     }

	return response()->json($response);
});
Route::middleware('auth:api')->post('api/projects/updateProject', function(Request $request){
	//$project = $request->project;
	$user = User::where('api_token', $request->api_token)->first();
	$project = Project::find( $request->id );
	$project->name = $request->name;
	$project->isPublic = $request->isPublic;
	$project->description = $request->description;
	$project->save();
//s	print_r($project); 
	//	echo "ppppppppppp";
//	print_r( $request->project );
	return response()->json( $project );
});
Route::middleware('auth:api')->post('api/projects/addPartner', function(Request $request){
	$user = User::where('api_token', $request->api_token)->first();
	$current_group = Group::find($user->current_group);
	// check if user has auth on project
	$project = Project::find($request->project_id);
	if( 'new' == $request->groupType ){
		$email = $request->newGroup;
		if(!$email){
			$email = $request->admin_email;
		}
		$user = User::where('email','=',$email)->first();
		if(!$user){
	        $user = User::create([
	            'name' => $email,
	            'email' => $email,
	            'password' => Hash::make(rand()),
	            'isAlias' => 1,
	        ]);
	        $accountType = 'Group Partner';
	        $group = Group::create([
	            'name' => $email,
	            'isAlias' => 1,
	        ]);
	        $project->groups()->attach($group->id,[
	            'isSecondary' => '1',
	            'invited' => 1,
	            'invitedDate' => Carbon::now()
	        ]);
	        $user->groups()->attach($group->id,[
	            'role' => 'admin',
	            'isAdmin' => 1
	        ]);
			$user->current_group = $group->id;
	        $account = Account::create([
	            'name' => $accountType,
	            'monthlyFee' => '0',
	            'group_id' => $group->id,
	            'active' => 1
	        ]);
	        $user->save();
		}
	}
	else{
		if( $request->group_id ){
			$partner = Group::find($request->group_id);
			$permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
			$inviteCode = substr(str_shuffle($permitted_chars), 0, 12);
	        $project->groups()->attach($request->group_id,[
	            'isSecondary' => '1',
	            'invited' => 1,
	            'invitedDate' => Carbon::now(),
	            'inviteCode' => $inviteCode
	        ]);
			$current_group->teams()->attach($partner->id,[]);
            $partner->admin->notify(new groupInvite($project,$partner,$current_group, $inviteCode));
		}
		else if( $request->admin_email){
			// invite USER by email
			$email = $request->admin_email;
			$user = User::where('email','=',$email)->first();
			if(!$user){
		        $user = User::create([
		            'name' => $email,
		            'email' => $email,
		            'password' => Hash::make(rand()),
		        ]);
		        $accountType = 'Group Partner';
		        $group = Group::create([
		            'name' => $email
		        ]);
		        $user->groups()->attach($group_id,[
		            'role' => 'admin',
		            'isAdmin' => 1
		        ]);
				$user->current_group = $group->id;
		        $account = Account::create([
		            'name' => $accountType,
		            'monthlyFee' => '0',
		            'group_id' => $group->id,
		            'active' => 1
		        ]);
		        $project->groups()->attach($group->id,[
		            'isSecondary' => '1',
		            'invited' => 1,
		            'invitedDate' => Carbon::now()
		        ]);

		        $user->save();

				//create NEW user from email
			}
		}

	}
	return response()->json($request->all() );

//	return response()->json($response);
});

Route::post('api/search-projects', 'joinery\projects\ProjectsController@search');
