<?php
namespace joinery\projects;

use Illuminate\Support\ServiceProvider;

class ProjectsServiceProvider extends ServiceProvider
{
    private $_packageTag = 'projects';

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom( __DIR__.'/routes/api.php');
        $this->loadRoutesFrom( __DIR__.'/routes/web.php');
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        $this->loadTranslationsFrom(__DIR__.'/lang/', $this->_packageTag);
        $this->publishes([
            __DIR__.'/assets/js' => public_path('vendor/joinery'),
            __DIR__.'/assets/sass' => public_path('vendor/joinery'),
        ], 'public');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Joinery\Projects\ProjectsController');
        $this->app->make('Joinery\Projects\Project');
        $this->loadViewsFrom(__DIR__.'/views', 'projects');
        $this->mergeConfigFrom(__DIR__.'/config/'.$this->_packageTag.'.php', $this->_packageTag);
        $this->app->singleton(joinery\projects\ProjectsController\ProjectsController::class, function () {
            return new ProjectsController();
        });
        $this->app->alias(ProjectsController::class, 'projects');
    }
}
