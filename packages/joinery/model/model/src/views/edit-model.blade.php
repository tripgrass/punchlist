@extends(config('{{#models#}}.GroupsBladeExtended'))

@section('template_title')
  @lang('{{#models#}}::{{#models#}}.editing-{{#associated#}}', ['name' => ${{#associated#}}->name])
@endsection

@section('template_linked_css')
    @if(config('{{#models#}}.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('{{#models#}}.datatablesCssCDN') }}">
    @endif
    @if(config('{{#models#}}.fontAwesomeEnabled'))
        <link rel="stylesheet" type="text/css" href="{{ config('{{#models#}}.fontAwesomeCdn') }}">
    @endif
    @include('{{#models#}}::partials.styles')
    @include('{{#models#}}::partials.bs-visibility-css')
@endsection

@section('content')
    <div class="container">
        @if(config('{{#models#}}.enablePackageBootstapAlerts'))
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    @include('{{#models#}}::partials.form-status')
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">
                            @lang('{{#models#}}::{{#models#}}.editing-{{#model#}}', ['name' => ${{#model#}}->name])
                            <div class="pull-right">
                                <a href="{{ route('{{#associateds#}}') }}" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="top" title="@lang('{{#models#}}::{{#models#}}.tooltips.back-{{#associateds#}}')">
                                    @if(config('{{#models#}}.fontAwesomeEnabled'))
                                        <i class="fas fa-fw fa-reply-all" aria-hidden="true"></i>
                                    @endif
                                    @lang('{{#models#}}::{{#models#}}.buttons.back-to-{{#associateds#}}')
                                </a>
                                <a href="{{ url('/{{#associateds#}}/' . ${{#associated#}}->id) }}" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="left" title="@lang('{{#models#}}::{{#models#}}.tooltips.back-{{#associateds#}}')">
                                    @if(config('{{#models#}}.fontAwesomeEnabled'))
                                        <i class="fas fa-fw fa-reply" aria-hidden="true"></i>
                                    @endif
                                    @lang('{{#models#}}::{{#models#}}.buttons.back-to-{{#associated#}}')
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        {!! Form::open(array('route' => ['{{#associateds#}}.update', ${{#associated#}}->id], 'method' => 'PUT', 'role' => 'form', 'class' => 'needs-validation')) !!}
                            {!! csrf_field() !!}
                            <div class="form-group has-feedback row {{ $errors->has('name') ? ' has-error ' : '' }}">
                                @if(config('{{#models#}}.fontAwesomeEnabled'))
                                    {!! Form::label('name', trans('{{#models#}}::forms.create_{{#associated#}}_label_{{#associated#}}name'), array('class' => 'col-md-3 control-label')); !!}
                                @endif
                                <div class="col-md-9">
                                    <div class="input-{{#model#}}">
                                        {!! Form::text('name', ${{#associated#}}->name, array('id' => 'name', 'class' => 'form-control', 'placeholder' => trans('{{#models#}}::forms.create_{{#associated#}}_ph_{{#associated#}}name'))) !!}
                                        <div class="input-{{#model#}}-append">
                                            <label class="input-{{#model#}}-text" for="name">
                                                @if(config('{{#models#}}.fontAwesomeEnabled'))
                                                    <i class="fa fa-fw {{ trans('{{#models#}}::forms.create_{{#associated#}}_icon_{{#associated#}}name') }}" aria-hidden="true"></i>
                                                @else
                                                    @lang('{{#models#}}::forms.create_{{#associated#}}_label_{{#associated#}}name')
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback row {{ $errors->has('email') ? ' has-error ' : '' }}">
                                @if(config('{{#models#}}.fontAwesomeEnabled'))
                                    {!! Form::label('email', trans('{{#models#}}::forms.create_{{#associated#}}_label_email'), array('class' => 'col-md-3 control-label')); !!}
                                @endif
                                <div class="col-md-9">
                                    <div class="input-{{#model#}}">
                                        {!! Form::text('email', ${{#associated#}}->email, array('id' => 'email', 'class' => 'form-control', 'placeholder' => trans('{{#models#}}::forms.create_{{#associated#}}_ph_email'))) !!}
                                        <div class="input-{{#model#}}-append">
                                            <label for="email" class="input-{{#model#}}-text">
                                                @if(config('{{#models#}}.fontAwesomeEnabled'))
                                                    <i class="fa fa-fw {{ trans('{{#models#}}::forms.create_{{#associated#}}_icon_email') }}" aria-hidden="true"></i>
                                                @else
                                                    @lang('{{#models#}}::forms.create_{{#associated#}}_label_email')
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="pw-change-container">
                                <div class="form-group has-feedback row {{ $errors->has('password') ? ' has-error ' : '' }}">
                                    @if(config('{{#models#}}.fontAwesomeEnabled'))
                                        {!! Form::label('password', trans('{{#models#}}::forms.create_{{#associated#}}_label_password'), array('class' => 'col-md-3 control-label')); !!}
                                    @endif
                                    <div class="col-md-9">
                                        <div class="input-{{#model#}}">
                                            {!! Form::password('password', array('id' => 'password', 'class' => 'form-control ', 'placeholder' => trans('{{#models#}}::forms.create_{{#associated#}}_ph_password'))) !!}
                                            <div class="input-{{#model#}}-append">
                                                <label class="input-{{#model#}}-text" for="password">
                                                    @if(config('{{#models#}}.fontAwesomeEnabled'))
                                                        <i class="fa fa-fw {{ trans('{{#models#}}::forms.create_{{#associated#}}_icon_password') }}" aria-hidden="true"></i>
                                                    @else
                                                        @lang('{{#models#}}::forms.create_{{#associated#}}_label_password')
                                                    @endif
                                                </label>
                                            </div>
                                        </div>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group has-feedback row {{ $errors->has('password_confirmation') ? ' has-error ' : '' }}">
                                    @if(config('{{#models#}}.fontAwesomeEnabled'))
                                        {!! Form::label('password_confirmation', trans('{{#models#}}::forms.create_{{#associated#}}_label_pw_confirmation'), array('class' => 'col-md-3 control-label')); !!}
                                    @endif
                                    <div class="col-md-9">
                                        <div class="input-{{#model#}}">
                                            {!! Form::password('password_confirmation', array('id' => 'password_confirmation', 'class' => 'form-control', 'placeholder' => trans('{{#models#}}::forms.create_{{#associated#}}_ph_pw_confirmation'))) !!}
                                            <div class="input-{{#model#}}-append">
                                                <label class="input-{{#model#}}-text" for="password_confirmation">
                                                    @if(config('{{#models#}}.fontAwesomeEnabled'))
                                                        <i class="fa fa-fw {{ trans('{{#models#}}::forms.create_{{#associated#}}_icon_pw_confirmation') }}" aria-hidden="true"></i>
                                                    @else
                                                        @lang('{{#models#}}::forms.create_{{#associated#}}_label_pw_confirmation')
                                                    @endif
                                                </label>
                                            </div>
                                        </div>
                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-6 mb-2">
                                    <a href="#" class="btn btn-outline-secondary btn-block btn-change-pw mt-3" title="@lang('{{#models#}}::forms.change-pw')">
                                        <i class="fa fa-fw fa-lock" aria-hidden="true"></i>
                                        <span></span> @lang('{{#models#}}::forms.change-pw')
                                    </a>
                                </div>
                                <div class="col-12 col-sm-6">
                                    {!! Form::button(trans('{{#models#}}::forms.save-changes'), array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2 btn-save','type' => 'button', 'data-toggle' => 'modal', 'data-target' => '#confirmSave', 'data-title' => trans('{{#models#}}::modals.edit_{{#associated#}}__modal_text_confirm_title'), 'data-message' => trans('{{#models#}}::modals.edit_{{#associated#}}__modal_text_confirm_message'))) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('{{#models#}}::modals.modal-save')
    @include('{{#models#}}::modals.modal-delete')

@endsection

@section('template_scripts')
    @include('{{#models#}}::scripts/csrf-token')
    @include('{{#models#}}::scripts.delete-modal-script')
    @include('{{#models#}}::scripts.save-modal-script')
    @include('{{#models#}}::scripts.check-changed')
    @if(config('{{#models#}}.tooltipsEnabled'))
        @include('{{#models#}}::scripts.tooltips')
    @endif
@endsection

