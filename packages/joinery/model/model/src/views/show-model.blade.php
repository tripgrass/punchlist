@extends(config('{{#models#}}.{{#Models#}}BladeExtended'))

@section('template_title')
    @lang('{{#models#}}::{{#models#}}.showing-{{#model#}}', ['name' => ${{#model#}}->name])
@endsection

@section('template_linked_css')
    @if(config('{{#models#}}.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('{{#models#}}.datatablesCssCDN') }}">
    @endif
    @if(config('{{#models#}}.fontAwesomeEnabled'))
        <link rel="stylesheet" type="text/css" href="{{ config('{{#models#}}.fontAwesomeCdn') }}">
    @endif
    @include('{{#models#}}::partials.styles')
    @include('{{#models#}}::partials.bs-visibility-css')
@endsection

@section('content')
    <div class="container">
        @if(config('{{#models#}}.enablePackageBootstapAlerts'))
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    @include('{{#models#}}::partials.form-status')
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">
                            @lang('{{#models#}}::{{#models#}}.showing-{{#model#}}-title', ['name' => ${{#model#}}->name])
                            <div class="float-right">
                                <a href="{{ route('{{#models#}}') }}" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="left" title="@lang('{{#models#}}::{{#models#}}.tooltips.back-{{#models#}}')">
                                    @if(config('{{#models#}}.fontAwesomeEnabled'))
                                        <i class="fas fa-fw fa-reply-all" aria-hidden="true"></i>
                                    @endif
                                    @lang('{{#models#}}::{{#models#}}.buttons.back-to-{{#models#}}')
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <h4 class="text-muted text-center">
                            {{ ${{#model#}}->name }}
                        </h4>
                        @if(${{#model#}}->email)
                            <p class="text-center" data-toggle="tooltip" data-placement="top" title="@lang('{{#models#}}::{{#models#}}.tooltips.email-{{#model#}}', ['{{#model#}}' => ${{#model#}}->email])">
                                {{ Html::mailto(${{#model#}}->email, ${{#model#}}->email) }}
                            </p>
                        @endif
                        <div class="row mb-4">
                            <div class="col-3 offset-3 col-sm-4 offset-sm-2 col-md-4 offset-md-2 col-lg-3 offset-lg-3">
                                <a href="/{{#models#}}/{{${{#model#}}->id}}/edit" class="btn btn-block btn-md btn-warning">
                                    @lang('{{#models#}}::{{#models#}}.buttons.edit-{{#model#}}')
                                </a>
                            </div>
                            <div class="col-3 col-sm-4 col-md-4 col-lg-3">
                                {!! Form::open(array('url' => '{{#models#}}/' . ${{#model#}}->id, 'class' => 'form-inline')) !!}
                                    {!! Form::hidden('_method', 'DELETE') !!}
                                    {!! Form::hidden('{{#model#}}_id', ${{#model#}}->id) !!}
                                    {!! Form::button(trans('{{#models#}}::{{#models#}}.buttons.delete-{{#model#}}'), array('class' => 'btn btn-danger btn-md btn-block','type' => 'button', 'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete {{#Associated#}}', 'data-message' => 'Are you sure you want to delete this {{#model#}}?')) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <ul class="list-{{#model#}} list-{{#model#}}-flush">
                            <li class="list-{{#model#}}-item">
                                <div class="row">
                                    <div class="col-4 col-sm-3">
                                        <strong>
                                            @lang('{{#models#}}::{{#models#}}.show-{{#model#}}.id')
                                        </strong>
                                    </div>
                                    <div class="col-8 col-sm-9">
                                        {{ ${{#model#}}->id }}
                                    </div>
                                </div>
                            </li>
                            @if (${{#model#}}->name)
                                <li class="list-{{#model#}}-item">
                                    <div class="row">
                                        <div class="col-4 col-sm-3">
                                            <strong>
                                                @lang('{{#models#}}::{{#models#}}.show-{{#model#}}.name')
                                            </strong>
                                        </div>
                                        <div class="col-8 col-sm-9">
                                            {{ ${{#model#}}->name }}
                                        </div>
                                    </div>
                                </li>
                            @endif
                            @if (${{#model#}}->email)
                                <li class="list-{{#model#}}-item">
                                    <div class="row">
                                        <div class="col-12 col-sm-3">
                                            <strong>
                                                @lang('{{#models#}}::{{#models#}}.show-{{#model#}}.email')
                                            </strong>
                                        </div>
                                        <div class="col-12 col-sm-9">
                                            {{ ${{#model#}}->email }}
                                        </div>
                                    </div>
                                </li>
                            @endif
                            @if (${{#model#}}->created_at)
                                <li class="list-{{#model#}}-item">
                                    <div class="row">
                                        <div class="col-4 col-sm-3">
                                            <strong>
                                                @lang('{{#models#}}::{{#models#}}.show-{{#model#}}.created')
                                            </strong>
                                        </div>
                                        <div class="col-8 col-sm-9">
                                            {{ ${{#model#}}->created_at }}
                                        </div>
                                    </div>
                                </li>
                            @endif
                            @if (${{#model#}}->updated_at)
                                <li class="list-{{#model#}}-item">
                                    <div class="row">
                                        <div class="col-4 col-sm-3">
                                            <strong>
                                                @lang('{{#models#}}::{{#models#}}.show-{{#model#}}.updated')
                                            </strong>
                                        </div>
                                        <div class="col-8 col-sm-9">
                                            {{ ${{#model#}}->updated_at }}
                                        </div>
                                    </div>
                                </li>
                            @endif
                            @if (${{#model#}}->{{#associateds#}})
                                <li class="list-{{#model#}}-item">
                                    <div class="row">
                                        <div class="col-4 col-sm-3">
                                            <strong>
                                                @lang('{{#models#}}::{{#models#}}.show-{{#model#}}.{{#associateds#}}')
                                            </strong>
                                        </div>
                                        <div class="col-8 col-sm-9">
                                            <ul>
                                             @foreach(${{#model#}}->{{#associateds#}} as ${{#associated#}})
                                                <li><a href={{ URL::to('{{#associateds#}}/' . ${{#associated#}}->id) }}>{{ ${{#associated#}}->name }}</a></li>
                                             @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('{{#models#}}::modals.modal-delete')
@endsection
@section('template_scripts')
    @include('{{#models#}}::scripts/csrf-token')
    @include('{{#models#}}::scripts.delete-modal-script')

    @include('{{#models#}}::scripts.delete-modal-script')
    @if(config('{{#models#}}.tooltipsEnabled'))
        @include('{{#models#}}::scripts.tooltips')
    @endif
@endsection
