@extends('{{#models#}}::layouts.app')

@section('template_title')
    @lang('{{#models#}}::{{#models#}}.create-new-{{#associated#}}')
@endsection

@section('template_linked_css')
    @if(config('{{#models#}}.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('{{#models#}}.datatablesCssCDN') }}">
    @endif
    @if(config('{{#models#}}.fontAwesomeEnabled'))
        <link rel="stylesheet" type="text/css" href="{{ config('{{#models#}}.fontAwesomeCdn') }}">
    @endif
    @include('{{#models#}}::partials.styles')
    @include('{{#models#}}::partials.bs-visibility-css')
@endsection

@section('content')
<?php 
    $edit = 0;
    $title = "Create New {{#Model#}}";
    $btn_text = "Create New {{#Model#}}";
    $form_name = "";
    $form_id = "NULL";
    if( isset(${{#model#}}) && is_object(${{#model#}})){
        $edit = 1;
        $title = "Edit {{#Model#}}";
        $btn_text = "Update {{#Model#}}";
        $form_name = ${{#model#}}->name;
        $form_id = ${{#model#}}->id;
//        print_r(${{#model#}});
    }
?>
    <div class="container">
        @if(config('{{#models#}}.enablePackageBootstapAlerts'))
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <?php 
//                    @include('{{#models#}}::partials.form-status')
                ?>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">
                            {{ $title }}
                            <div class="pull-right">
                                <a href="/{{#models#}}" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="left" title="@lang('Back to {{#Models#}}')">
                                    @if(config('{{#models#}}.fontAwesomeEnabled'))
                                        <i class="fas fa-fw fa-reply-all" aria-hidden="true"></i>
                                    @endif
                                   Back to {{#Models#}}
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        {!! Form::open(array('route' => '{{#models#}}.store', 'method' => 'POST', 'role' => 'form', 'class' => 'needs-validation')) !!}
                            {!! Form::hidden('id', $form_id, array('id' => 'id')) !!}
                            {!! csrf_field() !!}
                            <div class="form-group has-feedback row {{ $errors->has('email') ? ' has-error ' : '' }}">
                                @if(config('{{#models#}}.fontAwesomeEnabled'))
                                    {!! Form::label('name', '{{#Model#}} Name', array('class' => 'col-md-3 control-label')); !!}
                                @endif
                                <div class="col-md-9">
                                    <div class="input-{{#model#}}">
                                        {!! Form::text('name', $form_name, array('id' => 'name', 'class' => 'form-control', 'placeholder' => 'Name')) !!}
                                    </div>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback row {{ $errors->has('email') ? ' has-error ' : '' }}">
                                @if(config('{{#models#}}.fontAwesomeEnabled'))
                                    {!! Form::label('{{#associateds#}}', '{{#Model#}} Members', array('class' => 'col-md-3 control-label')); !!}
                                @endif
                                <div class="col-md-9">
                                    <div class="input-{{#model#}}">
<?php 
$select_{{#associateds#}} = [];
foreach(${{#associateds#}} as ${{#associated#}}){
    $select_{{#associateds#}}[${{#associated#}}->id] = ${{#associated#}}->name;
}
$selected_{{#associateds#}} = [];
if(isset(${{#model#}}) && isset(${{#model#}}->{{#associateds#}})){
    foreach( ${{#model#}}->{{#associateds#}} as ${{#associated#}}){
        $selected_{{#associateds#}}[${{#associated#}}->id] = ${{#associated#}}->id;
    }
}
?>
{{Form::select('{{#associateds#}}',$select_{{#associateds#}},$selected_{{#associateds#}},array('multiple'=>'multiple','name'=>'{{#associateds#}}[]', 'id' => '{{#associateds#}}', 'class' => 'form-control'))}}
                                    </div>
                                    @if ($errors->has('{{#associateds#}}'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('{{#associateds#}}') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            {!! Form::button( $btn_text, array('class' => 'btn btn-success margin-bottom-1 mb-1 float-right','type' => 'submit' )) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('template_scripts')
    @if(config('{{#models#}}.tooltipsEnabled'))
        @include('{{#models#}}::scripts.tooltips')
    @endif
@endsection
