@extends(config('{{#{{#models#}}#}}.{{#Models#}}BladeExtended'))

@section('template_title')
    @lang('{{#{{#models#}}#}}::{{#{{#models#}}#}}.showing-all-{{#associateds#}}') @endsection

@section('template_linked_css')
    @if(config('{{#{{#models#}}#}}.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('{{#{{#models#}}#}}.datatablesCssCDN') }}">
    @endif
    @if(config('{{#{{#models#}}#}}.fontAwesomeEnabled'))
        <link rel="stylesheet" type="text/css" href="{{ config('{{#{{#models#}}#}}.fontAwesomeCdn') }}">
    @endif
    @include('{{#{{#models#}}#}}::partials.styles')
    @include('{{#{{#models#}}#}}::partials.bs-visibility-css')
@endsection

@section('content')
    <div class="container">
        @if(config('{{#{{#models#}}#}}.enablePackageBootstapAlerts'))
            <div class="row">
                <div class="col-sm-12">
                  <?php  @include('{{#{{#models#}}#}}::partials.form-status') ?>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                               Showing all {{#Models#}}
                            </span>

                            <div class="btn-{{#model#}} pull-right btn-{{#model#}}-xs">
                                @if(config('{{#{{#models#}}#}}.softDeletedEnabled'))
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i>
                                        <span class="sr-only">
                                            @lang('{{#{{#models#}}#}}::{{#{{#models#}}#}}.{{#associateds#}}-menu-alt')
                                        </span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ route('{{#associateds#}}.create') }}">
                                                @if(config('{{#{{#models#}}#}}.fontAwesomeEnabled'))
                                                    <i class="fa fa-fw fa-{{#associated#}}-plus" aria-hidden="true"></i>
                                                @endif
                                               New {{#Model#}}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/{{#associateds#}}/deleted">
                                                @if(config('{{#{{#models#}}#}}.fontAwesomeEnabled'))
                                                    <i class="fa fa-fw fa-{{#model#}}" aria-hidden="true"></i>
                                                @endif
                                                @lang('{{#{{#models#}}#}}::{{#{{#models#}}#}}.show-deleted-{{#associateds#}}')
                                            </a>
                                        </li>
                                    </ul>
                                @else
                                    <a href="/{{#{{#models#}}#}}/create" class="btn btn-default btn-sm pull-right" data-toggle="tooltip" data-placement="left" title="@lang('{{#{{#models#}}#}}::{{#{{#models#}}#}}.tooltips.create-new')">
                                        @if(config('{{#{{#models#}}#}}.fontAwesomeEnabled'))
                                            <i class="fa fa-fw fa-{{#associated#}}-plus" aria-hidden="true"></i>
                                        @endif
                                        New {{#Model#}}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-body" id="{{#model#}}-card-body">
                    </div>

                </div>
            </div>
        </div>
    </div>
    @include('{{#{{#models#}}#}}::modals/modal-delete')

@endsection
@section('template_scripts')
    @include('{{#{{#models#}}#}}::scripts/csrf-token')
    @if ((count(${{#{{#models#}}#}}) > config('{{#{{#models#}}#}}.datatablesJsStartCount')) && config('{{#{{#models#}}#}}.enabledDatatablesJs'))
        @include('{{#{{#models#}}#}}::scripts.datatables')
    @endif
    @include('{{#{{#models#}}#}}::scripts.delete-modal-script')
    @include('{{#{{#models#}}#}}::scripts.save-modal-script')
    @if(config('{{#{{#models#}}#}}.tooltipsEnabled'))
        @include('{{#{{#models#}}#}}::scripts.tooltips')
    @endif
@endsection
