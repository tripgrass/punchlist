<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Laravel Users app.blade.php language lines
    |--------------------------------------------------------------------------
    */

    'nav' => [
        'toggle-nav-alt'    => 'Toggle Navigation',
        'login'             => 'Login',
        'register'          => 'Register',
        '{{#models#}}'             => '{{#Models#}}',
        'logout'            => 'Logout',
    ],

];
