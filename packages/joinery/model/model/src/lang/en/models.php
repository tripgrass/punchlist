<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Laravel {{#Models#}} Blades Language Lines
    |--------------------------------------------------------------------------
    */

    'showing-all-{{#models#}}'     => 'Showing All {{#Models#}}',
    '{{#models#}}-menu-alt'        => 'Show {{#Models#}} Management Menu',
    'create-new-{{#model#}}'       => 'Create New {{#Model#}}',
    'show-deleted-{{#models#}}'    => 'Show Deleted {{#Model#}}',
    'editing-{{#model#}}'          => 'Editing {{#Model#}} :name',
    'showing-{{#model#}}'          => 'Showing {{#Model#}} :name',
    'showing-{{#model#}}-title'    => ':name\'s Information',

    '{{#models#}}-table' => [
        'caption'   => '{1} :{{#models#}}count {{#model#}} total|[2,*] :{{#models#}}count total {{#models#}}',
        'id'        => 'ID',
        'name'      => 'Name',
        'email'     => 'Email',
        'role'      => 'Role',
        'created'   => 'Created',
        'updated'   => 'Updated',
        'actions'   => 'Actions',
        'updated'   => 'Updated',
    ],

    'buttons' => [
        'create-new'    => '<span class="hidden-xs hidden-sm">New {{#Model#}}</span>',
        'delete'        => '<i class="far fa-trash-alt fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Delete</span><span class="hidden-xs hidden-sm hidden-md"> {{#Model#}}</span>',
        'show'          => '<i class="fas fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Show</span><span class="hidden-xs hidden-sm hidden-md"> {{#Model#}}</span>',
        'edit'          => '<i class="fas fa-pencil-alt fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Edit</span><span class="hidden-xs hidden-sm hidden-md"> {{#Model#}}</span>',
        'back-to-{{#models#}}' => '<span class="hidden-sm hidden-xs">Back to </span><span class="hidden-xs">{{#Models#}}</span>',
        'back-to-{{#model#}}'  => 'Back  <span class="hidden-xs">to {{#Model#}}</span>',
        'delete-{{#model#}}'   => '<i class="far fa-trash-alt fa-fw" aria-hidden="true"></i>  <span class="hidden-xs">Delete</span><span class="hidden-xs"> {{#Model#}}</span>',
        'edit-{{#model#}}'     => '<i class="fas fa-pencil-alt fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Edit</span><span class="hidden-xs"> {{#Model#}}</span>',
    ],

    'tooltips' => [
        'delete'        => 'Delete',
        'show'          => 'Show',
        'edit'          => 'Edit',
        'create-new'    => 'Create New {{#Model#}}',
        'back-{{#models#}}'    => 'Back to {{#models#}}',
        'email-{{#model#}}'    => 'Email :{{#model#}}',
        'submit-search' => 'Submit {{#Models#}} Search',
        'clear-search'  => 'Clear Search Results',
    ],

    'messages' => [
        '{{#model#}}NameTaken'          => '{{#Model#}}name is taken',
        '{{#model#}}NameRequired'       => '{{#Model#}}name is required',
        'fNameRequired'          => 'First Name is required',
        'lNameRequired'          => 'Last Name is required',
        'emailRequired'          => 'Email is required',
        'emailInvalid'           => 'Email is invalid',
        'passwordRequired'       => 'Password is required',
        'PasswordMin'            => 'Password needs to have at least 6 characters',
        'PasswordMax'            => 'Password maximum length is 20 characters',
        'captchaRequire'         => 'Captcha is required',
        'CaptchaWrong'           => 'Wrong captcha, please try again.',
        'roleRequired'           => '{{#Model#}} role is required.',
        '{{#model#}}-creation-success'  => 'Successfully created {{#model#}}!',
        'update-{{#model#}}-success'    => 'Successfully updated {{#model#}}!',
        'delete-success'         => 'Successfully deleted the {{#model#}}!',
        'cannot-delete-yourself' => 'You cannot delete yourself!',
    ],

    'show-{{#model#}}' => [
        'id'                => '{{#Model#}} ID',
        'name'              => '{{#Model#}}name',
        'email'             => '<span class="hidden-xs">{{#Model#}} </span>Email',
        'role'              => '{{#Model#}} Role',
        'created'           => 'Created <span class="hidden-xs">at</span>',
        'updated'           => 'Updated <span class="hidden-xs">at</span>',
        'users'             => 'Users',
        'labelRole'         => '{{#Model#}} Role',
        'labelAccessLevel'  => '<span class="hidden-xs">{{#Model#}}</span> Access Level|<span class="hidden-xs">{{#Model#}}</span> Access Levels',
    ],

    'search'  => [
        'title'         => 'Showing Search Results',
        'found-footer'  => ' Record(s) found',
        'no-results'    => 'No Results',
    ],
];
