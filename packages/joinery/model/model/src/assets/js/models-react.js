import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class Example extends Component {
    constructor(props) {
        super(props);
        this.state = {
            prevUrl : "",
            nextUrl : "",
            {{#models#}}: []
        };
        axios
            .post('/api/{{#models#}}')
            .then(response => {
                console.log('from handle submit', response);
                this.setState({
                    prevUrl: response.data.prev_page_url,
                    nextUrl: response.data.next_page_url,
                     {{#models#}}: [...response.data.data]
                })
            });
        this.handlePrev = this.handlePrev.bind(this);            
        this.handleNext = this.handleNext.bind(this);            
        this.handleSearch = this.handleSearch.bind(this);            
    }
    handleSearch(e){
        e.preventDefault();
        var _searchTerm = $('#{{#model#}}_search_box').val();
         axios
            .post('api/search-{{#models#}}', {
                searchTerm: _searchTerm 
              })
            .then(response => {
                console.log('from search submit', response);
                this.setState({
                    prevUrl: response.data.prev_page_url,
                    nextUrl: response.data.next_page_url,
                     {{#models#}}: [...response.data]
                })
            });

    }
    handlePrev(){
     axios
            .post(this.state.prevUrl)
            .then(response => {
                this.setState({
                    prevUrl: response.data.prev_page_url,
                    nextUrl: response.data.next_page_url,
                     {{#models#}}: [...response.data.data]
                })
            });

    }
    handleNext(){
         axios
            .post(this.state.nextUrl)
            .then(response => {
                this.setState({
                    prevUrl: response.data.prev_page_url,
                    nextUrl: response.data.next_page_url,
                     {{#models#}}: [...response.data.data]
                })
            });
    }
    render() {
        return (
            <div>
                <form method="POST" action="/search-{{#models#}}" accept-charset="UTF-8" role="form" className="needs-validation" id="search_{{#models#}}" _lpchecked="1">
                    <div className="input-group mb-3">
                        <input id="{{#model#}}_search_box" onChange={this.handleSearch} className="form-control" placeholder="" aria-label="{{#models#}}::forms.search-{{#models#}}-ph" name="{{#model#}}_search_box" type="text"></input>
                        <div className="input-group-append">
                            <a href="#" className="btn btn-warning clear-search" data-toggle="tooltip" title="Clear Search Results">
                                <i className="fas fa-times" aria-hidden="true"></i>
                               <span className="sr-only">Clear Search Results</span>
                            </a>
                            <button className="btn btn-secondary"  data-toggle="tooltip" data-placement="bottom" title="Submit {{#Models#}} Search"><i className="fas fa-search" aria-hidden="true"></i> <span className="sr-only"> Submit {{#Models#}} Search </span></button>
                        </div>
                    </div>
                </form>            
                <div className="table-responsive {{#models#}}-table">
                    <table className="table table-striped table-sm data-table">
                        <caption id="{{#model#}}_count"></caption>
                        <thead className="thead">
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th className="hidden-sm hidden-xs hidden-md">Created</th>
                                <th className="hidden-sm hidden-xs hidden-md">Updated</th>
                                <th className="no-search no-sort"></th>
                                <th className="no-search no-sort"></th>
                                <th className="no-search no-sort"></th>
                            </tr>
                        </thead>
                        <tbody id="{{#model#}}_table">
                            { this.state.{{#models#}}.map({{#model#}} => (
                                 <tr key={ {{#model#}}.id}>
                                    <td>{ {{#model#}}.id}</td>
                                    <td>{ {{#model#}}.name}</td>
                                    <td className="hidden-sm hidden-xs hidden-md">{ {{#model#}}.created_at}</td>
                                    <td className="hidden-sm hidden-xs hidden-md">{ {{#model#}}.updated_at}</td>
                                    <td>
                                        <a className="btn btn-sm btn-success btn-block" href={"/{{#models#}}/" + {{#model#}}.id } data-toggle="tooltip">
                                            View {{#Model#}}
                                        </a>
                                    </td>
                                    <td>
                                    <form method="POST" accept-charset="UTF-8" className="" data-toggle="tooltip" title="Delete">
                                    <input name="_token" type="hidden" value=""></input>
                                                                        <input name="_method" type="hidden" value="DELETE"></input>
                                                                        <input name="{{#model#}}_id" type="hidden" value={ {{#model#}}.id}></input>
                                                                        <button className="btn btn-danger btn-sm" style={{width: '100%'}}  type="button"  data-toggle="modal" data-target="#confirmDelete" data-title="Delete Group" data-message="Are you sure you want to delete this group?">Delete</button>
                                                                    </form>
                                    </td>
                                    <td>
                                        <a className="btn btn-sm btn-info btn-block" href={"/{{#models#}}/" + {{#model#}}.id + "/edit"} data-toggle="tooltip">
                                            Edit {{#Model#}}
                                        </a>
                                    </td>
                                </tr>))
                            } 
                        </tbody>
                        <tbody id="search_results"></tbody>
                    </table>
                </div>
                <div className="paginated-links">
                    <span onClick={this.handlePrev}>Prev</span><span onClick={this.handleNext}>Next</span>
                </div>
            </div>
        );
    }
}
if (document.getElementById('{{#model#}}-card-body')) {
    ReactDOM.render(<Example />, document.getElementById('{{#model#}}-card-body'));
}
