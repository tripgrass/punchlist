<?php

namespace joinery\{{#models#}};

use Illuminate\Support\ServiceProvider;

class {{#Models#}}ServiceProvider extends ServiceProvider
{
    private $_packageTag = '{{#models#}}';

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom( __DIR__.'/routes/api.php');
        $this->loadRoutesFrom( __DIR__.'/routes/web.php');
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        $this->loadTranslationsFrom(__DIR__.'/lang/', $this->_packageTag);
        $this->publishes([
            __DIR__.'/assets/js' => public_path('vendor/joinery'),
            __DIR__.'/assets/sass' => public_path('vendor/joinery'),
        ], 'public');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Joinery\{{#Models#}}\{{#Models#}}Controller');
        $this->app->make('Joinery\{{#Models#}}\{{#Model#}}');
        $this->loadViewsFrom(__DIR__.'/views', '{{#models#}}');
        $this->mergeConfigFrom(__DIR__.'/config/'.$this->_packageTag.'.php', $this->_packageTag);
        $this->app->singleton(joinery\{{#models#}}\{{#Models#}}Controller\{{#Models#}}Controller::class, function () {
            return new {{#Models#}}Controller();
        });
        $this->app->alias({{#Models#}}Controller::class, '{{#models#}}');
    }
}
