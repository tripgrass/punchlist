<?php
use Joinery\{{#Models#}}\{{#Model#}};

Route::group(['middleware' => 'web', 'namespace' => 'joinery\{{#models#}}'], function () {
    Route::resource('{{#models#}}', '{{#Models#}}Controller', [
        'names' => [
            'index'   => '{{#models#}}',
            'destroy' => '{{#model#}}.destroy',
        ],
    ]);
});

?>