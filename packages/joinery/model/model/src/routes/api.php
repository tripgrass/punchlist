<?php

use Illuminate\Http\Request;
use Joinery\{{#Models#}}\{{#Model#}};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('api/{{#models#}}', function(Request $request){
	$response = {{#Model#}}::paginate(2);
	return response()->json($response);

});
Route::post('api/search-{{#models#}}', 'joinery\{{#models#}}\{{#Models#}}Controller@search');
