<?php

namespace Joinery\{{#Models#}};

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;


class {{#Models#}}Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pagintaionEnabled = config('{{#models#}}.enablePagination');
        if ($pagintaionEnabled) {
           // $groups = config('groups.defaultGroupModel')::paginate(config('groups.paginateListSize'));
        } else {
            ${{#models#}} = config('{{#models#}}.default{{#Model#}}Model')::paginate(2);
        }

        $data = [
            '{{#models#}}'             => ${{#models#}},
            'pagintaionEnabled' => $pagintaionEnabled,
        ];
        return view(config('{{#models#}}.show{{#Models#}}Blade'), $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(config('{{#models#}}.create{{#Model#}}Blade'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( 'NULL' == $request->id ){
            ${{#model#}} = new {{#Model#}};
        }
        else{
            ${{#model#}} = {{#Model#}}::find($request->id);            
        }
        ${{#model#}}->name = $request->name;
        ${{#model#}}->save();
       if(isset($request->{{#associateds#}})){
            ${{#model#}}->{{#associateds#}}()->sync($request->{{#associateds#}});
	    }
        ${{#models#}} = {{#Model#}}::all();
        $data = [
            '{{#models#}}'             => ${{#models#}},
            'pagintaionEnabled' => 0
        ];
        return view('{{#models#}}::show-{{#models#}}', $data); 
   }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if( $id ){
            ${{#model#}} = {{#Model#}}::find($id);            
        }
        $data = [
            '{{#model#}}'             => ${{#model#}},
            'pagintaionEnabled' => 0
        ];
        return view('{{#models#}}::show-{{#model#}}', $data); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        ${{#model#}} = config('{{#models#}}.default{{#Model#}}Model')::findOrFail($id);
   		${{#associateds#}} = {{#Associated#}}::all();
		$data = [
			'{{#associateds#}}' => ${{#associateds#}},
            '{{#model#}}'          => ${{#model#}},
        ];
        return view(config('{{#models#}}.editIndividual{{#Model#}}Blade'))->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        {{#Model#}}::destroy($id);
        ${{#models#}} = {{#Model#}}::all();
        $data = [
            '{{#models#}}'             => ${{#models#}},
            'pagintaionEnabled' => 0
        ];
        return redirect('/{{#models#}}');
    }

   /**
     * Method to search the users.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $searchTerm = $request->input('searchTerm');
        /*
        $searchRules = [
            'group_search_box' => 'required|string|max:255',
        ];
        $searchMessages = [
            'group_search_box.required' => 'Search term is required',
            'group_search_box.string'   => 'Search term has invalid characters',
            'group_search_box.max'      => 'Search term has too many characters - 255 allowed',
        ];
        $validator = Validator::make($request->all(), $searchRules, $searchMessages);

        if ($validator->fails()) {
            return response()->json([
                json_encode($validator),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
*/
         $results = config('{{#models#}}.default{{#Model#}}Model')::where('id', 'like', $searchTerm.'%')
                            ->orWhere('name', 'like', '%'.$searchTerm.'%')->get();
      
    return response()->json($results);
    }    
}
