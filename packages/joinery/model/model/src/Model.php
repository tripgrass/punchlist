<?php

namespace joinery\{{#models#}};

use Illuminate\Database\Eloquent\Model;

class {{#Model#}} extends Model
{
    /**
     * The {{#associated#}}s that belong to the group.
     */
    public function {{#associated#}}s()
    {
        return $this->belongsToMany('App\{{#Associated#}}');
    }    
}
