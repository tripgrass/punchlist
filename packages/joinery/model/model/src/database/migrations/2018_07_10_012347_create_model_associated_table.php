<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create{{#Model#}}{{#Associated#}}Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('{{#model#}}_{{#associated#}}', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('{{#model#}}_id');
            $table->integer('{{#associated#}}_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('{{#model#}}_{{#associated#}}', function (Blueprint $table) {
            //
        });
    }
}
